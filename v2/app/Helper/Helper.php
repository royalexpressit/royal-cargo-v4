<?php
use App\Waybill;
use App\User;
use App\Branch;
use App\City;
use App\BranchActionLog;
use App\Ctf;
use App\CtfLog;
use App\CtfSerial;
use App\CodCity;
use App\Package;
use App\PackageSerial;
use Illuminate\Support\Facades\Log;

	function user_role($role){
		if($role == 1){
			return 'Admin';
		}elseif($role == 2){
			return 'Supervisor';
		}elseif($role == 3){
			return 'Courier';
		}else{
			return 'Cash On Delivery';
		}
	}

	/** setup custom dashboard date **/
	function setup_date($date){
		Session::put('set-dashboard-date',$date);
	}

	/** get current dashboard date **/
	function get_date(){
		if(Session::get('set-dashboard-date')){
			return Session::get('set-dashboard-date');
		}else{
			return date('Y-m-d');
		}
	}

	//
	function finding_date($date){
		return date('Y-m-d 00:00:00', strtotime($date));
	}

	/** total waybill records **/
	function total_records(){
		$count = Waybill::count();

		return $count;
	}


	/** short number format **/
	function number_format_short( $n, $precision = 1 ) {
		if ($n < 900) {
			// 0 - 900
			$n_format = number_format($n, $precision);
			$suffix = '';
		} else if ($n < 900000) {
			// 0.9k-850k
			$n_format = number_format($n / 1000, $precision);
			$suffix = 'K';
		} else if ($n < 900000000) {
			// 0.9m-850m
			$n_format = number_format($n / 1000000, $precision);
			$suffix = 'M';
		} else if ($n < 900000000000) {
			// 0.9b-850b
			$n_format = number_format($n / 1000000000, $precision);
			$suffix = 'B';
		} else {
			// 0.9t+
			$n_format = number_format($n / 1000000000000, $precision);
			$suffix = 'T';
		}

	  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
	  // Intentionally does not affect partials, eg "1.50" -> "1.50"
		if ( $precision > 0 ) {
			$dotzero = '.' . str_repeat( '0', $precision );
			$n_format = str_replace( $dotzero, '', $n_format );
		}

		return $n_format . $suffix;
	}

	/** start waybill created_at date in database **/
	function start_date(){
		$date = Waybill::first();
		if($date){
			return $date->created_at;
		}else{
			return 'No data';
		}
		
	}

	/** outbound status by city **/
	function outbound_status($branch_id){
		$date 		= get_date();
		$city_id 	= user()->city_id;
		
		if(is_sorting($branch_id) == 1){
			$status = Waybill::select('id')
				->leftjoin('action_logs as a','a.waybill_id','=','waybills.id')
				->select('waybills.waybill_no')
				->where('waybills.active',1)
				->where('waybills.action_id','>=',4)
				->where('a.action_id',4)
				->where('waybills.origin',$city_id)
				->whereDate('a.created_at','like',$date.'%')
				->count();
		}else{
			$status = Waybill::select('id')
				->leftjoin('action_logs as a','a.waybill_id','=','waybills.id')
				->select('waybills.waybill_no')
				->where('waybills.active',1)
				->where('a.action_id',1)
				->where('waybills.origin',$city_id)
				->where('a.branch_id',$branch_id)
				->whereDate('waybills.created_at','like',$date.'%')
				->count();
		}
		
		return $status;
	}

	/** inbound status by city **/
	function inbound_status($branch_id){
		$date = get_date();
		$city_id = user()->city_id;

		if(is_sorting($branch_id) == 1){
			$status = Waybill::select('id')
				->leftjoin('users as u','u.id','=','waybills.user_id')
				->leftjoin('branches as b','b.id','=','u.branch_id')
				->whereDate('waybills.created_at','like',$date.'%')
				->select('waybills.waybill_no')
				->where('waybills.active',1)
				->where('waybills.action_id','>',6)
				->where('waybills.destination',$city_id)
				->count();
		}else{
			//main city branches
			// $status = Waybill::select('id')
			// 	->leftjoin('package_waybills as pw','pw.waybill_id','=','waybills.id')
			// 	->leftjoin('packages as p','p.id','=','pw.package_id')
			// 	->whereDate('waybills.created_at','like',$date.'%')
			// 	->select('waybills.waybill_no')
			// 	->where('waybills.active',1)
			// 	->where('waybills.action_id','>',6)
			// 	->where('p.to_branch_id',$branch_id)
			// 	->count();

			//regional
			// $status = Waybill::select('id')
			// 	->leftjoin('package_waybills as pw','pw.waybill_id','=','waybills.id')
			// 	->leftjoin('packages as p','p.id','=','pw.package_id')
			// 	->whereDate('waybills.created_at','like',$date.'%')
			// 	->select('waybills.waybill_no')
			// 	->where('waybills.active',1)
			// 	->where('waybills.action_id','>',6)
			// 	->where('waybills.destination',$city_id)
			// 	->count();
			$status = 0;
		}
		
		// $status = Waybill::select('id')
		// 	->whereDate('created_at','like',$date.'%')
		// 	->select('waybill_no')
		// 	->where('active',1)
		// 	->where('action_id','>',6)
		// 	->where('destination',$city_id)
		// 	->count();

		return $status;
	}

	/** transti status by city **/
	function transit_status($city_id){
		$date = get_date();
		
		$status = Waybill::select('id')->where('transit_date','like',$date.'%')
			->where('current_status','>=',1)
			->where('transit',$city_id)
			->count();

		return $status;
	}


	/** outbound collected by branches of related city **/
	function collected_by_branches($city_id){
		$date = get_date();
		
		$branches = User::join('waybills as w','w.user_id','=','users.id')
			->join('branches as b','b.id','=','users.branch_id')
			->where('outbound_date','like',$date.'%')
			->where('w.origin',$city_id)
			->select(DB::raw('b.name,b.id, COUNT(*) as total'))
			->groupBy('b.name','b.id')
			->get();	

		return $branches;	
	}

	/** inbound handover to branches of related city **/
	function inbound_handover_to_branches($city_id){
		$date = get_date();
		$city_id = user()->city_id;
		
		$branches = BranchActionLog::join('branches as b','b.id','=','branch_action_logs.to_branch')
			->join('cities as c','c.id','=','b.city_id')
			->where('action_date','like',$date.'%')
			->where('action_id',6)
			->where('city_id',$city_id)
			->select(DB::raw('b.name,b.id, COUNT(*) as total'))
			->groupBy('b.name','b.id')
			->orderBy('total','desc')
			->get();	

		return $branches;	
	}

	/** view outbound collected by branch **/
	function outbound_collected_by_branch($branch_id){
		$date = get_date();

		$total = Branch::join('users as u','u.branch_id','=','branches.id')
            ->join('waybills as w','w.user_id','=','u.id')
            ->select('w.waybill_no')
            ->where('u.branch_id',$branch_id)
            ->where('w.outbound_date','like',$date.'%')
            ->count();

        return $total;
	}

	/** branch info **/
	function branch($branch_id){
    	$response 	= array();
    	$branch 	= Branch::find($branch_id);

    	if($branch){
    		$response['id'] 				= $branch->id;
    		$response['name'] 				= $branch->name;
    		$response['city_id'] 			= $branch->city_id;
			$response['is_main_office'] 	= $branch->is_main_office;
			$response['is_transit_area'] 	= $branch->is_transit_area;
			$response['is_sorting'] 		= $branch->is_sorting;
    	}else{
    		$response['id'] 				= '';
    		$response['name'] 				= '';
    		$response['city_id'] 			= '';
			$response['is_main_office'] 	= '';
			$response['is_transit_area'] 	= '';
			$response['is_sorting'] 		= '';
    	}

    	return $response;
    }

    /** view outbound collected by delivery/counter **/
    function collected_by_courier(){
		$date = get_date();
		$branch_id = user()->branch_id;

		$users = DB::table('users')->leftjoin('waybills as w','w.user_id','=','users.id')
			->leftjoin('branches as b','b.id','=','users.branch_id')
			->where('branch_id',$branch_id)
			->select(DB::raw('user_id,users.name,users.image,b.name as branch, COUNT(*) as total'))
			->whereDate('w.created_at','like',$date.'%')
			->groupBy('w.user_id','users.name','users.image','b.name')
			->orderBy('total','desc')
			->get();	

		return $users;	
	}

	/** city info **/
	function city($city_id){
		$city = City::find($city_id);
		if($city){
			$response['id'] 				= $city->id;
			$response['name'] 				= $city->name;
			$response['mm_name'] 			= $city->mm_name;
			$response['shortcode'] 			= $city->shortcode;
			$response['is_service_point'] 	= $city->is_service_point;
			$response['state'] 				= $city->state;
		}else{
			$response['id'] 				= '';
			$response['name'] 				= '';
			$response['mm_name'] 			= '';
			$response['shortcode'] 			= '';
			$response['is_service_point'] 	= '';
			$response['state'] 				= '';
		}

		return $response;
	}

	/** fetched city listing **/
	function fetched_all_cities(){
		$cities = City::select('id','name','mm_name','shortcode')->orderBy('name')->get();

		return $cities;
	}

	/** fetched city listing **/
	function fetched_all_branches(){
		$branches = Branch::select('id','name')->orderBy('name')->get();

		return $branches;
	}

	/** fetched city listing by city_id **/
	function fetched_all_branches_by_city($city_id){
		$branches = Branch::select('id','name')->where('city_id',$city_id)->orderBy('name')->get();

		return $branches;
	}

	/** user info with id **/
	function user_info($user_id){
		$user  = User::find($user_id);

		return $user;
	}

	/** auth user info **/
	function user(){
		return Auth::user();
	}

	/** saved inbound branch-in failed logs **/
	function saved_inbound_branch_in_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_dist/logs/inbound-branch-in-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved inbound handover failed logs **/
	function saved_inbound_received_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_dist/logs/inbound-received-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved outbound received failed logs **/
	function saved_outbound_received_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_dist/logs/outbound-received-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved inbound handover failed logs **/
	function saved_outbound_branch_out_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_dist/logs/outbound-branch-out-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved inbound handover failed logs **/
	function saved_inbound_handover_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_dist/logs/inbound-handover-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved inbound handover failed logs **/
	function saved_scan_branch_out($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_dist/logs/scan-branch-out-ygn.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved inbound handover failed logs **/
	function saved_scan_inbound_handover($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_dist/logs/scan-handover-ygn.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/* waybill batch_id */
	function batch_id(){
		return time();
	}

	/** allowed users to access city **/
	function allowed_users(){
		$array = array(
			'880' => '1,2,3,4,5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94',
			'916' => '1,2,3,4,5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94',
			'917' => '1,2,3,4,5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94',
			'898' => '1,2,3,4,5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94',		
		);

		return $array;
	}

	function allowed_config($type){
		if($type == 1){
			//routes config
			$array = array('18');
		}elseif($type == 2){
			//routes config
			$array = array('18');
		}else{

		}
		

		return $array;
	}

	/** change city for allowed user **/
	function change_cities($user_id){
		$allow 	= 0;
		
		foreach(allowed_users() as $key => $raw){
			if($key == $user_id){
				++$allow;
			}

			if($allow > 0){
				$branches = explode(',', $raw);
			}else{
				$branches = array();
			}

			
		}
		
		return $branches;
		// if($allow > 0){
		// 	$branches = explode(',', $raw);
		// }else{
		// 	$branches = array();
		// }

		// return $branches;
	}

	/** fixed time to scan **/
	function active_time(){
		$times = array(
			'10' 	=> '10',
			'44' 	=> '15'
		);

		return $times;
	}

	/** set active time for city **/
	function set_active_time($city_id){
		foreach(active_time() as $key => $raw){
			if($key == $city_id){
				return $raw;
			}else{
				return 'not set';
			}
		}
	}

	/** breadcrumbs bar **/
	function breadcrumbs(){
		$response = array();

		if (strpos(url()->previous(), 'outbound/handover-by-branch/') !== false){
           $response['title'] = 'handover-by-branch';
           $response['route'] = url()->previous();
        }

        return $response;
	}

	/** fixed client lists **/
	function client_lists(){
		$clients = User::select('id','name')->whereIn('id',[200,1])->orderBy('name')->get();

		return $clients;
	}

	/** fetched main branch from city **/
	function main_branch($city_id){
		$sorting 	= Branch::select('id','name')->where('is_sorting',1)->where('city_id',$city_id)->first();
		$main 		= Branch::select('id','name')->where('is_main_office',1)->where('city_id',$city_id)->first();
		if($sorting){
			return $sorting;
		}else{
			return $main;
		}
	}

	/** main branch for action logs to sync **/
	function main_office($city_id){
		$main 		= Branch::select('id','name')->where('is_main_office',1)->where('city_id',$city_id)->first();
		if($main){
			return $main->name;
		}else{
			return 'null';
		}
	}

	/** checked server point or not **/
	function check_service_point($city_id){
		$city = City::select('is_service_point')->where('id',$city_id)->first();

		return $city->is_service_point;
	}

	/** cargo current status **/
	function current_status($action_id){
		if($action_id == 1 ){
			return '<span data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="ရုံးတွင်ရှိနေဆဲ">1.Collected <i data-feather="help-circle"></i></span>';
		}elseif($action_id == 2 ){
			return '<span data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="ရုံးခွဲသို့လွှဲရန် အထုပ်ထုပ်ထား">2.Created Package <i data-feather="help-circle"></i></span>';
		}elseif($action_id == 3 ){
			return '<span data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="ရုံးခွဲသို့လွှဲရန် စာရင်ထုတ်ပြီး">3.Handover CTF <i data-feather="help-circle"></i></span>';
		}elseif($action_id == 4 ){
			return '<span data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="ရုံးခွဲမှစာဝင်ထားပြီး">4.Branch In CTF <i data-feather="help-circle"></i></span>';
		}elseif($action_id == 5 ){
			return 'Created Package';
		}elseif($action_id == 6 ){
			return 'Created CTF';
		}elseif($action_id == 7 ){
			return 'Inbound Branch In';
		}elseif($action_id == 8 ){
			return 'Created Package';
		}elseif($action_id == 9 ){
			return 'Created CTF';
		}elseif($action_id == 10 ){
			return '<span data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="လုပ်ဆောင်မှုပြီးမြောက်သွားပါပြီ">Completed <i data-feather="help-circle"></i></span>';
		}else{
			return 'Undefined';
		}
	}

	/** cargo transit status **/
	function current_transit_status($status){
		if($status == 1 ){
			return 'Transit Branch In';
		}elseif($status == 2 ){
			return 'Transit Handover';
		}elseif($status == 3 ){
			return 'Transit Received';
		}elseif($status == 4 ){
			return 'Transit Branch Out';
		}else{
			return 'Undefined';
		}
	}

	
	function allowed_ctf_for_all_cities(){
		$users = array(
			'18','19'
		);

		return $users;
	}

	function ctf_count(){
		$response = array();
		$city_id  = user()->city_id;
		$date 	  = get_date();

		// $response['outbound_handover'] 		= CtfLog::where('action_type','outbound-handover')
		// 	->where('from_city',$city_id)
		// 	->groupBy('ctf_no')
		// 	->select('ctf_no')
		// 	->count();

		// $status = Waybill::where('outbound_date','like',$date.'%')
		// 	->where('current_status','<=',4)
		// 	->where('origin',$city)
		// 	->groupBy('current_status')
		// 	->select( DB::raw('current_status , COUNT(*) as status_count'))
		// 	->get();

		$status = CtfLog::where('from_city',$city_id)
			->groupBy('action_type')
			->select( DB::raw('action_type, COUNT(*) as count'))
			->get();

			foreach ($status as $key => $value) {
				// if($value->current_status == 1){
				// 	$response['collected'] = $value->status_count;
				// }

				// if($value->current_status == 2){
				// 	$response['handover'] = $value->status_count;
				// }

				// if($value->current_status == 3){
				// 	$response['received'] = $value->status_count;
				// }

				// if($value->current_status == 4){
				// 	$response['branch_out'] = $value->status_count+$inbound;
				// }
			}
		$response['outbound_handover'] 		= 2;
		$response['outbound_received'] 		= 2;
		$response['outbound_branch_out'] 	= 3;
		$response['inbound_branch_in'] 		= 4;
		$response['inbound_handover'] 		= 5;
		$response['inbound_received'] 		= 6;	
		

		return $response;
	}

	function today_ctf_count(){
		$response = array();
		$city_id  = user()->city_id;
		$date 	  = get_date();

		// $response['outbound_handover'] 		= CtfLog::where('action_date','like',$date.'%')
		// 	->where('action_type','outbound-handover')
		// 	->where('from_city',$city_id)
		// 	->groupBy('ctf_no')
		// 	->select('ctf_no')
		// 	->count();
		$response['outbound_handover'] 		= 2;
		$response['outbound_received'] 		= 2;
		$response['outbound_branch_out'] 	= 3;
		$response['inbound_branch_in'] 		= 4;
		$response['inbound_handover'] 		= 5;
		$response['inbound_received'] 		= 6;

		return $response;
	}

	

	function sent_to_odoo($cargos,$city,$status){
		$data = array(
			"cargo" 	=> $cargos,
			"city" 		=> $city, 
			"status" 	=> $status
		);       

		$data_string = json_encode($data);  
	    $ch = curl_init('http://uat.royalx.biz/royalx/cargo/status');                                                                                                          
		//$ch = curl_init('http://network.royalx.biz/royalx/cargo/status');                                                                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
			                                                                                                                     
		$result = curl_exec($ch);

		//$obj = json_decode($result);
	}


	/** Package generate **/
	function package_generate(){
    	$last = Package::select('id')->orderBy('id','desc')->first();
		if($last){
			/** 999999,1999998,2999997,3999996,4999995,5999994 **/
			$seq = PackageSerial::select('id','last_seq','max_limit')->where('active',1)->first();
			if($last->id > 999999){
				//updated active to '0'
				PackageSerial::where('id',$seq->id)->update(['active'=>0]);

				//fetched new sequence
				$new = PackageSerial::select('id','last_seq','max_limit')->where('active',1)->first();

				$serial = $new->last_seq+1;
				PackageSerial::where('id',$new->id)->update(['last_seq' => $serial]);

				$pkg_no  = 'PG'.str_pad($serial, 6, '0', STR_PAD_LEFT);
			}else{
				$serial = $seq->last_seq+1;
				PackageSerial::where('id',$seq->id)->update(['last_seq' => $serial]);

				$pkg_no  = 'PG'.str_pad($serial, 6, '0', STR_PAD_LEFT);
			}
			return $pkg_no;

		}else{

			return 'PG000001';
		}	
	}

	/** Package generate **/
	function ctf_generate($type){
		$phase 	= ($type == 'rejected'? 'RJ':'AA');
		
		$seq = CtfSerial::select('id','serial_character','last_seq','max_limit')->where('active',1)->first();
		if($seq){
			/** 999999,1999998,2999997,3999996,4999995,5999994 **/
			//$seq = CtfSerial::select('id','serial_character','last_seq','max_limit')->where('active',1)->first();
			
			if($seq->last_seq > 999999){
				//updated active to '0'
				CtfSerial::where('id',$seq->id)->update(['active'=>0]);

				//fetched new sequence
				$new = CtfSerial::select('id','serial_character','last_seq','max_limit')->where('active',1)->first();

				//set new series
				$phase = ($type == 'rejected'? 'RJ':$new->serial_character);

				$serial = $new->last_seq+1;
				CtfSerial::where('id',$new->id)->update(['last_seq' => $serial]);

				$ctf_no  = str_pad($serial, 6, '0', STR_PAD_LEFT).$phase;
			}else{
				$serial = $seq->last_seq+1;
				CtfSerial::where('id',$seq->id)->update(['last_seq' => $serial]);

				$ctf_no  = str_pad($serial, 6, '0', STR_PAD_LEFT).$phase;
			}
			return $ctf_no;

		}else{
			return '000001AA';
		}	
	}

	function main_actions($branch_id){
		$main = Branch::where('is_main_office',1)->where('id',$branch_id)->first();

		if($main){
			return 'main';
		}else{
			return 'branch';
		}
	}

	function is_sorting($branch_id){
		$sorting = Branch::where('is_sorting',1)->where('id',$branch_id)->first();

		if($sorting){
			return 1;
		}else{
			return 0;
		}
	}

	function fetched_branches($city_id,$type){
		$branches = Branch::select('name')
			->where('city_id',user()->city_id)
			->where('is_sorting',$type)
			->orderBy('name','asc')
			->get();

		return $branches;
	}

	function dashboard_status(){
		$date = get_date();
		$response['waybills'] 	= Waybill::select('id')->where('created_at','like',$date.'%')->count();
		$response['packages'] 	= Package::select('id')->where('created_at','like',$date.'%')->count();
		$response['ctfs'] 		= CTF::select('id')->where('created_at','like',$date.'%')->count();

		return $response;
	}

	function fetched_sorting(){

		return 11;
	}


	function testing(){

		$file = "_dist/logs/outbound-users.txt";
		$searchfor = '2022-05-10';

		// the following line prevents the browser from parsing this as HTML.
		header('Content-Type: text/plain');

		// get the file contents, assuming the file to be readable (and exist)
		$contents = file_get_contents($file);
		// escape special characters in the query
		$pattern = preg_quote($searchfor, '/');
		// finalise the regular expression, matching the whole line
		$pattern = "/^.*$pattern.*\$/m";
		// search, and store all matching occurences in $matches
		if(preg_match_all($pattern, $contents, $matches)){
		   echo "Found matches:\n";
		   echo "<pre>";
		   echo implode("\n", $matches[0]);
		   echo "</pre>";
		}
		else{
		   echo "No matches found";
		}
	}

	function outbound_ctf_status($type){
		$response 	= array();
		$date       = get_date();
		$city_id 	= user()->city_id;
		$branch_id 	= user()->branch_id;

		if($type == 3){
			//branch to branch
			if(is_sorting(user()->branch_id) == 0){
				$ctf = DB::table('ctfs')
	                ->select('completed', DB::raw('count(*) as total'))
	                ->where('city_id',$city_id)
	                ->where('branch_id',$branch_id)
	                ->where('action_id',3)
	                ->whereDate('created_at','like',$date.'%')
	                ->groupBy('completed')
	                ->get();

	            if($ctf){
					if($ctf->count() == 1){
						if($ctf[0]->completed == 0){
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= $ctf[0]->total;
							$response['completed'] 	= 0;	
						}else{
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= 0;
							$response['completed'] 	= $ctf[0]->total;
						}
					}elseif($ctf->count() == 2){
						$response['total'] 		= $ctf[0]->total+$ctf[1]->total;
						$response['progress'] 	= $ctf[0]->total;
						$response['completed'] 	= $ctf[1]->total;
					}else{
						$response['total'] 		= 0;
						$response['progress'] 	= 0;
						$response['completed'] 	= 0;
					}
				}else{
					$response['total'] 		= 0;
					$response['progress'] 	= 0;
					$response['completed'] 	= 0;
				
				}
			}else{
				$ctf = DB::table('ctfs')
	                ->select('completed', DB::raw('count(*) as total'))
	                ->where('city_id',$city_id)
	                ->where('action_id',3)
	                ->whereDate('created_at','like',$date.'%')
	                ->groupBy('completed')
	                ->get();

	            if($ctf){
					if($ctf->count() == 1){
						if($ctf[0]->completed == 0){
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= $ctf[0]->total;
							$response['completed'] 	= 0;	
						}else{
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= 0;
							$response['completed'] 	= $ctf[0]->total;
						}
					}elseif($ctf->count() == 2){
						$response['total'] 		= $ctf[0]->total+$ctf[1]->total;
						$response['progress'] 	= $ctf[0]->total;
						$response['completed'] 	= $ctf[1]->total;
					}else{
						$response['total'] 		= 0;
						$response['progress'] 	= 0;
						$response['completed'] 	= 0;
					}
				}else{
					$response['total'] 		= 0;
					$response['progress'] 	= 0;
					$response['completed'] 	= 0;
				
				}
			}  

		}else{
			//city to city
			$ctf = DB::table('ctfs')
                ->select('completed', DB::raw('count(*) as total'))
                ->where('city_id',$city_id)
                ->where('branch_id',$branch_id)
                ->where('action_id',6)
                ->whereDate('created_at','like',$date.'%')
                ->groupBy('completed')
                ->get();

            if($ctf){
				if($ctf->count() == 1){
					if($ctf[0]->completed == 0){
						$response['total'] 		= $ctf[0]->total;
						$response['progress'] 	= $ctf[0]->total;
						$response['completed'] 	= 0;	
					}else{
						$response['total'] 		= $ctf[0]->total;
						$response['progress'] 	= 0;
						$response['completed'] 	= $ctf[0]->total;
					}
					
				}elseif($ctf->count() == 2){
					$response['total'] 		= $ctf[0]->total+$ctf[1]->total;
					$response['progress'] 	= $ctf[0]->total;
					$response['completed'] 	= $ctf[1]->total;
				}else{
					$response['total'] 		= 0;
					$response['progress'] 	= 0;
					$response['completed'] 	= 0;
				}
			}else{
				$response['total'] 		= 0;
				$response['progress'] 	= 0;
				$response['completed'] 	= 0;
			
			}
		}
		

		return $response;
	}

	function inbound_ctf_status($type){
		$response 	= array();
		$date       = get_date();
		$city_id 	= user()->city_id;
		$branch_id 	= user()->branch_id;

		if($type == 6){
			//branch to branch
			if(is_sorting(user()->branch_id) == 0){
				$ctf = DB::table('ctfs')
	                ->select('completed', DB::raw('count(*) as total'))
	                ->where('city_id',$city_id)
	                ->where('branch_id',$branch_id)
	                ->where('action_id',3)
	                ->whereDate('created_at','like',$date.'%')
	                ->groupBy('completed')
	                ->get();

	            if($ctf){
					if($ctf->count() == 1){
						if($ctf[0]->completed == 0){
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= $ctf[0]->total;
							$response['completed'] 	= 0;	
						}else{
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= 0;
							$response['completed'] 	= $ctf[0]->total;
						}
					}elseif($ctf->count() == 2){
						$response['total'] 		= $ctf[0]->total+$ctf[1]->total;
						$response['progress'] 	= $ctf[0]->total;
						$response['completed'] 	= $ctf[1]->total;
					}else{
						$response['total'] 		= 0;
						$response['progress'] 	= 0;
						$response['completed'] 	= 0;
					}
				}else{
					$response['total'] 		= 0;
					$response['progress'] 	= 0;
					$response['completed'] 	= 0;
				
				}
			}else{
				$ctf = DB::table('ctfs')
	                ->select('completed', DB::raw('count(*) as total'))
	                ->where('to_city_id',$city_id)
	                ->where('action_id',6)
	                ->whereDate('created_at','like',$date.'%')
	                ->groupBy('completed')
	                ->get();

	            if($ctf){
					if($ctf->count() == 1){
						if($ctf[0]->completed == 0){
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= $ctf[0]->total;
							$response['completed'] 	= 0;	
						}else{
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= 0;
							$response['completed'] 	= $ctf[0]->total;
						}
					}elseif($ctf->count() == 2){
						$response['total'] 		= $ctf[0]->total+$ctf[1]->total;
						$response['progress'] 	= $ctf[0]->total;
						$response['completed'] 	= $ctf[1]->total;
					}else{
						$response['total'] 		= 0;
						$response['progress'] 	= 0;
						$response['completed'] 	= 0;
					}
				}else{
					$response['total'] 		= 0;
					$response['progress'] 	= 0;
					$response['completed'] 	= 0;
				
				}
			}  

		}else{
			//city to city
			if(is_sorting(user()->branch_id) == 0){
				$ctf = DB::table('ctfs')
	                ->select('completed', DB::raw('count(*) as total'))
	                ->where('to_branch_id',$branch_id)
	                ->where('action_id',9)
	                ->whereDate('created_at','like',$date.'%')
	                ->groupBy('completed')
	                ->get();

	            if($ctf){
					if($ctf->count() == 1){
						if($ctf[0]->completed == 0){
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= $ctf[0]->total;
							$response['completed'] 	= 0;	
						}else{
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= 0;
							$response['completed'] 	= $ctf[0]->total;
						}
					}elseif($ctf->count() == 2){
						$response['total'] 		= $ctf[0]->total+$ctf[1]->total;
						$response['progress'] 	= $ctf[0]->total;
						$response['completed'] 	= $ctf[1]->total;
					}else{
						$response['total'] 		= 0;
						$response['progress'] 	= 0;
						$response['completed'] 	= 0;
					}
				}else{
					$response['total'] 		= 0;
					$response['progress'] 	= 0;
					$response['completed'] 	= 0;
				
				}
			}else{
				$ctf = DB::table('ctfs')
	                ->select('completed', DB::raw('count(*) as total'))
	                ->where('branch_id',$branch_id)
	                ->where('action_id',9)
	                ->whereDate('created_at','like',$date.'%')
	                ->groupBy('completed')
	                ->get();

	            if($ctf){
					if($ctf->count() == 1){
						if($ctf[0]->completed == 0){
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= $ctf[0]->total;
							$response['completed'] 	= 0;	
						}else{
							$response['total'] 		= $ctf[0]->total;
							$response['progress'] 	= 0;
							$response['completed'] 	= $ctf[0]->total;
						}
					}elseif($ctf->count() == 2){
						$response['total'] 		= $ctf[0]->total+$ctf[1]->total;
						$response['progress'] 	= $ctf[0]->total;
						$response['completed'] 	= $ctf[1]->total;
					}else{
						$response['total'] 		= 0;
						$response['progress'] 	= 0;
						$response['completed'] 	= 0;
					}
				}else{
					$response['total'] 		= 0;
					$response['progress'] 	= 0;
					$response['completed'] 	= 0;
				
				}
			}
		}
		

		return $response;
	}

	function package_status($type){
		$response 	= array();
		$date       = get_date();
		$city_id 	= user()->city_id;
		$branch_id  = user()->branch_id;

		if($type == 'outbound'){
			//outbound
			$packages = DB::table('packages')
                ->select('packages.no')
                ->where('from_city_id',$city_id)
                ->where('from_branch_id',$branch_id)
                ->where('action_id','>=',2)
                ->where('action_id','<=',6)
                ->whereDate('created_at','like',$date.'%')
                ->count();

		}else{
			//inbound
			$packages = DB::table('packages')
                ->select('packages.no')
                ->where('to_city_id',$city_id)
                ->where('to_branch_id',$branch_id)
                ->where('action_id','>=',6)
                ->whereDate('created_at','like',$date.'%')
                ->count();
		}
		

		return $packages;
	}

	/** check waybill destination from digital **/
	function check_destination($params){

		//$waybills = array("MDY16959590718YGN", "YGN16959600718YGN", "Toyota");
		//$waybills = '[\''.$params.'\']';

		$data = array(
			'login' 			=> 'cargouser',
			'password' 			=> 'cargouser',
			'db' 				=> 'digital',
			'waybill_numbers' 	=> $params
		); 

		//$data_string = json_encode($waybills); 
		//var_dump($waybills);
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  //CURLOPT_URL => 'https://uatdigital.royalx.biz/api/royal/waybill-destination',
			CURLOPT_URL => 'https://digital.royalx.biz/api/royal/waybill-destination',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  //CURLOPT_POSTFIELDS => array('waybill_numbers' => '[\'MDY16959590718YGN\']'),
		  CURLOPT_POSTFIELDS => $data,
		  CURLOPT_HTTPHEADER => array(
		    'access-token: access_token_0114bc7cdeb37408c9c161d03d4eef93c62a54a0',
		    'Cookie: session_id=321c6d1a15216eda5c0ded88477709541af6aa9b'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$data = json_decode($response);

		return $data;

		// echo "<pre>";
		// print_r($data[0]->to_city_short_code);
		// echo "</pre>";
		
	}

	function sync_waybill($waybill_no,$status,$branch,$city){
		$curl = curl_init();

		$data = array(
			'login' 			=> 'cargouser',
			'password' 			=> 'cargouser',
			'db' 				=> 'digital',
			'username' 			=> 'cargouser',
			'waybill_number' 	=> $waybill_no,//'YGN16959080706MDY',
			'action' 			=> $status,//'branch-in',
			'branch' 			=> $branch,//'MDY-C1'
			'city_short_code' 	=> $city
		); 

		curl_setopt_array($curl, array(
		  //CURLOPT_URL => 'https://uatdigital.royalx.biz/api/royal/cargo-action-job',
		  CURLOPT_URL => 'https://uatdigital.royalx.biz/api/royal/cargo-action-job',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => $data,
		  CURLOPT_HTTPHEADER => array(
		    'access-token: access_token_0114bc7cdeb37408c9c161d03d4eef93c62a54a0',
		    'Cookie: session_id=13acb664f0d00e3bc9c1609b6cdae6f0ec9dab66'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;
	}

	function is_cod($city_id){
		$check = Branch::select('id')->where('name','like','%'.'-COD')->where('city_id',$city_id)->first();
	
		if($check){
			return $check->id;
		}else{
			return 0;
		}
	}

	function fetched_cod_city($city_id){
		$cod = CodCity::select('cod_branch_id')->where('city_id',$city_id)->first();
		if($cod){
			return $cod->cod_branch_id;
		}else{
			return 0;
		}
	}

	function fetched_reject_branch($city_id){
		$branch = CodCity::join('branches as b','b.id','=','cod_cities.cod_branch_id')
			->where('cod_cities.city_id',$city_id)
			->select('b.id','b.name')
			->first();
		if($branch){
			return $branch;
		}else{
			return 0;
		}
	}

	function checked_schedule(){
		$image = time().'.png';
		App\User::where('id', 1)->update(['image' => $image]);
	}

	function waybill_sync_to_digital_job(){
		$temp = array();
		$waybills = DB::table('waybill_jobs')
            ->join('waybills as w','w.id','=','waybill_jobs.waybill_id')
            ->join('cities as c','c.id','=','waybill_jobs.city_id')
            ->select('w.id','w.waybill_no','waybill_jobs.status','waybill_jobs.params as branch','waybill_jobs.synced','c.shortcode as city')
            ->where('synced',0)
            ->orderBy('waybill_jobs.id','asc')
            ->limit(30)
            ->get();

        foreach($waybills as $waybill){
        	//update sync status
            DB::table('waybill_jobs')->where('waybill_id', $waybill->id)->update(['synced' => 1]);
            
            sync_waybill($waybill->waybill_no,$waybill->status,$waybill->branch,$waybill->city);
        
            array_push($temp,$waybill->waybill_no);
        }
        Log::info(implode(",",$temp));
	}

	function waybill_check_end_point_job(){
		$raw        = '';

        $waybills = DB::table('waybills')
            ->where('end_point',NULL)
            ->limit(30)
            ->orderBy('id','asc')
            ->get();

        foreach($waybills as $key => $waybill){
            if($key > 0){
                $raw .= ','.'\''.$waybill->waybill_no.'\''; 
            }else{
                $raw .= '\''.$waybill->waybill_no.'\'';  
            }

            DB::table('waybills')->where('waybill_no',$waybill->waybill_no)->update(['end_point'=>'no data']);
        }

        $params 	= '['.$raw.']'; 
        $response 	= check_destination($params);

        if(!$waybills->isEmpty()){
            foreach($response as $key => $waybill){
                DB::table('waybills')->where('waybill_no',$waybill->waybill_no)->update(['end_point'=>$waybill->to_city_short_code]);
            } 
        }
	}

	function pkg_ctf_checked_job(){
		//fetched complted package_waybills
		$pkgs = DB::table('package_waybills')
			->select('package_id','completed')
			->where('completed',1)
			->where('checked',0)
			->groupBy('package_id','completed')
			->orderBy('package_id','asc')
			->limit(30)
			->get();

			foreach($pkgs as $pkg){
				$count = DB::table('package_waybills')->select('package_id')->where('package_id',$pkg->package_id)->where('completed',0)->count();
			 	
			 	if($count == 0){
					//updated ctf_package
					DB::table('ctf_packages')->where('package_id',$pkg->package_id)->update(['completed'=>1]);
					
					$ctfs = DB::table('ctf_packages')->select('ctf_id')->where('package_id',$pkg->package_id)->get();
					
					foreach($ctfs as $ctf){
						//updated ctf
						$count = DB::table('ctf_packages')->select('ctf_id')->where('ctf_id',$ctf->ctf_id)->where('completed',0)->count();
						if($count == 0){
							DB::table('ctfs')->where('id',$ctf->ctf_id)->update(['completed'=>1,'completed_at'=> date('Y-m-d H:i:s')]);
						}
					}
				}

				DB::table('package_waybills')->where('package_id',$pkg->package_id)->update(['checked'=>1]);
			}

		return array('message' => 'working to check completed packages.');
	}

	function bk_job_one($limit){
		//fetched waybills,action_logs
		$waybills = DB::table('waybills')->orderBy('id','asc')->limit($limit)->get();
		foreach($waybills as $waybill){
			DB::table('bk_waybills')->insert([
				'waybill_id'    => $waybill->id,
				'waybill_no'    => $waybill->waybill_no,
				'origin'     	=> $waybill->origin,
				'transit_1'     => $waybill->transit_1,
				'transit_2'     => $waybill->transit_2,
				'destination'   => $waybill->destination,
				'batch_id'     	=> $waybill->batch_id,
				'user_id'     	=> $waybill->user_id,
				'action_id'     => $waybill->action_id,
				'same_day'     	=> $waybill->same_day,
				'qty'     		=> $waybill->qty,
				'end_point'     => $waybill->end_point,
				'active'     	=> $waybill->active,
				'created_at'    => $waybill->created_at,
				'updated_at'    => $waybill->updated_at,
			]);

			$logs 				= DB::table('action_logs')->where('waybill_id',$waybill->id)->get();
			$package_waybills 	= DB::table('package_waybills')->where('waybill_id',$waybill->id)->get();

			foreach($logs as $log){
				DB::table('bk_action_logs')->insert([
					'waybill_id'    => $log->waybill_id,
					'action_id'     => $log->action_id,
					'user_id'     	=> $log->user_id,
					'branch_id'     => $log->branch_id,
					'city_id'     	=> $log->city_id,
					'log'     		=> $log->log,
					'created_at'    => $log->created_at,
					'updated_at'    => $log->updated_at,
				]);

				//deleted records
				DB::table('action_logs')->where('waybill_id',$waybill->id)->delete();
			}

			foreach($package_waybills as $package_waybill){
				DB::table('bk_package_waybills')->insert([
					'package_id'    =>   $package_waybill->package_id,
					'waybill_id'    =>   $package_waybill->waybill_id,
					'completed'     =>   $package_waybill->completed,
					'active'     	=>   $package_waybill->active,
					'checked'     	=>   $package_waybill->checked,
					'created_at'    =>   $package_waybill->created_at,
					'updated_at'    =>   $package_waybill->updated_at,
				]);

				//deleted records
				DB::table('package_waybills')->where('waybill_id',$package_waybill->waybill_id)->delete();
			}


			//deleted records
			DB::table('waybills')->where('id',$waybill->id)->delete();
		}
	}

	function bk_job_two($limit){
		//fetched waybills,action_logs
		$packages = DB::table('packages')->orderBy('id','asc')->limit($limit)->get();
		
		foreach($packages as $package){
			DB::table('bk_packages')->insert([
				'package_id'    	=> $package->id,
				'package_no'    	=> $package->package_no,
				'user_id'     		=> $package->user_id,
				'from_branch_id'    => $package->from_branch_id,
				'from_city_id'     	=> $package->from_city_id,
				'to_branch_id'   	=> $package->to_branch_id,
				'to_city_id'     	=> $package->to_city_id,
				'action_id'     	=> $package->action_id,
				'completed'     	=> $package->completed,
				'package_type_id'   => $package->package_type_id,
				'remark'     		=> $package->remark,
				'completed_at'     	=> $package->completed_at,
				'created_at'    	=> $package->created_at,
				'updated_at'    	=> $package->updated_at,
			]);

			$ctf_packages 	= DB::table('ctf_packages')->where('package_id',$package->id)->get();
			//$package_waybills 	= DB::table('package_waybills')->where('waybill_id',$waybill->id)->get();

			foreach($ctf_packages as $ctf_package){
				DB::table('bk_ctf_packages')->insert([
					'ctf_id'    	=> $ctf_package->ctf_id,
					'package_id'    => $ctf_package->package_id,
					'completed'     => $ctf_package->completed,
					'created_at'    => $ctf_package->created_at,
					'updated_at'    => $ctf_package->updated_at
				]);

				$ctf = DB::table('ctfs')->where('id',$ctf_package->ctf_id)->first();
				if($ctf){
					//synced
					DB::table('bk_ctfs')->insert([
						'ctf_id'    	=> $ctf->id,
						'ctf_no'    	=> $ctf->ctf_no,
						'user_id'     	=> $ctf->user_id,
						'branch_id'    	=> $ctf->branch_id,
						'city_id'    	=> $ctf->city_id,
						'action_id'    	=> $ctf->action_id,
						'rejected'    	=> $ctf->rejected,
						'completed'    	=> $ctf->completed,
						'completed_at'  => $ctf->completed_at,
						'created_at'    => $ctf->created_at,
						'updated_at'    => $ctf->updated_at
					]);

					//delete record
					DB::table('ctfs')->where('id',$ctf_package->ctf_id)->delete();
				}



				//deleted records
				DB::table('ctf_packages')->where('package_id',$package->id)->delete();
			}

			//deleted records
			DB::table('packages')->where('id',$package->id)->delete();
		}
	}


	function waybill_layer(){
		$response = array();

		$primary 	= DB::table('waybills')->select('id')->count();
		$secondary 	= DB::table('bk_waybills')->select('id')->count();
		$date 		= DB::table('waybills')->select('created_at')->first();

		$response['primary'] 	= $primary;
		$response['secondary']  = $secondary;
		$response['date'] 		= $date? $date->created_at:'No data';

		return $response;
	}

	function action_log_layer(){
		$response = array();

		$primary 	= DB::table('action_logs')->select('id')->count();
		$secondary 	= DB::table('bk_action_logs')->select('id')->count();
		$date 		= DB::table('action_logs')->select('created_at')->first();

		$response['primary'] 	= $primary;
		$response['secondary']  = $secondary;
		$response['date'] 		= $date? $date->created_at:'No data';

		return $response;
	}

	function package_waybill_layer(){
		$response = array();

		$primary 	= DB::table('package_waybills')->select('id')->count();
		$secondary 	= DB::table('bk_package_waybills')->select('id')->count();
		$date 		= DB::table('package_waybills')->select('created_at')->first();

		$response['primary'] 	= $primary;
		$response['secondary']  = $secondary;
		$response['date'] 		= $date? $date->created_at:'No data';

		return $response;
	}

	function package_layer(){
		$response = array();

		$primary 	= DB::table('packages')->select('id')->count();
		$secondary 	= DB::table('bk_packages')->select('id')->count();
		$date 		= DB::table('packages')->select('created_at')->first();

		$response['primary'] 	= $primary;
		$response['secondary']  = $secondary;
		$response['date'] 		= $date? $date->created_at:'No data';

		return $response;
	}

	function ctf_package_layer(){
		$response = array();

		$primary 	= DB::table('ctf_packages')->select('id')->count();
		$secondary 	= DB::table('bk_ctf_packages')->select('id')->count();
		$date 		= DB::table('ctf_packages')->select('created_at')->first();

		$response['primary'] 	= $primary;
		$response['secondary']  = $secondary;
		$response['date'] 		= $date? $date->created_at:'No data';

		return $response;
	}

	function ctf_layer(){
		$response = array();

		$primary 	= DB::table('ctfs')->select('id')->count();
		$secondary 	= DB::table('bk_ctfs')->select('id')->count();
		$date 		= DB::table('ctfs')->select('created_at')->first();

		$response['primary'] 	= $primary;
		$response['secondary']  = $secondary;
		$response['date'] 		= $date? $date->created_at:'No data';

		return $response;
	}

	//backup layer methods
	function to_sycn_ctfs(){
		$ctfs = DB::table('ctfs')->where('completed',1)->where('id','>',1170)->orderBy('id','asc')->limit(30)->get();

		return $ctfs;
	}

	function allowed_city_to_transferred($city_id){
		$cities = array('109','110','129','130','131','132','133');

		if(in_array($city_id,$cities)){
			return 1;
		}else{
			return 0;
		}
	}

	function wrong_letters_branches(){
		$branches = DB::table('branches')
			->select('id','name')
			->whereIn('city_id',[102,129,109,130,110,131,132,133])
			->where('is_sorting',0)
			->orderBy('name')
			->get();

		return $branches;
	}

	function synced_ctfs(){
		$ctfs = DB::table('bk_ctfs')->orderBy('id','desc')->limit(20)->get();

		return $ctfs;
	}

	function synced_waybill_action_logs(){
		$response = array();
		$waybills = DB::table('bk_waybills')->select('waybill_id')->where('synced',0)->orderBy('id','asc')->limit(3)->get();

		//backup action_logs table
		if($waybills){
			foreach($waybills as $waybill){
				$logs 	= DB::table('action_logs')->where('waybill_id',$waybill->waybill_id)->get();

				foreach($logs as $log){
					DB::table('bk_action_logs')->insert([
						'waybill_id'    => $log->waybill_id,
						'action_id'     => $log->action_id,
						'user_id'     	=> $log->user_id,
						'branch_id'     => $log->branch_id,
						'city_id'     	=> $log->city_id,
						'log'     		=> $log->log,
						'created_at'    => $log->created_at,
						'updated_at'    => $log->updated_at,
					]);
				}

				//deleted records
				DB::table('action_logs')->where('waybill_id',$waybill->waybill_id)->delete();
				DB::table('bk_waybills')->where('waybill_id',$waybill->waybill_id)->update(['synced'=>1]);

			}
		}

		$response['success'] = 1;

		return $response;
	}

