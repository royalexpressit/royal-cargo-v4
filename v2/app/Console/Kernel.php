<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\WaybillJob;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        //$schedule->command('demo:cron')->everyMinute();


        //sync waybill logs to digital
        $schedule->call(function () {
            info("Waybill sync logs cron 30 records per one minute ". now());
            
            //call waybill sync logs to digital
            waybill_sync_to_digital_job();
        })->everyMinute();

        //sync waybill's end-point from digital
        $schedule->call(function () {
            info("Waybill's end point cron". now());

            //call waybill sync logs to digital
            waybill_check_end_point_job();
        })->everyTwoMinutes();

        //sync waybill's end-point from digital
        $schedule->call(function () {
            info("Synced waybill action logs ". now());

            //call checked completed pkg & ctf
            synced_waybill_action_logs();
        })->everyMinute();

        //sync waybill's end-point from digital
        /*
        $schedule->call(function () {
            info("Checked Completed package & ctf cron ". now());

            //call checked completed pkg & ctf
            pkg_ctf_checked_job();
        })->everyTwoMinutes();
        */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
