<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Waybill;
use App\ActionLog;
use App\Branch;
use App\Package;
use App\PackageType;
use App\PackageWaybill;
use App\Ctf;
use App\CtfPackage;
use App\City;
use App\WaybillJob;
use App\Route;
use App\History;
use Illuminate\Support\Facades\DB;

class WaybillController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function scan(){
        if(check_service_point(Auth::user()->city_id)){
            return view('operator.service-point.scan.scan');
        }else{
            $branch_id = user()->branch_id;

            return view('operator.main-city.scan.scan',compact('branch_id'));
        }
        
    }

    public function outbound(){
        $statistics = array();

        $statistics['waybills'] = Waybill::select('id')->where('origin',user()->city_id)->count();
        $statistics['packages'] = Package::select('id')->where('from_city_id',user()->city_id)->count();
        $statistics['ctfs']     = CTF::select('id')->where('city_id',user()->city_id)->count();
        $statistics['branches'] = Branch::select('id')->where('city_id',user()->city_id)->count();

        return view('operator.main-city.outbound.dashboard',compact('statistics'));
    }

    public function inbound(){
        $statistics = array();

        $statistics['waybills'] = Waybill::select('id')->where('origin',user()->city_id)->count();
        $statistics['packages'] = Package::select('id')->where('from_city_id',user()->city_id)->count();
        $statistics['ctfs']     = CTF::select('id')->where('city_id',user()->city_id)->count();
        $statistics['branches'] = Branch::select('id')->where('city_id',user()->city_id)->count();

        return view('operator.main-city.inbound.dashboard',compact('statistics'));
    }

    public function scan_outbound(){
        $user = DB::table('users')->join('branches as b','b.id','=','users.branch_id')
            ->select('users.name as name','users.city_id as city_id','b.id as branch_id','b.name as branch')
            ->where('users.id',user()->id)
            ->first();

        $branch_id      = $user->branch_id;
        $city_id        = $user->city_id;
        $branch_type    = is_sorting($branch_id);
        $date           = get_date();
        $min_date       = finding_date('- 3day');

        if(!$_GET || $_GET['action']==1){
            $deliveries = DB::table('users')->select('id','name')->where('branch_id',$branch_id)->where('role',3)->where('active',1)->orderBy('name')->get();
            
            if(check_service_point(Auth::user()->city_id)){
                return view('operator.service-point.scan.outbound.form-1',compact('deliveries','branch_type'));
            }else{
                if(is_sorting(user()->branch_id) == 1){
                    return view('operator.main-city.scan.outbound.form-11',compact('deliveries','branch_type'));
                }else{
                    return view('operator.main-city.scan.outbound.form-1',compact('deliveries','branch_type'));
                }
            }
        }elseif(!$_GET || $_GET['action']==2){
            $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->where('active',1)->orderBy('name')->get();
            $types      = DB::table('package_types')->select('id','name','icon')->orderBy('name')->where('group_pkg',1)->get();
            $cities     = DB::table('routes')->join('cities as c','c.id','=','routes.to_city_id')
                    ->select('c.id','c.name','c.mm_name','c.shortcode','c.state')
                    ->where('routes.from_city_id',$city_id)
                    ->where('routes.active',1)
                    ->orderBy('c.name')
                    ->get();

            if(check_service_point(Auth::user()->city_id)){
                $city_id = user()->city_id;
                return view('operator.service-point.scan.outbound.form-2',compact('cities','types','city_id','branch_type'));
            }else{
                return view('operator.main-city.scan.outbound.form-2',compact('branches','types','branch_type'));
            }
        }elseif(!$_GET || $_GET['action']==3){
            if(check_service_point(Auth::user()->city_id)){
                $packages = DB::table('packages')->join('cities as c','c.id','=','packages.to_city_id')
                    ->join('package_waybills as wp','wp.package_id','=','packages.id')
                    ->select('packages.created_at','packages.id','packages.package_no','c.shortcode as to_city')
                    ->where('packages.completed',0)
                    ->where('packages.action_id',5)
                    ->where('from_city_id',$city_id)
                    ->whereDate('packages.created_at','>=',$min_date)
                    ->groupBy('packages.id')
                    ->orderBy('packages.id','desc')
                    ->get();

                return view('operator.service-point.scan.outbound.form-3',compact('packages','branch_type'));
            }else{
                $packages = DB::table('packages')->join('branches as b','b.id','=','packages.to_branch_id')
                    ->join('package_waybills as wp','wp.package_id','=','packages.id')
                    ->select('packages.created_at','packages.id','packages.package_no','b.name as to_branch')
                    ->where('packages.completed',0)
                    ->where('packages.action_id',2)
                    ->where('from_branch_id',$branch_id)
                    ->whereDate('packages.created_at','>=',$min_date)
                    ->groupBy('packages.id')
                    ->orderBy('packages.id','desc')
                    ->get();

                return view('operator.main-city.scan.outbound.form-3',compact('packages','branch_type'));
            }
        }elseif(!$_GET || $_GET['action']==4){
            $row     = 0;
            $city_id = user()->city_id;
            
            if(check_service_point(Auth::user()->city_id)){
                $cities  = Route::join('cities as c','c.id','=','routes.to_city_id')
                    ->select('c.id','c.name','c.mm_name','c.shortcode','c.state')
                    ->where('routes.from_city_id',$city_id)
                    ->where('routes.active',1)
                    ->orderBy('c.name')
                    ->get();


                $packages = Package::join('cities as c','c.id','=','packages.to_city_id')
                    ->join('package_waybills as wp','wp.package_id','=','packages.id')
                    ->join('package_types as pt','pt.id','=','packages.package_type_id')
                    ->select('packages.created_at','packages.id','packages.package_no','c.id as to_city_id','c.shortcode as to_city','pt.name as type','pt.icon as icon',DB::raw('wp.package_id , COUNT(*) as qty'))
                    ->where('packages.completed',0)
                    ->where('packages.action_id',5)
                    ->where('wp.active',1)
                    ->where('packages.from_city_id',$city_id)
                    ->whereDate('packages.created_at','>=',$min_date)
                    ->groupBy('wp.package_id')
                    ->orderBy('packages.id','desc')
                    ->get();

                return view('operator.service-point.scan.outbound.form-4',compact('packages','row','branches','cities','city_id','branch_type'));
            }else{
                $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->where('active',1)->orderBy('name')->get();

                $packages = DB::table('packages')->join('branches as b','b.id','=','packages.to_branch_id')
                    ->join('package_waybills as wp','wp.package_id','=','packages.id')
                    ->join('package_types as pt','pt.id','=','packages.package_type_id')
                    ->select('packages.created_at','packages.id','packages.package_no','b.id as to_branch_id','b.name as to_branch','pt.name as type','pt.icon as icon',DB::raw('wp.package_id , COUNT(*) as qty'))
                    ->where('packages.completed',0)
                    ->where('packages.action_id',2)
                    ->where('wp.active',1)
                    ->where('from_branch_id',$branch_id)
                    ->whereDate('packages.created_at','>=',$min_date)
                    ->groupBy('wp.package_id')
                    ->orderBy('packages.id','desc')
                    ->get();

                return view('operator.main-city.scan.outbound.form-4',compact('packages','row','branches','branch_type'));
            }
        }elseif(!$_GET || $_GET['action']==5){
            $deliveries = DB::table('users')->select('id','name')->where('branch_id',$branch_id)->where('role',3)->where('active',1)->orderBy('name')->get();
            
            return view('operator.main-city.scan.outbound.form-5',compact('deliveries','branch_type'));
        }elseif(!$_GET || $_GET['action']==6){
            $row = 0;
            $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->where('active',1)->orderBy('name')->get();
            
            $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                ->join('packages as p','p.id','=','cp.package_id')
                ->join('branches as b','b.id','=','p.from_branch_id')
                ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name as from_branch_name','p.to_branch_id',DB::raw('count(*) as total'))
                ->where('p.to_branch_id',$branch_id)
                ->where('ctfs.completed',0)
                ->where('ctfs.action_id',3)
                ->whereDate('ctfs.created_at','>=',$min_date)
                ->groupBy('ctfs.created_at','ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','p.to_branch_id')
                ->orderBy('ctfs.id','desc')
                ->get();
            
            return view('operator.main-city.scan.outbound.form-6',compact('ctfs','row','branches','branch_type'));
        }elseif(!$_GET || $_GET['action']==7){
            if($_GET['type']=='city'){
                //create package form
                //$cities     = City::select('id','name','mm_name','shortcode','state')->orderBy('name')->get();
                $cities  = DB::table('routes')->join('cities as c','c.id','=','routes.to_city_id')
                    ->select('c.id','c.name','c.mm_name','c.shortcode','c.state')
                    ->where('routes.from_city_id',$city_id)
                    ->where('routes.active',1)
                    ->orderBy('c.name')
                    ->get();
                $types      = DB::table('package_types')->select('id','name','icon')->orderBy('name')->get();
                
                return view('operator.main-city.scan.outbound.form-7',compact('cities','types','city_id','branch_type'));
            }else{
                //create package form
                $branches   = Branch::select('id','name')->where('city_id',$city_id)->where('active',1)->orderBy('name')->get();
                $types      = PackageType::select('id','name','icon')->orderBy('name')->get();
            
                return view('operator.main-city.scan.outbound.form-10',compact('branches','types','branch_type'));
            }
        }elseif(!$_GET || $_GET['action']==8){
            //added waybill into existing package form
            if(check_service_point(Auth::user()->city_id)){
                $packages = Package::join('cities as c','c.id','=','packages.to_city_id')
                    ->join('package_waybills as wp','wp.package_id','=','packages.id')
                    ->select('packages.id','packages.package_no','c.shortcode as to_city')
                    ->where('packages.completed',0)
                    ->where('packages.action_id',5)
                    ->where('from_city_id',$city_id)
                    ->whereDate('packages.created_at','>=',$min_date)
                    ->groupBy('packages.id')
                    ->orderBy('packages.id')
                    ->get();

                return view('operator.service-point.scan.outbound.form-3',compact('packages'));
            }else{
                $packages = DB::table('packages')->join('cities as c','c.id','=','packages.to_city_id')
                    ->select('packages.created_at','packages.id','packages.package_no','c.shortcode as to_city')
                    ->where('packages.completed',0)
                    ->where('packages.action_id',5)
                    ->where('packages.from_city_id',$city_id)
                    ->whereDate('packages.created_at','>=',$min_date)
                    ->orderBy('packages.id','desc')
                    ->get();

                return view('operator.main-city.scan.outbound.form-8',compact('packages','branch_type'));
            }
        }elseif(!$_GET || $_GET['action']==9){
            $row     = 0;
            if($_GET['type']=='city'){
                $cities  = DB::table('routes')->join('cities as c','c.id','=','routes.to_city_id')
                    ->select('c.id','c.name','c.mm_name','c.shortcode','c.state')
                    ->where('routes.from_city_id',$city_id)
                    ->where('routes.active',1)
                    ->orderBy('c.name')
                    ->get();

                $packages = DB::table('packages')->leftjoin('cities as c','c.id','=','packages.to_city_id')
                    ->leftjoin('package_types as pt','pt.id','=','packages.package_type_id')
                    ->select('packages.created_at','packages.id','packages.package_no','c.id as to_city_id','c.shortcode as to_city','pt.name as type','pt.icon as icon')                    
                    ->where('packages.completed',0)
                    ->where('packages.action_id',5)
                    ->where('packages.from_city_id',$city_id)
                    ->whereDate('packages.created_at','>=',$min_date)
                    ->orderBy('packages.id','desc')
                    ->get();

                return view('operator.main-city.scan.outbound.form-12',compact('packages','cities','branch_type','row'));
            }else{
                /*
                $packages = Package::join('cities as c','c.id','=','packages.to_city_id')
                ->select('packages.id','packages.package_no','c.shortcode as to_city')
                ->where('from_branch_id',$branch_id)
                ->where('from_city_id',$city_id)
                ->where('completed',0)
                ->orderBy('id')
                ->get();
                */
            
                return view('operator.main-city.scan.outbound.form-9',compact('packages','row'));
            }
            
        }else{
            $deliveries = DB::table('users')->select('id','name')->where('branch_id',$branch_id)->where('role',3)->where('active',1)->orderBy('name')->get();
            
            if(check_service_point(Auth::user()->city_id)){
                return view('operator.service-point.scan.outbound.form-1',compact('deliveries','branch_type'));
            }else{
                if(is_sorting(user()->branch_id) == 1){
                    return view('operator.main-city.scan.outbound.form-11',compact('deliveries','branch_type'));
                }else{
                    return view('operator.main-city.scan.outbound.form-1',compact('deliveries','branch_type'));
                }
            }
        }
    }

    public function scan_inbound(){
        $branch_id  = user()->branch_id;
        $city_id    = user()->city_id;
        $branch_type= is_sorting($branch_id);
        $date       = date('Y-m-d');
        $min_date   = finding_date('- 5day');
        $row        = 0;

        if(!$_GET || $_GET['action']==1){
            $row = 0;
            $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->where('active',1)->orderBy('name')->get();

            if(check_service_point(Auth::user()->city_id)){
                $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                    ->join('packages as p','p.id','=','cp.package_id')
                    ->join('cities as c','c.id','=','p.from_city_id')
                    ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','p.from_city_id','c.shortcode as from_city_name','p.to_city_id','ctfs.rejected',DB::raw('count(*) as total'))
                    ->where('p.to_city_id',$city_id)
                    ->where('ctfs.completed',0)
                    ->where('ctfs.action_id',6)
                    ->whereDate('ctfs.created_at','>=',$min_date)
                    ->groupBy('ctfs.id','ctfs.ctf_no','p.from_city_id','c.shortcode','p.to_city_id')
                    ->orderBy('ctfs.ctf_no','desc')
                    ->get();

                return view('operator.service-point.scan.inbound.form-1',compact('ctfs','row'));
            }else{
                if(is_sorting(user()->branch_id) == 1){
                    $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                        ->join('packages as p','p.id','=','cp.package_id')
                        ->join('cities as c','c.id','=','p.from_city_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','p.from_city_id','c.shortcode as from_city_name','p.to_city_id','ctfs.rejected',DB::raw('count(*) as total'))
                        ->where('p.to_city_id',$city_id)
                        ->where('ctfs.completed',0)
                        ->where('ctfs.action_id',6)
                        ->whereDate('ctfs.created_at','>=',$min_date)
                        ->groupBy('ctfs.id','ctfs.ctf_no','p.from_city_id','c.shortcode','p.to_city_id')
                        ->orderBy('ctfs.ctf_no','desc')
                        ->get();

                    return view('operator.main-city.scan.inbound.form-1',compact('ctfs','row','branches'));
                }else{
                    $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                        ->join('packages as p','p.id','=','cp.package_id')
                        ->join('branches as b','b.id','=','p.from_branch_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name as from_branch_name','p.to_branch_id','ctfs.rejected',DB::raw('ctfs.id , COUNT(*) as total'))
                        ->where('p.to_branch_id',$branch_id)
                        ->where('ctfs.completed',0)
                        ->where('ctfs.action_id',8)
                        ->whereDate('ctfs.created_at','>=',$min_date)
                        ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','p.to_branch_id')
                        ->orderBy('ctfs.ctf_no','desc')
                        ->get();
                    
                    return view('operator.main-city.scan.inbound.form-6',compact('ctfs','row','branches'));
                }
                
            }
        }elseif(!$_GET || $_GET['action']==2){
            $cities  = Route::join('cities as c','c.id','=','routes.to_city_id')
                    ->select('c.id','c.name','c.mm_name','c.shortcode','c.state')
                    ->where('routes.from_city_id',$city_id)
                    ->where('routes.active',1)
                    ->orderBy('c.name')
                    ->get();

            $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->where('active',1)->orderBy('name')->get();
            $types      = DB::table('package_types')->select('id','name')->orderBy('name')->where('group_pkg',1)->get();
            
            if(check_service_point(Auth::user()->city_id)){
                return view('operator.service-point.scan.inbound.form-2',compact('branches','types','cities'));
            }else{
                if(is_sorting(user()->branch_id) == 1){
                    return view('operator.main-city.scan.inbound.form-2',compact('branches','types','cities'));
                }else{
                    return view('layouts.404');
                }
            }
        }elseif(!$_GET || $_GET['action']==3){
            if($_GET['type']=='city'){
                $cities     = Route::join('cities as c','c.id','=','routes.to_city_id')
                    ->select('c.id','c.name','c.mm_name','c.shortcode','c.state')
                    ->where('routes.from_city_id',$city_id)
                    ->where('routes.active',1)
                    ->orderBy('c.name')
                    ->get();
                $types      = DB::table('package_types')->select('id','name','icon')->orderBy('name')->get();
                
                if(check_service_point(Auth::user()->city_id)){
                    return view('operator.service-point.scan.inbound.form-4',compact('cities','types','city_id'));
                }else{
                    return view('operator.main-city.scan.inbound.form-12',compact('cities','types','city_id'));
                }
            }elseif($_GET['type']=='branch'){
                $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->where('active',1)->orderBy('name')->get();
                $types      = DB::table('package_types')->select('id','name','icon')->orderBy('name')->get();
            
                if(check_service_point(Auth::user()->city_id)){
                    return view('operator.service-point.scan.inbound.form-5',compact('branches','types'));
                }else{
                    return view('operator.main-city.scan.inbound.form-3',compact('branches','types'));
                }
            }else{
                //$cod        = fetched_cod_city($city_id);
                $branches   = DB::table('branches')->select('id','name')->where('name','like','%'.'-COD')->get();
                $types      = DB::table('package_types')->select('id','name','icon')->where('id',3)->get();
           
                if(check_service_point(Auth::user()->city_id)){
                    return view('operator.service-point.scan.inbound.form-6',compact('branches','types'));
                }else{
                    return view('operator.main-city.scan.inbound.form-3',compact('branches','types'));
                }
            }
        }elseif(!$_GET || $_GET['action']==4){
            //created new ctf form
            $packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
                ->select('packages.created_at','packages.id','packages.package_no','b.name as to_branch')
                ->where('completed',0)
                ->where('from_branch_id',$branch_id)
                ->orderBy('id','desc')
                ->get();
            
            if(check_service_point(Auth::user()->city_id)){
                return view('operator.service-point.scan.inbound.form-3',compact('packages'));
            }else{
                if(is_sorting(user()->branch_id) == 1){
                    return view('operator.main-city.scan.inbound.form-4',compact('packages'));
                }else{
                    return view('layouts.404');
                }
            }
        }elseif(!$_GET || $_GET['action']==5){
            $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->where('active',1)->orderBy('name')->get();
            
            $packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
                ->join('package_waybills as wp','wp.package_id','=','packages.id')
                ->join('package_types as pt','pt.id','=','packages.package_type_id')
                ->select('packages.created_at','packages.id','packages.package_no','b.name as to_branch','b.id as to_branch_id','pt.id as package_type_id','pt.name as type','pt.icon as icon',DB::raw('wp.package_id , COUNT(*) as qty'))
                ->where('packages.completed',0)
                ->where('packages.action_id','>=',8)
                ->where('packages.action_id','<=',10)
                ->where('packages.from_branch_id',$branch_id)
                ->whereDate('packages.created_at','>=',$min_date)
                ->groupBy('wp.package_id')
                ->orderBy('packages.id','desc')
                ->get();

            if(check_service_point(Auth::user()->city_id)){
                return view('operator.service-point.scan.inbound.form-7',compact('packages','branches','row'));
            }else{
                if(is_sorting(user()->branch_id) == 1){
                    return view('operator.main-city.scan.inbound.form-5',compact('packages','branches','row'));
                }else{
                    return view('layouts.404');
                }
            }
        }elseif(!$_GET || $_GET['action']==6){
            $row = 0;
            $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->orderBy('name')->get();

            if(is_sorting(user()->branch_id) == 1){
                $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                    ->join('packages as p','p.id','=','cp.package_id')
                    ->join('branches as b','b.id','=','p.from_branch_id')
                    ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name as from_branch_name','p.to_branch_id','ctfs.rejected',DB::raw('ctfs.id , COUNT(*) as total'))
                    ->where('p.to_branch_id',$branch_id)
                    ->where('ctfs.action_id',6)
                    ->where('ctfs.completed',0)
                    ->whereDate('ctfs.created_at','>=',$min_date)
                    ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','p.to_branch_id')
                    ->orderBy('ctfs.ctf_no')
                    ->get();
            }else{
                $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                    ->join('packages as p','p.id','=','cp.package_id')
                    ->join('branches as b','b.id','=','p.from_branch_id')
                    ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name as from_branch_name','p.to_branch_id','ctfs.rejected',DB::raw('ctfs.id , COUNT(*) as total'))
                    ->where('p.to_branch_id',$branch_id)
                    ->where('ctfs.action_id',9)
                    ->where('ctfs.completed',0)
                    ->whereDate('ctfs.created_at','>=',$min_date)
                    ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','p.to_branch_id')
                    ->orderBy('ctfs.ctf_no','desc')
                    ->get();
            }
            
            return view('operator.main-city.scan.inbound.form-6',compact('ctfs','row','branches'));
        }elseif(!$_GET || $_GET['action']==7){
            //create package form
            $cities     = DB::table('cities')->select('id','name','mm_name','shortcode','state')->orderBy('name')->get();
            $types      = DB::table('package_types')->select('id','name')->orderBy('name')->get();
            
            return view('operator.main-city.scan.inbound.form-7',compact('cities','types'));
        }elseif(!$_GET || $_GET['action']==8){
            $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->orderBy('name')->get();
            $types      = DB::table('package_types')->select('id','name','icon')->orderBy('name')->where('group_pkg',2)->get();
            
            return view('operator.main-city.scan.inbound.form-8',compact('branches','types'));
        }elseif(!$_GET || $_GET['action']==9){
            //created new ctf form
            $packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
                ->select('packages.id','packages.package_no','b.name as to_branch')
                ->where('completed',0)
                ->where('from_branch_id',$branch_id)
                ->orderBy('id')
                ->get();
            
            return view('operator.main-city.scan.inbound.form-9',compact('packages'));
        }elseif(!$_GET || $_GET['action']==10){
            $row = 0;

            $branches   = DB::table('branches')->select('id','name')->where('city_id',$city_id)->orderBy('name')->get();

            $packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
                ->join('package_waybills as wp','wp.package_id','=','packages.id')
                ->join('package_types as pt','pt.id','=','packages.package_type_id')
                ->select('packages.created_at','packages.id','packages.package_no','b.id as to_branch_id','b.name as to_branch','pt.id as type_id','pt.name as type','pt.icon as icon',DB::raw('wp.package_id , COUNT(*) as qty'))
                ->where('from_branch_id',$branch_id)
                ->where('packages.completed',0)
                ->where('packages.action_id',8)
                ->whereDate('packages.created_at','>=',$min_date)
                ->groupBy('wp.package_id')
                ->orderBy('packages.id')
                ->get();

            // $packages = Package::join('cities as c','c.id','=','packages.to_city_id')
            //         ->join('package_waybills as wp','wp.package_id','=','packages.id')
            //         ->select('packages.created_at','packages.id','packages.package_no','c.shortcode as to_city',DB::raw('wp.package_id , COUNT(*) as qty'))
            //         ->where('packages.completed',0)
            //         ->where('packages.action_id',5)
            //         ->where('packages.from_city_id',$city_id)
            //         ->groupBy('wp.package_id')
            //         ->orderBy('packages.id')
            //         ->get();
            //return $packages;
            return view('operator.main-city.scan.inbound.form-10',compact('packages','branches','row'));
        }elseif(!$_GET || $_GET['action']==0){
            //created new ctf form
            //$city_id = user()->city_id;
            //$cities = City::select('id','name','mm_name','shortcode','state')->orderBy('shortcode')->get();
            $cities  = Route::join('cities as c','c.id','=','routes.to_city_id')
                    ->select('c.id','c.name','c.mm_name','c.shortcode','c.state')
                    ->where('routes.from_city_id',$city_id)
                    ->where('routes.active',1)
                    ->orderBy('c.name')
                    ->get();
                    
            return view('operator.service-point.scan.inbound.form-3',compact('cities','city_id'));
        }else{
            return view('operator.main-city.scan.branch-in-form-1');
        }
    }


    public function print_qr(){
        $qr = $_GET['qr'];

        $ctf = CTF::leftjoin('users as u','u.id','=','ctfs.user_id')
            ->leftjoin('branches as b1','b1.id','=','ctfs.branch_id')
            ->leftjoin('branches as b2','b2.id','=','ctfs.to_branch_id')
            ->leftjoin('cities as c1','c1.id','=','ctfs.city_id')
            ->leftjoin('cities as c2','c2.id','=','ctfs.to_city_id')
            ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','u.name as exported_by','b1.name as from_branch','b2.name as to_branch','c1.shortcode as from_city','c2.shortcode as to_city')
            ->where('ctf_no',$qr)
            ->first();

        $packages = DB::table('ctf_packages')
            ->leftjoin('ctfs as c','c.id','=','ctf_packages.ctf_id')
            ->leftjoin('packages as p','p.id','=','ctf_packages.package_id')
            ->leftjoin('package_types as pt','pt.id','=','p.package_type_id')
            ->select('p.package_no','pt.name')
            ->where('c.ctf_no',$qr)
            ->get();

        $qty = CtfPackage::where('ctf_id',$ctf->id)->count();

        return view('layouts.print',compact('qr','packages','ctf','qty','pkg_types'));
    }

    public function search_data(){
        return view('partials.search-data');
    }

    public function search_data_result(Request $request,$param){
        $qty = 0;

        if($param == 'form-1'){

            $check1 = DB::table('waybills')->select('waybill_no')->where('waybill_no',$request->waybill)->first();
            $check2 = DB::table('bk_waybills')->select('waybill_no')->where('waybill_no',$request->waybill)->first();

            if($check1){
                $waybill = Waybill::leftjoin('users as u','u.id','=','waybills.user_id')
                    ->join('cities as c1','c1.id','=','waybills.origin')
                    ->leftjoin('cities as t1','t1.id','=','waybills.transit_1')
                    ->leftjoin('cities as t2','t2.id','=','waybills.transit_2')
                    ->leftjoin('cities as c2','c2.id','=','waybills.destination')
                    ->select('waybills.id','waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','t1.shortcode as transit_1','t2.shortcode as transit_2','waybills.qty','waybills.same_day','waybills.action_id','u.name as courier','waybills.active','waybills.end_point','waybills.user_id')
                    ->where('waybill_no',$request->waybill)
                    ->first();

                $logs    = ActionLog::join('branches as b','b.id','=','action_logs.branch_id')
                        ->select('action_logs.action_id','action_logs.log','action_logs.created_at','b.name as branch')
                        ->where('waybill_id',$waybill->id)
                        ->get();

                $packages    = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                        ->select('p.id','p.package_no','p.package_type_id','p.remark')
                        ->where('waybill_id',$waybill->id)
                        ->get(); 
            }elseif($check2){
                $waybill = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                    ->join('cities as c1','c1.id','=','bk_waybills.origin')
                    ->leftjoin('cities as t1','t1.id','=','bk_waybills.transit_1')
                    ->leftjoin('cities as t2','t2.id','=','bk_waybills.transit_2')
                    ->leftjoin('cities as c2','c2.id','=','bk_waybills.destination')
                    ->select('bk_waybills.waybill_id as id','bk_waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','t1.shortcode as transit_1','t2.shortcode as transit_2','bk_waybills.qty','bk_waybills.same_day','bk_waybills.action_id','u.name as courier','bk_waybills.active','bk_waybills.end_point','bk_waybills.user_id')
                    ->where('waybill_no',$request->waybill)
                    ->first();

                $logs    = DB::table('bk_action_logs')->join('branches as b','b.id','=','bk_action_logs.branch_id')
                        ->select('bk_action_logs.action_id','bk_action_logs.log','bk_action_logs.created_at','b.name as branch')
                        ->where('waybill_id',$waybill->id)
                        ->get();

                $packages    = DB::table('bk_package_waybills')->join('bk_packages as p','p.package_id','=','bk_package_waybills.package_id')
                        ->select('p.id','p.package_no','p.package_type_id','p.remark')
                        ->where('waybill_id',$waybill->id)
                        ->get();
            }else{
                $waybill    = array();
                $logs       = array();
                $packages   = array();
            }
            return view('partials.view-waybill',compact('waybill','logs','packages'));
        }elseif($param == 'form-2'){
            $package_no = $_GET['package_no'];
            $qty        = 0;
            $remove     = 0;

            $check1 = DB::table('packages')->select('package_no')->where('package_no',$package_no)->first();
            $check2 = DB::table('bk_packages')->select('package_no')->where('package_no',$package_no)->first();

            if($check1){
                $package = Package::join('package_types as pt','pt.id','=','packages.package_type_id')
                    ->select('packages.id','package_no','packages.remark','pt.name as package_type','packages.completed')
                    ->where('package_no',$package_no)
                    ->first(); 

                if($package){
                    $waybills   = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                        ->join('waybills as w','w.id','=','package_waybills.waybill_id')
                        ->leftjoin('users as u','u.id','=','w.user_id')
                        ->select('package_waybills.id','waybill_no','waybill_id','p.package_no','u.name as courier','package_waybills.active','w.qty','package_waybills.completed')
                        ->where('p.package_no',$package_no)
                        ->get();

                    $branches = Package::join('branches as b','b.id','=','packages.from_branch_id')
                        ->leftjoin('branches as b2','b2.id','=','packages.to_branch_id')
                        ->leftjoin('cities as c1','c1.id','=','packages.from_city_id')
                        ->leftjoin('cities as c2','c2.id','=','packages.to_city_id')
                        ->select('b.name as from_branch','b2.name as to_branch','c1.shortcode as from_city','c2.shortcode as to_city')
                        ->where('package_no',$package_no)
                        ->first();

                    $ctf  = CtfPackage::join('packages as p','p.id','=','ctf_packages.package_id')
                        ->join('ctfs as c','c.id','=','ctf_packages.ctf_id')
                        ->select('c.id','c.ctf_no')
                        ->where('p.id',$package->id)
                        ->first();


                }else{
                    $branches   = array();

                    $waybills   = array();

                    $ctf  = array();
                }
            }elseif($check2){
                $package = Package::join('package_types as pt','pt.id','=','packages.package_type_id')
                    ->select('packages.id','package_no','packages.remark','pt.name as package_type','packages.completed')
                    ->where('package_no',$package_no)
                    ->first(); 

                if($package){
                    $waybills   = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                        ->join('waybills as w','w.id','=','package_waybills.waybill_id')
                        ->leftjoin('users as u','u.id','=','w.user_id')
                        ->select('waybill_no','waybill_id','p.package_no','u.name as courier','package_waybills.active','w.qty','package_waybills.completed')
                        ->where('p.package_no',$package_no)
                        ->get();

                    $branches = Package::join('branches as b','b.id','=','packages.from_branch_id')
                        ->leftjoin('branches as b2','b2.id','=','packages.to_branch_id')
                        ->leftjoin('cities as c1','c1.id','=','packages.from_city_id')
                        ->leftjoin('cities as c2','c2.id','=','packages.to_city_id')
                        ->select('b.name as from_branch','b2.name as to_branch','c1.shortcode as from_city','c2.shortcode as to_city')
                        ->where('package_no',$package_no)
                        ->first();

                    $ctf  = CtfPackage::join('packages as p','p.id','=','ctf_packages.package_id')
                        ->join('ctfs as c','c.id','=','ctf_packages.ctf_id')
                        ->select('c.id','c.ctf_no')
                        ->where('p.id',$package->id)
                        ->first();


                }else{
                    $branches   = array();

                    $waybills   = array();

                    $ctf  = array();
                }
            }else{
                $package    = array();
                $branches   = array();
                $waybills   = array();
                $ctf        = array();
            }

            return view('partials.view-package',compact('package_no','package','waybills','branches','qty','remove','ctf'));
        }else{
            $ctf_no = $_GET['ctf_no'];

            $ctf    = CTF::join('users as u','u.id','=','ctfs.user_id')
                    ->join('branches as b','b.id','=','ctfs.branch_id')
                    ->join('cities as c','c.id','=','ctfs.city_id')
                    ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','u.name as exported_by','b.name as branch','c.shortcode as city')
                    ->where('ctf_no',$ctf_no)
                    ->first();

            if($ctf){
               $packages    = CtfPackage::join('packages as p','p.id','=','ctf_packages.package_id')
                ->join('users as u','u.id','=','p.user_id')
                ->select('package_no','user_id','u.name as created_by','p.completed_at')
                ->where('ctf_packages.ctf_id',$ctf->id)
                ->get(); 

                $qty = CtfPackage::where('ctf_id',$ctf->id)->count(); 
            }else{
                $packages   = 0;
                $qty        = 0;
            }

            return view('partials.view-ctf',compact('ctf','packages','qty'));
        }
    }

    
    public function view_ctf($id){
        $package_no = $id;
        $qty        = 0;
        $remove     = 0;

        $ctf    = CTF::join('cities as c','c.id','=','ctfs.city_id')
                ->join('branches as b','b.id','=','ctfs.branch_id')
                ->select('ctfs.id','ctfs.ctf_no','b.name as branch','c.shortcode as city')
                ->where('ctfs.id',$id)
                ->first(); 

        $packages    = CtfPackage::join('packages as p','p.id','=','ctf_packages.package_id')
            ->join('users as u','u.id','=','p.user_id')
            ->select('package_no','user_id','u.name as created_by','p.completed_at')
            ->where('ctf_packages.ctf_id',$ctf->id)
            ->get(); 
           
        return view('partials.view-ctf',compact('ctf','packages','qty'));
    }

    public function view_package($id){
        $package_no = $id;
        $qty        = 0;
        $remove     = 0;

        $check1 = DB::table('packages')->select('package_no')->where('package_no',$id)->first();
        $check2 = DB::table('bk_packages')->select('package_no')->where('package_no',$id)->first();

        if($check1){
            $package = Package::join('package_types as pt','pt.id','=','packages.package_type_id')
                ->select('packages.id','package_no','packages.remark','pt.name as package_type','packages.completed')
                ->where('package_no',$package_no)
                ->first(); 

            if($package){
                // $branches = Package::join('branches as b','b.id','=','packages.from_branch_id')
                //     ->leftjoin('branches as b2','b2.id','=','packages.to_branch_id')
                //     ->select('b.name as from_branch','b2.name as to_branch')
                //     ->where('package_no',$package_no)
                //     ->first();

                // $waybills   = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                //     ->join('waybills as w','w.id','=','package_waybills.waybill_id')
                //     ->join('users as u','u.id','=','w.user_id')
                //     ->select('waybill_no','waybill_id','p.package_no','u.name as courier','w.qty','package_waybills.active')
                //     ->where('p.package_no',$package_no)
                //     ->get();

                $waybills   = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('waybills as w','w.id','=','package_waybills.waybill_id')
                    ->leftjoin('users as u','u.id','=','w.user_id')
                    ->select('package_waybills.id','waybill_no','waybill_id','p.package_no','u.name as courier','package_waybills.active','w.qty','package_waybills.completed')
                    ->where('p.package_no',$package_no)
                    ->get();

                $branches = Package::join('branches as b','b.id','=','packages.from_branch_id')
                    ->leftjoin('branches as b2','b2.id','=','packages.to_branch_id')
                    ->leftjoin('cities as c1','c1.id','=','packages.from_city_id')
                    ->leftjoin('cities as c2','c2.id','=','packages.to_city_id')
                    ->select('b.name as from_branch','b2.name as to_branch','c1.shortcode as from_city','c2.shortcode as to_city')
                    ->where('package_no',$package_no)
                    ->first();

                $ctf  = CtfPackage::join('packages as p','p.id','=','ctf_packages.package_id')
                    ->join('ctfs as c','c.id','=','ctf_packages.ctf_id')
                    ->select('c.id','c.ctf_no')
                    ->where('p.id',$package->id)
                    ->first();


            }else{
                $branches   = array();

                $waybills   = array();

                $ctf  = array();
            }


        }elseif($check2){
            $package = DB::table('bk_packages')->join('package_types as pt','pt.id','=','bk_packages.package_type_id')
                ->select('bk_packages.id','bk_packages.package_no','bk_packages.remark','pt.name as package_type','bk_packages.completed','bk_packages.completed_at')
                ->where('bk_packages.package_no',$package_no)
                ->first(); 

            if($package){
                // $branches = Package::join('branches as b','b.id','=','packages.from_branch_id')
                //     ->leftjoin('branches as b2','b2.id','=','packages.to_branch_id')
                //     ->select('b.name as from_branch','b2.name as to_branch')
                //     ->where('package_no',$package_no)
                //     ->first();

                // $waybills   = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                //     ->join('waybills as w','w.id','=','package_waybills.waybill_id')
                //     ->join('users as u','u.id','=','w.user_id')
                //     ->select('waybill_no','waybill_id','p.package_no','u.name as courier','w.qty','package_waybills.active')
                //     ->where('p.package_no',$package_no)
                //     ->get();

                $waybills   = DB::table('bk_package_waybills')->join('bk_packages as p','p.package_id','=','bk_package_waybills.package_id')
                    ->join('bk_waybills as w','w.waybill_id','=','bk_package_waybills.waybill_id')
                    ->leftjoin('users as u','u.id','=','w.user_id')
                    ->select('waybill_no','w.waybill_id','p.package_no','u.name as courier','bk_package_waybills.active','w.qty','bk_package_waybills.completed')
                    ->where('p.package_no',$package_no)
                    ->get();

                $branches = DB::table('bk_packages')->join('branches as b','b.id','=','bk_packages.from_branch_id')
                    ->leftjoin('branches as b2','b2.id','=','bk_packages.to_branch_id')
                    ->leftjoin('cities as c1','c1.id','=','bk_packages.from_city_id')
                    ->leftjoin('cities as c2','c2.id','=','bk_packages.to_city_id')
                    ->select('b.name as from_branch','b2.name as to_branch','c1.shortcode as from_city','c2.shortcode as to_city')
                    ->where('package_no',$package_no)
                    ->first();

                $ctf  = DB::table('bk_ctf_packages')->join('bk_packages as p','p.package_id','=','bk_ctf_packages.package_id')
                    ->join('bk_ctfs as c','c.ctf_id','=','bk_ctf_packages.ctf_id')
                    ->select('c.id','c.ctf_no')
                    ->where('p.id',$package->id)
                    ->first();


            }else{
                $branches   = array();

                $waybills   = array();

                $ctf  = array();
            }
        }else{
                $package    = array();
                $branches   = array();
                $waybills   = array();
                $ctf        = array();
        }

        return view('partials.view-package',compact('package_no','package','waybills','branches','qty','remove','ctf'));
    }

    public function view_waybill($id){

        $check1 = DB::table('waybills')->select('waybill_no')->where('waybill_no',$id)->first();
        $check2 = DB::table('bk_waybills')->select('waybill_no')->where('waybill_no',$id)->first();
        
        if($check1){
            $waybill = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                    ->join('cities as c1','c1.id','=','waybills.origin')
                    ->leftjoin('cities as t1','t1.id','=','waybills.transit_1')
                    ->leftjoin('cities as t2','t2.id','=','waybills.transit_2')
                    ->leftjoin('cities as c2','c2.id','=','waybills.destination')
                    ->select('waybills.id','waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','t1.shortcode as transit_1','t2.shortcode as transit_2','waybills.qty','waybills.same_day','waybills.action_id','u.name as courier','waybills.active','waybills.end_point','waybills.user_id')
                    ->where('waybill_no',$id)
                    ->first();

            $logs    = DB::table('action_logs')->join('branches as b','b.id','=','action_logs.branch_id')
                    ->select('action_logs.action_id','action_logs.log','action_logs.created_at','b.name as branch')
                    ->where('waybill_id',$waybill->id)
                    ->get();
            $packages    = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->select('p.id','p.package_no','p.package_type_id','p.remark')
                    ->where('waybill_id',$waybill->id)
                    ->get(); 
        }elseif($check2){
            $waybill = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                    ->join('cities as c1','c1.id','=','bk_waybills.origin')
                    ->leftjoin('cities as t1','t1.id','=','bk_waybills.transit_1')
                    ->leftjoin('cities as t2','t2.id','=','bk_waybills.transit_2')
                    ->leftjoin('cities as c2','c2.id','=','bk_waybills.destination')
                    ->select('bk_waybills.waybill_id as id','bk_waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','t1.shortcode as transit_1','t2.shortcode as transit_2','bk_waybills.qty','bk_waybills.same_day','bk_waybills.action_id','u.name as courier','bk_waybills.active','bk_waybills.end_point','bk_waybills.user_id')
                    ->where('waybill_no',$id)
                    ->first();

            $logs    = DB::table('bk_action_logs')->join('branches as b','b.id','=','bk_action_logs.branch_id')
                ->select('bk_action_logs.action_id','bk_action_logs.log','bk_action_logs.created_at','b.name as branch')
                ->where('waybill_id',$waybill->id)
                ->get();

            $packages    = DB::table('bk_package_waybills')->join('bk_packages as p','p.package_id','=','bk_package_waybills.package_id')
                ->select('p.id','p.package_no','p.package_type_id','p.remark')
                ->where('waybill_id',$waybill->id)
                ->get();

        }else{
            $waybill    = array();
            $logs       = array();
            $packages   = array();
        }
        
        return view('partials.view-waybill',compact('waybill','logs','packages'));
    }

    public function outbound_status($status){
        $json = 'json/outbound/status/'.$status;

        return view('operator.main-city.waybills.outbound',compact('json','status'));
    }

    public function inbound_status($status){
        $json = 'json/inbound/status/'.$status;

        return view('operator.main-city.waybills.inbound',compact('json','status'));
    }

    public function json_outbound_status($status){
        $date       = get_date();
        $city_id    = user()->city_id;
        
        if($status == 'pending'){
            //pending status
            $primary    = DB::table('waybills')->where('action_id',1)->where('waybills.active',1)->where('origin',$city_id)->whereDate('waybills.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_waybills')->where('action_id',1)->where('bk_waybills.active',1)->where('origin',$city_id)->whereDate('bk_waybills.created_at','like',$date.'%')->count();

            if($primary > 0){
                $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('action_id',1)
                    ->where('waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at','desc')
                    ->paginate(100);
            }else{
                $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','bk_waybills.end_point')
                    ->where('action_id',1)
                    ->where('bk_waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('bk_waybills.created_at','like',$date.'%')
                    ->orderBy('bk_waybills.created_at','desc')
                    ->paginate(100);
            }
        }elseif($status == 'progress'){
            //progress status
            $primary    = DB::table('waybills')->where('action_id','>',1)->where('action_id','<',5)->where('waybills.active',1)->where('origin',$city_id)->whereDate('waybills.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_waybills')->where('action_id','>',1)->where('action_id','<',5)->where('bk_waybills.active',1)->where('origin',$city_id)->whereDate('bk_waybills.created_at','like',$date.'%')->count();
            if($primary > 0){
               $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('action_id','>',1)
                    ->where('action_id','<',5)
                    ->where('waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at','desc')
                    ->paginate(100); 
            }else{
                $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','bk_waybills.end_point')
                    ->where('action_id','>',1)
                    ->where('action_id','<',5)
                    ->where('bk_waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('bk_waybills.created_at','like',$date.'%')
                    ->orderBy('bk_waybills.created_at','desc')
                    ->paginate(100); 
            }
        }elseif($status == 'completed'){
            //completed status
            $primary    = DB::table('waybills')->where('action_id','>=',5)->where('waybills.active',1)->where('origin',$city_id)->whereDate('waybills.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_waybills')->where('action_id','>=',5)->where('bk_waybills.active',1)->where('origin',$city_id)->whereDate('bk_waybills.created_at','like',$date.'%')->count();

            if($primary > 0){
                $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('action_id','>=',5)
                    ->where('waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at','desc')
                    ->paginate(100);
            }else{
                $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','bk_waybills.end_point')
                    ->where('action_id','>=',5)
                    ->where('bk_waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('bk_waybills.created_at','like',$date.'%')
                    ->orderBy('bk_waybills.created_at','desc')
                    ->paginate(100);
            }
        }elseif($status == 'cancelled'){
            //cancelled status
            $primary    = DB::table('waybills')->where('waybills.active',0)->where('origin',$city_id)->whereDate('waybills.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_waybills')->where('bk_waybills.active',0)->where('origin',$city_id)->whereDate('bk_waybills.created_at','like',$date.'%')->count();

            if($primary > 0){
                $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('waybills.active',0)
                    ->where('origin',$city_id)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at','desc')
                    ->paginate(100);
            }else{
                $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','bk_waybills.end_point')
                    ->where('bk_waybills.active',0)
                    ->where('origin',$city_id)
                    ->whereDate('bk_waybills.created_at','like',$date.'%')
                    ->orderBy('bk_waybills.created_at','desc')
                    ->paginate(100);
            }
        }elseif($status == 'same-day'){
            //same day waybill status
            $primary    = DB::table('waybills')->where('same_day',1)->where('active',1)->where('waybills.active',1)->where('origin',$city_id)->whereDate('waybills.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_waybills')->where('same_day',1)->where('active',1)->where('bk_waybills.active',1)->where('origin',$city_id)->whereDate('bk_waybills.created_at','like',$date.'%')->count();

            if($primary > 0){
                $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('same_day',1)
                    ->where('waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at','desc')
                    ->paginate(100);
            }else{
                $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','bk_waybills.end_point')
                    ->where('same_day',1)
                    ->where('bk_waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('bk_waybills.created_at','like',$date.'%')
                    ->orderBy('bk_waybills.created_at','desc')
                    ->paginate(100);
            }
        }elseif($status == 'all'){
            //all status
            $primary    = DB::table('waybills')->where('active',1)->where('waybills.active',1)->where('origin',$city_id)->whereDate('waybills.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_waybills')->where('active',1)->where('bk_waybills.active',1)->where('origin',$city_id)->whereDate('bk_waybills.created_at','like',$date.'%')->count();

            if($primary > 0){
                $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at','desc')
                    ->paginate(100);
            }else{
                $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','bk_waybills.end_point')
                    ->where('bk_waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('bk_waybills.created_at','like',$date.'%')
                    ->orderBy('bk_waybills.created_at','desc')
                    ->paginate(100); 
            }
        }else{
            $waybills = array();
        }

        return response()->json($waybills);        
    }
    
    public function json_inbound_status($status){
        $date       = get_date();
        $city_id    = user()->city_id;

        if($status == 'pending'){
            //pending status
            $waybills = Waybill::leftjoin('users as u','u.id','=','waybills.user_id')
                ->join('cities as c1','c1.id','=','waybills.origin')
                ->join('cities as c2','c2.id','=','waybills.destination')
                ->join('action_logs as a','a.waybill_id','=','waybills.id')
                ->select('waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','u.name as courier','waybills.action_id','same_day','qty')
                ->where('waybills.active',1)
                ->where('waybills.action_id',7)
                ->where('a.action_id',7)
                ->where('destination',$city_id)
                ->whereDate('a.created_at','like',$date.'%')
                ->paginate(100);
        }elseif($status == 'progress'){
            //progress status
            $waybills = Waybill::leftjoin('users as u','u.id','=','waybills.user_id')
                ->join('cities as c1','c1.id','=','waybills.origin')
                ->join('cities as c2','c2.id','=','waybills.destination')
                ->join('action_logs as a','a.waybill_id','=','waybills.id')
                ->select('waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','u.name as courier','waybills.action_id','same_day','qty')
                ->where('waybills.active',1)
                ->where('waybills.action_id','>',7)
                ->where('waybills.action_id','<',10)
                ->where('a.action_id',8)
                ->where('destination',$city_id)
                ->whereDate('a.created_at','like',$date.'%')
                ->paginate(100);
        }elseif($status == 'completed'){
            //completed status
            $waybills = Waybill::leftjoin('users as u','u.id','=','waybills.user_id')
                ->join('cities as c1','c1.id','=','waybills.origin')
                ->join('cities as c2','c2.id','=','waybills.destination')
                ->join('action_logs as a','a.waybill_id','=','waybills.id')
                ->select('waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','u.name as courier','waybills.action_id','same_day','qty')
                ->where('waybills.active',1)
                ->where('waybills.action_id',10)
                ->where('a.action_id',10)
                ->where('destination',$city_id)
                ->whereDate('a.created_at','like',$date.'%')
                ->paginate(100);
        }elseif($status == 'all'){
            //all status
            $primary    = DB::table('waybills')->where('waybills.active',1)->where('destination',$city_id)->whereDate('waybills.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_waybills')->where('bk_waybills.active',1)->where('destination',$city_id)->whereDate('bk_waybills.created_at','like',$date.'%')->count();

            if($primary > 0){
                $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->join('cities as c1','c1.id','=','waybills.origin')
                    ->join('cities as c2','c2.id','=','waybills.destination')
                    ->join('action_logs as a','a.waybill_id','=','waybills.id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','waybills.action_id','same_day','qty','b.name as branch','waybills.end_point','c1.shortcode as origin','c2.shortcode as destination')
                    ->where('waybills.active',1)
                    ->where('destination',$city_id)
                    ->where('waybills.action_id','>',7)
                    ->where('a.action_id',7)
                    ->whereDate('a.created_at','like',$date.'%')
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }else{
                $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                    ->leftjoin('branches as b','b.id','=','u.branch_id')
                    ->join('cities as c1','c1.id','=','bk_waybills.origin')
                    ->join('cities as c2','c2.id','=','bk_waybills.destination')
                    ->join('bk_action_logs as a','a.waybill_id','=','bk_waybills.id')
                    ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','bk_waybills.end_point','c1.shortcode as origin','c2.shortcode as destination')
                    ->where('bk_waybills.active',1)
                    ->where('destination',$city_id)
                    ->where('waybills.action_id','>',7)
                    ->whereDate('bk_waybills.created_at','like',$date.'%')
                    ->orderBy('bk_waybills.waybill_no','asc')
                    ->paginate(100); 
            }
        }else{
            $waybills = array();
        }

        return response()->json($waybills);        
    }

    public function outbound_statistics(){
        $users = User::join('action_logs as a','a.user_id','=','users.id')
            ->select('user_id','waybill_id')
            ->select(DB::raw('user_id,name, COUNT(*) as total'))
            ->where('a.action_id',5)
            ->groupBy('user_id','name')
            ->orderBy('total','desc')
            ->get();

            // User::join('waybills as w','w.user_id','=','users.id')
            // ->join('branches as b','b.id','=','users.branch_id')
            // ->where('outbound_date','like',$date.'%')
            // ->where('w.origin',$city_id)
            // ->select(DB::raw('b.name,b.id, COUNT(*) as total'))
            // ->groupBy('b.name','b.id')
            // ->get();

        
        //return $users;

        return view('partials.sorting-staff-outbound',compact('users'));
    }

    public function ctfs_status($status){
        $json = 'json/ctfs/'.$status;
        return view('operator.main-city.ctfs.outbound',compact('status','json'));
    }

    public function json_ctfs_status(){

        $ctfs = CTF::join('users as u','u.id','=','ctfs.user_id')
            ->join('cities as c','c.id','=','ctfs.city_id')
            ->join('branches as b','b.id','=','ctfs.branch_id')
            ->select('ctf_no','u.name as created_by','c.name as city','b.name as branch','action_id','completed')
            ->paginate(50);

        return $ctfs;
    }

    public function action_logs(){
        $json = 'json/action-logs';
        
        return view('operator.main-city.waybills.logs',compact('json'));
    }

    public function json_action_logs(){
        $city_id = user()->city_id;
        $date    = get_date();

        $action_logs = ActionLog::join('branches as b','b.id','=','action_logs.branch_id')
            ->join('waybills as w','w.id','=','action_logs.waybill_id')
            ->select('waybill_no','log','b.name as branch','action_logs.action_id','action_logs.created_at')
            ->where('action_logs.city_id',$city_id)
            ->whereDate('w.created_at','like',$date.'%')
            ->orderBy('action_logs.id','desc')
            ->paginate(100);

        return response()->json($action_logs);
    }

    public function sync_waybills(){
        $json = 'json/sync-data/waybills';
        
        return view('operator.main-city.waybills.sync-waybills',compact('json'));
    }

    public function json_sync_waybills(){
        $city_id    = user()->city_id;
        $date       = get_date();

        $action_logs = WaybillJob::join('waybills as w','w.id','=','waybill_jobs.waybill_id')
            ->select('w.created_at','w.id','w.waybill_no','waybill_jobs.status','waybill_jobs.params','waybill_jobs.synced')
            ->where('waybill_jobs.city_id',$city_id)
            ->whereDate('waybill_jobs.created_at','like','%'.$date.'%')
            ->orderBy('waybill_jobs.synced','desc')
            ->paginate(50);

        return response()->json($action_logs);
    }

    public function collected_by_courier(){
       $json = 'json/outbound/collected-by-courier';

        return view('operator.main-city.waybills.collected-by-courier',compact('json'));
    }

    public function json_collected_by_courier(){
        $origin = user()->city_id;
        $date   = get_date();
        $count  = 0;
        
        $couriers = Waybill::leftjoin('users as u','u.id','=','waybills.user_id')
            ->leftjoin('branches as b', 'b.id', '=', 'u.branch_id')
            ->select('user_id','u.name','b.name AS branch','b.id AS branch_id',DB::raw('count(*) as count'))
            ->whereDate('waybills.created_at','like','%'.$date.'%')
            ->where('origin',$origin)
            ->groupBy('user_id')
            ->orderBy('u.name')
            ->get();


        return $couriers;
    }

    public function view_collected_by_courier($id){
       $json = 'json/outbound/collected-by-courier/'.$id;

        return view('operator.main-city.waybills.view-collected-by-courier',compact('json','id'));
    }

    public function view_collected_by_branch($id){
       $json = 'json/outbound/collected-by-branch/'.$id;

        return view('operator.main-city.waybills.view-collected-by-branch',compact('json','id'));
    }

    public function json_view_collected_by_courier($id){
        $origin = user()->city_id;
        $date   = get_date();
        $count  = 0;

        $courier = Waybill::leftjoin('users as u','u.id','=','waybills.user_id')
            ->leftjoin('branches as b', 'b.id', '=', 'u.branch_id')
            ->select('waybills.created_at','waybill_no','u.name','b.name AS branch','b.id AS branch_id','action_id','same_day','qty')
            ->where('user_id',$id)
            ->where('origin',$origin)
            ->whereDate('waybills.created_at','like','%'.$date.'%')
            ->paginate(100);

        return $courier;
    }

    public function json_view_collected_by_branch($id){
        $origin = user()->city_id;
        $date   = get_date();
        $count  = 0;

        $courier = Waybill::leftjoin('users as u','u.id','=','waybills.user_id')
            ->leftjoin('branches as b', 'b.id', '=', 'u.branch_id')
            ->leftjoin('action_logs as a','a.waybill_id','=','waybills.id')
            ->select('waybills.created_at','waybill_no','u.name','b.name AS branch','b.id AS branch_id','waybills.action_id','same_day','qty')
            ->where('a.branch_id',$id)
            ->where('origin',$origin)
            ->where('a.action_id',1)
            ->whereDate('waybills.created_at','like','%'.$date.'%')
            ->paginate(100);

        return $courier;
    }


    public function json_fetched_ctf($ctf_id){

        $response = array();
        $qty = 0;

        $ctf = CTF::join('users as u','u.id','=','ctfs.user_id')
            ->join('branches as b','b.id','=','ctfs.branch_id')
            ->select('ctfs.id','ctfs.ctf_no','u.name as username','b.name as branch_name','ctfs.completed')
            ->where('ctfs.id',$ctf_id)
            ->first();

        $packages  = CtfPackage::join('packages as p','p.id','=','ctf_packages.package_id')
            ->join('branches as b','b.id','=','p.from_branch_id')
            ->leftjoin('cities as c','c.id','=','p.to_city_id')
            ->select('ctf_packages.id','ctf_packages.ctf_id','ctf_packages.package_id','p.package_no','b.name as from_branch_name','c.name as to_city_name')
            ->where('ctf_id',$ctf_id)
            ->where('ctf_packages.completed',0)
            ->orderBy('id','asc')->get();

        if($packages->count() == 0){
            //Ctf::where('id',$request->ctf_id)->update(['completed'=>1]);
        }

        //return response()->json($packages);
        return view('operator.main-city.ctfs.view-ctf',compact('packages','qty','ctf'));
    }


    public function search($term){
        $response = array();
        $arr      = array();

        $waybills  = Waybill::select('waybill_no as name','waybill_no as url','waybill_no as icon')->get();
        $response['listItems'] = $waybills;

        return $response;
    }

    public function ctfs_history(){
        $date = date('Y-m-d');
        $city_id = user()->city_id;
        $json = 'json/ctf/changes-history';

        return view('operator.main-city.ctfs.history',compact('json'));
    }

    public function packages_history(){
        $date = date('Y-m-d');
        $city_id = user()->city_id;
        $json = 'json/packages/changes-history';

        return view('operator.main-city.packages.history',compact('json'));
    }

    public function view_packages_history($id){
        $history = History::join('packages as p','p.id','=','histories.data_id')
            ->select('package_no','log')
            ->where('histories.data_id',$id)
            ->get();

        return $history;
    }

    public function json_ctfs_history(){
        $date = date('Y-m-d');
        $city_id = user()->city_id;

        // $ctfs = Ctf::join('users as u','u.id','=','ctfs.user_id')
        //     ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','u.name as created_by','ctfs.history')
        //     ->where('history','!=',NULL)
        //     ->where('ctfs.city_id',$city_id)
        //     ->whereDate('ctfs.created_at','like',$date.'%')
        //     ->paginate(100);

        $ctfs = History::leftjoin('ctfs as c','c.id','=','histories.data_id')
            ->join('users as u','u.id','=','c.user_id')
            ->select('c.created_at','c.id','c.ctf_no','u.name as created_by')
            ->where('c.city_id',$city_id)
            ->whereDate('c.created_at','like',$date.'%')
            ->groupBy('c.id')
            ->paginate(50);

        return response()->json($ctfs);
    }

    public function json_packages_history(){
        $date       = date('Y-m-d');
        $city_id    = user()->city_id;

        // $packages = History::join('packages as p','p.id','=','histories.data_id')
        //     ->join('users as u','u.id','=','p.user_id')
        //     ->join('branches as b','b.id','=','p.from_branch_id')
        //     ->select('p.created_at','p.id','p.package_no','u.name as created_by','b.name as branch','log')
        //     ->where('p.from_city_id',$city_id)
        //     ->whereDate('p.created_at','like',$date.'%')
        //     ->paginate(100);

        $packages = History::leftjoin('packages as p','p.id','=','histories.data_id')
            ->join('users as u','u.id','=','p.user_id')
            ->select('p.created_at','p.id','p.package_no','u.name as created_by')
            ->where('p.from_city_id',$city_id)
            ->whereDate('p.created_at','like',$date.'%')
            ->groupBy('p.id')
            ->paginate(50);

        return response()->json($packages);
    }

    public function delete_data(){
        if(Auth::user()->role == 1){
            return view('admin.delete-data');
        }
    }

    public function deleted_data(Request $request){
        if(Auth::user()->role == 1){
            $type = $request->type;

            if($type == 'ctf'){
                $ctf_no = $request->ctf_no;
                $package_no = 0;
                $result = $request->ctf_no;
                $waybill_no = 0;

                $ctf = Ctf::select('id','ctf_no')->where('ctf_no',$ctf_no)->first();

                $packages = Ctf::leftjoin('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                    ->leftjoin('packages as p','p.id','=','cp.package_id')
                    ->select('package_no')
                    ->where('ctf_no',$request->ctf_no)
                    ->get();

                $waybills = Ctf::leftjoin('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                    ->leftjoin('packages as p','p.id','=','cp.package_id')
                    ->leftjoin('package_waybills as pw','pw.package_id','=','cp.package_id')
                    ->leftjoin('waybills as w','w.id','=','pw.waybill_id')
                    ->select('p.package_no','waybill_no')
                    ->where('ctf_no',$request->ctf_no)
                    ->get();

                    
                /*
                //Delete data
                Ctf::leftjoin('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                    ->leftjoin('packages as p','p.id','=','cp.package_id')
                    ->leftjoin('package_waybills as pw','pw.package_id','=','cp.package_id')
                    ->leftjoin('waybills as w','w.id','=','pw.waybill_id')
                    ->select('p.package_no','waybill_no')
                    ->delete(['ctf_no',$request->ctf_no]);
                */

                return view('admin.deleted-result',compact('ctf_no','package_no','waybill_no','ctf','packages','waybills','type','result'));
            }elseif($type == 'package'){
                $ctf_no = 0;
                $package_no = $request->package_no;

                // $packages = Ctf::leftjoin('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                //     ->leftjoin('packages as p','p.id','=','cp.package_id')
                //     ->select('package_no')
                //     ->where('ctf_no',$request->ctf_no)
                //     ->get();

                $waybills = PackageWaybill::leftjoin('waybills as w','w.id','=','package_waybills.waybill_id')
                    ->leftjoin('packages as p','p.id','=','package_waybills.package_id')
                    ->select('w.id','w.waybill_no')
                    ->where('p.package_no',$package_no)
                    ->get();

                    
                /*
                //Delete data
                Ctf::leftjoin('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                    ->leftjoin('packages as p','p.id','=','cp.package_id')
                    ->leftjoin('package_waybills as pw','pw.package_id','=','cp.package_id')
                    ->leftjoin('waybills as w','w.id','=','pw.waybill_id')
                    ->select('p.package_no','waybill_no')
                    ->delete(['ctf_no',$request->ctf_no]);
                */

                return view('admin.deleted-result',compact('ctf_no','package_no','waybills','type'));
            }else{
                $ctf_no     = 0;
                $package_no = 0;
                $waybill_no = $request->waybill_no;
                $result     = $request->waybill_no;

                $waybill = Waybill::select('id','waybill_no')->where('waybill_no',$waybill_no)->first();
                

                return view('admin.deleted-result',compact('ctf_no','package_no','waybill_no','waybill','type','result'));
            }
            
        }
    }

    public function sync_data(){

        return view('operator.sync-data.dashboard');
    }

    public function sync_endpoint(){
        $city_id    = user()->city_id;
        $date       = get_date();
        $json       = 'json/sync-data/endpoint';
        
        
        return view('operator.sync-data.waybill-endpoint',compact('json','city_id'));
    }

    public function json_sync_endpoint(){
        $city_id    = user()->city_id;
        $date       = get_date();

        $waybills = Waybill::join('users as u','u.id','=','waybills.user_id')
                ->join('branches as b','b.id','=','u.branch_id')
                ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                ->where('waybills.active',1)
                ->where('origin',$city_id)
                ->whereDate('waybills.created_at','like',$date.'%')
                ->orderBy('waybills.created_at')
                ->paginate(100);

        return $waybills;

    }

    public function print_package($package_no){
        $qty        = 0;
        $remove     = 0;

            $package = Package::join('package_types as pt','pt.id','=','packages.package_type_id')
                ->leftjoin('cities as c1','c1.id','=','packages.from_city_id')
                ->leftjoin('cities as c2','c2.id','=','packages.to_city_id')
                ->leftjoin('branches as b1','b1.id','=','packages.from_branch_id')
                ->leftjoin('branches as b2','b2.id','=','packages.to_branch_id')
                ->select('packages.id','packages.package_no','packages.remark','packages.created_at','pt.name as package_type','c1.shortcode as origin','c2.shortcode as destination','b1.name as from_branch','b2.name as to_branch')
                ->where('package_no',$package_no)
                ->first(); 

            $products = DB::table('packages')
                    ->join('package_waybills as pw','pw.package_id','=','packages.id')
                    ->join('waybills as w','w.id','=','pw.waybill_id')
                    ->select('pw.id')
                    ->where('packages.package_no',$package_no)
                    ->where('pw.active',1)
                    ->sum('w.qty');

            $waybills = Package::join('package_waybills as wp','wp.package_id','=','packages.id')
                    ->select('packages.package_no',DB::raw('wp.package_id , COUNT(*) as qty'))
                    ->where('packages.package_no',$package_no)
                    ->where('wp.active',1)
                    ->groupBy('wp.package_id')
                    ->first();

        return view('partials.print-pkg',compact('package','waybills','products'));
    }

    public function print_package_waybills($package_no){
        $qty        = 0;
        $remove     = 0;

            $package = Package::join('package_types as pt','pt.id','=','packages.package_type_id')
                ->leftjoin('cities as c1','c1.id','=','packages.from_city_id')
                ->leftjoin('cities as c2','c2.id','=','packages.to_city_id')
                ->leftjoin('branches as b1','b1.id','=','packages.from_branch_id')
                ->leftjoin('branches as b2','b2.id','=','packages.to_branch_id')
                ->select('packages.id','packages.package_no','.packages.remark','packages.created_at','pt.name as package_type','c1.shortcode as origin','c2.shortcode as destination','b1.name as from_branch','b2.name as to_branch')
                ->where('package_no',$package_no)
                ->first(); 

            $products = DB::table('packages')
                    ->join('package_waybills as pw','pw.package_id','=','packages.id')
                    ->join('waybills as w','w.id','=','pw.waybill_id')
                    ->select('pw.id')
                    ->where('packages.package_no',$package_no)
                    ->where('pw.active',1)
                    ->sum('w.qty');

            $waybills = Package::join('package_waybills as wp','wp.package_id','=','packages.id')
                    ->join('waybills as w','w.id','=','wp.waybill_id')
                    ->leftjoin('users as u','u.id','=','w.user_id')
                    ->select('w.waybill_no','u.name as courier','wp.created_at','w.qty')
                    ->where('packages.package_no',$package_no)
                    ->where('wp.active',1)
                    ->orderBy('w.waybill_no','asc')
                    ->get();
        //return $waybills;
        return view('partials.print-package-waybills',compact('package','waybills','products'));
    }

    public function unknown_outbound(){
        $json = 'json/unknown/outbound';
        
        return view('operator.main-city.waybills.outbound-unknown',compact('json'));
    }

    public function unknown_inbound(){
        $json = 'json/unknown/inbound';
        
        return view('operator.main-city.waybills.inbound-unknown',compact('json'));
    }

    public function json_unknown_outbound(){
        $city_id = user()->city_id;
        $date    = get_date();

        $waybills = Waybill::select('waybills.created_at','waybills.id','waybills.waybill_no','action_id','qty','waybills.end_point')
                ->where('waybills.user_id',0)
                ->where('waybills.active',1)
                ->where('origin',$city_id)
                ->whereDate('waybills.created_at','like',$date.'%')
                ->orderBy('waybills.created_at')
                ->paginate(100);

        return $waybills;
    }

    public function json_unknown_inbound(){
        $city_id = user()->city_id;
        $date    = get_date();

        $waybills = Waybill::select('waybills.created_at','c.shortcode as origin','waybills.id','waybills.waybill_no','action_id','qty','waybills.end_point')
                ->leftjoin('cities as c','c.id','=','waybills.origin')
                ->where('waybills.user_id',0)
                ->where('waybills.active',1)
                ->where('destination',$city_id)
                ->whereDate('waybills.created_at','like',$date.'%')
                ->orderBy('waybills.created_at')
                ->paginate(100);

        return $waybills;
    }

    public function download_failed_logs(){

        return view('partials.download-failed-logs');
    }
}
