<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\City;
use App\Branch;
use App\Waybill;
use App\ActionLog;
use App\Transit;
use App\BranchActionLog;
use App\Transferred;
use App\BranchTransferredLogs;
use App\TransferredLogs;
use App\Weight;
use App\Package;
use App\PackageType;
use App\PackageWaybill;
use App\RejectedWaybill;
use App\FailedWaybillLog;
use App\Ctf;
use App\CtfPackage;
use App\CtfLog;
use App\CtfGenerator;
use App\WaybillJob;
use App\Route;
use App\History;
use App\CodCity;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller{

	/** user login api **/
    public function login(Request $request){
    	$response = array();
    	$error    = array();

    	$username = $request->username.'@royalx.net';
    	$password = $request->password;

    	if($username || $password){
    		$temp = array();
    		$user = User::leftjoin('cities as c','c.id','=','users.city_id')
    			->leftjoin('branches as b','b.id','=','users.branch_id')
    			->select('users.id','users.name','users.email','users.role','users.branch_id','users.city_id','c.name as city_name','c.shortcode as city_shortcode','c.is_service_point','users.remark','users.active','b.name as branch_name','b.is_main_office as is_main_office','b.is_sorting as is_sorting')
    			->where('email',$username)
    			->where('mobile_password',$password)
    			->first();
    		
    		if($user){
    			if($user->active == 1){
    				$data['id'] 				= $user->id;
	    			$data['name'] 				= $user->name;
		    		$data['email'] 				= $user->email;
		    		$data['role_id'] 			= $user->role;
		    		$data['branch_id'] 			= $user->branch_id;
		    		$data['city_id'] 			= $user->city_id;
		    		$data['role'] 				= user_role($user->role);
		    		$data['branch_name'] 		= $user->branch_name;
		    		$data['city_name'] 			= $user->city_name;
		    		$data['city_shortcode'] 	= $user->city_shortcode;
		    		$data['is_sorting'] 		= $user->is_sorting;
		    		$data['is_main_office'] 	= $user->is_main_office;
		    		$data['is_service_point'] 	= $user->is_service_point;
		    		$data['remark'] 			= $user->remark;

		    		array_push($temp, $data);
		    		$response['success'] = 1;
		    		$response['data'] 	 = $temp;
    			}else{
					$response['success'] 	= 0;
    				$response['error']		= 'account is deactivated.';
    			}
    		}else{
    			$response['success'] 	= 0;
    			$response['error']		= 'user not found.';
    		}
    		
    	}else{
    		$response['success'] 	= 0;
    		$response['error']		= 'username and password is required.';
    	}

    	return response()->json($response);
    }

    /* outbound api collections */
    public function outbound(Request $request){
    	$response 	= array();
    	$passed   	= array();
    	$failed   	= array();
    	$batch_id   = batch_id();
    	
    	if($request->action_id == 1){
    		$raw 		= $request->waybills;
    		$username 	= $request->username;

    		foreach($raw as $key => $data){
    			$check = DB::table('waybills')->select('waybill_no')->where('origin',$request->user_city_id)->where('waybill_no',$data['waybill_no'])->first();
	            	
	            if(!$check){
	            	$id = DB::table('waybills')->insertGetId([
	            		'waybill_no'        => $data['waybill_no'],
				        'batch_id'        	=> $batch_id,
				        'origin'         	=> $request->user_city_id,
				        'action_id'   		=> 1,
				        'qty'    			=> $data['qty'],
				        'user_id'  			=> $request->courier_id,
				        'same_day'    		=> $request->same_day,
				        'active'    		=> 1,
				        'created_at'    	=> date('Y-m-d H:i:s'),
				        'updated_at'    	=> date('Y-m-d H:i:s'),
	            	]);

			        DB::table('action_logs')->insert([
				        'waybill_id'    => $id,
				        'action_id'     => 1,
				        'user_id'      	=> $request->user_id,
				        'branch_id'    	=> $request->user_branch_id,
				        'city_id'      	=> $request->user_city_id,
				        'log'  			=> $username.' သည် '.$request->courier_name.' ထံမှ စာကောက်ထားပါသည်။',
				    	'created_at'    => date('Y-m-d H:i:s'),
				        'updated_at'    => date('Y-m-d H:i:s'),
				    ]);

			        array_push($passed, $data['waybill_no']);
	            }else{
	            	array_push($failed, $data['waybill_no']);
	            }

	            $response['success'] 	= 1;
	            $response['passed'] 	= $passed;
            	$response['failed'] 	= $failed;
    		}
    	}elseif($request->action_id == 2){
    		$raw 		= $request->waybills;
    		$username 	= $request->username;
    		$count      = 0;

    		foreach($raw as $data){
    			$waybill = DB::table('waybills')->select('waybill_no','id')->where('origin',$request->user_city_id)->where('waybill_no',$data)->where('action_id',1)->first();
	            	
	            if($waybill){
	            	$count++;

	            	if($count == 1 && $request->package_id == 0){
	            		//generate package number
				    	$num = package_generate();

				    	$package_id = DB::table('packages')->insertGetId([
					        'package_no'    	=> $num,
							'user_id'     		=> $request->user_id,
							'from_branch_id' 	=> $request->user_branch_id,
							'from_city_id'  	=> $request->user_city_id,
							'to_branch_id'  	=> $request->to_branch_id,
							'to_city_id'    	=> $request->user_city_id,
							'action_id'    		=> 2,
							'completed'    		=> 0,
							'package_type_id' 	=> $request->pkg_type,  			
							'remark' 			=> $request->remark,
							'created_at'    	=> date('Y-m-d H:i:s'),
				        	'updated_at'    	=> date('Y-m-d H:i:s'),
					    ]);			
					}
					//updated waybills
					DB::table('waybills')->where('id',$waybill->id)->update(['action_id' => 2]);
			        
			        DB::table('package_waybills')->insert([
			        	'package_id' 	=> $request->package_id==0? $package_id:$request->package_id,
			        	'waybill_id' 	=> $waybill->id,
			        	'checked' 		=> 0,
			        	'created_at'    => date('Y-m-d H:i:s'),
				        'updated_at'    => date('Y-m-d H:i:s'),
					]);	

					if($request->package_id == 0){
				        $log  			= $username.' မှ အထုတ်အမှတ် '.$num.' ဖြင့် '.$request->to_branch_name.' သို့ပို့ရန် အထုပ်ထုပ်ထားပါသည်။';
				    }elseif($request->action=='save' || $request->action=='continue' ){
				        $log  			= $username.' မှ အထုတ်အမှတ် '.$request->package_no.' ဖြင့် '.$request->to_branch_name.' သို့ပို့ရန် အထုပ်ထုပ်ထားပါသည်။';
				    }else{
				        $log  			= $username.' မှ အထုပ်အမှတ် '.$request->package_no.' ထဲသို့စာထပ်ထည့်ထားပါသည်။';
				    }

				    //saved action logs
			        DB::table('action_logs')->insert([
					    'waybill_id'    => $waybill->id,
				        'action_id'     => 2,
				        'user_id'      	=> $request->user_id,
				        'branch_id'     => $request->user_branch_id,
				        'city_id'      	=> $request->user_city_id,
				        'log' 			=> $log,
						'created_at'    => date('Y-m-d H:i:s'),
				        'updated_at'    => date('Y-m-d H:i:s'),
					]);
	            }else{
	            	array_push($failed,$data);
	            }
    		}

    		if($count > 0){
    			$response['success'] 		= 1;
			    $response['package_id'] 	= $request->package_id == 0? $package_id:$request->package_id;
			    $response['package_no'] 	= $request->package_id == 0? $num:$request->package_no;
			    $response['option'] 		= $request->action;
			}else{
				$response['success'] 		= 0;
			    $response['package_id'] 	= $request->package_id;
			    $response['package_no'] 	= $request->package_no;
			    $response['option'] 		= $request->action;
			}

	        $response['failed'] 	= $failed;
    	}elseif($request->action_id == 3){
    		$response 	= array();
    		$raw      	= $request->packages;
    		$username 	= $request->username;
    		$count      = 0;

    		foreach($raw as $key => $data){
    			$package = DB::table('packages')->select('package_no','id','to_branch_id','to_city_id')->where('from_city_id',$request->user_city_id)->where('id',$data)->where('completed',0)->first();
	            	
	            if($package){
	            	$count++;

	            	if($count == 1){
	            		//generate ctf number
				    	$ctf_no		= ctf_generate('normal');

				    	//created ctf for package
				    	$ctf_id = DB::table('ctfs')->insertGetId([
					        'ctf_no'    		=> $ctf_no,
							'user_id'     		=> $request->user_id,
							'branch_id' 		=> $request->user_branch_id,
							'city_id'  			=> $request->user_city_id,
							'to_branch_id'  	=> $package->to_branch_id,
							'to_city_id'    	=> $package->to_city_id,
							'action_id'    		=> 3,
							'completed'    		=> 0, 			
							'created_at'    	=> date('Y-m-d H:i:s'),
				        	'updated_at'    	=> date('Y-m-d H:i:s'),
					    ]);
					}

					//(need to change with job table)
					$waybills = DB::table('package_waybills')->select('waybill_id')->where('package_id',$data)->get();

					//saved logs for created ctf (need to change with job table)
		            foreach($waybills as $waybill){
		            	DB::table('waybills')->where('id',$waybill->waybill_id)->update(['action_id' => 3]);

		            	DB::table('action_logs')->insert([
							'waybill_id'    => $waybill->waybill_id,
						    'action_id'     => 3,
						    'user_id'      	=> $request->user_id,
						    'branch_id'     => $request->user_branch_id,
						    'city_id'      	=> $request->user_city_id,
						    'log' 			=> $username.' မှ ၎င်းစာပါဝင်သော အထုပ်အား  အမှတ်စဉ် '.$ctf_no.' ဖြင့် စာရင်းသွင်းထားပါသည်။',
							'created_at'    => date('Y-m-d H:i:s'),
						    'updated_at'    => date('Y-m-d H:i:s'),
						]);
					}

					//completed package,package_waybills,waybills
					DB::table('packages')->where('id',$data)->update(['completed'=>1]);

					//PackageWaybill::where('package_id',$data)->update(['completed'=>1]);
					//DB::table('waybills')->join('package_waybills as pw','pw.waybill_id','=','waybills.id')->where('pw.package_id',$data)->update(['action_id'=>9]);
		    		
		    		//created ctf_package
		    		DB::table('ctf_packages')->insert([
					    'ctf_id'    	=> $ctf_id,
						'package_id'    => $data,			
						'created_at'    => date('Y-m-d H:i:s'),
				        'updated_at'    => date('Y-m-d H:i:s'),
					]);
	            }    
    		}
            if($count > 0){
				$response['success'] 	= 1;
            	$response['ctf_no'] 	= $ctf_no;
			}else{
				$response['success'] 	= 0;
            	$response['ctf_no'] 	= null;
			}
        }elseif($request->action_id == 4){
    		$response 	= array();
    		$raw 		= $request->waybills;
    		$username 	= $request->username;
    		$is_sorting = is_sorting($request->user_branch_id);
    		
    		if($request->user_branch_id == 11){
    			$params 	= 'YGN-Hub';
    		}elseif($request->user_branch_id == 14){
    			$params 	= 'MDY-Hub';
    		}else{
    			$params 	= main_office($request->user_city_id);
    		}

	    	//check scan type (with package_id)
	    	if($request->package_id == 0){
	    		//scan without package
	    		foreach($raw as $key => $data){
					$waybill 	= DB::table('waybills')->select('id','action_id')->where('waybill_no',$data)->where('origin',$request->user_city_id)->where('action_id','<=',3)->first();

	    			//direct branch in from shorting (eg.shop.com,etc...)
	    			$new 		= DB::table('waybills')->select('id')->where('waybill_no',$data)->where('origin',$request->user_city_id)->first();

	    			if($waybill){
	    				DB::table('waybills')->where('id',$waybill->id)->update([
					        'action_id'   		=> $is_sorting == 1? 4:10,
					        'destination'    	=> $is_sorting == 1? NULL:$request->user_city_id,
					    ]);

		            	DB::table('package_waybills')->where('waybill_id',$waybill->id)->update(['completed'=>1]);

		            	if($waybill->action_id < 3){
		            		$log = $username.' မှ စာလက်ခံ လုပ်ဆောင်ထားပါသည်။(တစ်ဖက်ရုံးမှ အဆင့်ကျော်ခဲ့သည်။)';
		            	}else{
		            		$log = $username.' မှ စာလက်ခံ လုပ်ဆောင်ထားပါသည်။';
		            	}

						DB::table('action_logs')->insert([
						    'waybill_id'    => $waybill->id,
					        'action_id'     => $is_sorting == 1? 4:10,
					        'user_id'      	=> $request->user_id,
					        'branch_id'     => $request->user_branch_id,
					        'city_id'      	=> $request->user_city_id,
					        'log' 			=> $log,
							'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);

						//created waybill job
						DB::table('waybill_jobs')->insert([
						    'waybill_id'    => $waybill->id,
					        'status'     	=> 'branch-in',
					        'params'      	=> $params,
					        'user_id'     	=> $request->user_id,
					        'city_id'      	=> $request->user_city_id,
							'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);
	    				
	    				array_push($passed,$data);
	    			}elseif(!$new){
	    				//additional param for shop
	    				$id = DB::table('waybills')->insertGetId([
					        'waybill_no'        => $request->shop==1? $data['waybill_no']:$data,
				        	'origin'         	=> $request->user_city_id,
				        	'action_id'    		=> 4,
				        	'qty'    			=> 1,
				        	'user_id'  			=> $request->shop==1? $request->delivery_id:0,
				        	'same_day'    		=> 0,
				        	'active'    		=> 1,
				        	'created_at'    	=> date('Y-m-d H:i:s'),
					        'updated_at'    	=> date('Y-m-d H:i:s'),
		            	]);

	    				if($request->shop==1){
			            	DB::table('weights')->insert([
						        'waybill_id' 	=> $id,
						        'weight' 		=> $data['weight'],
					        	'created_at'    => date('Y-m-d H:i:s'),
						        'updated_at'    => date('Y-m-d H:i:s'),
			            	]);
			            }      

			            if($request->shop==1){
					        $log  = $username.' သည် '.$request->delivery_name.' ထံမှ စာကောက်ထားပါသည်။';
					    }else{
					        $log  = $username.' မှ(စတင်ပြီး) စာလက်ခံ လုပ်ဆောင်ထားပါသည်။';
					    }

			            DB::table('action_logs')->insert([
						    'waybill_id'    => $id,
					        'action_id'     => 4,
					        'user_id'      	=> $request->user_id,
					        'branch_id'     => $request->user_branch_id,
					        'city_id'      	=> $request->user_city_id,
					        'log' 			=> $log,
							'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);

						DB::table('waybill_jobs')->insert([
						    'waybill_id' 	=> $id,
						    'status'  		=> 'branch-in',
							'params'  		=> $params,
							'user_id'  		=> $request->user_id,
							'city_id'  		=> $request->user_city_id,
					        'created_at'    => date('Y-m-d H:i:s'),
						    'updated_at'    => date('Y-m-d H:i:s'),
			            ]);


					    if($request->shop==1){
	    					array_push($passed,$data['waybill_no']);
	    				}else{
	    					array_push($passed,$data);
	    				}
	    			}else{
	    				//saved failed logs
						$log_date 	= date('Y-m-d H:i:s');

						if($request->shop == 1){
							$log_no 	= $data['waybill_no'].','.$request->username;
							array_push($failed,$data['waybill_no']);
						}else{
							$log_no 	= $data.','.$request->username;
							array_push($failed,$data);
						}
			            
			            $log_txt    = $log_date.',-,'.$log_no;
						saved_outbound_received_failed($log_txt);
	    			}
	    		}

	    		$response['success'] 	= 1;
	    		$response['passed'] 	= $passed;
	    		$response['failed'] 	= $failed;
	    		
	    	}else{
	    		//scan with package
	    		$waybill = DB::table('waybills')->select('id','waybill_no')->where('waybill_no',$request->waybills)->where('action_id',3)->first();
	    		
	    		//direct branch in from shorting (eg.shop.com,unknown etc...)
	    		$package = DB::table('packages')->select('package_no')->where('id',$request->package_id)->first();
	    		
	    		if($package){
	    			$package_no = $package->package_no;
	    		}else{
	    			$package_no = 0;
	    		}

	    		if($waybill){
		    		if($is_sorting == 1){
		    			DB::table('waybills')->where('id',$waybill->id)->update(['action_id' => 4]);
		    		}else{
		    			DB::table('waybills')->where('id',$waybill->id)->update(['destination' => $request->user_city_id,'action_id' => 10]);
			    	}
		    		
		    		DB::table('package_waybills')->where('waybill_id',$waybill->id)->where('package_id',$request->package_id)->update(['completed'=>1,'checked'=>1]);

		    		//saved action_logs 
		    		DB::table('action_logs')->insert([
						'waybill_id'    => $waybill->id,
						'action_id'     => $is_sorting == 1? 4:10,
						'user_id'      	=> $request->user_id,
						'branch_id'     => $request->user_branch_id,
						'city_id'      	=> $request->user_city_id,
						'log' 			=> $username.' မှ စာဝင်လက်ခံ လုပ်ဆောင်ထားပါသည်။',
						'created_at'    => date('Y-m-d H:i:s'),
						'updated_at'    => date('Y-m-d H:i:s'),
					]);

						//updated also ctf_packages status
						// $count = PackageWaybill::where('package_id',$request->package_id)->where('completed',0)->count();
						// if($count < 1){
						// 	CtfPackage::where('package_id',$request->package_id)->update(['completed'=>1]);
						// }

					//created waybill job
					DB::table('waybill_jobs')->insert([
						'waybill_id'    => $waybill->id,
						'status'     	=> 'branch-in',
						'params'      	=> $params,
						'user_id'     	=> $request->user_id,
						'city_id'      	=> $request->user_city_id,
						'created_at'    => date('Y-m-d H:i:s'),
						'updated_at'    => date('Y-m-d H:i:s'),
					]);

	    			$response['success'] 	= 1;
	    			$response['item'] 		= $waybill->id;
	    			
	    		}else{
		    		//failed logs
		    		$log_date 	= date('Y-m-d H:i:s');
					$log_no 	= $request->waybills.','.$request->username;
					$log_txt    = $log_date.','.$package_no.','.$log_no; //36
					saved_outbound_received_failed($log_txt);

		    		$response['success'] 	= 0;
		    		$response['item'] 		= 0;
	    		}
	    	}
	    	
           	return response()->json($response);
        }elseif($request->action_id == 5){
    		$raw 			= $request->waybills;
    		$username 		= $request->username;
    		$service_point 	= check_service_point($request->user_city_id);
    		$count      	= 0;

    		foreach($raw as $key => $data){
    			if($service_point == 1){
    				$waybill 	= DB::table('waybills')->select('waybill_no','id')->where('waybill_no',$data)->where('action_id',1)->first();
    				$transit_1 	= DB::table('waybills')->select('waybill_no','id','destination')->where('waybill_no',$data)->where('transit_1',NULL)->where('action_id',10)->first();
    			}else{
    				$waybill 	= DB::table('waybills')->select('waybill_no','id')->where('waybill_no',$data)->where('action_id',4)->first();
    				$transit_1 	= DB::table('waybills')->select('waybill_no','id','destination')->where('waybill_no',$data)->where('transit_1',NULL)->where('action_id',7)->first();
    			}

	            if($waybill){
	            	$count++;
	            	if($count == 1 && $request->package_id == 0){
	            		//generate package number
				    	$num = package_generate();

				    	$package_id = DB::table('packages')->insertGetId([
					        'package_no'    	=> $num,
							'user_id'     		=> $request->user_id,
							'from_branch_id' 	=> $request->user_branch_id,
							'from_city_id'  	=> $request->user_city_id,
							'to_city_id'    	=> $request->to_city_id,
							'action_id'    		=> 5,
							'completed'    		=> 0,
							'package_type_id' 	=> $request->pkg_type,  			
							'remark' 			=> $request->remark,
							'created_at'    	=> date('Y-m-d H:i:s'),
				        	'updated_at'    	=> date('Y-m-d H:i:s'),
					    ]);		
					}else{
						$old_pkg    = DB::table('packages')->where('id',$request->package_id)->select('id','to_city_id')->first();
					}
					
					DB::table('waybills')->where('id',$waybill->id)->update(['action_id' => 5,'destination' => $request->package_id==0? $request->to_city_id:$old_pkg->to_city_id]);

			        DB::table('package_waybills')->insert([
			        	'package_id' 	=> $request->package_id==0? $package_id:$request->package_id,
			        	'waybill_id' 	=> $waybill->id,
			        	'checked' 		=> 0,
			        	'created_at'    => date('Y-m-d H:i:s'),
				        'updated_at'    => date('Y-m-d H:i:s'),
					]);	

					if($request->package_id==0){
				        $log  	= $username.' မှ အထုတ်အမှတ် '.$num.' ဖြင့် '.$request->to_city_name.' သို့ပို့ရန် အထုပ်ထုပ်ထားပါသည်။';
				    }else{
				        $log  	= $username.' မှ အထုပ်အမှတ် '.$request->package_no.' ထဲသို့စာထပ်ထည့်ထားပါသည်။';
				    }

			        DB::table('action_logs')->insert([
						'waybill_id'    => $waybill->id,
					    'action_id'     => 5,
					    'user_id'      	=> $request->user_id,
					    'branch_id'     => $request->user_branch_id,
					    'city_id'      	=> $request->user_city_id,
					    'log' 			=> $log,
						'created_at'    => date('Y-m-d H:i:s'),
					    'updated_at'    => date('Y-m-d H:i:s'),
					]);
	            }elseif($transit_1){
	            	$count++;

	            	if($count == 1 && $request->package_id == 0){
	            		//generate package number
				    	$num = package_generate();

						$package_id = DB::table('packages')->insertGetId([
					        'package_no'    	=> $num,
							'user_id'     		=> $request->user_id,
							'from_branch_id' 	=> $request->user_branch_id,
							'from_city_id'  	=> $request->user_city_id,
							'to_city_id'    	=> $request->to_city_id,
							'action_id'    		=> 5,
							'completed'    		=> 0,
							'package_type_id' 	=> $request->pkg_type,  			
							'remark' 			=> $request->remark,
							'created_at'    	=> date('Y-m-d H:i:s'),
				        	'updated_at'    	=> date('Y-m-d H:i:s'),
					    ]);		
					}else{
						$old_pkg    = DB::table('packages')->where('id',$request->package_id)->select('id','to_city_id')->first();
					}

			        DB::table('waybills')->where('waybill_no',$data)->update([
					   	'transit_1'   => $transit_1->destination,
				       	'destination' => $request->to_city_id,
				        'action_id'   => 5,
		            ]);

		            DB::table('package_waybills')->insert([
			        	'package_id' 	=> $request->package_id==0? $package_id:$request->package_id,
			        	'waybill_id' 	=> $transit_1->id,
			        	'checked' 		=> 0,
			        	'created_at'    => date('Y-m-d H:i:s'),
				        'updated_at'    => date('Y-m-d H:i:s'),
					]);	

					if($request->package_id == 0){
						$log  = $username.' မှ '.$request->to_city_name.' သို့တဆင့်ပို့ရန်  အထုပ်ထုပ်ထားပါသည်။';
					}else{
						$log  = $username.' မှ အထုပ်အမှတ် '.$request->package_no.' ထဲသို့စာထပ်ထည့်ထားပါသည်။';
					}

					//saved action logs (need to change job table)
					DB::table('action_logs')->insert([
					    'waybill_id'    => $transit_1->id,
				        'action_id'     => 5,
				        'user_id'      	=> $request->user_id,
				        'branch_id'     => $request->user_branch_id,
				        'city_id'      	=> $request->user_city_id,
				        'log' 			=> $log,
						'created_at'    => date('Y-m-d H:i:s'),
				        'updated_at'    => date('Y-m-d H:i:s'),
					]);			    
	            }else{
	            	//saved failed logs
					$log_date 	= date('Y-m-d H:i:s');
			        $log_no 	= $data.','.$request->user_id;
			        $log_txt    = $log_date.','.$request->to_city_id.','.$log_no; //36
					saved_outbound_branch_out_failed($log_txt);

	            	//return waybill_no for failed 
	            	if($request->action == 'save' || $request->action == 'continue'){
	            		array_push($failed,$data);
	            	}else{
	            		array_push($failed,$data);
	            		//array_push($failed,$data['waybill_no']);
	            	}
	            }

	            //saved data from scan form
	            if($request->user_city_id == 102){
	            	$log_date 	= date('Y-m-d H:i:s');
	            	if($count > 0 ){
				    	$log_no 	= $data.','.($request->package_id == 0? $num:$request->package_no).','.$request->username;
				    }else{
				    	$log_no 	= $data.','.$request->package_no.','.$request->username;
				    }
				    $log_txt    = $log_date.','.$request->to_city_id.','.$log_no; //36
					saved_scan_branch_out($log_txt);
	            }
	            
    		}

			if($count > 0){
				$response['success'] 		= 1;
			    $response['package_id'] 	= $request->package_id == 0? $package_id:$request->package_id;
			    $response['package_no'] 	= $request->package_id == 0? $num:$request->package_no;
			    $response['option'] 		= $request->action;
			}else{
				$response['success'] 		= 0;
			    $response['package_id'] 	= $request->package_id;
			    $response['package_no'] 	= $request->package_no;
			    $response['option'] 		= $request->action;
			}

	        $response['failed'] 	= $failed;
    	}elseif($request->action_id == 6){
    		$response 	= array();
    		$raw      	= $request->packages;
    		$username 	= $request->username;
    		$count      = 0;

    		foreach($raw as $key => $data){
    			$package = DB::table('packages')->select('package_no','id','package_type_id','to_branch_id','to_city_id')->where('id',$data)->where('completed',0)->first();
	            	
	            if($package){
	            	$count++;

	            	if($count == 1){
	            		//generate ctf number
	            		if($package->package_type_id == 3){
	            			$ctf_no		= ctf_generate('rejected');
	            		}else{
	            			$ctf_no		= ctf_generate('normal');
	            		}

				    	//created ctf for package
				    	$ctf_id = DB::table('ctfs')->insertGetId([
					        'ctf_no'    		=> $ctf_no,
							'user_id'     		=> $request->user_id,
							'branch_id' 		=> $request->user_branch_id,
							'city_id'  			=> $request->user_city_id,
							'to_branch_id'  	=> $package->to_branch_id,
							'to_city_id'    	=> $package->to_city_id,
							'action_id'    		=> 6,
							'rejected'    		=> $package->package_type_id == 3? 1:0,
							'completed'    		=> 0, 			
							'created_at'    	=> date('Y-m-d H:i:s'),
				        	'updated_at'    	=> date('Y-m-d H:i:s'),
					    ]);
					}

					//completed package,package_waybills,waybills
					DB::table('packages')->where('id',$data)->update(['completed'=>1]);

					//PackageWaybill::where('package_id',$data)->update(['completed'=>1]);
					//Waybill::join('package_waybills as pw','pw.waybill_id','=','waybills.id')->where('pw.package_id',$data)->update(['action_id'=>6]);
	    			

		    		

					$waybills = DB::table('package_waybills')->select('waybill_id')->where('package_id',$data)->get();

					if($package->package_type_id == 3){
				    	$log  			= $username.' မှ ၎င်းစာပါဝင်သော အထုပ်အား  အမှတ်စဉ် '.$ctf_no.'(Rejected) ဖြင့် စာရင်းသွင်းထားပါသည်။';
				    }else{
				    	$log  			= $username.' မှ ၎င်းစာပါဝင်သော အထုပ်အား  အမှတ်စဉ် '.$ctf_no.' ဖြင့် စာရင်းသွင်းထားပါသည်။';
				    }
					//saved logs for created package
	            	foreach($waybills as $waybill){
	            		DB::table('waybills')->where('id',$waybill->waybill_id)->update(['action_id' => 6]);

		            	DB::table('action_logs')->insert([
							'waybill_id'    => $waybill->waybill_id,
						    'action_id'     => 6,
						    'user_id'      	=> $request->user_id,
						    'branch_id'     => $request->user_branch_id,
						    'city_id'      	=> $request->user_city_id,
						    'log' 			=> $log,
							'created_at'    => date('Y-m-d H:i:s'),
						    'updated_at'    => date('Y-m-d H:i:s'),
						]);
					}

					//created ctf_package
		    		DB::table('ctf_packages')->insert([
					    'ctf_id'    	=> $ctf_id,
						'package_id'    => $data,			
						'created_at'    => date('Y-m-d H:i:s'),
				        'updated_at'    => date('Y-m-d H:i:s'),
					]);
	            }    
    		}
            if($count > 0){
				$response['success'] 	= 1;
            	$response['ctf_no'] 	= $ctf_no;
			}else{
				$response['success'] 	= 0;
            	$response['ctf_no'] 	= null;
			}
        }elseif($request->action_id == 0){
    		$raw 		= $request->waybills;
    		$username 	= $request->username;

    		foreach($raw as $key => $data){
    			$check = Waybill::select('waybill_no','id')->where('waybill_no',$data['waybill_no'])->first();
	            	
	            if(!$check){
	            	$new                    = new Waybill();
			        $new->waybill_no        = $data['waybill_no'];
			        $new->batch_id        	= $batch_id;
			        $new->origin         	= $request->user_city_id;
			        $new->action_id    		= 4;
			        $new->qty    			= $data['qty'];
			        $new->user_id  			= $request->courier_id; //delivery man or counter
			        $new->same_day    		= $request->same_day;
			        $new->active    		= 1;

			        if($new->save()){
			            //saved logs for collected action (action_id = 1)
			            $action                 = new ActionLog();
				        $action->waybill_id    	= $new->id;
				        $action->action_id      = 4;
				        $action->user_id      	= $request->user_id;
				        $action->branch_id     	= $request->user_branch_id;
				        $action->city_id      	= $request->user_city_id;
				        $action->log  			= $username.' သည် '.$request->courier_name.' ထံမှ စာကောက်ထားပါသည်။';
				        $action->save();

				        array_push($passed, $data['waybill_no']);
				    }
	            }else{
	            	//return waybill_no for failed 
	            	array_push($failed, $data['waybill_no']);
	            }

	            $response['success'] 	= 1;
	            $response['passed'] 	= $passed;
            	$response['failed'] 	= $failed;
    		}
    	}else{
    		$response = 'action_id is ('.$request->action_id.') invalid.';
    	}

    	return response()->json($response);
    }

    /* inbound api collections */
    public function inbound(Request $request){
    	$response 	= array();
    	$passed   	= array();
    	$failed   	= array();
    	
    	if($request->action_id == 7){
    		$response 	= array();
    		$raw 		= $request->waybills;
    		$username 	= $request->username;
    		$batch_id   = batch_id();

    		if($request->user_branch_id == 11){
    			$params 	= 'YGN-Hub';
    			$action_id  = 7;
    		}elseif($request->user_branch_id == 14){
    			$params 	= 'MDY-Hub';
    			$action_id  = 7;
    		}else{
    			$params 	= main_office($request->user_city_id);
    			$action_id  = 10;
    		}

	    	//check scan type (without package)
	    	if($request->package_id == 0){
	    		foreach($raw as $key => $data){
	    			$waybill 	= DB::table('waybills')->select('waybill_no','id')->where('waybill_no',$data)->where('action_id','<=',6)->first();
	    			$check 		= DB::table('waybills')->select('waybill_no','id')->where('waybill_no',$data)->first();

	    			if($waybill){
	    				//updated waybills
	    				DB::table('waybills')->where('id',$waybill->id)->update(['action_id' => $action_id]);	

	    				//updated package_waybills status(completed = 1)
						DB::table('package_waybills')->where('waybill_id',$waybill->id)->update(['completed'=>1]);

						//saved logs for branch-in waybill
						DB::table('action_logs')->insert([
						    'waybill_id'    => $waybill->id,
					        'action_id'     => $action_id,
					        'user_id'      	=> $request->user_id,
					        'branch_id'     => $request->user_branch_id,
					        'city_id'      	=> $request->user_city_id,
					        'log' 			=> $username.' မှ '.$request->from_city_name.' မြို့စာဝင် (စာဖြင့်)လက်ခံရရှိထားပါသည်။',
							'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);

						//created waybill job
						DB::table('waybill_jobs')->insert([
						    'waybill_id'    => $waybill->id,
					        'status'     	=> 'branch-in',
					        'params'      	=> $params,
					        'user_id'     	=> $request->user_id,
					        'city_id'      	=> $request->user_city_id,
							'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);
							
	    				array_push($passed,$data);
	    			}elseif(!$check){
	    				//saved new waybill
	    				$id = DB::table('waybills')->insertGetId([
		            		'waybill_no'        => $data,
					        'origin'         	=> $request->from_city_id,
					        'destination'    	=> $request->user_city_id,
					        'action_id'    		=> $action_id,
					        'user_id'  			=> 0,
					        'active'    		=> 1,
					        'qty'    			=> 1,
					        'created_at'    	=> date('Y-m-d H:i:s'),
					        'updated_at'    	=> date('Y-m-d H:i:s'),
		            	]);

		            	//saved logs for branch-in waybill
						DB::table('action_logs')->insert([
						    'waybill_id'    => $id,
					        'action_id'     => $action_id,
					        'user_id'      	=> $request->user_id,
					        'branch_id'     => $request->user_branch_id,
					        'city_id'      	=> $request->user_city_id,
					        'log' 			=> $username.' မှ '.$request->from_city_name.' မြို့စာဝင် (စာဖြင့်)လက်ခံရရှိထားပါသည်။',
							'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);

						//created waybill job
						DB::table('waybill_jobs')->insert([
						    'waybill_id'    => $id,
					        'status'     	=> 'branch-in',
					        'params'      	=> $params,
					        'user_id'     	=> $request->user_id,
					        'city_id'      	=> $request->user_city_id,
							'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);

						array_push($passed,$data);
	    			}else{
	    				//saved failed logs
						$log_date 	= date('Y-m-d H:i:s');
			            $log_no 	= $data.','.$request->username;
			            $log_txt    = $log_date.','.$request->from_city_id.','.$log_no; //36
						saved_inbound_branch_in_failed($log_txt);

	    				array_push($failed,$data);
	    			}
	    		}

	    		$response['success'] 	= 0;
	    		$response['passed'] 	= $passed;
	    		$response['failed'] 	= $failed;
	    	}else{
	    		//check scan type (with package)
	    		$waybill = DB::table('waybills')->select('id','waybill_no')->where('waybill_no',$request->waybills)->where('action_id',6)->first();

	    		if($waybill){
	    			//updated waybill
	    			DB::table('waybills')->where('id',$waybill->id)->update(['action_id' => $action_id]);

	    			//updated package waybills
		            DB::table('package_waybills')->where('waybill_id',$waybill->id)->update(['completed'=>1]);

					DB::table('action_logs')->insert([
						'waybill_id'    => $waybill->id,
					    'action_id'     => $action_id,
					    'user_id'      	=> $request->user_id,
					    'branch_id'     => $request->user_branch_id,
					    'city_id'      	=> $request->user_city_id,
					    'log' 			=> $username.' မှ မြို့စာဝင် (အထုပ်ဖြင့်)လက်ခံရရှိထားပါသည်။',
						'created_at'    => date('Y-m-d H:i:s'),
					    'updated_at'    => date('Y-m-d H:i:s'),
					]);

					//created waybill job
					DB::table('waybill_jobs')->insert([
						'waybill_id'    => $waybill->id,
					    'status'     	=> 'branch-in',
					    'params'      	=> $params,
					    'user_id'     	=> $request->user_id,
					    'city_id'      	=> $request->user_city_id,
						'created_at'    => date('Y-m-d H:i:s'),
					    'updated_at'    => date('Y-m-d H:i:s'),
					]);
	    			
	    			$response['success'] 	= 1;
	    			$response['item'] 		= $waybill->id;
	    		}else{
	    			//saved failed logs
					$log_date 	= date('Y-m-d H:i:s');
			        $log_no 	= $request->waybills.','.$request->username;
			        $log_txt    = $log_date.',-,'.$log_no; //36
					saved_inbound_branch_in_failed($log_txt);

	    			$response['success'] 	= 0;
	    			$response['item'] 		= 0;
	    		}
	    	}
            
           	return response()->json($response);
    	}elseif($request->action_id == 8){
    		$raw 		= $request->waybills;
    		$username 	= $request->username;
    		$count      = 0;

    		foreach($raw as $key => $data){
    			$waybill 		= DB::table('waybills')->select('waybill_no','id','origin','action_id')->where('waybill_no',$data)->where('action_id','>=',4)->first();
	            	
	            if($waybill){
	            	if($waybill->action_id == 4 || $waybill->action_id == 7 || $waybill->action_id == 10){
		            	$count++;

		            	if($count == 1 && $request->package_id == 0){
		            		if($request->pkg_type == 5 || $request->pkg_type == 7){
		            			$num = str_replace('PG','WL',package_generate());
		            		}elseif($request->pkg_type == 8){
		            			$num = str_replace('PG','MO',package_generate());
		            		}elseif($request->pkg_type == 9){
		            			$num = str_replace('PG','ME',package_generate());
		            		}else{
		            			$num = package_generate();
		            		}
		            		
		            		$package_id = DB::table('packages')->insertGetId([
						        'package_no'    	=> $num,
								'user_id'     		=> $request->user_id,
								'from_branch_id' 	=> $request->user_branch_id,
								'from_city_id'  	=> $request->user_city_id,
								'to_branch_id'  	=> $request->to_branch_id,
								'to_city_id'    	=> $request->user_city_id,
								'action_id'    		=> 8,
								'completed'    		=> 0,
								'package_type_id' 	=> $request->pkg_type,  			
								'remark' 			=> $request->remark,
								'created_at'    	=> date('Y-m-d H:i:s'),
					        	'updated_at'    	=> date('Y-m-d H:i:s'),
						    ]);	
						}
					}

					//updated waybill
					if($waybill->action_id == 7 || $waybill->action_id == 10 || $waybill->action_id == 4){
						//branch-in or transferred waybill
						DB::table('waybills')->where('id',$waybill->id)->update(['action_id' => 8]);

						DB::table('package_waybills')->insert([
				        	'package_id' 	=> $request->package_id==0? $package_id:$request->package_id,
				        	'waybill_id' 	=> $waybill->id,
				        	'checked' 		=> 0,
				        	'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);

						if($request->package_id == 0){
					        $log  			= $username.' မှ အထုတ်အမှတ် '.$num.' ဖြင့် '.$request->to_branch_name.' သို့ပို့ရန် အထုပ်ထုပ်ထားပါသည်။';
					    }elseif($request->action=='save' || $request->action=='continue' ){
					        $log  			= $username.' မှ အထုတ်အမှတ် '.$request->package_no.' ဖြင့် '.$request->to_branch_name.' သို့ပို့ရန် အထုပ်ထုပ်ထားပါသည်။';
					    }else{
					        $log  			= $username.' မှ အထုပ်အမှတ် '.$request->package_no.' ထဲသို့စာထပ်ထည့်ထားပါသည်။';
					    }

				        DB::table('action_logs')->insert([
						    'waybill_id'    => $waybill->id,
					        'action_id'     => 8,
					        'user_id'      	=> $request->user_id,
					        'branch_id'     => $request->user_branch_id,
					        'city_id'      	=> $request->user_city_id,
					        'log' 			=> $log,
							'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);
					}else{
						//return waybill_no for failed 
	            		array_push($failed, $data);
					}
	            }else{
	            	//saved failed logs
					$log_date 	= date('Y-m-d H:i:s');
		            $log_no 	= $data.','.$request->username;
		            $log_txt    = $log_date.','.$request->to_branch_name.','.$log_no; //36
					saved_inbound_handover_failed($log_txt);

	            	//return waybill_no for failed 
	            	array_push($failed, $data);
	            }

	            //saved data from scan form
	            if($request->user_city_id == 102){
	            	$log_date 	= date('Y-m-d H:i:s');
	            	if($count > 0 ){
				    	$log_no 	= $data.','.($request->package_id == 0? $num:$request->package_no).','.$request->username;
				    }else{
				    	$log_no 	= $data.','.$request->package_no.','.$request->username;
				    }
				    $log_txt    = $log_date.','.$request->to_branch_id.','.$log_no; //36
					saved_scan_inbound_handover($log_txt);
	            }
    		}

			if($count > 0){
				$response['success'] 		= 1;
			    $response['package_id'] 	= $request->package_id == 0? $package_id:$request->package_id;
			    $response['package_no'] 	= $request->package_id == 0? $num:$request->package_no;
			    $response['option'] 		= $request->action;
			}else{
				$response['success'] 		= 0;
			    $response['package_id'] 	= $request->package_id;
			    $response['package_no'] 	= $request->package_no;
			    $response['option'] 		= $request->action;
			}

	        $response['failed'] 	= $failed;
    	}elseif($request->action_id == 9){
    		$response 	= array();
    		$raw      	= $request->packages;
    		$username 	= $request->username;
    		$count      = 0;

    		foreach($raw as $key => $data){
    			$package = DB::table('packages')->select('package_no','id','package_type_id','to_branch_id','to_city_id')->where('id',$data)->where('completed',0)->first();
	            	
	            if($package){
	            	$count++;

	            	if($count == 1){
	            		if($package->package_type_id == 3){
	            			$ctf_no		= ctf_generate('rejected');
	            		}else{
	            			$ctf_no		= ctf_generate('normal');
	            		}

				    	//created ctf for package
				    	$ctf_id = DB::table('ctfs')->insertGetId([
					        'ctf_no'    		=> $ctf_no,
							'user_id'     		=> $request->user_id,
							'branch_id' 		=> $request->user_branch_id,
							'city_id'  			=> $request->user_city_id,
							'to_branch_id'  	=> $package->to_branch_id,
							'to_city_id'    	=> $package->to_city_id,
							'action_id'    		=> 9,
							'completed'    		=> 0, 			
							'created_at'    	=> date('Y-m-d H:i:s'),
				        	'updated_at'    	=> date('Y-m-d H:i:s'),
					    ]);
					}

					$waybills = DB::table('package_waybills')->select('waybill_id')->where('package_id',$data)->get();

					//saved logs for created ctf
		            foreach($waybills as $waybill){
		            	DB::table('action_logs')->insert([
							'waybill_id'    => $waybill->waybill_id,
						    'action_id'     => 9,
						    'user_id'      	=> $request->user_id,
						    'branch_id'     => $request->user_branch_id,
						    'city_id'      	=> $request->user_city_id,
						    'log' 			=> $username.' မှ ၎င်းစာပါဝင်သော အထုပ်အား  အမှတ်စဉ် '.$ctf_no.' ဖြင့် စာရင်းသွင်းထားပါသည်။',
							'created_at'    => date('Y-m-d H:i:s'),
						    'updated_at'    => date('Y-m-d H:i:s'),
						]);
					}

					//completed package,package_waybills,waybills
					DB::table('packages')->where('id',$data)->update(['completed'=>1]);

					//PackageWaybill::where('package_id',$data)->update(['completed'=>1]);
					DB::table('waybills')->join('package_waybills as pw','pw.waybill_id','=','waybills.id')->where('pw.package_id',$data)->update(['action_id'=>9]);
		    		
		    		//created ctf_package
		    		DB::table('ctf_packages')->insert([
					    'ctf_id'    	=> $ctf_id,
						'package_id'    => $data,			
						'created_at'    => date('Y-m-d H:i:s'),
				        'updated_at'    => date('Y-m-d H:i:s'),
					]);
	            }    
    		}
            if($count > 0){
				$response['success'] 	= 1;
            	$response['ctf_no'] 	= $ctf_no;
			}else{
				$response['success'] 	= 0;
            	$response['ctf_no'] 	= null;
			}

        }elseif($request->action_id == 10){
    		$response 	= array();
    		$raw 		= $request->waybills;
    		$username 	= $request->username;

	    	//check scan type (with package)
	    	if($request->package_id == 0){
	    		//scan without package
	    		foreach($raw as $key => $data){
	    			$waybill = Waybill::select('waybill_no','id')->where('waybill_no',$data)->where('action_id',9)->first();

	    			if($waybill){
	    				//updated waybills
	    				DB::table('waybills')->where('id',$waybill->id)->update(['action_id' => 10]);

	    				//updated package_waybills status(completed = 1)
	    				DB::table('package_waybills')->where('waybill_id',$waybill->id)->update(['completed'=>1]);	

	    				//saved logs for branch-in waybill
						DB::table('action_logs')->insert([
						    'waybill_id'    => $waybill->id,
					        'action_id'     => 10,
					        'user_id'      	=> $request->user_id,
					        'branch_id'     => $request->user_branch_id,
					        'city_id'      	=> $request->user_city_id,
					        'log' 			=> $username.' မှ ရုံးစာဝင် (စာဖြင့်)လက်ခံရရှိထားပါသည်။',
							'created_at'    => date('Y-m-d H:i:s'),
					        'updated_at'    => date('Y-m-d H:i:s'),
						]);

	    				array_push($passed,$data);
	    			}else{
	    				//failed logs
	    				$log_date 	= date('Y-m-d H:i:s');
				    	$log_no 	= $data.','.$request->username;
				    	$log_txt    = $log_date.',-,'.$log_no; //36
						saved_inbound_received_failed($log_txt);

	    				array_push($failed,$data);
	    			}
	    		}

	    		$response['success'] 	= 0;
	    		$response['passed'] 	= $passed;
	    		$response['failed'] 	= $failed;
	    		
	    	}else{
	    		//scan with package
	    		$waybill 	= DB::table('waybills')->select('id','waybill_no')->where('waybill_no',$request->waybills)->where('action_id',9)->first();
	    		$rejected 	= DB::table('waybills')->select('id','waybill_no')->where('waybill_no',$request->waybills)->where('action_id',6)->first();

	    		if($waybill){
	    			//updated waybills
	    			DB::table('waybills')->where('id',$waybill->id)->update(['action_id' => 10]);

	    			//updated package_waybills status(completed = 1)
	    			DB::table('package_waybills')->where('waybill_id',$waybill->id)->update(['completed'=>1]);	
	    			
	    			//saved logs for branch-in waybill
					DB::table('action_logs')->insert([
						'waybill_id'    => $waybill->id,
					    'action_id'     => 10,
					    'user_id'      	=> $request->user_id,
					    'branch_id'     => $request->user_branch_id,
					    'city_id'      	=> $request->user_city_id,
					    'log' 			=> $username.' မှ ရုံးစာဝင် (အထုပ်ဖြင့်)လက်ခံရရှိထားပါသည်။',
						'created_at'    => date('Y-m-d H:i:s'),
					    'updated_at'    => date('Y-m-d H:i:s'),
					]);

	    			$response['success'] 	= 1;
	    			$response['item'] 		= $waybill->id;
	    		}elseif($rejected){
	    			$rejected->action_id = 10;
	    			if($rejected->save()){
	    				PackageWaybill::where('waybill_id',$rejected->id)->update(['completed'=>1]);

	    				//saved logs for created package
					    $action                 = new ActionLog();
						$action->waybill_id    	= $rejected->id;
						$action->action_id      = 10;
						$action->user_id      	= $request->user_id;
						$action->branch_id     	= $request->user_branch_id;
						$action->city_id      	= $request->user_city_id;
						$action->log  			= $username.' မှ စာလက်ခံ ရရှိထားပါသည်။';
						$action->save();

	    				$response['success'] 	= 1;
	    				$response['item'] 		= $rejected->id;
	    			}else{
	    				$response['success'] 	= 1;
	    				$response['item'] 		= 0;
	    			}
	    		}else{
	    			//failed logs
	    			$log_date 	= date('Y-m-d H:i:s');
				    $log_no 	= $request->waybills.','.$request->username;
					saved_inbound_received_failed($log_txt);

	    			array_push($failed,$request->waybills);

	    			$response['success'] 	= 0;
	    			$response['item'] 		= 0;
	    		}
	    	}
	    	
            
           	return response()->json($response);
        }elseif($request->action_id == 5){
    		$raw 		= $request->waybills;
    		$username 	= $request->username;
    		$count      = 0;

    		foreach($raw as $key => $data){
    			$waybill = Waybill::select('waybill_no','id')->where('waybill_no',$data)->where('action_id',4)->first();
	            	
	            if($waybill){
	            	$count++;

	            	if($count == 1 && $request->package_id == 0){
	            		//generate package number
				    	$num = package_generate();

						$package                	= new Package();
						$package->package_no    	= $num;
						$package->user_id     		= $request->user_id;
						$package->from_branch_id 	= $request->user_branch_id;
						$package->from_city_id  	= $request->user_city_id;
						//$package->to_branch_id  	= $request->to_branch_id;
						$package->to_city_id    	= $request->to_city_id;
						$package->completed    		= 0;
						$package->package_type_id 	= $request->pkg_type;  			
						$package->remark 			= $request->remark;
						$package->save();	
					}

			        $waybill->batch_id        	= batch_id();
			        $waybill->action_id    		= 5;
			        
			        if($waybill->save()){
			        	$pkg_waybill 				= new PackageWaybill;
			        	$pkg_waybill->package_id 	= $request->package_id==0? $package->id:$request->package_id;
			        	$pkg_waybill->waybill_id 	= $waybill->id;
			        	$pkg_waybill->save();

			            //saved logs for created package
			            $action                 = new ActionLog();
				        $action->waybill_id    	= $waybill->id;
				        $action->action_id      = 5;
				        $action->user_id      	= $request->user_id;
				        $action->branch_id     	= $request->user_branch_id;
				        $action->city_id      	= $request->user_city_id;
				        $action->log  			= $username.' မှ '.$request->to_city_name.' အတွက် အထုပ်ထုပ်ထားပါသည်။';
				        $action->save();
				    }
	            }else{
	            	//return waybill_no for failed 
	            	array_push($failed, $data);
	            }
    		}

    		if($count > 0){
    			$response['success'] 		= 1;
			    $response['package_id'] 	= $package->id;
			    $response['package_no'] 	= $num;
			    $response['option'] 		= 'close';
			}else{
				$response['success'] 		= 0;
			    $response['package_id'] 	= $request->package_id;
			    $response['package_no'] 	= $request->package_no;
			    $response['option'] 		= 'continue';
			}

	        $response['failed'] 	= $failed;
    	}else{
    		$response = 'action_id invalid ('.$request->action_id.')';
    	}

    	return response()->json($response);
    }

    public function fetched_count(Request $request){
    	$branch = '';
    	$status = $request->status;
    	$type 	= $request->type;

    	if($status == 'branch-in' && $type == 'inbound'){
    		$count = 1;
    	}else{
    		$count = 0;
    	}

    	return $count;
    }

    public function sent_odoo_api_callback(Request $request){
		sent_to_odoo($request->cargos,$request->city,$request->status);

		return response()->json(['status' => 'sent']);
	}

	public function fetched_waybills(Request $request){
		$response 	= array();
		$limit 		= $request->limit;

		$waybills 	= Waybill::select('id','waybill_no','outbound_date','transit_date','inbound_date')->orderBy('id','asc')->limit($limit)->get();

		return response()->json($waybills);
	}

	public function created_user(Request $request){
		$response = array();
		$check = User::where('email',$request->email)->first();

		if(!$check){
			$user 					= new User;
			$user->name 			= $request->username;
			$user->email 			= $request->email;
			$user->password 		= bcrypt($request->password);
			$user->mobile_password 	= $request->password;
			$user->city_id 			= $request->city_id;
			$user->branch_id 		= $request->branch_id;
			$user->role 			= $request->role;
			$user->active 			= 1;
			if($user->save()){
				$response['success'] = 1;
			}else{
				$response['success'] = 0;
			}
		}else{
			$response['success'] = 0;
		}
		

		return response()->json($response);
	}

	public function created_branch(Request $request){
		$response = array();
		$check = Branch::where('name',$request->branch)->first();

		if(!$check){
			$branch 				= new Branch;
			$branch->city_id 		= $request->city_id;
			$branch->name 			= $request->branch;
			$branch->is_main_office = $request->office;
			$branch->is_sorting 	= $request->sorting;
			$branch->is_transit_area= $request->transit;
			$branch->active 		= 1;
			if($branch->save()){
				$response['success'] = 1;
			}else{
				$response['success'] = 0;
			}
		}else{
			$response['success'] = 0;
		}
		
		return response()->json($response);
	}

	public function updated_user(Request $request){
		$response = array();
		$user = User::find($request->user_id);

		$user->name 		= $request->username;
		$user->city_id 		= $request->city_id;
		$user->branch_id 	= $request->branch_id;
		$user->role 		= $request->role;
		$user->active 		= $request->active;
		if($user->save()){
			$response['success'] = 1;
		}else{
			$response['success'] = 0;
		}

		return response()->json($response);
	}

	public function reset_password(Request $request){
		$response = array();
		$user = User::find($request->user_id);

		if($request->new_pass == $request->confirmed_pass){
			$user->password 		= bcrypt($request->new_pass);
			$user->mobile_password 	= $request->new_pass;
			if($user->save()){
				$response['success'] = 1;
			}else{
				$response['success'] = 0;
			}
		}else{
			$response['success'] = 0;
		}
		

		return response()->json($response);
	}

	public function created_city(Request $request){
		$response = array();
		$check = City::where('shortcode',$request->shortcode)->first();

		if(!$check){
			$city 					= new City;
			$city->name 			= $request->name;
			$city->mm_name 			= $request->mm_name;
			$city->state 			= $request->state;
			$city->shortcode 		= $request->shortcode;
			$city->is_service_point = $request->service_point;
			if($city->save()){
				$response['success'] = 1;
			}else{
				$response['success'] = 0;
			}
		}else{
			$response['success'] = 0;
		}
		

		return response()->json($response);
	}

    public function change_city_and_branch(Request $request){
    	$user_id    = $request->user_id;
    	$city_id 	= $request->city_id;
    	$branch_id 	= $request->branch_id;

    	$user 				= User::find($user_id);
    	$user->branch_id 	= $branch_id;
    	$user->city_id 		= $city_id;

    	if($user->save()){
    		return response()->json(['success' => 1]);
    	}else{
    		return response()->json(['success' => 0]);
    	}
    }


    public function fetched_waybill_to_sync(){
    	//fetched first 10 jobs to sync

    }

    public function submit_cancelled(Request $request){
    	$waybill_no 	= $request->waybill_no;
    	
    	$check = Waybill::select('waybill_no')->where('waybill_no',$waybill_no)->first();
    	if($check){
    		$waybill = RejectedWaybill::select('waybill_no')
    		->where('waybill_no',$waybill_no)
    		->first();

	    	if($waybill){
	    		$response['success'] 	= 0;
	    		$response['msg'] 		= 'Waybill has already submitted to reject.';
	    	}else{
	    		//updated active = 0 at waybills
	    		// DB::table('waybills')->select('waybill_no')->where('waybill_no',$waybill_no)
	    		// 	->update(['active' => 0]);
	    		
	    		$waybill 				= new RejectedWaybill;
	    		$waybill->waybill_no 	= $waybill_no;
	    		$waybill->cancelled_date= date('Y-m-d H:m:i');
		    	$waybill->user_id   	= $request->user_id;
		    	$waybill->branch_id  	= $request->user_branch_id;
		    	$waybill->city_id   	= $request->user_city_id;
		    	$waybill->reason		= $request->reason;
		    	$waybill->status		= 0;
	    		$waybill->save();

	    		$response['success'] 	= 1;
	    		$response['msg'] 		= 'successful submitted.';
	    	}
    	}else{
    		$response['success'] 	= 0;
	    	$response['msg'] 		= 'Waybill not found.';
    	}
    	

    	return response()->json($response);
    }

    public function approved_waybill(Request $request){
    	$response = array();
    	$cancelled = RejectedWaybill::select('id','waybill_no')->where('id',$request->id)->where('status',0)->first();
    	if($cancelled){
    		$cancelled->status = 1;
    		$cancelled->save();

    		//updated active = 0 at waybills
	    	DB::table('waybills')->select('waybill_no')->where('waybill_no',$cancelled->waybill_no)
	    		->update(['active' => 0]);


    		$response['success'] 	= 1;
    		$response['waybill_no'] = $cancelled->waybill_no;
    	}else{
    		$response['success'] 	= 0;
    		$response['waybill_no'] = $cancelled->waybill_no;
    	}

    	return $response;
    }

    public function generate_ctf(Request $request){
    	$response = array();
    	$last 	= CtfGenerator::latest()->first();
    	if($last){
    		//generate ctf number
    		$num = generate_ctf_number($request->user_branch_name,$last->id,$request->to_branch_name);

    		$new 					= new CtfGenerator;
    		$new->ctf_no			= $num;
    		$new->user_id 			= $request->user_id;
    		$new->action_id 		= $request->type;
    		$new->save();

    		$response['ctf'] = $num;
    		$response['id']  = $new->id;

    	}else{
    		//create new ctf number
    		$num = generate_ctf_number($request->user_branch_name,0,$request->to_branch_name);

    		$new 					= new CtfGenerator;
    		$new->ctf_no			= $num;
    		$new->user_id 			= $request->user_id;
    		$new->action_id 		= $request->type;
    		$new->save();

    		$response['ctf'] = $num;
    		$response['id']  = $new->id;
    		
    	}

    	return response()->json($response);
    }

    public function fetched_ctf_waybills(Request $request){
    	$response = array();
    	$branch   = CtfGenerator::select('user_branch_id')
    		->where('ctf_no',$request->ctf_no)
    		->where('type','outbound-handover')
    		->first();

    	if($branch){
    		$from_branch = branch($branch->user_branch_id)['name'];
    	}else{
    		$from_branch = '...';
    	}

    	$waybills = Waybill::join('ctf_logs as ctf','ctf.waybill_id','=','waybills.id')
    		->where('ctf.ctf_no',$request->ctf_no)
    		->where('ctf.completed',0)
    		->where('waybills.active',1)
    		->select('waybills.id','waybills.waybill_no')
    		->get();
    	
    	$response['data'] 			= $waybills;
    	$response['count'] 			= $waybills->count();
    	$response['from_branch'] 	= $from_branch;

    	return response()->json($response);
    }

    public function fetched_ctf_city(Request $request){
    	$response = array();

    	$waybills = CtfGenerator::join('cities as c','c.id','=','ctf_generators.user_city_id')
    		->where('ctf_generators.ctf_no',$request->ctf_no)
    		->where('ctf_generators.type','outbound-branch-out')
    		->select('c.name as city_name','c.mm_name as city_mm_name','c.shortcode as shortcode','ctf_generators.ctf_no')
    		->first();
    	

    	return response()->json($waybills);
    }

    public function cancelled_info(Request $request){
    	$waybill = RejectedWaybill::join('users as u','u.id','=','rejected_waybills.user_id')
    		->join('branches as b','b.id','=','u.branch_id')
    		->select('rejected_waybills.cancelled_date','rejected_waybills.waybill_no','u.name as user','b.name as branch','reason')
    		->where('waybill_no',$request->waybill_no)
    		->first();

    	return response()->json($waybill);

    }

    public function sent_to_odoo(){

    	//fetched records
    	$jobs = WaybillJob::select('waybill_no')
    		->where('completed',0)
            ->get(10);

        sent_to_odoo('D34512789','YGN','in');

        return response()->json(['status' => 'sent']);
    }

    public function ctf_export(Request $request){
    	$num 	  = time();
    	$response = array();
    	$raw      = $request->waybills;

    	$temp 		= explode(",",$raw);
    	$username 	= $request->username;

    	//created ctf
    	$ctf  = new Ctf;
    	$ctf->ctf_no 		= $num;
    	$ctf->user_id  		= $request->user_id;
    	$ctf->branch_id 	= $request->user_branch_id;
    	$ctf->city_id 		= $request->user_city_id;
    	$ctf->action_id 	= 4;
    	$ctf->completed 	= 0;
    	$ctf->save();

    	for($i = 0; $i < count($temp); $i++) { 
    		$num = trim($temp[$i]);

    		//exported ctf for package
			Package::where('id',$temp[$i])->update(['completed'=>1]);

    		//created ctf_package
    		$ctf_pkg = new CtfPackage;
    		$ctf_pkg->ctf_id 		= $ctf->id;
    		$ctf_pkg->package_id 	= trim($temp[$i]);
			$ctf_pkg->save();

    	}

    }

    public function fetched_package_by_branch($branch_id,$status){
    	if($status == 'draft'){
    		$packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
	            ->select('b.id as branch_id','b.name as branch_name')
	            ->where('completed',0)
	            ->where('to_branch_id',$branch_id)
	            ->select('packages.id','package_no','b.name as branch_name')
	            ->paginate(50);	
    	}else{
    		$packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
	            ->select('b.id as branch_id','b.name as branch_name')
	            ->where('completed',1)
	            ->where('to_branch_id',$branch_id)
	            ->select('packages.id','package_no','b.name as branch_name')
	            ->paginate(50);	
    	}
    	

        return $packages;
    }

    public function fetched_package_types(){
    	$response = array();

    	$packages  = PackageType::select('id','name','remark')->orderBy('name','asc')->get();

    	return response()->json($packages);
    }

   

    public function fetched_package_waybills(Request $request){
    	$response = array();

    	$waybills  = PackageWaybill::join('waybills as w','w.id','=','package_waybills.waybill_id')
    		->select('w.id','w.waybill_no','package_waybills.package_id','package_waybills.completed','w.qty','w.end_point')
    		->where('package_waybills.completed',0)
    		->where('package_waybills.active',1)
    		->where('w.active',1)
    		->where('package_waybills.package_id',$request->package_id)
    		->orderBy('qty','desc')
    		->get();

    	if($waybills->count() == 0){
    		CtfPackage::where('package_id',$request->package_id)->update(['completed'=>1]);
    	}

    	return response()->json($waybills);
    }

    public function fetched_cities(Request $request){
    	$user = User::find($request->user_id);
    	if($user){
    		$cities = Route::join('cities as c','c.id','=','routes.to_city_id') 
    			->select('c.id','c.name','c.mm_name','c.shortcode','c.state','routes.active')
	    		->where('routes.from_city_id',$user->city_id)
	    		->orderBy('c.shortcode')
	    		->get();

	    	
	    }else{
	    	$cities = array();
	    }
    	

    	return response()->json($cities);
    }

    public function fetched_branches(Request $request){
		$branches = Branch::select('id','name','is_sorting','active')
			->where('city_id',$request->city_id)
			->where('active',1)
			->orderBy('name','asc')
			->get();

		return response()->json($branches);
    }

    public function fetched_routes(Request $request){
		$routes = Route::join('cities as c1','c1.id','=','routes.from_city_id')
            ->join('cities as c2','c2.id','=','routes.to_city_id')   
            ->select('routes.id','c1.shortcode as from_city','c1.mm_name as from_city_name','c2.shortcode as to_city','c2.mm_name as to_city_name','routes.active')
            ->where('routes.from_city_id',$request->city_id)
            ->orderBy('c1.shortcode')
            ->orderBy('c2.shortcode')
            ->get();

		return response()->json($routes);
    }

    public function fetched_couriers(Request $request){
		$users = User::join('branches as b','b.id','=','users.branch_id')
			->select('users.id','users.name','b.name as branch')
			->where('role',3)
			->where('users.active',1)
			->where('branch_id',$request->branch_id)
			->orderBy('name','asc')
			->get();

		return response()->json($users);
    }

    public function fetched_outbound_packages(Request $request){
    	$branch_id 	= $request->branch_id;
    	$date 		= $request->date;

    	if($request->action_id == 2){
    		//fine
    		$packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
    			//->join('package_waybills as wp','wp.package_id','=','packages.id')
	            //->select('packages.created_at','packages.package_no','b.name as to_branch',DB::raw('wp.package_id , COUNT(*) as qty'))
->select('packages.id as package_id','packages.created_at','packages.package_no','b.name as to_branch') 
 ->where('packages.completed',0)
	            ->where('packages.action_id',2)
	            //->where('wp.active',1)
	            ->where('packages.from_branch_id',$request->branch_id)
	            ->whereDate('packages.created_at','like',$date.'%')
	            //->groupBy('wp.package_id')
	            ->orderBy('packages.id','desc')
	            ->get();

	        /*z.com
    		$packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
    			->join('package_waybills as wp','wp.package_id','=','packages.id')
	            ->select('packages.id','packages.package_no','b.name as to_branch',DB::raw('wp.package_id , COUNT(*) as qty'))
	            ->where('packages.completed',0)
	            ->where('packages.action_id',2)
	            ->where('packages.from_branch_id',$request->branch_id)
	            ->groupBy('wp.package_id')
	            ->orderBy('packages.id')
	            ->get();
	        */
    	}else{
        	$packages = Package::join('cities as c','c.id','=','packages.to_city_id')
	            //->join('package_waybills as wp','wp.package_id','=','packages.id')
	            //->select('packages.created_at','packages.package_no','c.name as to_city',DB::raw('wp.package_id , COUNT(*) as qty'))
->select('packages.id as package_id','packages.created_at','packages.package_no','c.name as to_city')
	            ->where('packages.completed',0)
	            ->where('packages.action_id',5)
	            //->where('wp.active',1)
	            ->where('packages.from_branch_id',$request->branch_id)
	            ->whereDate('packages.created_at','like',$date.'%')
	            //->groupBy('wp.package_id')
	            ->orderBy('packages.id','desc')
	            ->get();
    	}

        return response()->json($packages);
    }

    public function fetched_inbound_packages(Request $request){
    	$branch_id 	= $request->branch_id;
    	$date 		= $request->date;

    	if($request->action_id == 8){
    		$packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
    			//->join('package_waybills as wp','wp.package_id','=','packages.id')
	            //->select('packages.created_at','packages.package_no','b.name as to_branch',DB::raw('wp.package_id , COUNT(*) as qty'))
->select('packages.id as package_id','packages.created_at','packages.package_no','b.name as to_branch')	            
->where('packages.completed',0)
	            ->where('packages.action_id',8)
	            ->where('packages.from_branch_id',$request->branch_id)
	            ->whereDate('packages.created_at','like',$date.'%')
	            //->groupBy('wp.package_id')
	            ->orderBy('packages.id')
	            ->get();

	       	/*//z.com
    		$packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
    			->join('package_waybills as wp','wp.package_id','=','packages.id')
	            ->select('packages.id','packages.package_no','b.name as to_branch',DB::raw('wp.package_id , COUNT(*) as qty'))
	            ->where('packages.completed',0)
	            ->where('packages.action_id',2)
	            ->where('packages.from_branch_id',$request->branch_id)
	            ->groupBy('wp.package_id')
	            ->orderBy('packages.id')
	            ->get();
	            */

    	}else{
        	$packages = Package::join('cities as c','c.id','=','packages.to_city_id')
	            //->join('package_waybills as wp','wp.package_id','=','packages.id')
	            //->select('packages.created_at','packages.package_no','c.name as to_city',DB::raw('wp.package_id , COUNT(*) as qty'))
->select('packages.id as package_id','packages.created_at','packages.package_no','c.name as to_city')	            
->where('packages.completed',0)
	            ->where('packages.action_id',5)
	            ->where('packages.from_branch_id',$request->branch_id)
	            ->whereDate('packages.created_at','like',$date.'%')
	            //->groupBy('wp.package_id')
	            ->orderBy('packages.id')
	            ->get();
    	}

        return response()->json($packages);
    }

    public function removed_waybill(Request $request){
    	$response = array();

    	$check = Waybill::select('action_id')->where('id',$request->item)->first();
    	
    	if($check){
    		if($check->action_id == 2){
    			Waybill::where('id',$request->item)->update(['action_id'=>1]);
		    	PackageWaybill::where('waybill_id',$request->item)->where('package_id',$request->package_id)->update(['active'=>0]);
		    	$package = Package::select('package_no')->where('id',$request->package_id)->first();
		    		
		    	//saved logs for removed waybill from package
				$action                 	= new ActionLog();
				$action->waybill_id    		= $request->item;
				$action->action_id      	= 3;
				$action->user_id      		= $request->user_id;
				$action->branch_id    		= $request->user_branch_id;
				$action->city_id  			= $request->user_city_id;
				$action->log  				= $request->username.' သည် ဤစာအား '.$package->package_no.'အထုပ်မှ ပယ်ဖျတ်ထားပါသည်။';
				$action->save();
				
				$response['success'] 	= 1;
	    		$response['item'] 		= $request->item;
    		}elseif($check->action_id == 5){
    			Waybill::where('id',$request->item)->update(['action_id'=>4]);
		    	PackageWaybill::where('waybill_id',$request->item)->where('package_id',$request->package_id)->update(['active'=>0]);
		    	$package = Package::select('package_no')->where('id',$request->package_id)->first();
		    		
		    	//saved logs for removed waybill from package
				$action                 	= new ActionLog();
				$action->waybill_id    		= $request->item;
				$action->action_id      	= 6;
				$action->user_id      		= $request->user_id;
				$action->branch_id    		= $request->user_branch_id;
				$action->city_id  			= $request->user_city_id;
				$action->log  				= $request->username.' သည် ဤစာအား '.$package->package_no.'အထုပ်မှ ပယ်ဖျတ်ထားပါသည်။';
				$action->save();

				$response['success'] 	= 1;
	    		$response['item'] 		= $request->item;
    		}elseif($check->action_id == 8){
    			Waybill::where('id',$request->item)->update(['action_id'=>7]);
		    	PackageWaybill::where('waybill_id',$request->item)->where('package_id',$request->package_id)->update(['active'=>0]);
		    	$package = Package::select('package_no')->where('id',$request->package_id)->first();
		    		
		    	//saved logs for removed waybill from package
				$action                 	= new ActionLog();
				$action->waybill_id    		= $request->item;
				$action->action_id      	= 9;
				$action->user_id      		= $request->user_id;
				$action->branch_id    		= $request->user_branch_id;
				$action->city_id  			= $request->user_city_id;
				$action->log  				= $request->username.' သည် ဤစာအား '.$package->package_no.'အထုပ်မှ ပယ်ဖျတ်ထားပါသည်။';
				$action->save();

				$response['success'] 	= 1;
	    		$response['item'] 		= $request->item;
    		}else{
    			$response['success'] 	= 0;
	    		$response['item'] 		= $request->item;
    		}
    	}else{
    		$response['success'] 	= 0;
	    	$response['item'] 		= $request->item;
    	}

    	return response()->json($response);
    }

    public function cancelled_waybill(Request $request){
    	$response = array();
    	$log = $request->username.' မှ အကြောင်းပြချက် <strong class="text-danger">'.$request->reason.'.</strong> ဖြင့် cancelled လုပ်ထားပါသည်။';

    	Waybill::where('id',$request->item)->where('active',1)->update(['active'=>0]);
    	//PackageWaybill::where('waybill_id',$request->item)->where('package_id',$request->package_id)->update(['active'=>0]);
    	//$package = Package::select('package_no')->where('id',$request->package_id)->first();
    		
    	//saved logs for removed waybill from package
		$action                 	= new ActionLog();
		$action->waybill_id    		= $request->item;
		$action->action_id      	= 0;
		$action->user_id      		= $request->user_id;
		$action->branch_id    		= $request->user_branch_id;
		$action->city_id  			= $request->user_city_id;
		$action->log  				= $log;
		$action->save();
    	

    	$response['success'] 	= 1;
    	$response['item'] 		= $request->item;
    	$response['log'] 		= $log;
    	$response['branch'] 	= branch($request->user_branch_id)['name'];
    	$response['date'] 		= date('Y-m-d H:i:s');

    	return response()->json($response);
    }

    public function fetched_inbound_ctfs(Request $request){
    	$branch_id 			= $request->branch_id;
    	$date 	 			= $request->date;
    	$is_service_point 	= city(branch($branch_id)['city_id'])['is_service_point'];
		$min_date       	= finding_date('- 5day');

    	if($request->action_id == 6){
    		if(branch($branch_id)['is_sorting'] == 1 || $is_service_point == 1){
	    		$ctfs = DB::table('ctfs')->join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
		            ->join('packages as p','p.id','=','cp.package_id')
		            ->join('cities as c','c.id','=','p.from_city_id')
		            ->select('ctfs.id','ctfs.created_at','ctfs.ctf_no','p.from_city_id','c.name as from_city_name','p.to_city_id',DB::raw('ctfs.id , COUNT(*) as pkg_qty'))
		            ->where('p.to_city_id',branch($branch_id)['city_id'])
		            //->where('p.to_branch_id',$branch_id)
		            ->where('ctfs.completed',0)
		            ->where('ctfs.action_id','>=',6)
		            ->whereDate('ctfs.created_at','>=',$min_date)
		            ->groupBy('ctfs.id','ctfs.ctf_no','p.from_city_id','c.name','p.to_city_id')
		            ->orderBy('ctfs.ctf_no','desc')
		            ->get();
    		}else{
    			$ctfs = array();
    		}
	    }else{
	       $ctfs = DB::table('ctfs')->join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
		        ->join('packages as p','p.id','=','cp.package_id')
		        ->join('cities as c','c.id','=','p.from_city_id')
		        ->select('ctfs.id','ctfs.created_at','ctfs.ctf_no','p.from_city_id','c.name as from_city_name','p.to_city_id',DB::raw('ctfs.id , COUNT(*) as pkg_qty'))
		        ->where('p.to_branch_id',$branch_id)
		        ->where('ctfs.completed',0)
		        ->where('ctfs.action_id','>=',9)
		        ->whereDate('ctfs.created_at','>=',$min_date)
		        ->groupBy('ctfs.id','ctfs.ctf_no','p.from_city_id','c.name','p.to_city_id')
		        ->orderBy('ctfs.ctf_no','desc')
		        ->get();
	    }

        return $ctfs;
    }


    public function fetched_outbound_ctfs(Request $request){
    	$date 	 	= $request->date;
    	$min_date 	= finding_date('- 5day');

        $ctfs = DB::table('ctfs')->join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                ->join('packages as p','p.id','=','cp.package_id')
                ->join('branches as b','b.id','=','p.from_branch_id')
                ->join('branches as b2','b2.id','=','p.to_branch_id')
                ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name as from_branch_name','p.to_branch_id','b2.name as to_branch_name',DB::raw('ctfs.id , COUNT(*) as pkg_qty'))
                ->where('p.to_branch_id',$request->branch_id)
                ->where('ctfs.completed',0)
                ->where('ctfs.action_id',3)
                ->whereDate('ctfs.created_at','>=',$min_date)
                ->groupBy('ctfs.created_at','ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','p.to_branch_id','b2.name')
                ->orderBy('ctfs.ctf_no','desc')
                ->get();

        return $ctfs;
    }

    public function fetched_ctf_packages(Request $request){
    	$response = array();

    	$packages  = DB::table('ctf_packages')->leftjoin('packages as p','p.id','=','ctf_packages.package_id')
    		->leftjoin('branches as b','b.id','=','p.from_branch_id')
    		->leftjoin('package_types as pt','pt.id','=','p.package_type_id')
    		->leftjoin('cities as c','c.id','=','p.to_city_id')
    		->select('ctf_packages.created_at','ctf_packages.id','ctf_packages.ctf_id','ctf_packages.package_id','p.package_no','b.name as from_branch_name','c.shortcode as to_city_name','pt.name as type','pt.icon as icon')
    		->where('ctf_id',$request->ctf_id)
    		->where('ctf_packages.completed',0)
    		->orderBy('id','asc')->get();

    	if($packages->count() == 0){
    		DB::table('ctfs')->where('id',$request->ctf_id)->update(['completed'=>1,'completed_at'=>date('Y-m-d H:i:s')]);
    	}

    	return response()->json($packages);
    }

    public function check_waybills_package(Request $request){
    	$response = array();
    	$package_id = $request->package_id;

    	$count = PackageWaybill::select('package_id')->where('package_id',$package_id)->where('completed',0)->count();
    	
    	if($count == 0){
    		//updated complete package ctf
    		Package::where('id',$package_id)->update(['completed_at'=>date('Y-m-d H:i:s')]);
    		//updated complete package ctf
    		CtfPackage::where('package_id',$package_id)->update(['completed'=>1]);
    		
    		$response['success'] 	= 1;
    		$response['package_id'] = $package_id;
    	}else{
    		$response['success'] 	= 0;
    		$response['package_id'] = $package_id;
    	}

    	return $response;
    }

    public function check_packages_ctf(Request $request){
    	$response = array();
    	$ctf_id = $request->ctf_id;

    	$count = CtfPackage::select('ctf_id')->where('ctf_id',$ctf_id)->where('completed',0)->count();
    	
    	if($count == 0){
    		//updated complete package ctf
    		//CtfPackage::where('package_id',$package_id)->update(['completed'=>1]);
    		Ctf::where('id',$request->ctf_id)->update(['completed'=>1,'completed_at'=>date('Y-m-d H:i:s')]);

    		$response['success'] 	= 1;
    		$response['ctf_id'] = $ctf_id;
    	}else{
    		$response['success'] 	= 0;
    		$response['ctf_id'] = $ctf_id;
    	}

    	return $response;
    }

    public function change_ctf_info(Request $request){
    	$response = array();

    	if($request->action_id == 2){
    		$outbound 	= Ctf::select('id','history')->where('id',$request->ctf_id)->where('action_id',3)->where('completed',0)->first();
    		$inbound 	= Ctf::select('id','history')->where('id',$request->ctf_id)->where('action_id',9)->where('completed',0)->first();
    	
	    	if($outbound){
	    		$old_history = $outbound->history;
	    		$new_history = '['.date('Y-m-d H:i:s').'] '.$request->username.' changes branch to →'.$request->to_branch_name.' .';

	    		if(!$old_history){
	    			$outbound->history = $new_history;
	    		}else{
	    			$outbound->history = $old_history.'<br>'.$new_history;
	    		}
	    		$outbound->save();

	    		//updated complete package ctf
	    		$pkg = CtfPackage::join('packages as p','p.id','=','ctf_packages.package_id')
	    			->where('ctf_packages.ctf_id',$request->ctf_id)
	    			->update(['to_branch_id'=>$request->to_branch_id]);
	    		
	    		$response['success'] = 1;
	    	}elseif($inbound){
	    		$old_history = $inbound->history;
	    		$new_history = '['.date('Y-m-d H:i:s').'] '.$request->username.' changes branch to →'.$request->to_branch_name.' .';

	    		if(!$old_history){
	    			$inbound->history = $new_history;
	    		}else{
	    			$inbound->history = $old_history.'<br>'.$new_history;
	    		}
	    		
	    		$inbound->save();

	    		//updated complete package ctf
	    		$pkg = CtfPackage::join('packages as p','p.id','=','ctf_packages.package_id')
	    			->where('ctf_packages.ctf_id',$request->ctf_id)
	    			->update(['to_branch_id'=>$request->to_branch_id]);
	    		
	    		$response['success'] = 1;
	    	}else{
	    		$response['success'] = 0;
	    	}
	    }else{
	    	$inbound 	= Ctf::select('id','user_id','created_at')->where('id',$request->ctf_id)->where('action_id',6)->first();
	    	
	    	if($inbound){
	    		$inbound->action_id = 8;
	    		$inbound->save();


	    		$history 			= new History;
	    		$history->data_id 	= $request->ctf_id;
	    		$history->type 		= 'ctf';
	    		$history->log 		= '['.date('Y-m-d H:i:s').'] '.$request->username.' changes branch to →'.$request->to_branch_name.' .';
	    		$history->save();

	    		// $pkg = Package::join('ctf_packages as cp','cp.package_id','=','packages.id')
	    		// 	->join('ctfs as c','c.id','=','cp.ctf_id')
	    		// 	->where('c.id',$request->ctf_id)
	    		// 	->update(['to_branch_id'=>$request->to_branch_id]);

	    		//updated complete package ctf
		    	$pkg = CtfPackage::join('packages as p','p.id','=','ctf_packages.package_id')
		    		->where('ctf_packages.ctf_id',$request->ctf_id)
		    		->update(['to_branch_id'=>$request->to_branch_id,'p.action_id'=>8]);

	    		$response['success'] = 1;
	    	}else{
	    		$response['success'] = 0;
	    	}

	    	//
	    	//$inbound 	= Ctf::select('id')->where('id',$request->ctf_id)->where('action_id',9)->where('completed',0)->first();
    	
	    	// $old_history = $inbound->history;
	    	// $new_history = '['.date('Y-m-d H:i:s').'] '.$request->username.' changes branch to →'.$request->to_branch_name.' .';

	    	// if(!$old_history){
	    	// 	$inbound->history = $new_history;
	    	// }else{
	    	// 	$inbound->history = $old_history.'<br>'.$new_history;
	    	// }
	    		
	    	

	    	
	    		
	    	$response['success'] = 1;
	    	
	    }

    	return response()->json($response);
    }

    public function change_package_info(Request $request){
    	$response = array();

    	if($request->action_id == 2){
    		$outbound 	= Package::select('id','user_id','to_branch_id','created_at')->where('id',$request->pkg_id)->where('action_id',2)->first();
	    	$inbound 	= Package::select('id','user_id','to_branch_id','created_at')->where('id',$request->pkg_id)->where('action_id',18)->first();
	    	
	    	if($outbound){
	    		//old
	    		$check = History::where('data_id',$request->pkg_id)->where('type','pkg')->count();
	    		if(!$check){
	    			$old 				= new History;
		    		$old->data_id 		= $request->pkg_id;
		    		$old->type 			= 'pkg';
		    		$old->log 			= '['.$outbound->created_at.'] '.user_info($outbound->user_id)['name'].' creates package to →'.branch($outbound->to_branch_id)['name'].' .';
		    		$old->save();
	    		}
	    		
	    		$history 			= new History;
	    		$history->data_id 	= $request->pkg_id;
	    		$history->type 		= 'pkg';
	    		$history->log 		= '['.date('Y-m-d H:i:s').'] '.$request->username.' changes branch to →'.$request->to_branch_name.' .';
	    		$history->save();

	    		//updated complete package ctf
	    		$pkg = Package::where('id',$request->pkg_id)->update(['to_branch_id'=>$request->to_branch_id]);
	    		
	    		$response['success'] = 1;
	    	}elseif($inbound){
	    		//old
	    		$check = History::where('data_id',$request->pkg_id)->where('type','pkg')->count();
	    		if(!$check){
	    			$old 				= new History;
		    		$old->data_id 		= $request->pkg_id;
		    		$old->type 			= 'pkg';
		    		$old->log 			= '['.$outbound->created_at.'] '.user_info($outbound->user_id)['name'].' creates package to →'.branch($outbound->to_branch_id)['name'].' .';
		    		$old->save();
	    		}

	    		$history 			= new History;
	    		$history->data_id 	= $request->pkg_id;
	    		$history->type 		= 'pkg';
	    		$history->log 	= '['.date('Y-m-d H:i:s').'] '.$request->username.' changes branch to →'.$request->to_branch_name.' .';
	    		$history->save();

	    		$pkg = Package::where('id',$request->pkg_id)->update(['to_branch_id'=>$request->to_branch_id]);
	    		
	    		$response['success'] = 1;
	    	}else{
	    		$response['success'] = 0;
	    	}
    	}elseif($request->action_id == 5){
    		$outbound 	= Package::select('id','user_id','to_city_id','created_at')->where('id',$request->pkg_id)->where('action_id',5)->first();
	    	
	    	if($outbound){
	    		//old
	    		$check = History::where('data_id',$request->pkg_id)->where('type','pkg')->count();
	    		if(!$check){
	    			$old 				= new History;
		    		$old->data_id 		= $request->pkg_id;
		    		$old->type 			= 'pkg';
		    		$old->log 			= '['.$outbound->created_at.'] '.user_info($outbound->user_id)['name'].' creates package to →'.branch($outbound->to_city_id)['name'].' .';
		    		$old->save();
	    		}
	    		
	    		$history 			= new History;
	    		$history->data_id 	= $request->pkg_id;
	    		$history->type 		= 'pkg';
	    		$history->log 		= '['.date('Y-m-d H:i:s').'] '.$request->username.' changes city to →'.$request->to_city_name.' .';
	    		$history->save();

	    		//updated complete package ctf
	    		$pkg 		= Package::where('id',$request->pkg_id)->update(['to_city_id'=>$request->to_city_id]);
	    		$waybill 	= Waybill::join('package_waybills as pw','pw.waybill_id','=','waybills.id')->where('pw.package_id',$request->pkg_id)->update(['waybills.destination'=>$request->to_city_id]);
	    		
	    		$response['success'] = 1;
	    	}else{
	    		$response['success'] = 0;
	    	}
    	}else{
    		//$outbound 	= Package::select('id','user_id','to_city_id','created_at')->where('id',$request->pkg_id)->where('action_id',5)->first();
	    	$inbound 	= Package::select('id','user_id','to_branch_id','created_at')->where('id',$request->pkg_id)->where('action_id',8)->first();
	    	
	    	if($inbound){
	    		//old
	    		$check = History::where('data_id',$request->pkg_id)->where('type','pkg')->count();
	    		if(!$check){
	    			$old 				= new History;
		    		$old->data_id 		= $request->pkg_id;
		    		$old->type 			= 'pkg';
		    		$old->log 			= '['.$inbound->created_at.'] '.user_info($inbound->user_id)['name'].' creates package to →'.branch($inbound->to_branch_id)['name'].' .';
		    		$old->save();
	    		}

	    		$history 			= new History;
	    		$history->data_id 	= $request->pkg_id;
	    		$history->type 		= 'pkg';
	    		$history->log 	= '['.date('Y-m-d H:i:s').'] '.$request->username.' changes branch to →'.$request->to_branch_name.' .';
	    		$history->save();

	    		$pkg = Package::where('id',$request->pkg_id)->update(['to_branch_id'=>$request->to_branch_id]);
	    		
	    		$response['success'] = 1;
	    	}else{
	    		$response['success'] = 0;
	    	}
    	}

	    	

    	return response()->json($response);
    }


    public function search_routes($city_id){
    	$routes = Route::join('cities as c1','c1.id','=','routes.from_city_id')
            ->join('cities as c2','c2.id','=','routes.to_city_id')   
            ->select('routes.id','c1.shortcode as from_city','c1.mm_name as from_city_name','c2.shortcode as to_city','c2.mm_name as to_city_name','routes.active')
            ->orderBy('c1.shortcode')
            ->orderBy('c2.shortcode')
            ->where('routes.from_city_id',$city_id)
            ->paginate(100);

        return $routes;
    }

    public function change_route(Request $request){
    	$response = array();
    	$routes = Route::where('id',$request->route_id)->update(['active'=>$request->active]);

    	if($request->active == 1){
    		$response['active'] = $request->active;
    		$response['route_id'] = $request->route_id;
    	}else{
    		$response['active'] = $request->active;
    		$response['route_id'] = $request->route_id;
    	}

    	return response()->json($response);
    }

    public function deleted_data(Request $request){
    	$type = $request->type;
    	$response = array();

            if($type == 'ctf'){
                $ctf_no = $request->ctf;

                $ctf = Ctf::select('id')->where('ctf_no',$ctf_no)->first();

                $packages = Ctf::leftjoin('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                    ->leftjoin('packages as p','p.id','=','cp.package_id')
                    ->select('p.id')
                    ->where('ctf_no',$ctf_no)
                    ->get();

                $waybills = Ctf::leftjoin('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                    ->leftjoin('packages as p','p.id','=','cp.package_id')
                    ->leftjoin('package_waybills as pw','pw.package_id','=','cp.package_id')
                    ->leftjoin('waybills as w','w.id','=','pw.waybill_id')
                    ->select('w.id')
                    ->where('ctf_no',$ctf_no)
                    ->get();

                foreach($waybills as $waybill){
                	//delete waybills,action_logs,package_waybills,weights
                	Waybill::where('id',$waybill->id)->delete();
                	ActionLog::where('waybill_id',$waybill->id)->delete();
                	PackageWaybill::where('waybill_id',$waybill->id)->delete();
                	Weight::where('waybill_id',$waybill->id)->delete();
                }

                foreach($packages as $package){
                	//delete packages,ctf_packages,history
                	Package::where('id',$package->id)->delete();
                	CtfPackage::where('package_id',$package->id)->delete();
                	History::where('data_id',$package->id)->where('type','pkg')->delete();
                }

                Ctf::where('id',$ctf->id)->delete();
                $response['success'] = 1;
                
                return $response;
            }elseif($type == 'package'){
                $package_no = $request->pkg;

                $pkg = Package::select('id')->where('package_no',$package_no)->first();

                //delete package
                Package::where('id',$pkg->id)->delete();

                //delete package_waybills
                PackageWaybill::where('package_id',$pkg->id)->delete();

                //delete waybills
                Waybill::leftjoin('package_waybills as pw','pw.waybill_id','=','waybills.id')
                    ->select('waybills.id')
                    ->where('pw.package_id',$pkg->id)
                    ->delete();


                $response['success'] = 1;
            }else{
                $waybill_no = $request->waybill;

                $waybill = Waybill::select('id')->where('waybill_no',$waybill_no)->first();


                ActionLog::where('waybill_id',$waybill->id)->delete();
                Waybill::where('id',$waybill->id)->delete();

                return 1;
            }
        return response()->json($waybills);
    }

    public function check_end_point(Request $request){
    	$response 	= array();
    	$waybill_no = $request->waybill_no;
		$waybill 	='[\''.$waybill_no.'\']';

		$points 	= check_destination($waybill);

		if(!$points){
			Waybill::where('waybill_no',$waybill_no)->update(['end_point'=>'no data']);
			$response['success'] 	= 0;
	    	$response['end_point'] 	= 'no data';
		}else{
			Waybill::where('waybill_no',$waybill_no)->update(['end_point'=>$points[0]->to_city_short_code]);
	    	$response['success'] 	= 1;
	    	$response['end_point'] 	= $points[0]->to_city_short_code;
		}
    	return $response;
    }

    public function view_waybill(Request $request){
    	$response = array();
    	$waybill = $request->waybill_no;
    	/*
        $waybill = Waybill::leftjoin('users as u','u.id','=','waybills.user_id')
                ->join('cities as c1','c1.id','=','waybills.origin')
                ->leftjoin('cities as t1','t1.id','=','waybills.transit_1')
                ->leftjoin('cities as t2','t2.id','=','waybills.transit_2')
                ->leftjoin('cities as c2','c2.id','=','waybills.destination')
                ->select('waybills.id','waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','t1.shortcode as transit_1','t2.shortcode as transit_2','waybills.qty','waybills.same_day','waybills.action_id','u.name as courier','waybills.active')
                ->where('waybill_no',$waybill)
                ->first();
                
            $logs    = ActionLog::leftjoin('branches as b','b.id','=','action_logs.branch_id')
                ->select('action_logs.action_id','action_logs.log','action_logs.created_at','b.name as branch')
                ->where('waybill_id',$waybill->id)
                ->get();

            $packages    = PackageWaybill::leftjoin('packages as p','p.id','=','package_waybills.package_id')
                ->select('p.id','p.package_no','p.package_type_id','p.remark')
                ->where('waybill_id',$waybill->id)
                ->get(); 
    	*/
        $check1 = DB::table('waybills')->select('waybill_no')->where('waybill_no',$waybill)->first();
        $check2 = DB::table('bk_waybills')->select('waybill_no')->where('waybill_no',$waybill)->first();
        
        if($check1){
            $waybill = DB::table('waybills')->join('users as u','u.id','=','waybills.user_id')
                    ->join('cities as c1','c1.id','=','waybills.origin')
                    ->leftjoin('cities as t1','t1.id','=','waybills.transit_1')
                    ->leftjoin('cities as t2','t2.id','=','waybills.transit_2')
                    ->leftjoin('cities as c2','c2.id','=','waybills.destination')
                    ->select('waybills.id','waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','t1.shortcode as transit_1','t2.shortcode as transit_2','waybills.qty','waybills.same_day','waybills.action_id','u.name as courier','waybills.active','waybills.end_point')
                    ->where('waybill_no',$waybill)
                    ->first();

            $logs    = DB::table('action_logs')->join('branches as b','b.id','=','action_logs.branch_id')
                    ->select('action_logs.action_id','action_logs.log','action_logs.created_at','b.name as branch')
                    ->where('waybill_id',$waybill->id)
                    ->get();
            $packages    = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->select('p.id','p.package_no','p.package_type_id','p.remark')
                    ->where('waybill_id',$waybill->id)
                    ->get(); 
        }elseif($check2){
            $waybill = DB::table('bk_waybills')->join('users as u','u.id','=','bk_waybills.user_id')
                    ->join('cities as c1','c1.id','=','bk_waybills.origin')
                    ->leftjoin('cities as t1','t1.id','=','bk_waybills.transit_1')
                    ->leftjoin('cities as t2','t2.id','=','bk_waybills.transit_2')
                    ->leftjoin('cities as c2','c2.id','=','bk_waybills.destination')
                    ->select('bk_waybills.waybill_id as id','bk_waybills.waybill_no','c1.shortcode as origin','c2.shortcode as destination','t1.shortcode as transit_1','t2.shortcode as transit_2','bk_waybills.qty','bk_waybills.same_day','bk_waybills.action_id','u.name as courier','bk_waybills.active','bk_waybills.end_point')
                    ->where('waybill_no',$id)
                    ->first();

            $logs    = DB::table('bk_action_logs')->join('branches as b','b.id','=','bk_action_logs.branch_id')
                ->select('bk_action_logs.action_id','bk_action_logs.log','bk_action_logs.created_at','b.name as branch')
                ->where('waybill_id',$waybill->id)
                ->get();

            $packages    = DB::table('bk_package_waybills')->join('bk_packages as p','p.package_id','=','bk_package_waybills.package_id')
                ->select('p.id','p.package_no','p.package_type_id','p.remark')
                ->where('waybill_id',$waybill->id)
                ->get();

        }else{
            $waybill    = 'waybill not found.';
            $logs       = array();
            $packages   = array();
        }

    	$response['waybill'] 	= $waybill;
    	$response['logs'] 		= $logs;
    	$response['packages'] 	= $packages;


    	return response()->json($response);

    }

    public function check_rejected_branch(Request $request){

    	return fetched_reject_branch($request->city_id);
    }

    public function change_rejected_team(Request $request){
    	$check = CodCity::select('id','city_id','cod_branch_id')->where('city_id',$request->city_id)->first();
    	if($check){
    		$check->cod_branch_id = $request->branch_id;
    		$check->save();

    		$response['success'] 	= 1;
    		$response['msg'] 		= $request->label;
    	}else{
    		$response['success'] 	= 0;
    		$response['msg'] 		= $request->label;
    	}

    	return response()->json($response);
    }

    public function outbound_unknown(Request $request){
    	$date 		= $request->date;
    	$city_id 	= $request->city_id;
    	$response 	= array();

    	$waybills = Waybill::select('id')->where('origin',$city_id)->where('user_id',0)->where('active',1)->whereDate('created_at','like',$date.'%')->count();

    	$response['unknown_waybills'] = $waybills;

    	return response()->json($response);
    }

    public function inbound_unknown(Request $request){
    	$date 		= $request->date;
    	$city_id 	= $request->city_id;
    	$response 	= array();

    	$waybills = Waybill::select('id')->where('destination',$city_id)->where('user_id',0)->where('active',1)->whereDate('created_at','like',$date.'%')->count();

    	$response['unknown_waybills'] = $waybills;

    	return response()->json($response);
    }

    public function fetched_cod(Request $request){
    	$cods = DB::table('branches')->select('id','name')->where('name','like','%'.'-COD')->get();

    	return $cods;
    }

    public function change_password(Request $request){
    	
    }

    public function submit_feedback(Request $request){
    	$feedback = $request->feedback;
    	$username = $request->username;
    	$phone_no = $request->phone_no;

    	$response = array();

    	DB::table('feedback')->insert([
		    'feedback' 		=> $feedback, 
		    'username' 		=> $username,
		    'phone_no' 		=> $phone_no,
		    'created_at' 	=> date('Y-m-d H:i:s'),
		    'updated_at' 	=> date('Y-m-d H:i:s'),
		]);

		$response['success'] = 1;
		$response['message'] = 'You have been submitted feedback to support team.';

		return response()->json($response);
    }

    public function feedbacks(){
    	$response = array();

    	$feedbacks = DB::table('feedback')->leftjoin('users as u','u.name','=','feedback.username')
    		->select('u.name','u.image','feedback.phone_no','feedback.feedback','feedback.created_at')->orderBy('feedback.id','desc')->limit(5)->get();

    	return $feedbacks;
    }

    public function overview(Request $request){
    	$response = array();
    	
    	//ctfs
    	$response['ctf_in_imcompleted'] = Package::join('ctf_packages as cp','cp.package_id','=','packages.id')
            ->join('ctfs as c','c.id','=','cp.ctf_id')
            ->join('branches as b','b.id','=','packages.to_branch_id')
            ->select('c.ctf_no')
            ->whereDate('c.created_at','like',$request->date.'%')
            ->where('packages.to_branch_id',$request->branch_id)
            ->count();

        $response['ctf_in_completed'] 		= 0;    
        $response['ctf_out_completed'] 		= 0;    
    	$response['ctf_out_imcompleted'] 	= CTF::select('id')->where('branch_id',$request->branch_id)->whereDate('created_at','like',$request->date.'%')->count();
    	

    	//packages
    	$response['pkg_in_imcompleted'] 	= Package::select('id')->where('to_branch_id',$request->branch_id)->whereDate('created_at','like',$request->date.'%')->count();
    	$response['pkg_in_completed'] 		= Package::select('id')->where('from_branch_id',$request->branch_id)->whereDate('created_at','like',$request->date.'%')->count();
    	$response['pkg_out_imcompleted'] 	= 0;
    	$response['pkg_out_completed']  	= 0;

    	if(branch($request->branch_id)['is_sorting'] == 1){
    		//waybills for sorting
	    	$response['waybill_in_imcompleted'] = ActionLog::select('waybill_id')->where('branch_id',$request->branch_id)->where('action_id',7)->whereDate('created_at','like',$request->date.'%')->count();
	    	$response['waybill_in_completed'] 	= 0;
	    	$response['waybill_out_imcompleted']= ActionLog::select('waybill_id')->where('branch_id',$request->branch_id)->where('action_id',6)->whereDate('created_at','like',$request->date.'%')->count();
	    	$response['waybill_out_completed']  = 0;
	    }else{
	    	//waybills for branch
	    	$response['waybill_in_imcompleted'] = ActionLog::select('waybill_id')->where('branch_id',$request->branch_id)->where('action_id',10)->whereDate('created_at','like',$request->date.'%')->count();
	    	$response['waybill_in_completed'] 	= ActionLog::select('waybill_id')->where('branch_id',$request->branch_id)->where('action_id',10)->whereDate('created_at','like',$request->date.'%')->count();
	    	$response['waybill_out_imcompleted']= ActionLog::select('waybill_id')->where('branch_id',$request->branch_id)->where('action_id',1)->whereDate('created_at','like',$request->date.'%')->count();
	    	$response['waybill_out_completed']	= ActionLog::select('waybill_id')->where('branch_id',$request->branch_id)->where('action_id',3)->whereDate('created_at','like',$request->date.'%')->count();
	    }

    	return response()->json($response);

    }

    public function fetched_end_point(Request $request){
    	$response = array();
    	$end_point = Waybill::select('end_point')->where('waybill_no',$request->waybill_no)->first();

    	return response()->json($end_point);
    }

    public function synced_backup_waybills(Request $request){
    	$limit = $request->limit;
    	$response = array();

    	bk_job_one($limit);

    	$response['w_primary'] 		= number_format(waybill_layer()['primary']);
    	$response['w_secondary'] 	= number_format(waybill_layer()['secondary']);
    	$response['w_date'] 		= waybill_layer()['date'];

    	$response['a_primary'] 		= number_format(action_log_layer()['primary']);
    	$response['a_secondary'] 	= number_format(action_log_layer()['secondary']);
    	$response['a_date'] 		= action_log_layer()['date'];

    	$response['pw_primary'] 	= number_format(package_waybill_layer()['primary']);
    	$response['pw_secondary'] 	= number_format(package_waybill_layer()['secondary']);
    	$response['pw_date'] 		= package_waybill_layer()['date'];

    	return $response;
    }

    public function synced_backup_packages(Request $request){
    	$limit = $request->limit;
    	$response = array();

    	bk_job_two($limit);

    	$response['p_primary'] 		= number_format(package_layer()['primary']);
    	$response['p_secondary'] 	= number_format(package_layer()['secondary']);
    	$response['p_date'] 		= package_layer()['date'];

    	$response['cp_primary'] 	= number_format(ctf_package_layer()['primary']);
    	$response['cp_secondary'] 	= number_format(ctf_package_layer()['secondary']);
    	$response['cp_date'] 		= ctf_package_layer()['date'];

    	$response['c_primary'] 	= number_format(ctf_layer()['primary']);
    	$response['c_secondary'] 	= number_format(ctf_layer()['secondary']);
    	$response['c_date'] 		= ctf_layer()['date'];

    	return $response;
    }

    public function fetched_completed_ctf_packages(Request $request){
    	$response = array();
	/*
    	$packages  = CtfPackage::leftjoin('packages as p','p.id','=','ctf_packages.package_id')
    		->leftjoin('branches as b','b.id','=','p.from_branch_id')
    		->leftjoin('package_types as pt','pt.id','=','p.package_type_id')
    		->leftjoin('cities as c','c.id','=','p.to_city_id')
    		->select('ctf_packages.created_at','ctf_packages.id','ctf_packages.ctf_id','ctf_packages.package_id','p.package_no','b.name as from_branch_name','c.shortcode as to_city_name','pt.name as type','pt.icon as icon')
    		->where('ctf_id',$request->ctf_id)
    		->where('ctf_packages.completed',1)
    		->orderBy('id','asc')->get();

    	return response()->json($packages);
	*/
	$ctf = DB::table('ctfs')->where('id',$request->ctf_id)->first();

    	if($ctf){
			//synced to bk_ctfs table
			DB::table('bk_ctfs')->insert([
				'ctf_id'    	=> $ctf->id,
				'ctf_no'    	=> $ctf->ctf_no,
				'user_id'     	=> $ctf->user_id,
				'branch_id'    	=> $ctf->branch_id,
				'city_id'    	=> $ctf->city_id,
				'action_id'    	=> $ctf->action_id,
				'rejected'    	=> $ctf->rejected,
				'completed'    	=> $ctf->completed,
				'completed_at'  => $ctf->completed_at,
				'marked'  		=> 0,
				'created_at'    => $ctf->created_at,
				'updated_at'    => $ctf->updated_at
			]);

			//delete record from ctfs table
			DB::table('ctfs')->where('id',$request->ctf_id)->delete();
			$response['success'] = 1;
		}else{
			$response['success'] = 0;
		}

		return $response;
    }

    public function fetched_completed_package_waybills(Request $request){
    	$response = array();

    	$waybills  = PackageWaybill::join('waybills as w','w.id','=','package_waybills.waybill_id')
    		->select('w.id','w.waybill_no','package_waybills.package_id','package_waybills.completed','w.qty','w.end_point')
    		->where('package_waybills.completed',1)
    		->where('package_waybills.active',1)
    		->where('w.active',1)
    		->where('package_waybills.package_id',$request->package_id)
    		->orderBy('qty','desc')
    		->get();

    	return response()->json($waybills);
    }

    public function synced_waybills(Request $request){
		$response = array();

	    $waybill = DB::table('waybills')->where('id',$request->waybill_id)->first();
	    $package_waybill 	= DB::table('package_waybills')->where('waybill_id',$request->waybill_id)->where('package_id',$request->package_id)->first();

	    if($waybill){
	    	//backup waybill tables
		    DB::table('bk_waybills')->insert([
				'waybill_id'    => $waybill->id,
				'waybill_no'    => $waybill->waybill_no,
				'origin'     	=> $waybill->origin,
				'transit_1'     => $waybill->transit_1,
				'transit_2'     => $waybill->transit_2,
				'destination'   => $waybill->destination,
				'batch_id'     	=> $waybill->batch_id,
				'user_id'     	=> $waybill->user_id,
				'action_id'     => $waybill->action_id,
				'same_day'     	=> $waybill->same_day,
				'qty'     		=> $waybill->qty,
				'end_point'     => $waybill->end_point,
				'active'     	=> $waybill->active,
				'created_at'    => $waybill->created_at,
				'updated_at'    => $waybill->updated_at,
			]);	

			//delete record
		    DB::table('waybills')->where('id',$request->waybill_id)->delete();
	    	$response['success'] = 1;
	    }else{
	    	$response['success'] = 0;
	    }

	    //backup package_waybills table
	    if($package_waybill){
	    	DB::table('bk_package_waybills')->insert([
				'package_id'    =>   $package_waybill->package_id,
				'waybill_id'    =>   $package_waybill->waybill_id,
				'completed'     =>   $package_waybill->completed,
				'active'     	=>   $package_waybill->active,
				'checked'     	=>   $package_waybill->checked,
				'created_at'    =>   $package_waybill->created_at,
				'updated_at'    =>   $package_waybill->updated_at,
			]);

			//delete record
			DB::table('package_waybills')->where('waybill_id',$request->waybill_id)->where('package_id',$request->package_id)->delete();
	    }

		return $response;
    }

    public function synced_package(Request $request){
    	$package = DB::table('packages')->where('id',$request->package_id)->first();
		
		
			DB::table('bk_packages')->insert([
				'package_id'    	=> $package->id,
				'package_no'    	=> $package->package_no,
				'user_id'     		=> $package->user_id,
				'from_branch_id'    => $package->from_branch_id,
				'from_city_id'     	=> $package->from_city_id,
				'to_branch_id'   	=> $package->to_branch_id,
				'to_city_id'     	=> $package->to_city_id,
				'action_id'     	=> $package->action_id,
				'completed'     	=> $package->completed,
				'package_type_id'   => $package->package_type_id,
				'remark'     		=> $package->remark,
				'completed_at'     	=> $package->completed_at,
				'created_at'    	=> $package->created_at,
				'updated_at'    	=> $package->updated_at,
			]);

			$ctf_packages 	= DB::table('ctf_packages')->where('package_id',$package->id)->get();

			foreach($ctf_packages as $ctf_package){
				DB::table('bk_ctf_packages')->insert([
					'ctf_id'    	=> $ctf_package->ctf_id,
					'package_id'    => $ctf_package->package_id,
					'completed'     => $ctf_package->completed,
					'created_at'    => $ctf_package->created_at,
					'updated_at'    => $ctf_package->updated_at
				]);

				$ctf = DB::table('ctfs')->where('id',$ctf_package->ctf_id)->first();
				if($ctf){
					//synced
					DB::table('bk_ctfs')->insert([
						'ctf_id'    	=> $ctf->id,
						'ctf_no'    	=> $ctf->ctf_no,
						'user_id'     	=> $ctf->user_id,
						'branch_id'    	=> $ctf->branch_id,
						'city_id'    	=> $ctf->city_id,
						'action_id'    	=> $ctf->action_id,
						'rejected'    	=> $ctf->rejected,
						'completed'    	=> $ctf->completed,
						'completed_at'  => $ctf->completed_at,
						'created_at'    => $ctf->created_at,
						'updated_at'    => $ctf->updated_at
					]);

					//delete record
					DB::table('ctfs')->where('id',$ctf_package->ctf_id)->delete();
				}

				//deleted records
				DB::table('ctf_packages')->where('package_id',$package->id)->delete();
			}

			//deleted records
			DB::table('packages')->where('id',$package->id)->delete();
		
    }

    public function waybills_status(Request $request){
    	$branch_id 	= $request->branch_id;
    	$date 		= $request->date;
    	$response   = array();
    	$temp 		= array();

    	if(is_sorting($branch_id) == 0){
	    	$outbound = DB::table('waybills')
	    		->select('id')
				->leftjoin('users as u','u.id','=','waybills.user_id')
				->leftjoin('branches as b','b.id','=','u.branch_id')
				->whereDate('waybills.created_at','like',$date.'%')
				->where('waybills.active',1)
				->where('b.id',$branch_id)
				->count();

			$inbound = DB::table('waybills')
				->select('id')
				->leftjoin('action_logs as a','a.waybill_id','=','waybills.id')
				->whereDate('a.created_at','like',$date.'%')
				->where('waybills.active',1)
				->where('a.branch_id',$branch_id)
				->count();
		}else{
			$outbound = DB::table('waybills')
				->select('id')
				->leftjoin('action_logs as a','a.waybill_id','=','waybills.id')
				->where('waybills.active',1)
				->where('a.action_id',4)
				->whereDate('a.created_at','like',$date.'%')
				->where('a.branch_id',$branch_id)
				->count();

			$inbound = DB::table('waybills')
				->select('id')
				->leftjoin('action_logs as a','a.waybill_id','=','waybills.id')
				->where('waybills.active',1)
				->where('a.action_id',7)
				->where('a.branch_id',$branch_id)
				->whereDate('a.created_at','like',$date.'%')
				->count();
		}

		$data['outbound'] 	= $outbound;
		$data['inbound'] 	= $inbound;

		array_push($temp, $data);
		$response['data'] 	 = $temp;

    	return response()->json($data);
    }

    public function packages_status(Request $request){
		$branch_id 	= $request->branch_id;
    	$date 		= $request->date;
    	$response   = array();
    	$temp 		= array();

    	if(is_sorting($branch_id) == 0){
			$outbound = DB::table('packages')
	           	->select('packages.no')
	            ->where('from_branch_id',$branch_id)
	            ->where('action_id','>=',2)
	            ->where('action_id','<=',5)
	            ->whereDate('created_at','like',$date.'%')
	            ->count();

			$inbound = 0;
		}else{
			$outbound = DB::table('packages')
	           	->select('packages.no')
	            ->where('from_branch_id',$branch_id)
	            ->where('action_id','>=',2)
	            ->where('action_id','<=',5)
	            ->whereDate('created_at','like',$date.'%')
	            ->count();

			$inbound = DB::table('packages')
	           	->select('packages.no')
	            ->where('from_branch_id',$branch_id)
	            ->where('action_id',8)
	            ->whereDate('created_at','like',$date.'%')
	            ->count();
		}

		$data['outbound'] 	= $outbound;
		$data['inbound'] 	= $inbound;

		array_push($temp, $data);
		$response['data'] 	 = $temp;

    	return response()->json($data);
    }

    public function ctf_status(Request $request){
		$branch_id 	= $request->branch_id;
    	$date 		= $request->date;
    	$response   = array();
    	$temp 		= array();

		if(is_sorting($branch_id) == 0){
			$outbound = DB::table('ctfs')
	            ->select('id')
	            ->where('branch_id',$branch_id)
	            ->where('action_id',3)
	            ->whereDate('created_at','like',$date.'%')
	            ->count();

	        $inbound = DB::table('ctfs')
	            ->select('id')
	            ->where('to_branch_id',$branch_id)
	            ->where('action_id',9)
	            ->where('completed',1)
	            ->whereDate('created_at','like',$date.'%')
	            ->count();

		}else{
			//branch status
			$outbound = DB::table('ctfs')
	            ->select('id')
	            ->where('branch_id',$branch_id)
	            ->where('action_id',6)
	            ->whereDate('created_at','like',$date.'%')
	            ->count();

	        $inbound = DB::table('ctfs')
	            ->select('id')
	            ->where('to_branch_id',$branch_id)
	            ->where('action_id',6)
	            ->where('completed',1)
	            ->whereDate('created_at','like',$date.'%')
	            ->count();
		} 

		$data['outbound'] 	= $outbound;
		$data['inbound'] 	= $inbound;

		array_push($temp, $data);
		$response['data'] 	 = $temp;

		return response()->json($data);
    	 
    }


	public function view_package_waybills(Request $request){
    	$response = array();

    	$waybills = DB::table('waybills')->leftjoin('package_waybills as pw','pw.waybill_id','=','waybills.id')
    		->select('waybills.id','waybills.waybill_no')
    		->where('pw.package_id',$request->package_id)
    		->get();

    	return $waybills;

    }

    //marked already synced ctf to bk table
    public function marked_ctf(Request $request){
    	$response = array();

    	$ctf = DB::table('bk_ctfs')->select('ctf_id')->where('ctf_id',$request->ctf_id)->where('marked',0)->first();

    	if($ctf){
    		DB::table('bk_ctfs')->where('ctf_id',$request->ctf_id)->update(['marked' => 1]);
    		
    		$response['success'] = 1;
    	}else{

    		$response['success'] = 0;
    	}

    	return $response;
    }

	public function synced_packages(Request $request){
    	$ctf_packages = DB::table('ctf_packages')->where('ctf_id',$request->ctf_id)->get();
		
		foreach($ctf_packages as $ctf_package){
			//ctf_packages
			DB::table('bk_ctf_packages')->insert([
				'ctf_id'    	=> $ctf_package->ctf_id,
				'package_id'    => $ctf_package->package_id,
				'completed'     => $ctf_package->completed,
				'created_at'    => $ctf_package->created_at,
				'updated_at'    => $ctf_package->updated_at
			]);

			//package
			$package = DB::table('packages')->where('id',$ctf_package->package_id)->first();
			if($package){
				DB::table('bk_packages')->insert([
					'package_id'    	=> $package->id,
					'package_no'    	=> $package->package_no,
					'user_id'     		=> $package->user_id,
					'from_branch_id'    => $package->from_branch_id,
					'from_city_id'     	=> $package->from_city_id,
					'to_branch_id'   	=> $package->to_branch_id,
					'to_city_id'     	=> $package->to_city_id,
					'action_id'     	=> $package->action_id,
					'completed'     	=> $package->completed,
					'package_type_id'   => $package->package_type_id,
					'remark'     		=> $package->remark,
					'completed_at'     	=> $package->completed_at,
					'created_at'    	=> $package->created_at,
					'updated_at'    	=> $package->updated_at,
				]);

				//delete package
				$package = DB::table('packages')->where('id',$ctf_package->package_id)->delete();
			}
			
			
			DB::table('ctf_packages')->where('package_id',$ctf_package->package_id)->delete();
		}

		$backups = DB::table('bk_packages')
			->leftjoin('bk_ctf_packages as cp','cp.package_id','=','bk_packages.package_id')
			->select('bk_packages.package_id','bk_packages.package_no')
			->where('cp.ctf_id',$request->ctf_id)
			->get();

    	return $backups;
    }

    public function manual_sync_logs(Request $request){
    	$response 	= array();
		$waybill 	= DB::table('bk_waybills')->select('waybill_id')->where('waybill_no',$request->waybill_no)->first();

		//backup action_logs table
		if($waybill){
			$logs 	= DB::table('action_logs')->where('waybill_id',$waybill->waybill_id)->get();

			foreach($logs as $log){
				DB::table('bk_action_logs')->insert([
					'waybill_id'    => $log->waybill_id,
					'action_id'     => $log->action_id,
					'user_id'     	=> $log->user_id,
					'branch_id'     => $log->branch_id,
					'city_id'     	=> $log->city_id,
					'log'     		=> $log->log,
					'created_at'    => $log->created_at,
					'updated_at'    => $log->updated_at,
				]);
			}

			//deleted records
			DB::table('action_logs')->where('waybill_id',$waybill->waybill_id)->delete();
			DB::table('bk_waybills')->where('waybill_id',$waybill->waybill_id)->update(['synced'=>1]);

		}

		$response['success'] = 1;

		return $response;
    }
}
