<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Waybill;
use App\ActionLog;
use App\Branch;
use App\Package;
use App\PackageType;
use App\PackageWaybill;
use App\Ctf;
use App\CtfPackage;
use App\City;
use App\WaybillJob;
use App\Route;
use App\History;
use Illuminate\Support\Facades\DB;
class CustomController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function run_query(){

        if(Auth::user()->role == 1){
            return view('admin.run-query');
        }
    }

    public function user_guide(){
        return view('user-guide.home');
    }

    public function view_page($slug){
        $city_type = city(user()->city_id)['is_service_point'];
        $all_branches = DB::table('branches')
            ->select('id','name')
            ->where('city_id',user()->city_id)
            ->where('active',1)
            ->orderBy('name')
            ->get();

            $only_branches = DB::table('branches')
                ->select('id','name')
                ->where('city_id',user()->city_id)
                ->where('active',1)
                ->where('is_sorting',0)
                ->where('name','not like','%COD%')
                ->orderBy('name')
                ->get();

            $only_sorting = DB::table('branches')
                ->select('id','name')
                ->where('city_id',user()->city_id)
                ->where('active',1)
                ->where('is_sorting',1)
                ->orderBy('name')
                ->get();

        if($slug == 'outbound-process'){
            return view('user-guide.outbound-process',compact('all_branches','only_branches','only_sorting','city_type'));
        }elseif($slug == 'inbound-process'){
            return view('user-guide.inbound-process',compact('all_branches','only_branches','only_sorting','city_type'));
        }elseif($slug == 'transit-process'){
            return view('user-guide.transit-process',compact('all_branches','only_branches','only_sorting','city_type'));
        }elseif($slug == 'rejected-process'){
            return view('user-guide.rejected-process',compact('all_branches','only_branches','only_sorting','city_type'));
        }else{
            return '404';  
        }
        
    }

    public function run_query_result(Request $request){

    }

    public function blogs(){

        return view('blogs.home');
    }

    public function create_route(Request $request){
        $response   = array();
        $route_1    = $request->from_city;
        $route_2    = $request->to_city;

        $check = DB::table('routes')->select('id')->where('from_city_id',$route_1)->where('to_city_id',$route_2)->first();

        if(!$check){
            $id = DB::table('routes')->insertGetId([
                'from_city_id'  => $route_1,
                'to_city_id'    => $route_2,
                'active'        => 1,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);

            $response['success'] = 1;
            $response['item']    = $id;
        }else{
            $response['success'] = 0;
            $response['item']    = 0;
        }

        return $response;
    }
}
