<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\City;
use App\Route;

class CityController extends Controller
{

    public function index()
    {
        $json = 'json/cities';

        if(Auth::user()->role == 1){
            return view('admin.cities.cities',compact('json'));
        }else{
            return view('operator.cities.cities',compact('json'));    
        }

        
    }

    public function create(){
        if(Auth::user()->role == 1){
            $states = City::select('state')->orderBy('state')->groupBy('state')->get();

            return view('admin.cities.create',compact('states'));
        }
    }

    public function store(Request $request){
        $city = City::select('id')->where('shortcode',$request->shortcode)->first();
        if($city){
            return redirect('cities/create')->with('error','This shortcode is already used.');
        }else{
            $city                   = new City;
            $city->name             = $request->name;
            $city->mm_name          = $request->mm_name;
            $city->shortcode        = $request->shortcode;
            $city->is_service_point = $request->service_point? 1:0;
            if($city->save()){
                return redirect('cities')->with('success','New city <strong>'.$request->name.'</strong> has been created.');
            }else{
                return redirect('cities/create')->with('error','Something wrong.');
            }
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        if(Auth::user()->role == 1){
            $states = City::select('state')->orderBy('state')->groupBy('state')->get();
            $city = City::where('id',$id)->first();

            return view('admin.cities.edit',compact('city','states'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function json_cities(){
        $cities = City::leftjoin('cod_cities as cc','cc.city_id','=','cities.id')
            ->leftjoin('branches as b','b.id','=','cc.cod_branch_id')
            ->select('cities.id','cities.name','cities.mm_name','cities.shortcode','cities.state','cities.is_service_point','b.name as cod_branch')
            ->orderBy('cities.name')
            ->paginate(50);

        return response()->json($cities);
    }

    public function config_routes(){
        $json = 'json/config-routes';
        $cities = City::select('id','name','shortcode')->get();
        
        if(user()->role == 1){
            return view('admin.cities.routes',compact('json','cities'));
        }else{
            return view('operator.cities.routes',compact('json','cities'));
        }
        
    }

    public function json_config_routes(){
        

        $routes = Route::join('cities as c1','c1.id','=','routes.from_city_id')
            ->join('cities as c2','c2.id','=','routes.to_city_id')   
            ->select('routes.id','c1.shortcode as from_city','c1.mm_name as from_city_name','c2.shortcode as to_city','c2.mm_name as to_city_name','routes.active')
            ->orderBy('c1.shortcode')
            ->orderBy('c2.shortcode')
            ->paginate(100);

        return $routes;
    }

    public function create_routes(){
        //if(Auth::user()->role == 1){
            $cities = City::select('id','name','mm_name','shortcode','is_service_point','cod_city_id','state')->orderBy('shortcode')->get();

            return view('admin.cities.create-route',compact('cities'));
        //}
    }
}
