<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Branch;
use App\City;
use App\CodCity;

class BranchController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $json = 'json/branches';

        if(Auth::user()->role == 1){
            return view('admin.branches.branches',compact('json'));
        }elseif(Auth::user()->role == 2){
            return view('operator.branches.branches',compact('json'));
        }else{
            return '404';
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role == 1){
            $cities = City::orderBy('shortcode')->get();

            return view('admin.branches.create',compact('cities'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function json_branches(){
        $branches = Branch::join('cities as c','c.id','=','branches.city_id')
            ->select('branches.id','branches.name','branches.is_main_office','branches.is_transit_area','c.id as city_id','c.shortcode as city','branches.active')
            ->orderBy('name')
            ->paginate(50);

        return response()->json($branches);
    }

    public function view_branches($city_id){
        $branches = Branch::join('cities as c','c.id','=','branches.city_id')
            ->select('branches.id','branches.name','branches.is_main_office','branches.is_transit_area','c.id as city_id','c.shortcode as city','branches.active')
            ->where('city_id',$city_id)
            ->orderBy('name')
            ->get();

        $city   = City::where('id',$city_id)->first();

        $cods    = Branch::select('id','name')->where('name','like','%'.'-COD')->get();

        return view('operator.branches.view-branches',compact('branches','city_id','city','cods'));
    }
}
