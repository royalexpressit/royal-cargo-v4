<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\User;
use App\Waybill;
use App\City;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $json = 'json/users/name';

        if(Auth::user()->role == 1){
            return view('admin.users.users',compact('json'));
        }elseif(Auth::user()->role == 2){
            return view('operator.users.users',compact('json'));
        }else{
            return '404';
        }
    }


    public function create(){
        if(Auth::user()->role == 1){
            $cities = City::orderBy('shortcode')->get();
            return view('admin.users.create',compact('cities'));
        }else{
            return '404';
        }
    }


    public function store(Request $request){
        $exists = User::select('id')->where('email',$request->email)->first();

        if($exists){
            return redirect(url('users/create'))->with('warning','Email has been already used.');
        }else{
            print_r($_POST);
            exit;
            $user                   = new User;
            $user->name             = $request->username;
            $user->email            = $request->email;
            $user->password         = bcrypt($request->password);
            $user->mobile_password  = $request->password;
            $user->role             = $request->role;
            $user->city_id          = $request->city_id;
            $user->branch_id        = $request->branch_id;
            if($user->save()){
                return redirect(url('users'))->with('success','New username ('.$request->username.') has been created.');
            }else{
                return redirect(url('users/create'))->with('error','Something wrong.');
            }
        }
    }

    public function show($id){
        $user = User::find($id);

        if(Auth::user()->role == 1){
            return view('admin.users.view',compact('user'));
        }elseif(Auth::user()->role == 2){
            return view('operator.users.view',compact('user'));
        }else{
            return '404';
        }
        
    }

    public function edit($id){
        $user = User::find($id);

        if(Auth::user()->role == 1){
            return view('admin.users.edit',compact('user','id'));
        }elseif(Auth::user()->role == 2){
            return view('operator.users.view',compact('user'));
        }else{
            return '404';
        }
    }


    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile(){
        $user = User::find(Auth::id());

        if(Auth::user()->role == 1){
            $cities = City::orderBy('shortcode')->get();
            return view('admin.users.profile',compact('user','cities'));
        }elseif(Auth::user()->role == 2){
            return view('operator.users.profile',compact('user'));
        }elseif(Auth::user()->role == 3){
            return view('delivery.users.profile',compact('user'));
        }else{
            return '404';
        }
    }

    public function settings(){
        $user = User::find(Auth::id());

        if(Auth::user()->role == 1){
            $data = array();
            $cities = City::orderBy('shortcode')->get();

            $data['waybills']               = DB::table('waybills')->count();
            $data['packages']               = DB::table('packages')->count();
            $data['ctfs']                   = DB::table('ctfs')->count();
            $data['bk_waybills']            = DB::table('bk_waybills')->count();
            $data['bk_packages']            = DB::table('bk_packages')->count();
            $data['bk_ctfs']                = DB::table('bk_ctfs')->count();
            $data['bk_pending_waybills']    = DB::table('bk_waybills')->where('synced',0)->count();
            $data['bk_completed_waybills']  = DB::table('bk_waybills')->where('synced',1)->count();
            $data['logs']                   = DB::table('action_logs')->count();
            $data['bk_logs']                = DB::table('bk_action_logs')->count();

            return view('admin.users.settings',compact('user','cities','data'));
        }elseif(Auth::user()->role == 2){
            return view('operator.users.settings',compact('user'));
        }elseif(Auth::user()->role == 3){
            return view('delivery.users.settings',compact('user'));
        }else{
            return '404';
        }
    }

    public function json_users($sort){
        if($sort == 'name'){
            $users = User::join('cities as c','c.id','=','users.city_id')
                ->join('branches as b','b.id','=','users.branch_id')
                ->select('users.id','users.name','c.name as city','b.name as branch','image','users.role','users.image','users.active')
                ->orderBy('name')
                ->paginate(50);
        }else{
           $users = User::join('cities as c','c.id','=','users.city_id')
                ->join('branches as b','b.id','=','users.branch_id')
                ->select('users.id','users.name','c.name as city','b.name as branch','image','users.role','users.image','users.active')
                ->orderBy('id')
                ->paginate(50); 
        }
        

        return response()->json($users);
    }


    public function changed_password(Request $request){
        $old_password   = $request->old_pass;
        $new_password   = $request->new_pass;

        $user = User::find(Auth::id());   //get db User data   
        if(Hash::check($old_password, $user->password)) {   
            $user->password = bcrypt($new_password);
            $user->mobile_password = $new_password;
            $user->save();

            return response()->json(['success'=>1]);
        }else{
            return response()->json(['success'=>0]);
        }
    }

    public function scan_client_waybill_dashboard(){

        return view('operator.client-waybills.dashboard');
    }

    public function client_waybill_by_page($page){
        $date = get_date();

        if($page == 'collected-by-clients'){
            $clients = Waybill::join('users as u','u.id','=','waybills.user_id')
                ->where('outbound_date','like',$date.'%')
                ->whereIn('u.id',[200,1])
                ->select('u.id','u.name', DB::raw('count(*) as total'))
                ->groupBy('u.id','u.name')
                ->get();

            return view('operator.client-waybills.collected-by-clients',compact('clients'));
        }elseif($page == 'branch-out-by-clients'){
            return view('operator.client-waybills.branch-out-by-clients');
        }else{
            return '404';
        }
    }
    
}
