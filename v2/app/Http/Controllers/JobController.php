<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Waybill;
use App\WaybillJob;
use Illuminate\Support\Facades\DB;

class JobController extends Controller
{
    public function job_for_check_end_point(){
        $data       = array();
        $city_id    = $_GET['city_id'];
        $limit      = $_GET['limit'];
        $raw        = '';


        $waybills = Waybill::select('waybill_no')
            ->where('end_point',NULL)
            ->where('origin',$city_id)
            ->limit($limit)
            ->orderBy('id','asc')
            ->get();

        foreach($waybills as $key => $waybill){
            if($key > 0){
                $raw .= ','.'\''.$waybill->waybill_no.'\''; 
            }else{
               $raw .= '\''.$waybill->waybill_no.'\'';  
            }

            Waybill::where('waybill_no',$waybill->waybill_no)->update(['end_point'=>'no data']);
        }


        $params = '['.$raw.']';

        $response = check_destination($params);

        if(!$waybills->isEmpty()){
            foreach($response as $key => $waybill){
                Waybill::where('waybill_no',$waybill->waybill_no)->update(['end_point'=>$waybill->to_city_short_code]);
            } 
        }
            

        $data['success'] = 1;
        $data['message'] = 'End points are synced with digital.';

        return $data;
    }

    public function job_for_waybill(){
        $data       = array();
        $city_id    = $_GET['city_id'];
        $limit      = $_GET['limit'];
        $raw        = '';
        $city       = city(user()->city_id)['shortcode'];
        
        $waybills = WaybillJob::join('waybills as w','w.id','=','waybill_jobs.waybill_id')
            ->select('w.id','w.waybill_no','waybill_jobs.status','waybill_jobs.params','synced')
            ->where('city_id',$city_id)
            ->where('synced',0)
            ->limit($limit)
            ->orderBy('waybill_jobs.id','asc')
            ->get();

        foreach($waybills as $waybill){
            $response = sync_waybill($waybill->waybill_no,$waybill->status,$waybill->params,$city);

            //updated synced status
            WaybillJob::where('waybill_id',$waybill->id)->update(['synced'=>1]);
        }

        
        return $response;
    }

    public function job_check_end_point(){
        
    }
}
