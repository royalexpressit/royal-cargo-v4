<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Package;
use App\Ctf;
use App\PackageWaybill;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{   
    public function outbound_packages($status){
        $json = 'json/outbound/package';

        return view('operator.main-city.packages.outbound',compact('json','status'));
    }

    public function inbound_packages($status){
        $json = 'json/inbound/package';
        if(check_service_point(Auth::user()->city_id)){
            return view('operator.service-point.packages.inbound',compact('json','status'));
        }else{
            return view('operator.main-city.packages.inbound',compact('json','status'));
        }
    }

    public function json_outbound_packages($status){
        $from_city_id = user()->city_id;
        $date = get_date();
        
        if($status == 'draft'){
            $packages = Package::join('branches as b','b.id','=','packages.from_branch_id')
                ->select('b.id as branch_id','b.name as branch_name')
                ->select(DB::raw('from_branch_id,COUNT(*) as count'),'b.name as branch_name')
                ->where('completed',0)
                ->where('packages.action_id','>=',2)
                ->where('packages.action_id','<=',5)
                ->where('from_city_id',$from_city_id)
                ->where('packages.created_at','like','%'.$date.'%')
                ->groupBy('from_branch_id','branch_name')
                ->paginate(50);
        }elseif($status == 'completed'){
            $packages = Package::join('branches as b','b.id','=','packages.from_branch_id')
                ->select('b.id as branch_id','b.name as branch_name')
                ->select(DB::raw('from_branch_id,COUNT(*) as count'),'b.name as branch_name')
                ->where('completed',1)
                ->where('packages.action_id','>=',2)
                ->where('packages.action_id','<=',5)
                ->where('from_city_id',$from_city_id)
                ->where('packages.created_at','like','%'.$date.'%')
                ->groupBy('from_branch_id','branch_name')
                ->paginate(50);
        }else{
            $packages = Package::join('branches as b','b.id','=','packages.from_branch_id')
                ->select('b.id as branch_id','b.name as branch_name')
                ->select(DB::raw('from_branch_id,COUNT(*) as count'),'b.name as branch_name')
                ->groupBy('from_branch_id','branch_name')
                ->where('packages.action_id','>=',2)
                ->where('packages.action_id','<=',5)
                ->where('from_city_id',$from_city_id)
                ->where('packages.created_at','like','%'.$date.'%')
                ->paginate(50);
        }

        return $packages;
    }

    public function outbound_ctfs($status){
        $json = 'json/outbound/ctf/'.$status;

        return view('operator.main-city.ctfs.outbound',compact('json','status'));
    }

    public function json_outbound_ctfs($status){
        $from_city_id = user()->city_id;
        $date = get_date();
        
        if($status == 'draft'){
            $ctfs = Package::join('branches as b','b.id','=','packages.from_branch_id')
                ->select('b.id as branch_id','b.name as branch_name')
                ->select(DB::raw('from_branch_id,COUNT(*) as count'),'b.name as branch_name')
                ->where('completed',0)
                ->where('packages.action_id','>=',2)
                ->where('packages.action_id','<=',5)
                ->where('from_city_id',$from_city_id)
                ->groupBy('from_branch_id','branch_name')
                ->paginate(50);
        }elseif($status == 'completed'){
            $ctfs = Package::join('branches as b','b.id','=','packages.from_branch_id')
                ->select('b.id as branch_id','b.name as branch_name')
                ->select(DB::raw('from_branch_id,COUNT(*) as count'),'b.name as branch_name')
                ->where('completed',1)
                ->where('packages.action_id','>=',3)
                ->where('packages.action_id','<=',6)
                ->where('from_city_id',$from_city_id)
                ->groupBy('from_branch_id','branch_name')
                ->paginate(50);
        }else{
            $ctfs = Ctf::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                ->join('branches as b','b.id','=','ctfs.branch_id')
                ->join('cities as c','c.id','=','ctfs.city_id')
                ->join('users as u','u.id','=','ctfs.user_id')
                ->select(DB::raw('cp.ctf_id,COUNT(*) as count'),'ctfs.created_at','ctfs.ctf_no','b.id as branch_id','b.name as branch_name','c.shortcode as city_name','ctfs.completed','u.name as created_by')
                ->where('ctfs.action_id','>=',3)
                ->where('ctfs.action_id','<=',6)
                ->where('ctfs.city_id',$from_city_id)
                ->whereDate('ctfs.created_at','like',$date.'%')
                ->groupBy('cp.ctf_id')
                ->paginate(50);
        }

        return $ctfs;
    }

    public function json_outbound_ctfs_by_branch($branch_id){
        $from_city_id = user()->city_id;
        $date = get_date();
        

        $ctfs = Ctf::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
            ->join('branches as b','b.id','=','ctfs.branch_id')
            ->join('cities as c','c.id','=','ctfs.city_id')
            ->join('users as u','u.id','=','ctfs.user_id')
            ->select(DB::raw('cp.ctf_id,COUNT(*) as count'),'ctfs.created_at','ctfs.ctf_no','b.name as branch_name','c.shortcode as city_name','ctfs.completed','u.name as created_by')
            ->where('ctfs.action_id','>=',3)
            ->where('ctfs.action_id','<=',6)
            ->where('ctfs.branch_id',$branch_id)
            ->whereDate('ctfs.created_at','like',$date.'%')
            ->groupBy('cp.ctf_id')
            ->paginate(50);

        return $ctfs;
    }

    public function json_inbound_packages($status){
        $city_id    = user()->city_id;
        $date       = get_date();

        if($status == 'shipping'){
            if(city($city_id)['is_service_point'] == 1){
                $packages = Package::join('cities as c','c.id','=','packages.from_city_id')
                    ->join('cities as c2','c2.id','=','packages.to_city_id')
                    ->select(DB::raw('from_city_id,COUNT(*) as count'),'c.shortcode as from_city_name','c2.shortcode as to_city_name')
                    ->where('to_city_id',$city_id)
                    ->where('packages.created_at','like','%'.$date.'%')
                    ->where('packages.action_id',5)
                    ->where('packages.completed_at',NULL)
                    ->groupBy('from_city_id','from_city_name','to_city_name')
                    ->paginate(50);  
            }else{
                $packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
                    ->select('b.id as branch_id','b.name as branch_name')
                    ->select(DB::raw('to_branch_id,COUNT(*) as count'),'b.name as branch_name')
                    ->where('completed',0)
                    ->where('packages.action_id',8)
                    ->where('from_city_id',$from_city_id)
                    ->where('packages.created_at','like','%'.$date.'%')
                    ->groupBy('to_branch_id','branch_name')
                    ->paginate(50); 
            }
            
        }elseif($status == 'arrived'){
            if(city($city_id)['is_service_point'] == 1){
                $packages = Package::join('cities as c','c.id','=','packages.from_city_id')
                    ->join('cities as c2','c2.id','=','packages.to_city_id')
                    ->select(DB::raw('from_city_id,COUNT(*) as count'),'c.shortcode as from_city_name','c2.shortcode as to_city_name')
                    ->where('to_city_id',$city_id)
                    ->where('packages.created_at','like','%'.$date.'%')
                    ->where('packages.action_id',5)
                    ->where('packages.completed_at','like','%'.$date.'%')
                    ->groupBy('from_city_id','from_city_name','to_city_name')
                    ->paginate(50);  
            }else{
                $packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
                    ->select('b.id as branch_id','b.name as branch_name')
                    ->select(DB::raw('to_branch_id,COUNT(*) as count'),'b.name as branch_name')
                    ->where('completed',1)
                    ->where('packages.action_id',8)
                    ->where('from_city_id',$from_city_id)
                    ->where('packages.created_at','like','%'.$date.'%')
                    ->groupBy('to_branch_id','branch_name')
                    ->paginate(50);
            }
        }else{
            if(city($city_id)['is_service_point'] == 1){
                $packages = Package::join('cities as c','c.id','=','packages.from_city_id')
                    ->join('cities as c2','c2.id','=','packages.to_city_id')
                    ->select(DB::raw('from_city_id,COUNT(*) as count'),'c.shortcode as from_city_name','c2.shortcode as to_city_name')
                    ->where('to_city_id',$city_id)
                    ->where('packages.created_at','like','%'.$date.'%')
                    ->where('packages.action_id',5)
                    ->groupBy('from_city_id','from_city_name','to_city_name')
                    ->paginate(50);  
            }else{
                $packages = Package::join('branches as b','b.id','=','packages.to_branch_id')
                    ->select('b.id as branch_id','b.name as branch_name')
                    ->select(DB::raw('to_branch_id,COUNT(*) as count'),'b.name as branch_name')
                    ->where('from_city_id',$city_id)
                    ->where('packages.created_at','like','%'.$date.'%')
                    ->where('packages.action_id',8)
                    ->groupBy('to_branch_id','branch_name')
                    ->paginate(50);  
            }
        }

        return $packages;
    }

    public function view_outbound_package($id){
        $json = 'json/outbound/package/view/'.$id;
        $branch = branch($id);

        if(check_service_point(Auth::user()->city_id)){
            return view('operator.service-point.packages.view-outbound-package',compact('json','branch'));
        }else{
            return view('operator.main-city.packages.view-outbound-package',compact('json','branch'));
        }
    }

    public function view_inbound_package($id){
        $json = 'json/inbound/package/view/'.$id;
        $branch = branch($id);

        return view('operator.main-city.packages.view-inbound-package',compact('json','branch'));
    }

    public function json_view_outbound_package($id){
        $date = get_date();

        $packages = Package::join('branches as b','b.id','=','packages.from_branch_id')
                ->leftjoin('branches as b2','b2.id','=','packages.to_branch_id')
                ->join('users as u','u.id','=','packages.user_id')
                ->leftjoin('cities as c','c.id','=','packages.to_city_id')
                ->join('package_types as pt','pt.id','=','packages.package_type_id')
                ->select('packages.created_at','packages.package_no','b.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type','c.shortcode as to_city','packages.completed')
                ->where('from_branch_id',$id)
                ->where('packages.action_id','<=',5)
                ->where('packages.created_at','like','%'.$date.'%')
                ->paginate(30);

        return $packages;
    }

    public function json_view_inbound_package($id){
        $date = get_date();

        $packages = Package::join('branches as b','b.id','=','packages.from_branch_id')
                ->leftjoin('branches as b2','b2.id','=','packages.to_branch_id')
                ->join('users as u','u.id','=','packages.user_id')
                ->join('package_types as pt','pt.id','=','packages.package_type_id')
                ->select('packages.created_at','packages.package_no','b.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type','packages.completed')
                ->where('to_branch_id',$id)
                ->where('packages.action_id',8)
                ->where('packages.created_at','like','%'.$date.'%')
                ->paginate(30);

        return $packages;
    }

    public function view_incomplete_ctf($ctf_id){
        $json = 'json/incomplete-ctf/view/'.$ctf_id;

        return view('operator.main-city.ctfs.view-incomplete-ctf',compact('json'));
    }

    public function json_view_incomplete_ctf($ctf_id){
        $packages = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
            ->join('packages as p','p.id','=','cp.package_id')
            ->select('p.created_at','p.id','p.package_no','ctfs.completed','ctfs.ctf_no')
            ->where('ctfs.id',$ctf_id)
            ->paginate(25);

        return $packages;
    }

    public function view_incomplete_package($package_id){
        $json = 'json/incomplete-package/view/'.$package_id;

        return view('operator.main-city.packages.view-incomplete-package',compact('json'));
    }

    public function json_view_incomplete_package($package_id){
        $waybills = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
            ->join('waybills as w','w.id','=','package_waybills.waybill_id')
            ->select('w.id as waybill_id','w.waybill_no','p.id as package_id','p.package_no as package_no','package_waybills.completed','w.qty')
            ->where('package_waybills.package_id',$package_id)
            ->paginate(25);

        return $waybills;
    }

    public function package_types(){
        $types  = DB::table('package_types')->orderBy('name','asc')->get();

        return view('partials.package-types',compact('types'));
    }

    public function package_info($pkg_id){
        $package = Package::where('id',$pkg_id)->first();

        return $package;
    }

    public function duplicate_waybills(){
        $json = 'json/duplicate-waybills';

        return view('partials.duplicate-waybills',compact('json'));
    }

    public function json_duplicate_waybills(){
        $date = get_date();

        $waybills = DB::table('waybills')
            ->select('waybill_no',DB::raw('COUNT(*) as `count`'))
            ->groupBy('waybill_no')
            ->havingRaw('COUNT(*) > 1')
            ->whereDate('waybills.created_at','like',$date.'%')
            ->paginate(100);

        return response()->json($waybills);
    }

    public function view_duplicate_waybills($waybill_no){
        $waybills = DB::table('waybills')
            ->select('id','waybill_no','action_id')
            ->where('waybill_no',$waybill_no)
            ->get();

        return response()->json($waybills);
    }

    public function delete_duplicate_waybills(Request $request){
        $id = $request->id;
        $response = array();
        $waybill_no = DB::table('waybills')->select('waybill_no')->where('id',$id)->first();

        if($waybill_no){
            $response['item']    = $waybill_no->waybill_no;
        }else{
            $response['item']    = 0;
        }
        

        DB::table('waybills')->select('id')->where('id',$id)->delete();
        DB::table('action_logs')->select('waybill_id')->where('waybill_id',$id)->delete();
        DB::table('package_waybills')->select('waybill_id')->where('waybill_id',$id)->delete();

        $response['success'] = 1;
        

        return $response;
    }

    public function waybill_count(Request $request){
        $waybills = DB::table('package_waybills')
            ->leftjoin('packages as p','p.id','=','package_waybills.package_id')
            ->where('p.package_no',$request->package_no)
            ->where('package_waybills.active',1)
            ->count();

        return $waybills;
    }

    public function delete_package_waybill(Request $request){
        $response = array();

        $duplicate = DB::table('package_waybills')->select('id')->where('id',$request->item)->first();

        if($duplicate){
            DB::table('package_waybills')->select('id')->where('id',$request->item)->delete();
            $response['success'] = 1;
        }else{
            $response['success'] = 0;
        }
        return $response;
    }
}
