<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Waybill;
use App\User;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        if(Auth::user()->role == 1){
            $outbound = array();
            $inbound = array();

            $sorting_staffs = User::where('branch_id',11)->get();

            return view('admin.dashboard',compact('outbound','inbound','sorting_staffs'));
        }elseif(Auth::user()->role == 2){
            $sorting_staffs = User::where('branch_id',11)->limit(5)->get();
            $count = User::where('branch_id',11)->count();

            if(check_service_point(Auth::user()->city_id)){
                return view('operator.service-point.dashboard');
            }else{
                return view('operator.main-city.dashboard',compact('sorting_staffs','count'));
            }
        }elseif(Auth::user()->role == 3){
            return view('delivery.dashboard');
        }else{
           return view('404');     
        }
    }

    public function dashboard(){

        
        if(Auth::user()->role == 1){
            $outbound = array();
            $inbound = array();

            $sorting_staffs = User::where('branch_id',11)->get();

            return view('admin.dashboard',compact('outbound','inbound','sorting_staffs'));
        }elseif(Auth::user()->role == 2){
            $sorting_staffs = User::where('branch_id',11)->limit(5)->get();
            $count = User::where('branch_id',11)->count();

            if(check_service_point(Auth::user()->city_id)){
                return view('operator.service-point.dashboard');
            }else{
                return view('operator.main-city.dashboard',compact('sorting_staffs','count'));
            }
        }elseif(Auth::user()->role == 3){
            return view('delivery.dashboard');
        }else{
           return view('404');     
        }
        
    }

    public function changed_date(Request $request){
        $date = $request->set_date;
        setup_date($date);

        return response()->json($date);
    }

    
}
