<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Waybill;
use App\ActionLog;
use App\Branch;
use App\Package;
use App\PackageType;
use App\PackageWaybill;
use App\Ctf;
use App\CtfPackage;
use App\City;
use App\Route;
use App\WaybillJob;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $city_id = user()->city_id;

        $branches = Branch::where('city_id',$city_id)->get();
        $cities = Route::join('cities as c','c.id','=','routes.to_city_id')
                    ->select('c.id','c.name','c.mm_name','c.shortcode','c.state')
                    ->where('routes.from_city_id',$city_id)
                    ->where('routes.active',1)
                    ->orderBy('c.name')
                    ->get();

        return view('operator.reports.index',compact('branches','cities'));
    }


    public function report_1(Request $request){
        $date       = $request->form1_date;
        $branch_1   = $request->form1_1_branch;
        $branch_2   = $request->form1_2_branch;

        $json = 'json/report-1';

        return view('operator.reports.report-1',compact('date','branch_1','branch_2','json'));  
    }

    public function report_2(Request $request){
        $date       = $request->form2_date;
        $branch_1   = $request->form2_1_branch;
        $branch_2   = $request->form2_2_branch;

        $json = 'json/report-2';

        return view('operator.reports.report-2',compact('date','branch_1','branch_2','json'));  
    }

    public function report_3(Request $request){
        //return $_POST;
        $date       = $request->form3_date;
        $branch_1   = $request->form3_1_branch;

        $json = 'json/report-3';

        return view('operator.reports.report-3',compact('date','branch_1','json'));  
    }

    public function report_shop(Request $request){
        //return $_POST;
        $date       = $request->shop_date;
        $branch_1   = 'shop';

        $json = 'json/report-shop';

        return view('operator.reports.report-shop',compact('date','branch_1','json'));  
    }

    public function report_4(Request $request){
        $date           = $request->form4_date;
        $origin         = user()->city_id;
        $destination    = $request->form4_city;

        $json = 'json/report-4';

        return view('operator.reports.report-4',compact('date','origin','destination','json'));  
    }

    public function report_5(Request $request){
        $date           = $request->form5_date;
        $origin         = user()->city_id;
        $destination    = $request->form5_city;

        $json = 'json/report-5';

        return view('operator.reports.report-5',compact('date','origin','destination','json'));
    }

    public function report_6(Request $request){
        $date           = $request->form6_date;
        $origin         = user()->city_id;
        $destination    = $request->form6_city;

        $json = 'json/report-6';

        return view('operator.reports.report-6',compact('date','origin','destination','json'));  
    }

    public function report_7(Request $request){
        $date           = $request->form7_date;
        $origin         = $request->form7_city;
        $destination    = user()->city_id;

        $json = 'json/report-7';

        return view('operator.reports.report-7',compact('date','origin','destination','json'));  
    }

    public function report_8(Request $request){
        $date           = $request->form8_date;
        $from_branch    = $request->form8_1_branch;
        $to_branch      = $request->form8_2_branch;

        $json = 'json/report-8';

        return view('operator.reports.report-8',compact('date','from_branch','to_branch','json'));  
    }

    public function report_9(Request $request){
        $date         = $request->form9_date;
        $origin       = $request->origin;

        $json = 'json/report-9';

        return view('operator.reports.report-9',compact('date','origin','json')); 
    }

    public function report_10(Request $request){
        $date           = $request->form10_date;
        $branch_1       = $request->form10_1_branch;
        $branch_2       = $request->form10_2_branch;

        $json = 'json/report-10';

        return view('operator.reports.report-10',compact('date','branch_1','branch_2','json')); 
    }

    public function report_11(Request $request){
        $date           = $request->form11_date;
        $origin         = $request->form11_city;
        $destination    = user()->city_id;
        $type           = $request->type_11;

        $json = 'json/report-11';

        return view('operator.reports.report-11',compact('date','origin','destination','type','json')); 
    }

    public function report_12(Request $request){
        $date         = $request->form12_date;
        $from_branch  = $request->form12_1_branch;
        $to_branch    = $request->form12_2_branch;

        $json = 'json/report-12';

        return view('operator.reports.report-12',compact('date','from_branch','to_branch','json'));  
    }

    public function json_report_1($params){
        $city_id = user()->city_id;

        $arr        = explode('&', $params);
        $date       = $arr['0'];
        $branch_1   = $arr['1'];
        $branch_2   = $arr['2'];
        $option     = $arr['3'];

        if($branch_1 == 0 && $branch_2 != 0){
            if($option == 'print'){
                $ctfs = Package::join('ctf_packages as cp','cp.package_id','=','packages.id')
                    ->join('ctfs as c','c.id','=','cp.ctf_id')
                    ->join('users as u','u.id','=','c.user_id')
                    ->join('branches as b','b.id','=','u.branch_id')
                    ->select('c.id','c.ctf_no','c.created_at','c.completed','u.name as created_by','b.name as branch')
                    ->groupBy('c.id','c.ctf_no','c.created_at','c.completed','created_by','branch')
                    ->where('packages.to_branch_id',$branch_2)
                    ->get();
            }else{
                $ctfs = Package::join('ctf_packages as cp','cp.package_id','=','packages.id')
                    ->join('ctfs as c','c.id','=','cp.ctf_id')
                    ->join('users as u','u.id','=','c.user_id')
                    ->join('branches as b','b.id','=','u.branch_id')
                    ->select('c.id','c.ctf_no','c.created_at','c.completed','u.name as created_by','b.name as branch')
                    ->groupBy('c.id','c.ctf_no','c.created_at','c.completed','created_by','branch')
                    ->where('packages.to_branch_id',$branch_2)
                    ->paginate(50);
            }
        }elseif($branch_1 != 0 && $branch_2 == 0){
            if($option == 'print'){
                $ctfs = CTF::join('branches as b','b.id','=','ctfs.branch_id')
                    ->join('users as u','u.id','=','ctfs.user_id')
                    ->select('ctfs.created_at','ctfs.id','b.name as branch','ctfs.ctf_no','ctfs.completed','u.name as created_by')
                    ->where('ctfs.created_at','like','%'.$date.'%')
                    ->where('ctfs.branch_id',$branch_1)
                    ->get();
            }else{
                $ctfs = CTF::join('branches as b','b.id','=','ctfs.branch_id')
                    ->join('users as u','u.id','=','ctfs.user_id')
                    ->select('ctfs.created_at','ctfs.id','b.name as branch','ctfs.ctf_no','ctfs.completed','u.name as created_by')
                    ->where('ctfs.created_at','like','%'.$date.'%')
                    ->where('ctfs.branch_id',$branch_1)
                    ->paginate(50);
            }
        }elseif($branch_1 != 0 && $branch_2 != 0){
            //return 'all branch';
            if($option == 'print'){
                $ctfs = Package::join('ctf_packages as cp','cp.package_id','=','packages.id')
                    ->join('ctfs as c','c.id','=','cp.ctf_id')
                    ->join('users as u','u.id','=','c.user_id')
                    ->join('branches as b','b.id','=','u.branch_id')
                    ->select('c.id','c.ctf_no','c.created_at','c.completed','u.name as created_by','b.name as branch')
                    ->groupBy('c.id','c.ctf_no','c.created_at','c.completed','created_by','branch')
                    ->where('packages.from_branch_id',$branch_1)
                    ->where('packages.to_branch_id',$branch_2)
                    ->get();
            }else{
                $ctfs = Package::join('ctf_packages as cp','cp.package_id','=','packages.id')
                    ->join('ctfs as c','c.id','=','cp.ctf_id')
                    ->join('users as u','u.id','=','c.user_id')
                    ->join('branches as b','b.id','=','u.branch_id')
                    ->select('c.id','c.ctf_no','c.created_at','c.completed','u.name as created_by','b.name as branch')
                    ->groupBy('c.id','c.ctf_no','c.created_at','c.completed','created_by','branch')
                    ->where('packages.from_branch_id',$branch_1)
                    ->where('packages.to_branch_id',$branch_2)
                    ->paginate(50);
            }
        }else{
            //return 'all branch';
            if($option == 'print'){
                $ctfs = CTF::join('branches as b','b.id','=','ctfs.branch_id')
                    ->join('users as u','u.id','=','ctfs.user_id')
                    ->select('ctfs.created_at','ctfs.id','b.name as branch','ctfs.ctf_no','ctfs.completed','u.name as created_by')
                    ->where('ctfs.created_at','like','%'.$date.'%')
                    ->where('ctfs.city_id',$city_id)
                    ->get();
            }else{
                $ctfs = CTF::join('branches as b','b.id','=','ctfs.branch_id')
                    ->join('users as u','u.id','=','ctfs.user_id')
                    ->select('ctfs.created_at','ctfs.id','b.name as branch','ctfs.ctf_no','ctfs.completed','u.name as created_by')
                    ->where('ctfs.created_at','like','%'.$date.'%')
                    ->where('ctfs.city_id',$city_id)
                    ->paginate(50);
            }
        }

        return $ctfs;
    }

    public function json_report_2($params){
        $city_id = user()->city_id;

        $arr        = explode('&', $params);
        $date       = $arr['0'];
        $branch_1   = $arr['1'];
        $branch_2   = $arr['2'];
        $option     = $arr['3'];

        if($branch_1 == 0 && $branch_2 != 0){
            if($option == 'print'){
                $packages = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.to_branch_id',$branch_2)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id','<',6)
                    ->groupBy('p.id')
                    ->get();
            }else{
                $packages = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.to_branch_id',$branch_2)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id','<',6)
                    ->groupBy('p.id')
                    ->paginate(50);
            }
        }elseif($branch_1 != 0 && $branch_2 == 0){
            if($option == 'print'){
                $packages = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_branch_id',$branch_1)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id','<',6)
                    ->groupBy('p.id')
                    ->get();
            }else{
                $packages = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_branch_id',$branch_1)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id','<',6)
                    ->groupBy('p.id')
                    ->paginate(50);
            }
        }elseif($branch_1 != 0 && $branch_2 != 0){
            //return 'all branch';
            if($option == 'print'){
                $packages = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_branch_id',$branch_1)
                    ->where('p.to_branch_id',$branch_2)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id','<',6)
                    ->groupBy('p.id')
                    ->get();
            }else{
                $packages = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_branch_id',$branch_1)
                    ->where('p.to_branch_id',$branch_2)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id','<',6)
                    ->groupBy('p.id')
                    ->paginate(50);
            }
        }else{
            if($option == 'print'){
                $packages = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_city_id',$city_id)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id','<',6)
                    ->groupBy('p.id')
                    ->get();
            }else{
                $packages = PackageWaybill::join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_city_id',$city_id)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id','<',6)
                    ->groupBy('p.id')
                    ->paginate(50);
            }
        }

        return $packages;
    }

    public function json_report_3($params){
        $city_id = user()->city_id;

        $arr        = explode('&', $params);
        $date       = $arr['0'];
        $branch_1   = $arr['1'];
        $option     = $arr['2'];

        if($branch_1 != 0){
            if($option == 'print'){
                $waybills = Waybill::join('users as u','u.id','=','waybills.user_id')
                    ->join('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('waybills.active',1)
                    ->where('origin',$city_id)
                    ->where('u.branch_id',$branch_1)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at')
                    ->get();
            }else{
                $waybills = Waybill::join('users as u','u.id','=','waybills.user_id')
                    ->join('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('waybills.active',1)
                    ->where('origin',$city_id)
                    ->where('u.branch_id',$branch_1)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at')
                    ->paginate(200);
            }
        }else{
            if($option == 'print'){
                $waybills = Waybill::join('users as u','u.id','=','waybills.user_id')
                    ->join('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at')
                    ->get();
            }else{
                $waybills = Waybill::join('users as u','u.id','=','waybills.user_id')
                    ->join('branches as b','b.id','=','u.branch_id')
                    ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point')
                    ->where('waybills.active',1)
                    ->where('origin',$city_id)
                    ->whereDate('waybills.created_at','like',$date.'%')
                    ->orderBy('waybills.created_at')
                    ->paginate(200);
            }
        }

        return $waybills;
    }

    public function json_report_4($params){
        $arr            = explode('&', $params);
        $date           = $arr['0'];
        $origin         = $arr['1'];
        $destination    = $arr['2'];
        $option         = $arr['3'];

        if($destination == 0){
            $primary    = DB::table('ctfs')->where('city_id',$origin)->where('action_id',6)->whereDate('ctfs.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_ctfs')->where('city_id',$origin)->where('action_id',6)->whereDate('bk_ctfs.created_at','like',$date.'%')->count();
        }else{
            $primary    = DB::table('ctfs')->where('city_id',$origin)->where('to_city_id',$destination)->where('action_id',6)->whereDate('ctfs.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_ctfs')->where('city_id',$origin)->where('to_city_id',$destination)->where('action_id',6)->whereDate('bk_ctfs.created_at','like',$date.'%')->count();
        }

        if($primary > 0){
            if($option == 'print'){
                if($destination == 0){
                    $ctfs = CTF::join('cities as c1','c1.id','=','ctfs.city_id')
                        ->join('cities as c2','c2.id','=','ctfs.to_city_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','c1.shortcode as origin','c2.shortcode as destination','ctfs.completed')
                        ->where('ctfs.created_at','like','%'.$date.'%')
                        ->where('ctfs.city_id',$origin)
                        ->where('ctfs.action_id',6)
                        ->get();
                }else{
                    $ctfs = CTF::join('cities as c1','c1.id','=','ctfs.city_id')
                        ->join('cities as c2','c2.id','=','ctfs.to_city_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','c1.shortcode as origin','c2.shortcode as destination','ctfs.completed')
                        ->where('ctfs.created_at','like','%'.$date.'%')
                        ->where('ctfs.city_id',$origin)
                        ->where('ctfs.to_city_id',$destination)
                        ->where('ctfs.action_id',6)
                        ->get();
                }
            }else{
                if($destination == 0){
                    $ctfs = CTF::join('cities as c1','c1.id','=','ctfs.city_id')
                        ->join('cities as c2','c2.id','=','ctfs.to_city_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','c1.shortcode as origin','c2.shortcode as destination','ctfs.completed')
                        ->where('ctfs.created_at','like','%'.$date.'%')
                        ->where('ctfs.city_id',$origin)
                        ->where('ctfs.action_id',6)
                        ->paginate(50);
                }else{
                    $ctfs = CTF::join('cities as c1','c1.id','=','ctfs.city_id')
                        ->join('cities as c2','c2.id','=','ctfs.to_city_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','c1.shortcode as origin','c2.shortcode as destination','ctfs.completed')
                        ->where('ctfs.created_at','like','%'.$date.'%')
                        ->where('ctfs.city_id',$origin)
                        ->where('ctfs.to_city_id',$destination)
                        ->where('ctfs.action_id',6)
                        ->paginate(50);
                }
            }
        }elseif($secondary > 0){
            if($option == 'print'){
                if($destination == 0){
                    
                }else{
                    
                }
            }else{
                if($destination == 0){
                    
                }else{
                    
                }
            }
        }else{
            $ctfs = array();
        }

        return $ctfs;
    }

    public function json_report_5($params){
        $arr            = explode('&', $params);
        $date           = $arr['0'];
        $origin         = $arr['1'];
        $destination    = $arr['2'];
        $option         = $arr['3'];

        if($destination == 0){
            $primary    = DB::table('packages')->where('from_city_id',$origin)->where('action_id',5)->whereDate('packages.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_packages')->where('from_city_id',$origin)->where('action_id',5)->whereDate('bk_packages.created_at','like',$date.'%')->count();
        }else{
            $primary    = DB::table('packages')->where('from_city_id',$origin)->where('to_city_id',$destination)->where('action_id',5)->whereDate('packages.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_packages')->where('from_city_id',$origin)->where('to_city_id',$destination)->where('action_id',5)->whereDate('bk_packages.created_at','like',$date.'%')->count();
        }

        if($primary > 0){
            if($option == 'print'){
                if($destination == 0){
                    $packages = Package::join('users as u','u.id','=','packages.user_id')
                        ->join('branches as b','b.id','=','u.branch_id')
                        ->join('package_types as pt','pt.id','=','packages.package_type_id')
                        ->join('cities as c','c.id','=','packages.to_city_id')
                        ->select('packages.package_no','packages.created_at','packages.completed','u.name as created_by','b.name as branch','c.shortcode as destination','pt.name as package_type')
                        ->where('packages.from_city_id',$origin)
                        ->where('packages.created_at','like','%'.$date.'%')
                        ->get();
                }else{
                    $packages = Package::join('users as u','u.id','=','packages.user_id')
                        ->join('branches as b','b.id','=','u.branch_id')
                        ->join('package_types as pt','pt.id','=','packages.package_type_id')
                        ->join('cities as c','c.id','=','packages.to_city_id')
                        ->select('packages.package_no','packages.created_at','packages.completed','u.name as created_by','b.name as branch','c.shortcode as destination','pt.name as package_type')
                        ->where('packages.from_city_id',$origin)
                        ->where('packages.to_city_id',$destination)
                        ->where('packages.created_at','like','%'.$date.'%')
                        ->get();
                }
            }else{
                if($destination == 0){
                    $packages = Package::join('users as u','u.id','=','packages.user_id')
                        ->join('branches as b','b.id','=','u.branch_id')
                        ->join('package_types as pt','pt.id','=','packages.package_type_id')
                        ->join('cities as c','c.id','=','packages.to_city_id')
                        ->select('packages.package_no','packages.created_at','packages.completed','u.name as created_by','b.name as branch','c.shortcode as destination','pt.name as package_type')
                        ->where('packages.from_city_id',$origin)
                        ->where('packages.created_at','like','%'.$date.'%')
                        ->paginate(100);
                }else{
                    $packages = Package::join('users as u','u.id','=','packages.user_id')
                        ->join('branches as b','b.id','=','u.branch_id')
                        ->join('package_types as pt','pt.id','=','packages.package_type_id')
                        ->join('cities as c','c.id','=','packages.to_city_id')
                        ->select('packages.package_no','packages.created_at','packages.completed','u.name as created_by','b.name as branch','c.shortcode as destination','pt.name as package_type')
                        ->where('packages.from_city_id',$origin)
                        ->where('packages.to_city_id',$destination)
                        ->where('packages.created_at','like','%'.$date.'%')
                        ->paginate(100);
                }
            }
        }elseif($secondary > 0){
            if($option == 'print'){
                if($destination == 0){
                    
                }else{
                    
                }
            }else{
                if($destination == 0){
                    
                }else{
                    
                }
            }
        }else{
            $packages = array();
        }

        return $packages;
    }

    public function json_report_6($params){
        $arr            = explode('&', $params);
        $date           = $arr['0'];
        $origin         = $arr['1'];
        $destination    = $arr['2'];
        $option         = $arr['3'];

        if($destination == 0){
            $primary    = DB::table('waybills')->where('action_id','>=',6)->where('waybills.active',1)->where('origin',$origin)->whereDate('waybills.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_waybills')->where('action_id','>=',6)->where('bk_waybills.active',1)->where('origin',$origin)->whereDate('bk_waybills.created_at','like',$date.'%')->count();
        }else{
            $primary    = DB::table('waybills')->where('action_id','>=',6)->where('waybills.active',1)->where('origin',$origin)->where('destination',$destination)->whereDate('waybills.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_waybills')->where('action_id','>=',6)->where('bk_waybills.active',1)->where('origin',$origin)->where('destination',$destination)->whereDate('bk_waybills.created_at','like',$date.'%')->count();
        }
        
        
        if($primary > 0){
            if($option == 'print'){
                if($destination == 0){
                    $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                        ->leftjoin('cities as c1','c1.id','=','waybills.origin')
                        ->leftjoin('cities as c2','c2.id','=','waybills.destination')
                        ->select('waybills.created_at','waybills.id','waybills.waybill_no','u.name as courier','action_id','qty','c1.shortcode as origin','c2.shortcode as destination','waybills.end_point')
                        ->where('action_id','>=',6)
                        ->where('waybills.active',1)
                        ->where('origin',$origin)
                        ->whereDate('waybills.created_at','like',$date.'%')
                        ->orderBy('waybills.created_at','desc')
                        ->get();
                }else{
                    $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                        ->leftjoin('cities as c1','c1.id','=','waybills.origin')
                        ->leftjoin('cities as c2','c2.id','=','waybills.destination')
                        ->select('waybills.created_at','waybills.id','waybills.waybill_no','u.name as courier','action_id','qty','c1.shortcode as origin','c2.shortcode as destination','waybills.end_point')
                        ->where('action_id','>=',6)
                        ->where('waybills.active',1)
                        ->where('origin',$origin)
                        ->where('destination',$destination)
                        ->whereDate('waybills.created_at','like',$date.'%')
                        ->orderBy('waybills.created_at','desc')
                        ->get();
                }
            }else{
                if($destination == 0){
                    $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                        ->leftjoin('cities as c1','c1.id','=','waybills.origin')
                        ->leftjoin('cities as c2','c2.id','=','waybills.destination')
                        ->select('waybills.created_at','waybills.id','waybills.waybill_no','u.name as courier','action_id','qty','c1.shortcode as origin','c2.shortcode as destination','waybills.end_point')
                        ->where('action_id','>=',6)
                        ->where('waybills.active',1)
                        ->where('origin',$origin)
                        ->whereDate('waybills.created_at','like',$date.'%')
                        ->orderBy('waybills.created_at','desc')
                        ->paginate(100);
                }else{
                    $waybills = DB::table('waybills')->leftjoin('users as u','u.id','=','waybills.user_id')
                        ->leftjoin('cities as c1','c1.id','=','waybills.origin')
                        ->leftjoin('cities as c2','c2.id','=','waybills.destination')
                        ->select('waybills.created_at','waybills.id','waybills.waybill_no','u.name as courier','action_id','qty','c1.shortcode as origin','c2.shortcode as destination','waybills.end_point')
                        ->where('action_id','>=',6)
                        ->where('waybills.active',1)
                        ->where('origin',$origin)
                        ->where('destination',$destination)
                        ->whereDate('waybills.created_at','like',$date.'%')
                        ->orderBy('waybills.created_at','desc')
                        ->paginate(100);
                }
            }
        }elseif($secondary > 0){
            if($option == 'print'){
                if($destination == 0){
                    $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                        ->leftjoin('branches as b','b.id','=','u.branch_id')
                        ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','u.name as courier','action_id','qty','b.name as branch','bk_waybills.end_point')
                        ->where('action_id','>=',6)
                        ->where('bk_waybills.active',1)
                        ->where('origin',$origin)
                        ->whereDate('bk_waybills.created_at','like',$date.'%')
                        ->orderBy('bk_waybills.created_at','desc')
                        ->paginate(100);
                }else{
                    $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                        ->leftjoin('branches as b','b.id','=','u.branch_id')
                        ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','u.name as courier','action_id','qty','b.name as branch','bk_waybills.end_point')
                        ->where('action_id','>=',6)
                        ->where('bk_waybills.active',1)
                        ->where('origin',$origin)
                        ->where('destination',$destination)
                        ->whereDate('bk_waybills.created_at','like',$date.'%')
                        ->orderBy('bk_waybills.created_at','desc')
                        ->paginate(100);
                }
            }else{
                if($destination == 0){
                    $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                        ->leftjoin('branches as b','b.id','=','u.branch_id')
                        ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','u.name as courier','action_id','qty','b.name as branch','bk_waybills.end_point')
                        ->where('action_id','>=',6)
                        ->where('bk_waybills.active',1)
                        ->where('origin',$origin)
                        ->whereDate('bk_waybills.created_at','like',$date.'%')
                        ->orderBy('bk_waybills.created_at','desc')
                        ->get();
                }else{
                    $waybills = DB::table('bk_waybills')->leftjoin('users as u','u.id','=','bk_waybills.user_id')
                        ->leftjoin('branches as b','b.id','=','u.branch_id')
                        ->select('bk_waybills.created_at','bk_waybills.id','bk_waybills.waybill_no','u.name as courier','action_id','qty','b.name as branch','bk_waybills.end_point')
                        ->where('action_id','>=',6)
                        ->where('bk_waybills.active',1)
                        ->where('origin',$origin)
                        ->where('destination',$destination)
                        ->whereDate('bk_waybills.created_at','like',$date.'%')
                        ->orderBy('bk_waybills.created_at','desc')
                        ->get();
                }
            }
        }else{
            $waybills = array();
        }

        return $waybills;
    }

    public function json_report_7($params){
        $arr            = explode('&', $params);
        $date           = $arr['0'];
        $origin         = $arr['1'];
        $destination    = $arr['2'];
        $option         = $arr['3'];

        if($origin == 0){
            $primary    = DB::table('ctfs')->where('to_city_id',$destination)->where('action_id',6)->whereDate('ctfs.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_ctfs')->where('to_city_id',$destination)->where('action_id',6)->whereDate('bk_ctfs.created_at','like',$date.'%')->count();
        }else{
            $primary    = DB::table('ctfs')->where('city_id',$origin)->where('to_city_id',$destination)->where('action_id',6)->whereDate('ctfs.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_ctfs')->where('city_id',$origin)->where('to_city_id',$destination)->where('action_id',6)->whereDate('bk_ctfs.created_at','like',$date.'%')->count();
        }

        if($primary > 0){
            if($option == 'print'){
                if($origin == 0){
                    $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                        ->join('packages as p','p.id','=','cp.package_id')
                        ->join('cities as c','c.id','=','p.from_city_id')
                        ->join('cities as c2','c2.id','=','p.to_city_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','c.shortcode as origin','c2.shortcode as destination',DB::raw('count(*) as total'))
                        ->where('p.to_city_id',$destination)
                        ->where('ctfs.action_id',6)
                        ->whereDate('ctfs.created_at','like',$date.'%')
                        ->groupBy('ctfs.id','ctfs.ctf_no','p.from_city_id','c.shortcode','p.to_city_id')
                        ->orderBy('ctfs.ctf_no','desc')
                        ->get();
                }else{
                    $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                        ->join('packages as p','p.id','=','cp.package_id')
                        ->join('cities as c','c.id','=','p.from_city_id')
                        ->join('cities as c2','c2.id','=','p.to_city_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','c.shortcode as origin','c2.shortcode as destination',DB::raw('count(*) as total'))
                        ->where('p.from_city_id',$origin)
                        ->where('p.to_city_id',$destination)
                        ->where('ctfs.action_id',6)
                        ->whereDate('ctfs.created_at','like',$date.'%')
                        ->groupBy('ctfs.id','ctfs.ctf_no','p.from_city_id','c.shortcode','p.to_city_id')
                        ->orderBy('ctfs.ctf_no','desc')
                        ->get();
                }
            }else{
                if($origin == 0){
                    $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                        ->join('packages as p','p.id','=','cp.package_id')
                        ->join('cities as c','c.id','=','p.from_city_id')
                        ->join('cities as c2','c2.id','=','p.to_city_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','c.shortcode as origin','c2.shortcode as destination',DB::raw('count(*) as total'))
                        ->where('p.to_city_id',$destination)
                        ->where('ctfs.action_id',6)
                        ->whereDate('ctfs.created_at','like',$date.'%')
                        ->groupBy('ctfs.id','ctfs.ctf_no','p.from_city_id','c.shortcode','p.to_city_id')
                        ->orderBy('ctfs.ctf_no','desc')
                        ->paginate(100);
                }else{
                    $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                        ->join('packages as p','p.id','=','cp.package_id')
                        ->join('cities as c','c.id','=','p.from_city_id')
                        ->join('cities as c2','c2.id','=','p.to_city_id')
                        ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','c.shortcode as origin','c2.shortcode as destination',DB::raw('count(*) as total'))
                        ->where('p.from_city_id',$origin)
                        ->where('p.to_city_id',$destination)
                        ->where('ctfs.action_id',6)
                        ->whereDate('ctfs.created_at','like',$date.'%')
                        ->groupBy('ctfs.id','ctfs.ctf_no','p.from_city_id','c.shortcode','p.to_city_id')
                        ->orderBy('ctfs.ctf_no','desc')
                        ->paginate(100);
                }
            }
        }elseif($secondary > 0){
            if($option == 'print'){
                if($destination == 0){
                    
                }else{
                    
                }
            }else{
                if($destination == 0){
                    
                }else{
                    
                }
            }
        }else{
            $ctfs = array();
        }

        return $ctfs;
    }

    public function json_report_8($params){
        $city_id        = user()->city_id;
        $arr            = explode('&', $params);
        $date           = $arr['0'];
        $from_branch    = $arr['1'];
        $to_branch      = $arr['2'];
        $option         = $arr['3'];

        if($to_branch == 0 && $to_branch == 0){
            $primary    = DB::table('ctfs')->where('city_id',$city_id)->where('action_id',9)->whereDate('ctfs.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_ctfs')->where('city_id',$city_id)->where('action_id',9)->whereDate('bk_ctfs.created_at','like',$date.'%')->count();
        }else{
            $primary    = DB::table('ctfs')->where('branch_id',$from_branch)->where('to_branch_id',$to_branch)->where('action_id',9)->whereDate('ctfs.created_at','like',$date.'%')->count();
            $secondary  = DB::table('bk_ctfs')->where('branch_id',$from_branch)->where('to_branch_id',$to_branch)->where('action_id',9)->whereDate('bk_ctfs.created_at','like',$date.'%')->count();
        }

        if($primary > 0){
            if($option == 'print'){
                if($from_branch == 0){
                    if($to_branch == 0){
                        $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                            ->join('packages as p','p.id','=','cp.package_id')
                            ->join('branches as b','b.id','=','p.from_branch_id')
                            ->join('branches as b2','b2.id','=','p.to_branch_id')
                            ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','b.name as from_branch','b2.name as to_branch',DB::raw('count(*) as total'))
                            ->where('ctfs.city_id',$city_id)
                            ->where('ctfs.to_city_id',$city_id)
                            ->where('ctfs.action_id',9)
                            ->whereDate('ctfs.created_at','like',$date.'%')
                            ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','b2.name')
                            ->orderBy('ctfs.ctf_no','desc')
                            ->get();
                    }else{
                        $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                            ->join('packages as p','p.id','=','cp.package_id')
                            ->join('branches as b','b.id','=','p.from_branch_id')
                            ->join('branches as b2','b2.id','=','p.to_branch_id')
                            ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','b.name as from_branch','b2.name as to_branch',DB::raw('count(*) as total'))
                            ->where('p.to_city_id',$city_id)
                            ->where('p.to_branch_id',$to_branch)
                            ->where('ctfs.action_id',9)
                            ->whereDate('ctfs.created_at','like',$date.'%')
                            ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','b2.name')
                            ->orderBy('ctfs.ctf_no','desc')
                            ->get();
                    }  
                }else{
                    if($to_branch == 0){
                        $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                            ->join('packages as p','p.id','=','cp.package_id')
                            ->join('branches as b','b.id','=','p.from_branch_id')
                            ->join('branches as b2','b2.id','=','p.to_branch_id')
                            ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','b.name as from_branch','b2.name as to_branch',DB::raw('count(*) as total'))
                            ->where('p.from_branch_id',$from_branch)
                            ->where('ctfs.action_id',9)
                            ->whereDate('ctfs.created_at','like',$date.'%')
                            ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','b2.name')
                            ->orderBy('ctfs.ctf_no','desc')
                            ->get();
                    }else{
                        $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                            ->join('packages as p','p.id','=','cp.package_id')
                            ->join('branches as b','b.id','=','p.from_branch_id')
                            ->join('branches as b2','b2.id','=','p.to_branch_id')
                            ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','b.name as from_branch','b2.name as to_branch',DB::raw('count(*) as total'))
                            ->where('p.from_branch_id',$from_branch)
                            ->where('p.to_branch_id',$to_branch)
                            ->where('ctfs.action_id',9)
                            ->whereDate('ctfs.created_at','like',$date.'%')
                            ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','b2.name')
                            ->orderBy('ctfs.ctf_no','desc')
                            ->get();
                    }
                }
                
            }else{
                if($from_branch == 0){
                    if($to_branch == 0){
                        $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                            ->join('packages as p','p.id','=','cp.package_id')
                            ->join('branches as b','b.id','=','p.from_branch_id')
                            ->join('branches as b2','b2.id','=','p.to_branch_id')
                            ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','b.name as from_branch','b2.name as to_branch',DB::raw('count(*) as total'))
                            ->where('ctfs.city_id',$city_id)
                            ->where('ctfs.to_city_id',$city_id)
                            ->where('ctfs.action_id',9)
                            ->whereDate('ctfs.created_at','like',$date.'%')
                            ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','b2.name')
                            ->orderBy('ctfs.ctf_no','desc')
                            ->paginate(100);
                    }else{
                        $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                            ->join('packages as p','p.id','=','cp.package_id')
                            ->join('branches as b','b.id','=','p.from_branch_id')
                            ->join('branches as b2','b2.id','=','p.to_branch_id')
                            ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','b.name as from_branch','b2.name as to_branch',DB::raw('count(*) as total'))
                            ->where('p.to_city_id',$city_id)
                            ->where('p.to_branch_id',$to_branch)
                            ->where('ctfs.action_id',9)
                            ->whereDate('ctfs.created_at','like',$date.'%')
                            ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','b2.name')
                            ->orderBy('ctfs.ctf_no','desc')
                            ->paginate(100);
                    }  
                }else{
                    if($to_branch == 0){
                        $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                            ->join('packages as p','p.id','=','cp.package_id')
                            ->join('branches as b','b.id','=','p.from_branch_id')
                            ->join('branches as b2','b2.id','=','p.to_branch_id')
                            ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','b.name as from_branch','b2.name as to_branch',DB::raw('count(*) as total'))
                            ->where('p.from_branch_id',$from_branch)
                            ->where('ctfs.action_id',9)
                            ->whereDate('ctfs.created_at','like',$date.'%')
                            ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','b2.name')
                            ->orderBy('ctfs.ctf_no','desc')
                            ->paginate(100);
                    }else{
                        $ctfs = CTF::join('ctf_packages as cp','cp.ctf_id','=','ctfs.id')
                            ->join('packages as p','p.id','=','cp.package_id')
                            ->join('branches as b','b.id','=','p.from_branch_id')
                            ->join('branches as b2','b2.id','=','p.to_branch_id')
                            ->select('ctfs.created_at','ctfs.id','ctfs.ctf_no','ctfs.completed_at','b.name as from_branch','b2.name as to_branch',DB::raw('count(*) as total'))
                            ->where('p.from_branch_id',$from_branch)
                            ->where('p.to_branch_id',$to_branch)
                            ->where('ctfs.action_id',9)
                            ->whereDate('ctfs.created_at','like',$date.'%')
                            ->groupBy('ctfs.id','ctfs.ctf_no','p.from_branch_id','b.name','b2.name')
                            ->orderBy('ctfs.ctf_no','desc')
                            ->paginate(100);
                    }
                }
            }
        }elseif($secondary > 0){
            if($option == 'print'){
                if($destination == 0){
                    
                }else{
                    
                }
            }else{
                if($destination == 0){
                    
                }else{
                    
                }
            }
        }else{
            $ctfs = array();
        }

        return $ctfs;
    }

    public function json_report_9($params){
        $city_id = user()->city_id;
    }

    public function json_report_10($params){
        $city_id    = user()->city_id;
        $arr        = explode('&', $params);
        $date       = $arr['0'];
        $branch_1   = $arr['1'];
        $branch_2   = $arr['2'];
        $option     = $arr['3'];

        if($branch_1 == 0 && $branch_2 != 0){
            if($option == 'print'){
                $packages = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.to_branch_id',$branch_2)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id',8)
                    ->groupBy('p.id')
                    ->get();
            }else{
                $packages = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.to_branch_id',$branch_2)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id',8)
                    ->groupBy('p.id')
                    ->paginate(50);
            }
        }elseif($branch_1 != 0 && $branch_2 == 0){
            if($option == 'print'){
                $packages = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_branch_id',$branch_1)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id',8)
                    ->groupBy('p.id')
                    ->get();
            }else{
                $packages = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_branch_id',$branch_1)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id',8)
                    ->groupBy('p.id')
                    ->paginate(50);
            }
        }elseif($branch_1 != 0 && $branch_2 != 0){
            if($option == 'print'){
                $packages = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_branch_id',$branch_1)
                    ->where('p.to_branch_id',$branch_2)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id',8)
                    ->groupBy('p.id')
                    ->get();
            }else{
                $packages = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.from_branch_id',$branch_1)
                    ->where('p.to_branch_id',$branch_2)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id',8)
                    ->groupBy('p.id')
                    ->paginate(50);
            }
        }else{
            if($option == 'print'){
                $packages = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.to_city_id',$city_id)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id',8)
                    ->groupBy('p.id')
                    ->get();
            }else{
                $packages = DB::table('package_waybills')->join('packages as p','p.id','=','package_waybills.package_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->join('branches as b1','b1.id','=','p.from_branch_id')
                    ->join('branches as b2','b2.id','=','p.to_branch_id')
                    ->join('package_types as pt','pt.id','=','p.package_type_id')
                    ->select('p.created_at','p.package_no','p.completed','b1.name as from_branch','b2.name as to_branch','u.name as created_by','pt.name as package_type',DB::raw('p.id,COUNT(*) as qty'))
                    ->where('p.to_city_id',$city_id)
                    ->where('p.created_at','like','%'.$date.'%')
                    ->where('p.action_id',8)
                    ->groupBy('p.id')
                    ->paginate(50);
            }
        }

        return $packages;
    }

    public function json_report_11($params){
        $arr        = explode('&', $params);
        $date       = $arr['0'];
        $origin     = $arr['1'];
        $destination= $arr['2'];
        $type       = $arr['3'];
        $option     = $arr['4'];
        $service_point = check_service_point($destination);

        if($option == 'print'){
            if($origin == 0){
                if($service_point == 1){
                    $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                        ->join('cities as c','c.id','=','waybills.origin')
                        ->join('users as u','u.id','=','a.user_id')
                        ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                        ->where('waybills.active',1)
                        ->where('waybills.destination',$destination)
                        ->where('a.action_id',10)
                        ->whereDate('a.created_at','like',$date.'%')
                        ->orderBy('a.created_at')
                        ->get();
                }else{
                    if($type == 1){
                        $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                            ->join('cities as c','c.id','=','waybills.origin')
                            ->join('users as u','u.id','=','a.user_id')
                            ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                            ->where('waybills.active',1)
                            ->where('waybills.destination',$destination)
                            ->where('a.action_id',7)
                            ->whereDate('a.created_at','like',$date.'%')
                            ->orderBy('a.created_at')
                            ->get();
                    }else{
                        $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                            ->join('cities as c','c.id','=','waybills.origin')
                            ->join('users as u','u.id','=','a.user_id')
                            ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                            ->where('waybills.active',1)
                            ->where('waybills.transit_1',$destination)
                            ->where('a.action_id',7)
                            ->whereDate('a.created_at','like',$date.'%')
                            ->orderBy('a.created_at')
                            ->get();
                    }
                }
            }else{
                if($service_point == 1){
                    $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                        ->join('cities as c','c.id','=','waybills.origin')
                        ->join('users as u','u.id','=','a.user_id')
                        ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                        ->where('waybills.active',1)
                        ->where('waybills.origin',$origin)
                        ->where('waybills.destination',$destination)
                        ->where('a.action_id',10)
                        ->whereDate('a.created_at','like',$date.'%')
                        ->orderBy('a.created_at')
                        ->get();
                }else{
                    if($type == 1){
                        $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                            ->join('cities as c','c.id','=','waybills.origin')
                            ->join('users as u','u.id','=','a.user_id')
                            ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                            ->where('waybills.active',1)
                            ->where('waybills.origin',$origin)
                            ->where('waybills.destination',$destination)
                            ->where('a.action_id',7)
                            ->whereDate('a.created_at','like',$date.'%')
                            ->orderBy('a.created_at')
                            ->get();
                    }else{
                        $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                            ->join('cities as c','c.id','=','waybills.origin')
                            ->join('users as u','u.id','=','a.user_id')
                            ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                            ->where('waybills.active',1)
                            ->where('waybills.origin',$origin)
                            ->where('waybills.transit_1',$destination)
                            ->where('a.action_id',7)
                            ->whereDate('a.created_at','like',$date.'%')
                            ->orderBy('a.created_at')
                            ->get();
                    }
                }
            }
        }else{
            if($origin == 0){
                if($service_point == 1){
                    $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                        ->join('cities as c','c.id','=','waybills.origin')
                        ->join('users as u','u.id','=','a.user_id')
                        ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                        ->where('waybills.active',1)
                        ->where('waybills.destination',$destination)
                        ->where('a.action_id',10)
                        ->whereDate('a.created_at','like',$date.'%')
                        ->orderBy('a.created_at')
                        ->paginate(200);
                }else{
                    if($type == 1){
                        //destination
                        $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                            ->join('cities as c','c.id','=','waybills.origin')
                            ->join('users as u','u.id','=','a.user_id')
                            ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                            ->where('waybills.active',1)
                            ->where('waybills.destination',$destination)
                            ->where('a.action_id',7)
                            ->whereDate('a.created_at','like',$date.'%')
                            ->orderBy('a.created_at')
                            ->paginate(200);
                    }else{
                        //transit
                        $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                            ->join('cities as c','c.id','=','waybills.origin')
                            ->join('users as u','u.id','=','a.user_id')
                            ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                            ->where('waybills.active',1)
                            ->where('waybills.transit_1',$destination)
                            ->where('a.action_id',7)
                            ->whereDate('a.created_at','like',$date.'%')
                            ->orderBy('a.created_at')
                            ->paginate(200); 
                    }
                }
            }else{
                if($service_point == 1){
                    $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                        ->join('cities as c','c.id','=','waybills.origin')
                        ->join('users as u','u.id','=','a.user_id')
                        ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                        ->where('waybills.active',1)
                        ->where('waybills.origin',$origin)
                        ->where('waybills.destination',$destination)
                        ->where('a.action_id',10)
                        ->whereDate('a.created_at','like',$date.'%')
                        ->orderBy('a.created_at')
                        ->paginate(200);
                }else{
                    if($type == 1){
                        //destination
                        $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                            ->join('cities as c','c.id','=','waybills.origin')
                            ->join('users as u','u.id','=','a.user_id')
                            ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                            ->where('waybills.active',1)
                            ->where('waybills.origin',$origin)
                            ->where('waybills.destination',$destination)
                            ->where('a.action_id',7)
                            ->whereDate('a.created_at','like',$date.'%')
                            ->orderBy('a.created_at')
                            ->paginate(200);
                    }else{
                        //transit
                        $waybills = DB::table('waybills')->join('action_logs as a','a.waybill_id','=','waybills.id')
                            ->join('cities as c','c.id','=','waybills.origin')
                            ->join('users as u','u.id','=','a.user_id')
                            ->select('a.created_at','waybills.id','waybills.waybill_no','c.shortcode as origin','waybills.action_id','qty','waybills.end_point','u.name as action_by')
                            ->where('waybills.active',1)
                            ->where('waybills.origin',$origin)
                            ->where('waybills.transit_1',$destination)
                            ->where('a.action_id',7)
                            ->whereDate('a.created_at','like',$date.'%')
                            ->orderBy('a.created_at')
                            ->paginate(200);
                    }
                }
            }
        }

        return $waybills;
    }

    public function json_report_12($params){
        $arr            = explode('&', $params);
        $date           = $arr['0'];
        $from_branch    = $arr['1'];
        $to_branch      = $arr['2'];
        $option         = $arr['3'];

        if($option == 'print'){
            if($to_branch == 0){
                $waybills = DB::table('waybills')->join('package_waybills as pw','pw.waybill_id','=','waybills.id')
                    ->join('packages as p','p.id','=','pw.package_id')
                    ->join('branches as b','b.id','=','p.to_branch_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->select('p.created_at','waybills.id','waybills.waybill_no','waybills.action_id','b.name as to_branch','qty','waybills.end_point','u.name as action_by')
                    ->where('waybills.active',1)
                    ->where('p.from_branch_id',$from_branch)
                    ->where('p.action_id',8)
                    ->whereDate('p.created_at','like',$date.'%')
                    ->orderBy('waybills.waybill_no','asc')
                    ->get();
            }else{
                $waybills = DB::table('waybills')->join('package_waybills as pw','pw.waybill_id','=','waybills.id')
                    ->join('packages as p','p.id','=','pw.package_id')
                    ->join('branches as b','b.id','=','p.to_branch_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->select('p.created_at','waybills.id','waybills.waybill_no','waybills.action_id','b.name as to_branch','qty','waybills.end_point','u.name as action_by')
                    ->where('waybills.active',1)
                    ->where('p.from_branch_id',$from_branch)
                    ->where('p.to_branch_id',$to_branch)
                    ->where('p.action_id',8)
                    ->whereDate('p.created_at','like',$date.'%')
                    ->orderBy('waybills.waybill_no','asc')
                    ->get();
            }
            
        }else{
            if($to_branch == 0){
                $waybills = DB::table('waybills')->join('package_waybills as pw','pw.waybill_id','=','waybills.id')
                    ->join('packages as p','p.id','=','pw.package_id')
                    ->join('branches as b','b.id','=','p.to_branch_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->select('p.created_at','waybills.id','waybills.waybill_no','waybills.action_id','b.name as to_branch','qty','waybills.end_point','u.name as action_by')
                    ->where('waybills.active',1)
                    ->where('p.from_branch_id',$from_branch)
                    ->where('p.action_id',8)
                    ->whereDate('p.created_at','like',$date.'%')
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(200);
            }else{
                $waybills = DB::table('waybills')->join('package_waybills as pw','pw.waybill_id','=','waybills.id')
                    ->join('packages as p','p.id','=','pw.package_id')
                    ->join('branches as b','b.id','=','p.to_branch_id')
                    ->join('users as u','u.id','=','p.user_id')
                    ->select('p.created_at','waybills.id','waybills.waybill_no','waybills.action_id','b.name as to_branch','qty','waybills.end_point','u.name as action_by')
                    ->where('waybills.active',1)
                    ->where('p.from_branch_id',$from_branch)
                    ->where('p.to_branch_id',$to_branch)
                    ->where('p.action_id',8)
                    ->whereDate('p.created_at','like',$date.'%')
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(200);
            }
        }
        return $waybills;
    }

    public function json_report_shop($params){
        $city_id = user()->city_id;

        $arr        = explode('&', $params);
        $date       = $arr['0'];
        $option     = $arr['1'];

        
        if($option == 'print'){
            $waybills = DB::table('waybills')->join('users as u','u.id','=','waybills.user_id')
                ->join('branches as b','b.id','=','u.branch_id')
                ->join('weights as w','w.waybill_id','=','waybills.id')
                ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point','w.weight')
                ->where('waybills.active',1)
                ->where('origin',$city_id)
                ->where('u.branch_id',11)
                ->whereDate('waybills.created_at','like',$date.'%')
                ->orderBy('waybills.created_at')
                ->get();
        }else{
            $waybills = DB::table('waybills')->join('users as u','u.id','=','waybills.user_id')
                ->join('branches as b','b.id','=','u.branch_id')
                ->join('weights as w','w.waybill_id','=','waybills.id')
                ->select('waybills.created_at','waybills.id','waybills.waybill_no','same_day','u.name as courier','action_id','same_day','qty','b.name as branch','waybills.end_point','w.weight')
                ->where('waybills.active',1)
                ->where('origin',$city_id)
                ->where('u.branch_id',11)
                ->whereDate('waybills.created_at','like',$date.'%')
                ->orderBy('waybills.created_at')
                ->paginate(200);
        }
        
        return $waybills;
    }
}
