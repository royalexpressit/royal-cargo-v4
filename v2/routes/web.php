<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/last-id',function(){
	$last = App\Package::select('id')->orderBy('id','desc')->first();

	return $last;
});

Route::get('/','HomeController@dashboard');
/** Resource Controllers **/
Route::resource('/cities','CityController');
Route::resource('/users','UserController');
Route::resource('/reports','ReportController');
Route::resource('/branches','BranchController');

Route::get('/outbound','WaybillController@outbound');
Route::get('/inbound','WaybillController@inbound');
Route::get('/actions','WaybillController@scan');
Route::get('/scan/outbound','WaybillController@scan_outbound');
Route::get('/scan/inbound','WaybillController@scan_inbound');
Route::get('/scan/branch-out','WaybillController@scan_branch_out');
Route::get('/print/cft','WaybillController@print_qr');
Route::get('/search-data','WaybillController@search_data');
Route::get('/search-data/{param}','WaybillController@search_data_result')->name('search-data');
Route::get('/waybills/view/{id}','WaybillController@view_waybill');
Route::get('/outbound/status/{status}','WaybillController@outbound_status');
Route::get('/inbound/status/{status}','WaybillController@inbound_status');
Route::get('json/users/{sort}','UserController@json_users');
Route::get('json/cities','CityController@json_cities');
//Route::get('json/cities','CityController@json_cities');
Route::get('json/branches','BranchController@json_branches');
Route::get('json/outbound/status/{status}','WaybillController@json_outbound_status');
Route::get('json/inbound/status/{status}','WaybillController@json_inbound_status');
Route::get('/outbound/collected-by-courier','WaybillController@collected_by_courier');
Route::get('/outbound/collected-by-courier/{id}','WaybillController@view_collected_by_courier');
Route::get('/json/outbound/collected-by-courier','WaybillController@json_collected_by_courier');
Route::get('/json/outbound/collected-by-courier/{id}','WaybillController@json_view_collected_by_courier');
Route::get('/outbound/collected-by-branch/{id}','WaybillController@view_collected_by_branch');
Route::get('/json/outbound/collected-by-branch/{id}','WaybillController@json_view_collected_by_branch');

/*===== Start Package Route Collections =====*/
Route::get('/packages/view/{id}','WaybillController@view_package');
Route::get('outbound/package/{status}','PackageController@outbound_packages');
Route::get('inbound/package/{status}','PackageController@inbound_packages');
Route::get('outbound/package/view/{id}','PackageController@view_outbound_package');
Route::get('inbound/package/view/{id}','PackageController@view_inbound_package');
Route::get('json/outbound/package/view/{id}','PackageController@json_view_outbound_package');
Route::get('json/outbound/package/{status}','PackageController@json_outbound_packages');
Route::get('json/inbound/package/view/{id}','PackageController@json_view_inbound_package');
//Route::get('json/inbound/package/view/{id}','PackageController@json_view_outbound_package');
Route::get('json/inbound/package/{status}','PackageController@json_inbound_packages');
Route::get('incomplete-package/view/{package_id}','PackageController@view_incomplete_package');
Route::get('json/incomplete-package/view/{package_id}','PackageController@json_view_incomplete_package');
Route::get('/packages/changes-history','WaybillController@packages_history');
Route::get('/json/packages/changes-history','WaybillController@json_packages_history');
Route::get('/packages/changes-history/{id}','WaybillController@view_packages_history');
/*===== End Package Route Collections =====*/

/*===== Start CTF Route Collections =====*/
Route::get('ctfs/{status}','WaybillController@ctfs_status');
Route::get('json/ctfs/{status}','WaybillController@json_ctfs_status');
Route::get('json/fetched_ctf/{ctf_id}','WaybillController@json_fetched_ctf');
Route::get('incomplete-ctf/view/{ctf_id}','PackageController@view_incomplete_ctf');
Route::get('json/incomplete-ctf/view/{ctf_id}','PackageController@json_view_incomplete_ctf');
Route::get('outbound/ctf/{status}','PackageController@outbound_ctfs');
Route::get('inbound/ctf/{status}','PackageController@inbound_ctfs');
Route::get('json/outbound/ctf/{status}','PackageController@json_outbound_ctfs');
Route::get('json/outbound/ctf/{branch_id}/all','PackageController@json_outbound_ctfs_by_branch');
Route::get('json/inbound/ctf/{status}','PackageController@json_inbound_ctfs');
Route::get('/ctf/changes-history','WaybillController@ctfs_history');
Route::get('/json/ctf/changes-history','WaybillController@json_ctfs_history');
Route::get('/ctfs/view/{id}','WaybillController@view_ctf');
/*===== End CTF Route Collections =====*/

Route::get('sorting-staffs/outbound-statistics','WaybillController@outbound_statistics');
Route::get('action-logs','WaybillController@action_logs');
Route::get('json/action-logs','WaybillController@json_action_logs');



/*===== Start Setting Route Collections =====*/
Route::get('profile','UserController@profile');
Route::get('settings','UserController@settings');
Route::get('config-routes','CityController@config_routes');
Route::get('config-routes/create','CityController@create_routes');
Route::get('json/config-routes','CityController@json_config_routes');
Route::post('changed-date','HomeController@changed_date');
Route::get('sync-data/waybills','WaybillController@sync_waybills');
Route::get('sync-data/endpoint','WaybillController@sync_endpoint');
Route::get('json/sync-data/waybills','WaybillController@json_sync_waybills');
Route::get('json/sync-data/endpoint','WaybillController@json_sync_endpoint');
Route::get('/view-branches/{id}','BranchController@view_branches');
/*===== End Setting Route Collections =====*/


Route::post('report-1','ReportController@report_1')->name('report-1');
Route::post('report-2','ReportController@report_2')->name('report-2');
Route::post('report-3','ReportController@report_3')->name('report-3');
Route::post('report-4','ReportController@report_4')->name('report-4');
Route::post('report-5','ReportController@report_5')->name('report-5');
Route::post('report-6','ReportController@report_6')->name('report-6');
Route::post('report-7','ReportController@report_7')->name('report-7');
Route::post('report-8','ReportController@report_8')->name('report-8');
Route::post('report-9','ReportController@report_9')->name('report-9');
Route::post('report-10','ReportController@report_10')->name('report-10');
Route::post('report-11','ReportController@report_11')->name('report-11');
Route::post('report-12','ReportController@report_12')->name('report-12');
Route::post('shop-report','ReportController@report_shop')->name('report-shop');

Route::get('json/report-1/{params}','ReportController@json_report_1');
Route::get('json/report-2/{params}','ReportController@json_report_2');
Route::get('json/report-3/{params}','ReportController@json_report_3');
Route::get('json/report-4/{params}','ReportController@json_report_4');
Route::get('json/report-5/{params}','ReportController@json_report_5');
Route::get('json/report-6/{params}','ReportController@json_report_6');
Route::get('json/report-7/{params}','ReportController@json_report_7');
Route::get('json/report-8/{params}','ReportController@json_report_8');
Route::get('json/report-9/{params}','ReportController@json_report_9');
Route::get('json/report-10/{params}','ReportController@json_report_10');
Route::get('json/report-11/{params}','ReportController@json_report_11');
Route::get('json/report-12/{params}','ReportController@json_report_12');
Route::get('json/report-shop/{params}','ReportController@json_report_shop');
Route::get('jobs/synced-end-point','JobController@job_for_check_end_point');
Route::get('jobs/synced-waybill','JobController@job_for_waybill');

Route::get('search/{term}','WaybillController@search');
Route::get('delete/data','WaybillController@delete_data');
Route::post('deleted-data','WaybillController@deleted_data')->name('deleted-data');
Route::get('sync-data','WaybillController@sync_data');
Route::get('unknown/outbound','WaybillController@unknown_outbound');
Route::get('unknown/inbound','WaybillController@unknown_inbound');
Route::get('json/unknown/outbound','WaybillController@json_unknown_outbound');
Route::get('json/unknown/inbound','WaybillController@json_unknown_inbound');
Route::get('print/package/{package_no}','WaybillController@print_package');
Route::get('print/{package_no}/waybills','WaybillController@print_package_waybills');
Route::get('package-types','PackageController@package_types');
Route::get('package-info/{pkg_id}','PackageController@package_info');
Route::get('duplicate-waybills','PackageController@duplicate_waybills');
Route::get('json/duplicate-waybills','PackageController@json_duplicate_waybills');
Route::get('view-duplicate-waybills/{waybill_no}','PackageController@view_duplicate_waybills');
Route::post('delete-duplicate-waybills','PackageController@delete_duplicate_waybills');
Route::post('package/waybill-count','PackageController@waybill_count');
Route::post('delete-package-waybill','PackageController@delete_package_waybill');
Route::get('download/failed-logs','WaybillController@download_failed_logs');
Route::post('/create-route','CustomController@create_route');
