<?php

use Illuminate\Support\Facades\Route;

Auth::routes();
Route::get('/custom',function(){
	return synced_waybill_action_logs();
	//return 'work custom route file';
});


Route::get('/blogs/','CustomController@blogs');
Route::get('/user-guide','CustomController@user_guide');
Route::get('/user-guide/{slug}','CustomController@view_page');
Route::get('/run-query','CustomController@run_query');
Route::post('/run-query-result','CustomController@run_query_result')->name('run-query-result');


Route::get('/query-test/{waybill_no}',function($waybill_no){
	$response = array();
	$waybill = DB::table('waybills')->select('id','waybill_no')->where('waybill_no',$waybill_no)->first();

	if($waybill){
		DB::table('waybills')->where('waybill_no',$waybill_no)->update(['action_id'=>10]);
		DB::table('action_logs')->where('waybill_id',$waybill->id)->where('action_id',7)->update(['action_id'=>10]);
	
		$response['success'] = 1;
	}else{

		$response['success'] = 0;
	}
	return $response;
	

	//

	// $date = '04-11-2022';

	// $raw = DB::table('waybills')->leftjoin('action_logs as a','a.waybill_id','=','waybills.id')
	// ->select('waybills.waybill_no','waybills.origin','waybills.destination','a.user_id','a.created_at')
	// ->where('a.created_at','like',$date.'%')
	// ->where('waybills.destination',61)
	// ->get();
	
	// return $raw;
});

Route::get('/check-completed-pkg',function(){
	$pkgs = DB::table('package_waybills')
		->select('package_id','completed')
		->where('completed',1)
		->where('checked',0)
		->groupBy('package_id','completed')
		->limit(50)
		->orderBy('package_id','asc')
		->get();

	foreach($pkgs as $pkg){
		$count = DB::table('package_waybills')->select('package_id')->where('package_id',$pkg->package_id)->where('completed',0)->count();
	 	

	 	if($count == 0){
			//updated ctf_package
			DB::table('ctf_packages')->where('package_id',$pkg->package_id)->update(['completed'=>1]);
			
			$ctfs = DB::table('ctf_packages')->select('ctf_id')->where('package_id',$pkg->package_id)->get();
			
			foreach($ctfs as $ctf){
				//updated ctf
				$count = DB::table('ctf_packages')->select('ctf_id')->where('ctf_id',$ctf->ctf_id)->where('completed',0)->count();
				if($count == 0){
					DB::table('ctfs')->where('id',$ctf->ctf_id)->update(['completed'=>1,'completed_at'=> date('Y-m-d H:i:s')]);
				}
			}
		}

		DB::table('package_waybills')->where('package_id',$pkg->package_id)->update(['checked'=>1]);
	}

	return array('message' => 'working to check completed packages.');
});


Route::get('/check-end-point',function(){
	
	$waybills = array("MDY16959590718YGN", "YGN16959600718YGN");
	
	//$waybills = 'MDY16959590718YGN';
	$data = array(
		"waybill_numbers" 	=> '[\'MDY16959590718YGN\']'
	); 
	//$data_string = json_encode($data); 

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://uatdigital.royalx.biz/api/royal/waybill-destination',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		//CURLOPT_POSTFIELDS => array('waybill_numbers' => '[\'MDY16959590718YGN\']'),
		CURLOPT_POSTFIELDS => $data,
		CURLOPT_HTTPHEADER => array(
		    'access-token: access_token_4ccae8011028c43899f8c1871da7492e3cf65c5c',
		    'Cookie: session_id=321c6d1a15216eda5c0ded88477709541af6aa9b'
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$data = json_decode($response);

	return $data;
});


Route::get('/user-guides/{slug}',function(){
	

	return '<a href="'.url().'/_dist/videos/mobile/regional/01.Outbound.mp4">01.Outbound.mp4</a>';
});