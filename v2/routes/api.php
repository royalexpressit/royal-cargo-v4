<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/action/outbound','ApiController@outbound');
Route::post('/action/inbound','ApiController@inbound');
Route::post('/action/branch-out','ApiController@branch_out');
Route::post('/action/branch-in','ApiController@branch_in');
Route::post('/generate-ctf','ApiController@generate_ctf');
Route::post('/fetched-ctf-waybills','ApiController@fetched_ctf_waybills');
Route::post('/removed-waybill','ApiController@removed_waybill');
Route::post('/cancelled-waybill','ApiController@cancelled_waybill');
//Route::get('/fetched-package/{branch_id}/{status}','ApiController@fetched_package_by_branch');
Route::post('/fetched-ctf','ApiController@fetched_ctf');
Route::post('/fetched-package-waybills','ApiController@fetched_package_waybills');




Route::post('/login','ApiController@login');
Route::post('/action/ctf-export','ApiController@ctf_export');

Route::post('/fetched-package-types','ApiController@fetched_package_types');
Route::post('/fetched-inbound-ctfs','ApiController@fetched_inbound_ctfs');
Route::post('/fetched-cities','ApiController@fetched_cities');
Route::post('/fetched-branches','ApiController@fetched_branches');
Route::post('/fetched-couriers','ApiController@fetched_couriers');
Route::post('/fetched-routes','ApiController@fetched_routes');

Route::post('/change-city-and-branch','ApiController@change_city_and_branch');
Route::post('/fetched-outbound-ctfs','ApiController@fetched_outbound_ctfs');
//Route::post('/fetched-inbound-ctfs','ApiController@fetched_inbound_ctfs');
Route::post('/fetched-outbound-packages','ApiController@fetched_outbound_packages');
Route::post('/fetched-inbound-packages','ApiController@fetched_inbound_packages');
Route::post('/fetched-ctf-packages','ApiController@fetched_ctf_packages');
Route::post('/users/create','ApiController@created_user');
Route::post('/users/update','ApiController@updated_user');
Route::post('/users/reset-password','ApiController@reset_password');
Route::post('/cities/create','ApiController@created_city');
Route::post('/branch/create','ApiController@created_branch');

Route::post('/check-rejected-branch','ApiController@check_rejected_branch');
Route::post('/check-waybills-package','ApiController@check_waybills_package');
Route::post('/check-packages-ctf','ApiController@check_packages_ctf');
Route::post('/change-ctf-info','ApiController@change_ctf_info');
Route::post('/change-package-info','ApiController@change_package_info');
Route::post('/change-route','ApiController@change_route');
Route::get('/search-routes/{city_id}','ApiController@search_routes');
Route::post('/deleted-data','ApiController@deleted_data');
Route::post('/check-end-point','ApiController@check_end_point');
Route::post('/view-waybill','ApiController@view_waybill');
Route::post('/change-rejected-team','ApiController@change_rejected_team');
Route::post('/change-password','ApiController@change_password');
Route::post('/unknown/outbound','ApiController@outbound_unknown');
Route::post('/unknown/inbound','ApiController@inbound_unknown');
Route::post('/fetched-cod','ApiController@fetched_cod');
Route::get('/feedbacks','ApiController@feedbacks');
Route::post('/submit-feedback','ApiController@submit_feedback');
Route::post('/overview','ApiController@overview');
Route::post('/fetched-end-point','ApiController@fetched_end_point');
Route::post('/waybills-status','ApiController@waybills_status');
Route::post('/packages-status','ApiController@packages_status');
Route::post('/ctf-status','ApiController@ctf_status');

Route::post('/settings/synced-backup-waybills','ApiController@synced_backup_waybills');
Route::post('/settings/synced-backup-packages','ApiController@synced_backup_packages');
Route::post('/fetched-completed-ctf-packages','ApiController@fetched_completed_ctf_packages');
Route::post('/fetched-completed-package-waybills','ApiController@fetched_completed_package_waybills');
Route::post('/synced-waybills','ApiController@synced_waybills');
Route::post('/synced-packages','ApiController@synced_packages');
Route::post('/view-package-waybills','ApiController@view_package_waybills');
Route::post('/marked-ctf','ApiController@marked_ctf');
Route::post('/manual-sync-logs','ApiController@manual_sync_logs');
