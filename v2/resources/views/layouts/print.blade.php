<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		CTF Number
	</title>
</head>

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">

<style type="text/css">
	body{
		margin: 20px;
		background: #ffffff !important;
	}
	.content{
		width: 800px;
		margin: 0 auto;
		border: 4px double #ddd;
		height: 400px;
	}
	.top-left,
	.top-right{
		width: 45%;
		float: left;
		padding: 4px;
	}
	.left-content,
	.right-content{
		width: 45%;
		float: left;
		padding: 4px;
	}
	h1{
		margin-top: 10px;
	}
	ul{
		padding: 0;
		margin-top: 10px;
	}
	li{
		height: 60px;
		list-style: none;
	}
	.label{
		display: block;
		font-size: 16px;
	}
	.text{
		display: block;
		font-size: 20px;
	}
	.sm-text{
		font-size: 18px;
	}
	.pt-10{
		padding-top: 10px;
	}
	.pt-20{
		padding-top: 20px;
	}
	img{
		padding: 10px 20px;
	}
	.text-right{
		text-align: right;
	}
	.box{
		width: 100%;
	}
	.w-50{
		width: 40%;
		float: left;
	}
	.package-item{
		display: block;
	}
	.btn-count,.btn-count-text{
		display: inline-block;
		width: 40px;
		height: 20px;
		line-height: 20px;
		border-radius: 4px;
		text-align: center;
		background: #ff5722;
    	color: #fff;
    	cursor: pointer;
	}
	.message{
		text-align: center;
		border: 2px dashed #ff5722;
		width: 800px;
		margin: 10px auto;
		height: 40px;
		line-height: 40px;
	}
	.btn-print{
		margin-top: 10px;
		background: #009688;
		color: #ffffff;
		border-width: 0;
		border-radius: 4px;
		padding: 4px 10px;
	}
	.total{
		margin-left: 60px;
	}
	#total{
		font-size: 20px;
	}
	.disabled{
		background: transparent !important;
		pointer-events: none;
		color: #6e6b7b;
	}
	@media print {
		.btn-count{
			display: inline-block;
			width: 40px;
			height: 20px;
			line-height: 20px;
			text-align: center;
	    	color: #6e6b7b;
		}
		.message,.btn-print{
			display: none;
		}
	}
</style>
<body>
	<div class="content">
		<div class="top-left">
			<h3 class="text-center">CTF</h3>
		</div>
		<div class="top-right">
			<h3 class="text-right">{{ $ctf->created_at }}</h3>		
		</div>

		<div class="left-content">
			<center>
				<img src="https://chart.apis.google.com/chart?cht=qr&chs=200x200&chld=L|0&chl={{ $qr }}">
			</center>
			<div class="text-center">
				<span class="label">Created By</span>
				<span class="text">{{ $ctf->exported_by }}</span>
			</div>
			<center>
				<button class="btn-print" onclick="window.print();">
					Print CTF
				</button>
			</center>
		</div>
		<div class="right-content">
			<ul>
				<li>
					<div class="w-50">
						<span class="label">CTF No.</span>
					<span class="text">{{ $ctf->ctf_no }}</span>
					</div>
					<div class="w-50">
						<span class="label">Package (qty)</span>
						<span class="text">{{ $qty }}</span>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="w-50">
							<span class="label pt-10">From</span>
							<span class="sm-text"><strong>{{ $ctf->from_city }}</strong>/{{ $ctf->from_branch }}</span>
						</div>
						<div class="w-50">
							<span class="label pt-10">To</span>
							<span class="sm-text"><strong>{{ $ctf->to_city }}</strong>/{{ $ctf->to_branch==''? '-':$ctf->to_branch }}</span>
						</div>
					</div>
				</li>
			</ul>
			<div>
				<h5>Package Lists <span class="total">Total Waybills: <span id="total">0</span> </span></h5>
				@foreach($packages as $package)
				<span class="package-item">{{ $package->package_no }} <span class="btn-count item-{{ $package->package_no }}" id="{{ $package->package_no }}">0</span> <small><em>({{ $package->name }})</em></small></span>
				@endforeach
			</div>
		</div>

	</div>
	<div class="message">
		Package no ဘေးတွင်ရှိသော <span class="btn-count-text">0</span> ကိုနှိပ်ပြီး package ထဲတွင်ပါသော waybill အရေအတွက်စစ်ပါ။
	</div>
	<input type="hidden" id="url" value="{{ url('') }}">
	<input type="hidden" id="_token" value="{{ csrf_token() }}">
	<script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
	<script src="{{ asset('/_dist/js/config.js') }}"></script>
	<script type="text/javascript">
		var total 		= 0;

		$('body').delegate(".btn-count","click",function () {
			var package_no 	= $(this).attr('id');
			var _token  	= $("#_token").val();
			
			$.ajax({
	            type: 'post',
	            url: url+'/package/waybill-count',
	            dataType:'json',
	            data: {
	                'package_no' 	:package_no,
	                '_token' 		: _token
	            },
	            success: function(data) {   
	            	//console.log(data);
	            	$(".item-"+package_no).addClass('disabled');
	            	$(".item-"+package_no).text('('+data+')');
	            	total += parseInt(data);

	            	$("#total").text(total);
	            }
	        });
	        
	    });
	</script>
</body>
</html>

