<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-white">အရေအတွက်ပြည့်သွားပါပြီ</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p class="font-size-16">Scanned ဖတ်ရန်အတွက် system မှ သတ်မှတ်ထားသော အရေအတွက်
                ၂၅ စောင်ပြည့်သွားပါပြီ။ လက်ရှိစာများအား Collected လုပ်ပြီးမှ Scanned ပြန်ဖတ်ပါ။</p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-center-loading" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-white">အသိပေးချက်</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <h4>လုပ်ဆောင်နေပါသည် .....</h4>
                <div class="spinner-grow text-warning m-1" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-success m-1" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-danger m-1" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-center-same-city" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-white">Skip To Branch In</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p class="font-size-16">{{ city(user()->city_id)['shortcode'] }}(From) - {{ city(user()->city_id)['shortcode'] }}(To)
                တူနေသောကြောင့် branch out လုပ်ပါက systemမှ အလိုအလျောက် branch in အဆင့်ထိ လုပ်သွားမည်ဖြစ်သည်။ </p>
            </div>
        </div>
    </div>
</div>

<div id="tones" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Select Tones</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                </button>
                                                            </div>
                                                            <div class="modal-body bg-grey" >
                                                                <ul class="list-group unstyled-list"> 
                                                                    <li class="audio-card">
                                                                        <label>Tone 1</label>
                                                                        <audio controls>
                                                                            <source src="{{ asset('dist/alerts/alert-1.mp3') }}" type="audio/mpeg">
                                                                        </audio>
                                                                        <button class="choice-voice btn btn-success btn-bold btn-md text-upper pull-right" value="alert-1.mp3">Set Tone</button>
                                                                    </li>
                                                                    <li class="audio-card">
                                                                        <label>Tone 1</label>
                                                                        <audio controls>
                                                                            <source src="{{ asset('dist/alerts/alert-2.mp3') }}" type="audio/mpeg">
                                                                        </audio>
                                                                        <button class="choice-voice btn btn-primary btn-bold btn-md text-upper pull-right" value="alert-2.mp3">Set Tone</button>
                                                                    </li>
                                                                    <li class="audio-card">
                                                                        <label>Tone 1</label>
                                                                        <audio controls>
                                                                            <source src="{{ asset('dist/alerts/alert-3.mp3') }}" type="audio/mpeg">
                                                                        </audio>
                                                                        <button class="choice-voice btn btn-primary btn-bold btn-md text-upper pull-right" value="alert-3.mp3">Set Tone</button>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-light waves-effect" data-bs-dismiss="modal">Close</button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->