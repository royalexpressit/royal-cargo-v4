<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-md-start d-block d-md-inline-block mt-25">COPYRIGHT  &copy; 2022<a class="ms-25" href="{{ url('version/release-notes') }}" target="_blank">Cargo App <small class="text-muted">(version 2.1.1)</small></a>
      	<span class="d-none d-sm-inline-block">, All rights Reserved <small class="text-warning">({{ custom() }})</small></span></span>
      	<span class="float-md-end d-none d-md-block">Royal Express IT<i data-feather="heart"></i></span>
    </p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
<!-- END: Footer-->
<input type="hidden" id="url" value="{{ url('') }}">
<input type="hidden" id="_token" value="{{ csrf_token() }}">

<!-- Modal -->
<div class="modal fade modal-danger text-start" id="danger" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="myModalLabel120">Change Dashboard Date</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>မိမိပြောင်းလဲလိုသော ရက်စွဲရွေးပါ။</p>
                <label class="form-label text-primary" for="select2-icons">Choose Date</label>
                <input type="text" name="ctf_date" id="config_date" class="form-control flatpickr-basic" value="{{ get_date() }}" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary set-date btn-set" data-bs-dismiss="modal">ပြောင်းမည်</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="feedback" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalFullTitle">Feedback From</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-1">
                                    <div class="row">
                                        <div class="col-md-6">
                                           <label class="form-label" for="comment">Username</label>
                                            <input type="text" class="form-control" rows="3" id="acc_name" value="{{ user()->name }}" readonly > 
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label" for="phone_no">Phone No</label>
                                            <input type="number" class="form-control" id="phone_no" rows="3" placeholder="09421098259" pattern="[0-9.]+" />
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1">
                                    <label class="form-label" for="comment">Feeback</label>
                                    <textarea class="form-control" id="comment" rows="5" placeholder="Enter feedback ..."></textarea>
                                </div>
                                <div class="mb-1">
                                    <button type="button" class="btn btn-primary submit-feedback disabled">ပို့မည်</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="mb-4">
                            <div class="card">
                                <h4 class="m-1">Feedbacks</h4>
                                <div class="alert alert-success alert-dismissible fade show mx-1 alert-feedback hide" role="alert">
                                    <div class="alert-body">
                                        You have been submitted feedback to support team.
                                    </div>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                                <div id="fetched-feedbacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start of Async Drift Code -->
<script>
"use strict";

!function() {
  var t = window.driftt = window.drift = window.driftt || [];
  if (!t.init) {
    if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
    t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
    t.factory = function(e) {
      return function() {
        var n = Array.prototype.slice.call(arguments);
        return n.unshift(e), t.push(n), t;
      };
    }, t.methods.forEach(function(e) {
      t[e] = t.factory(e);
    }), t.load = function(t) {
      var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
      o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
      var i = document.getElementsByTagName("script")[0];
      i.parentNode.insertBefore(o, i);
    };
  }
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('5m9xt49b8z66');
</script>
<!-- End of Async Drift Code -->