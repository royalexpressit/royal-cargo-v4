<nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center" data-nav="brand-center">
    <div class="navbar-header d-xl-block d-none">
        <ul class="nav navbar-nav">
          	<li class="nav-item">
          		<a class="navbar-brand" href="{{ url('') }}">
          			<span class="brand-logo">
                		<img src="{{ asset('_dist/images/logo.png') }}">
                	</span>
              		<h2 class="brand-text mb-0">Cargo</h2>
              	</a>
            </li>
        </ul>
    </div>

     <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
          	<ul class="nav navbar-nav d-xl-none">
            	<li class="nav-item">
            		<a class="nav-link menu-toggle" href="#">
            			<i class="ficon" data-feather="menu"></i>
            		</a>
            	</li>
          	</ul>
          	<ul class="nav navbar-nav bookmark-icons">
            	<li class="nav-item d-none d-lg-block">
              		<a class="nav-link" href="{{ url('actions') }}" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Scan waybill">
              			<svg class="icon text-danger" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
						  	<path stroke-linecap="round" stroke-linejoin="round" d="M12 4v1m6 11h2m-6 0h-2v4m0-11v3m0 0h.01M12 12h4.01M16 20h4M4 12h4m12 0h.01M5 8h2a1 1 0 001-1V5a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1zm12 0h2a1 1 0 001-1V5a1 1 0 00-1-1h-2a1 1 0 00-1 1v2a1 1 0 001 1zM5 20h2a1 1 0 001-1v-2a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1z" />
						</svg>
              		</a>
            	</li>
            	<li class="nav-item d-none d-lg-block">
              <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#danger" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Change Date">
                <svg class="icon text-danger" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
  <path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
</svg>
              </a></li>
              <li class="nav-item d-none d-lg-block">
              <span class="font-medium-5 text-danger">{{ get_date() }}</span>
              </li>
            </ul>
        </div>
        <ul class="nav navbar-nav align-items-center ms-auto">
          	<li class="nav-item dropdown dropdown-language">
	            <a class="btn btn-sm btn-danger waves-effect waves-float waves-light" >
	              	<i data-feather='mail'></i>
	              	<span class="selected-language">Feedback</span>
	            </a>
          	</li>
          	<li class="nav-item d-none d-lg-block">
          		<a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a>
          	</li>
          	<li class="nav-item nav-search"><a class="nav-link nav-link-search"><i class="ficon" data-feather="search"></i></a>
	            <div class="search-input">
	              	<div class="search-input-icon"><i data-feather="search"></i></div>
	              	<input class="form-control input" type="text" placeholder="Search waybill..." tabindex="-1" data-search="search">
	              	<div class="search-input-close"><i data-feather="x"></i></div>
	              	<ul class="search-list search-list-main"></ul>
	            </div>
          	</li>
          	<li class="nav-item dropdown dropdown-notification me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge rounded-pill bg-danger badge-up">5</span></a>
	            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
	              	<li class="dropdown-menu-header">
		                <div class="dropdown-header d-flex">
		                  	<h4 class="notification-title mb-0 me-auto">Notifications</h4>
		                  	<div class="badge rounded-pill badge-light-primary">6 New</div>
		                </div>
	              	</li>
	              	<li class="scrollable-container media-list">
	              		<a class="d-flex" href="#">
		                  	<div class="list-item d-flex align-items-start">
		                    	<div class="me-1">
		                      		<div class="avatar"><img src="../../../app-assets/images/portrait/small/avatar-s-15.jpg" alt="avatar" width="32" height="32"></div>
		                    	</div>
		                    	<div class="list-item-body flex-grow-1">
		                      		<p class="media-heading"><span class="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small class="notification-text"> Won the monthly best seller badge.</small>
		                    	</div>
		                  	</div>
		                </a>
		                <a class="d-flex" href="#">
			                <div class="list-item d-flex align-items-start">
				                <div class="me-1">
				                    <div class="avatar"><img src="../../../app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar" width="32" height="32"></div>
				                </div>
				                <div class="list-item-body flex-grow-1">
				                    <p class="media-heading"><span class="fw-bolder">New message</span>&nbsp;received</p><small class="notification-text"> You have 10 unread messages</small>
				                </div>
			                 </div>
			            </a>
		              	<a class="d-flex" href="#">
		                  	<div class="list-item d-flex align-items-start">
			                    <div class="me-1">
			                      	<div class="avatar bg-light-danger">
			                        	<div class="avatar-content">MD</div>
			                      	</div>
			                    </div>
			                    <div class="list-item-body flex-grow-1">
			                      	<p class="media-heading"><span class="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small class="notification-text"> MD Inc. order updated</small>
			                    </div>
		                  	</div>
	                  	</a>
		                <div class="list-item d-flex align-items-center">
		                  	<h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
		                  	<div class="form-check form-check-primary form-switch">
		                    	<input class="form-check-input" id="systemNotification" type="checkbox" checked="">
		                    	<label class="form-check-label" for="systemNotification"></label>
		                  	</div>
		                </div>
		                <a class="d-flex" href="#">
		                  	<div class="list-item d-flex align-items-start">
			                    <div class="me-1">
			                      	<div class="avatar bg-light-danger">
			                        	<div class="avatar-content"><i class="avatar-icon" data-feather="x"></i></div>
			                      	</div>
			                    </div>
		                    	<div class="list-item-body flex-grow-1">
		                      		<p class="media-heading"><span class="fw-bolder">Server down</span>&nbsp;registered</p><small class="notification-text"> USA Server is down due to hight CPU usage</small>
		                    	</div>
		                  	</div>
		                </a>
	                	<a class="d-flex" href="#">
		                  	<div class="list-item d-flex align-items-start">
			                    <div class="me-1">
			                      	<div class="avatar bg-light-success">
			                        	<div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
			                      	</div>
			                    </div>
		                    	<div class="list-item-body flex-grow-1">
		                      		<p class="media-heading"><span class="fw-bolder">Sales report</span>&nbsp;generated</p><small class="notification-text"> Last month sales report generated</small>
		                    	</div>
		                  	</div>
	                  	</a>
	                  	<a class="d-flex" href="#">
	                  		<div class="list-item d-flex align-items-start">
	                    		<div class="me-1">
	                      			<div class="avatar bg-light-warning">
	                        			<div class="avatar-content"><i class="avatar-icon" data-feather="alert-triangle"></i></div>
	                      			</div>
	                    		</div>
	                    		<div class="list-item-body flex-grow-1">
	                      			<p class="media-heading"><span class="fw-bolder">High memory</span>&nbsp;usage</p><small class="notification-text"> BLR Server using high memory</small>
	                    		</div>
	                  		</div>
	                  	</a>
	              </li>
	              <li class="dropdown-menu-footer">
	              	<a class="btn btn-primary w-100" href="#">Read all notifications</a></li>
	            </ul>
          	</li>
          	<li class="nav-item dropdown dropdown-user">
          		<a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	              	<div class="user-nav d-sm-flex d-none">
	              		<span class="user-name fw-bolder">{{ user()->name }}</span>
	              		<span class="user-status" id="current-branch">{{ branch(user()->branch_id)['name'] }}</span>
	              	</div>
	              	<span class="avatar">
	              		<img class="round" src="{{ asset('_dist/images/profile.png') }}" alt="avatar" height="40" width="40">
	              		<span class="avatar-status-online"></span>
	              	</span>
              	</a>
            	<div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
            		<a class="dropdown-item" href="{{ url('profile') }}">
            			<i class="me-50" data-feather="user"></i> Profile
            		</a>
            		<a class="dropdown-item" href="{{ url('user-guide') }}" target="_blank">
            			<i class="me-50" data-feather="book"></i> User Guide
            		</a>
              		<div class="dropdown-divider"></div>
              		<a class="dropdown-item" href="{{ url('settings') }}">
              			<i class="me-50" data-feather="settings"></i> Settings
              		</a>
              		<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="me-50" data-feather="power"></i> Logout
                    </a>
              		<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
            	</div>
          	</li>
        </ul>
      </div>
    </nav>
    
    <ul class="main-search-list-defaultlist-other-list d-none">
      	<li class="auto-suggestion justify-content-between">
      		<a class="d-flex align-items-center justify-content-between w-100 py-50">
          		<div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No results found.</span></div>
          	</a>
      	</li>
    </ul>
	<!-- END: Header-->

    <!-- BEGIN: Main Menu-->
    <div class="horizontal-menu-wrapper">
      <div class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-shadow menu-border container-xxl" role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav">
        <div class="navbar-header">
          <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="../../../html/ltr/horizontal-menu-template/index.html"><span class="brand-logo">
                  <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="24">
                    <defs>
                      <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%" y2="89.4879456%">
                        <stop stop-color="#000000" offset="0%"></stop>
                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                      </lineargradient>
                      <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%" y2="100%">
                        <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                      </lineargradient>
                    </defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                        <g id="Group" transform="translate(400.000000, 178.000000)">
                          <path class="text-primary" id="Path" d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z" style="fill:currentColor"></path>
                          <path id="Path1" d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z" fill="url(#linearGradient-1)" opacity="0.2"></path>
                          <polygon id="Path-2" fill="#000000" opacity="0.049999997" points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                          <polygon id="Path-21" fill="#000000" opacity="0.099999994" points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                          <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994" points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                        </g>
                      </g>
                    </g>
                  </svg></span>
                <h2 class="brand-text mb-0">Vuexy</h2></a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i></a></li>
          </ul>
        </div>
        <div class="shadow-bottom"></div>
        
   	</div>
</div>
<!-- END: Main Menu-->