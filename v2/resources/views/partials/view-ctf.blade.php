<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - View CTF</title>
    
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
        	@if($ctf)
        	<div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">CTF</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item active text-capitalize"> View CTF</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-4 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-transaction">
	                            <div class="card-header">
	                              	<h4 class="card-title">CTF Details</h4>
	                            </div>
	                            <div class="card-body">
	                              	<div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather='package' class="list-icon"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">CTF No</h6>
		                                    	<small>အထုပ်အမှတ်</small>
		                                  	</div>
		                                </div>
	                                	<div class="font-medium-4 text-primary">{{ $ctf->ctf_no }}</div>
	                              	</div>
	                              	<div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather='home' class="list-icon"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">From Branch</h6>
		                                    	<small>အထုပ်ပို့သည့်ရုံး</small>
		                                  	</div>
		                                </div>
	                                	<div class="font-medium-4 text-primary">{{ $ctf->branch }}</div>
	                              	</div>
	                              	<div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather='map-pin' class="list-icon"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">From City</h6>
		                                    	<small>အထုပ်ပို့သည့်မြို့</small>
		                                  	</div>
		                                </div>
	                                	<div class="font-medium-4 text-primary">{{ $ctf->city }}</div>
	                              	</div>
	                              	<div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather='shopping-cart' class="list-icon"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Package Qty</h6>
		                                    	<small>စာအရေအတွက်</small>
		                                  	</div>
		                                </div>
	                                	<div class="font-medium-4 text-primary">{{ $qty }}</div>
	                              	</div>
	                              	<div class="transaction-item">
		                                <a href="{{ url('print/cft?qr='.$ctf->ctf_no) }}" class="btn btn-wide btn-relief-success waves-effect" target="_blank"><strong>CTF Print ထုတ်ရန်</strong></a>
                                    </div>
	                            </div>
                          	</div>
                        </div>
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header">
                                  	<h4 class="card-title text-primary">{{ $ctf->ctf_no }} အထုပ်တွင်ပါသော အထုပ်များ</h4>
                                  	<span>
                                  		<label>စာအမှတ်ရှာရန်</label>
                                  		<input type="search" class="form-control inline-search" id="search-waybill" placeholder="" >
                                    </span>
                                </div>
                                <hr>
                                <div class="card-body">
                                @if(!$packages->isEmpty())
                                    <div class="card card-transaction " style="max-height: 600px;">
                                        @foreach($packages as $key => $package)
                                          <div class="transaction-item list-item border-{{ $package->completed_at != ''? 'success':'warning' }} border-rounded">
                                            <div class=" drag-item">
	                                            <div class="d-flex">
	                                              	<div class="avatar bg-light-{{ $package->completed_at != ''? 'success':'warning' }} rounded">
		                                                <div class="avatar-content">
		                                                    <i data-feather='package' class="list-icon"></i>
		                                                </div>
	                                              	</div>
	                                              	<div class="transaction-percentage ">
	                                                	<h6 class="transaction-title font-medium-3 m-0 text-{{ $package->completed_at != ''? 'success':'warning' }}">{{ $package->package_no }}</h6>
	                                                	<small class="text-primary"><small class="text-muted">အထုပ်ထုပ်ထားသူ - </small>{{ $package->created_by }}</small>
	                                              	</div>
	                                            </div>
                                            </div>
                                            <div class="fw-bolder">
                                                <a href="{{ url('packages/view/'.$package->package_no) }}" class="btn btn-relief-primary waves-effect add-btn btn-md mx-1" >
	                                                ကြည့်ရန်
	                                            </a>
                                            </div>
                                          </div>
                                        @endforeach
                                    </div>
                                 @else
                                    <div class="demo-spacing-0">
                                        <div class="alert alert-warning alert-validation-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                <span>စာများ မရှိသေးပါ</span>
                                            </div>
                                      	</div>
                                    </div>
                                @endif 
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            @else
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      	<div class="col-12">
	                        <h3 class="content-header-title float-start mb-0">CTF</h3>
	                        <div class="breadcrumb-wrapper">
	                          	<ol class="breadcrumb">
		                            <li class="breadcrumb-item">
		                            	<a href="{{ url('') }}">Dashboard</a>
		                            </li>
	                            	<li class="breadcrumb-item active text-capitalize">View {{ $ctf->ctf_no }}</li>
	                          	</ol>
	                        </div>
                      	</div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
	                <div class="alert alert-warning cancelled-msg" role="alert">
	              		<div class="alert-body"><strong>ဝမ်းနည်းပါတယ်...</strong> ၎င်းစာရင်းအမှတ်အား systemထဲတွင် ရှာမတွေ့ပါ။</div>
	            	</div>
	                <div class="row match-height">
	                    
	                </div>
                </section>
            </div>
            @endif
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    
	
	

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <!-- END: Page JS-->
    <script>
      	$(window).on('load',  function(){
        	if (feather) {
          		feather.replace({ width: 14, height: 14 });
        	}
      	});

      	$(document).ready(function(){
        	var url     = $("#url").val();
        	var packages= [];
        	var added   = 0;

        	$("#quantity").text($("#qty").val());

	        $(".add-btn").on("click",function search(e) {
	            $('.export-btn-box').removeClass('hide');
	            $('.show-ctf').addClass('hide');

	            $('.show-msg').hide();
	            id = this.id;
	            $('.item-'+id).hide();
	            ++added;

	            //alert(id);
	            packages.push(id);

	            data = $('.drag-item-'+id).html();
	            $("#scanned-lists").prepend('<div class="transaction-item">'+data+'</div>');
	            
	            $('#listed').text(added);
	        });

	      	$('#filtered').on("change",function search(e) {
	        	branch = $("#filtered option:selected").text();
	        	if(branch == 'Select Branch'){
	            	$('.list-item').removeClass('hidden'); 
	        	}else{
	            	$('.list-item').addClass('hidden');
	            	$('.'+branch).removeClass('hidden');
	        	}
	        
	        	console.log(branch);
	        });

	      	$(".ctf-btn").on("click",function search(e) {
	            user_id         = $("#user_id").val();
	            username        = $("#username").val();
	            user_city_id    = $("#user_city_id").val();
	            user_branch_id  = $("#user_branch_id").val();
	                
	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	                       
	            $.ajax({
	                type: 'post',
	                //url: url+'/api/action/ctf-export',
	                url: url+'/api/action/outbound',
	                dataType:'json',
	                data: {
	                    'packages'      :packages,
	                    'user_id'       :user_id,
	                    'username'      :username,
	                    'user_city_id'  :user_city_id,
	                    'user_branch_id':user_branch_id,
	                    'action_id'     :3,
	                },
	                success: function(data) { 
	                    console.log(data);
	                    if(data.success == 1){
	                        $(".show-ctf").removeClass('hide');
	                        $("#ctf_no").text(data.ctf_no);
	                        $(".print-url").attr("href", url+"/print/cft?qr="+data.ctf_no)
	                    }

	                    packages= [];
	                    $("#scanned-lists").empty(); 
	                    $("#failed-lists").empty();
	                    $(".export-btn-box").addClass('hide');
	                },
	            });
	            $(this).val(''); 
	                
	            added = 0;
	            $('#listed').text(added);
	        });

      		$('body').delegate(".remove-btn","click",function () {
               	var id = $(this).attr('id');
               	console.log(id); 
               	$("#selected_item").val(id);
        	});

      		$('body').delegate(".btn-new","click",function () {
            	$('.waybill-box').toggle();
        	});


        	$('body').delegate(".confirm-btn","click",function () {
            	var item            = $("#selected_item").val();
            	var package_id      = $("#package_id").val();
            	var username        = $("#username").val();
            	var user_id         = $("#user_id").val();
            	var user_city_id    = $("#user_city_id").val();
            	var user_branch_id  = $("#user_branch_id").val();

	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
                       
	            $.ajax({
	                type: 'post',
	                url: url+'/api/removed-waybill',
	                dataType:'json',
	                data: {
	                    'item'      :item,
	                    'package_id':package_id,
	                    'username'  :username,
	                    'user_id'  :user_id,
	                    'user_city_id'  :user_city_id,
	                    'user_branch_id'  :user_branch_id,
	                },
	                success: function(data) { 
	                    console.log(data);
	                    
	                    if(data.success == 1){
	                    	$('.id-'+data.item).addClass('border-danger border-rounded');
	                    	$('.removed-msg-'+data.item).show();
							$('.btn-actions-'+data.item).hide();

							$('.item-bg-'+data.item).removeClass('bg-light-primary');
							$('.item-bg-'+data.item).addClass('bg-light-danger');

							$('.item-color-'+data.item).removeClass('text-primary');
							$('.item-color-'+data.item).addClass('text-danger');
	                    }
	                },
	            });   
            
        	});

	      	$('#search-waybill').on("keyup",function search(e) {
	        	var term = $(this).val();
	        	$('.list-item').hide();
	        	$(".transaction-item:contains('"+term+"')").show();
	    	});
	  	});
    </script>
</body>
</html>