<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Outbound</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Outbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ url('outbound') }}">Outbound</a>
                                    </li>
                                    <li class="breadcrumb-item active text-capitalize">Duplicate Waybills
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                  
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <!-- jobs/synced-end-point -->
                        
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="prev-btn"><i data-feather='skip-back'></i></button>
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="next-btn"><i data-feather='skip-forward'></i></button>
                            <button type="button" class="btn btn-outline-primary waves-effect waves-float waves-light">Records: <span id="to-records">0</span> of <span id="total-records">0</span></button>
                        </div>
                    </div>
                </div>
            </div>
              
            <div class="content-body">
                <div class="row" id="table-head">
                    <div class="col-12">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="table-dark">
                                        <tr>
                                            <th>Waybill No</th>
                                            <th>Qty</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="fetched-data">
                                                                            
                                    </tbody>
                                </table>
                                <div class="data-loading text-center m-2">
                                    <h5>Loading ...</h5>
                                    <div class="spinner-grow text-primary me-1" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                    <div class="spinner-grow text-danger me-1" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                    <div class="spinner-grow text-success me-1" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div class="alert alert-grey show-alert font-size-16 mx-2 text-center hide" role="alert">
                                    <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
                                      <div class="alert-body d-flex align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                        <strong>စာများ မရှိသေးပါ။</strong>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="json" value="{{ $json }}">
    <input type="hidden" id="city_id" value="{{ user()->city_id }}">

    <!-- Modal -->
              <div
                class="modal fade modal-danger text-start"
                id="duplicate"
                tabindex="-1"
                aria-labelledby="myModalLabel120"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="myModalLabel120">Danger Modal</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <ul class="list-group" id="waybill-listing">
                        
                        </ul>
                    </div>
                    <div class="modal-footer">
                      
                    </div>
                  </div>
                </div>
              </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>

    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });

        $(document).ready(function(){
            var json        = $("#json").val();

            //declared first loaded json data
            var load_json   = url+'/'+json;
            var waybills    = [];
            var item        = 0;
            var _token      = $("#_token").val();
            var limit       = 20;

            
            var fetched_data = function(){
                $.ajax({
                url: load_json,
                type: 'GET',
                data: {},
                success: function(data){
                    if(data.total > 0){
                        $.each( data.data, function( key, value ) {
                            $("#fetched-data").append(
                                '<tr class="item-'+value.waybill_no+'">'
                                    +'<td><span class="waybill-no text-danger">'+value.waybill_no+'</span></td>'
                                    +'<td class="h6">'+value.count+'</td>'
                                    +'<td>'
                                       +'<button class="btn btn-success btn-sm btn-rounded waves-effect waves-light btn-view" data-bs-toggle="modal" data-bs-target="#duplicate" value="'+value.waybill_no+'">'+replace_icon('eye')+'</a> '
                                       +'</td>'
                                +'</tr>'
                            );
                        });
                        $(".data-loading").hide();

                        $("#to-records").text(data.to);
                        $("#total-records").text(data.total);
                                    
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }else{
                        $(".show-alert").show();
                        $(".pagination").hide();
                        $(".data-loading").hide();
                    }
                }
            });
            };

            fetched_data();

            

            $('.pagination-btn').click(function(){
                //clicked url json data
                $(".data-loading").show();
                $("#fetched-data").empty();
                var clicked_url = $(this).val();
                        
                $(this).siblings().removeClass('active')
                $(this).addClass('active');
                $.ajax({
                    url: clicked_url,
                    type: 'GET',
                    data: {},
                    success: function(data){
                        $.each( data.data, function( key, value ) {
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td><span class="text-muted">'+value.created_at+'</span></td>'
                                    +'<td><span class="waybill-no text-danger">'+value.waybill_no+'</span></td>'
                                    +'<td>'+(value.courier == null? 'Anonymous':value.courier)+'</td>'
                                    +'<td class="h6 text-primary">'+(value.branch == null? 'Unknown':value.branch)+'</td>'
                                    +'<td>'+outbound_status(value.action_id)+'</td>'
                                    +'<td class="h6">'+value.qty+'</td>'
                                    +'<td>'+same_day(value.same_day)+'</td>'
                                    +'<td><span class="end-point-'+value.id+'">'+(value.end_point ==null? '---':value.end_point)+'</span></td>'
                                    +'<td>'
                                       +'<a href="'+url+'/waybills/view/'+value.waybill_no+'" class="btn btn-success btn-sm btn-rounded waves-effect waves-light">'+replace_icon('eye')+'</a> '
                                       +'<button class="btn btn-danger btn-sm btn-rounded waves-effect waves-light sync-odoo" data-bs-toggle="modal" data-bs-target="#sync-odoo" value='+value.waybill_no+' id="'+value.id+'">'+replace_icon('map-pin')+'</button>'
                                    +'</td>'
                                +'</tr>'
                            );
                        });
                        $(".data-loading").hide();
                                

                        $("#to-records").text(data.to);
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }
                });
            });

            $('body').delegate(".btn-view","click",function () {
                var waybill_no = $(this).val();
                console.log(waybill_no);
                $("#waybill-listing").empty();

                $.ajax({
                    url: url+'/view-duplicate-waybills/'+waybill_no,
                    type: 'GET',
                    data: {},
                    success: function(data){
                         $.each(data, function( key, value ) {
                            $("#waybill-listing").append('<li class="list-group-item w-100">'
                                +(value.waybill_no)
                                +'<span class="badge bg-primary mx-1">'+(value.action_id)+'</span>'
                            +'<button class="btn btn-danger btn-xs btn-rounded waves-effect waves-light pull-right btn-delete" value="'+value.id+'"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></button></li>');
                        });
                    }
                });
            });

            $('body').delegate(".btn-delete","click",function () {
                var id = $(this).val();
                console.log(id);

                $.ajax({
                    url: url+'/delete-duplicate-waybills',
                    type: 'POST',
                    data: {
                        id:id,
                        _token:_token
                    },
                    success: function(data){
                        console.log(data);

                        $('.item-'+data.item).hide();
                        $("#duplicate").modal('hide');
                    }
                });
            });
        });
    </script>
</body>
</html>