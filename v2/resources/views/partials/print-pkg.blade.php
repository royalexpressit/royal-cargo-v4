<html>
<head>
<style type="text/css">
  * {
  box-sizing: border-box;
}

html,
body {
  height: 100%;
  margin: 0;
}
body{
  display: none;
}

@media print{


body {
  font-family: "Ubuntu", sans-serif;
  background-color: #f95352;
  height: 100%;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #1c1c1c;
  display: flex;
  justify-content: center;
}

.ticket-system {
  max-width: 385px;
}
.ticket-system .top {
  display: flex;
  align-items: center;
  flex-direction: column;
}
.ticket-system .top .title {
  font-weight: normal;
  font-size: 1.6em;
  text-align: left;
  margin-left: 20px;
  margin-bottom: 50px;
  color: #fff;
}
.ticket-system .top .printer {
  width: 90%;
  height: 20px;
  border: 5px solid #fff;
  border-radius: 10px;
  box-shadow: 1px 3px 3px 0px rgba(0, 0, 0, 0.2);
}
.ticket-system .receipts-wrapper {
  overflow: hidden;
  margin-top: -10px;
  padding-bottom: 10px;
}
.ticket-system .receipts {
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  
}
.ticket-system .receipts .receipt {
  padding: 25px 30px;
  text-align: left;
  min-height: 200px;
  width: 88%;
  background-color: #fff;
  box-shadow: 1px 3px 8px 3px rgba(0, 0, 0, 0.2);
}
.ticket-system .receipts .receipt .airliner-logo {
  max-width: 80px;
}
.ticket-system .receipts .receipt .route {
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 30px 0;
}
.ticket-system .receipts .receipt .route .plane-icon {
  width: 40px;
  height: 40px;
}
.ticket-system .receipts .receipt .route h2 {
  font-weight: 300;
  font-size: 2.2em;
  margin: 0;
}
.ticket-system .receipts .receipt .details {
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
}
.ticket-system .receipts .receipt .details .item {
  display: flex;
  flex-direction: column;
  min-width: 70px;
}
.ticket-system .receipts .receipt .details .item span {
  font-size: 0.8em;
  color: rgba(28, 28, 28, 0.7);
  font-weight: 500;
}
.ticket-system .receipts .receipt .details .item h3 {
  margin-top: 10px;
  margin-bottom: 25px;
}
.ticket-system .receipts .receipt.qr-code {
  height: 110px;
  min-height: unset;
  position: relative;
  border-radius: 20px 20px 10px 10px;
  display: flex;
  align-items: center;
}
.ticket-system .receipts .receipt.qr-code::before {
  content: "";
  background: linear-gradient(to right, #fff 50%, #f95352 50%);
  background-size: 22px 4px, 100% 4px;
  height: 4px;
  width: 90%;
  display: block;
  left: 0;
  right: 0;
  top: -1px;
  position: absolute;
  margin: auto;
}
.ticket-system .receipts .receipt.qr-code .qr {
  width: 70px;
  height: 70px;
}
.ticket-system .receipts .receipt.qr-code .description {
  margin-left: 20px;
}
.ticket-system .receipts .receipt.qr-code .description h2 {
  margin: 0 0 5px 0;
  font-weight: 500;
}
.ticket-system .receipts .receipt.qr-code .description p {
  margin: 0;
  font-weight: 400;
}
h3{
  font-size: 16px;
}
.text-center{
  text-align: center;
}
}
</style>
</head>
<body onload="window.print()">
<main class="ticket-system">
   <div class="top">
   <div class="receipts-wrapper">
    <div class="receipt qr-code">
            
            
         </div>
      <div class="receipts">

         <div class="receipt">
          <div class="description text-center">
               <h2>{{ $package->package_no }}</h2>
            </div>
            <div class="route">
               <h2>{{ $package->origin }}</h2>
               <img src="{{ asset('_dist/images/icons8-truck-100.png') }}" class="plane-icon">
               <h2>{{ $package->destination }}</h2>
            </div>
            <div class="details">
               <div class="item">
                  <span>From Branch</span>
                  <h3>{{ $package->from_branch }}</h3>
               </div>
               <div class="item">
                  <span>To Branch</span>
                  <h3>{{ $package->to_branch }}</h3>
               </div>
               <div class="item">
                  <span>Date</span>
                  <h3>{{ $package->created_at }}</h3>
               </div>
               <div class="item">
                  <span>Waybills(Qty)</span>
                  <h3>{{ $waybills->qty }}</h3>
               </div>
               <div class="item">
                  <span>Package Type</span>
                  <h3>{{ $package->package_type }}</h3>
               </div>
               <div class="item">
                  <span>Products(Qty)</span>
                  <h3>{{ $products }}</h3>
               </div>
               <div class="item">
                  <span>Remark</span>
                 <p>{{ $package->remark }}</p>
               </div>
            </div>
         </div>
         
      </div>
   </div>
</main>
</body>
</html>