<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<style type="text/css">
		.block{
			width:49%;
		}
		ul{
			margin:0;
			padding:0;
		}
		ul li{
			list-style: none;
			padding:2px 6px;
		}
		.waybill_no{
			display: inline-block;
			width:200px;
		}
	</style>
</head>
<body>
	<div class="content">
		<h1>{{ $package->package_no }}, <small>Qty: {{ $products }}</small></h1>	
		<div class="block block-2">
		<ul>
			@foreach($waybills as $waybill)
			<li>
				<span class="waybill_no">{{ $waybill->waybill_no }}</span>
				<span class="qty">, {{ $waybill->qty }}</span>
			</li>
			@endforeach
		</ul>
		</div>
		<div class="block block-2">
		
		</div>
	</div>
</body>
</html>



