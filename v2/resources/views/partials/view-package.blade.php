<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - View Package</title>
    
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
    <style type="text/css">
    	.scroll{
    		overflow: scroll;
    	}
		.badge-xs{
			font-size: 12px;
			margin-left: 10px;
			padding: 2px 8px;
		}
    </style>
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
        	@if($package)
        	<div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      	<div class="col-12">
	                        <h3 class="content-header-title float-start mb-0">Package</h3>
	                        <div class="breadcrumb-wrapper">
	                          	<ol class="breadcrumb">
		                            <li class="breadcrumb-item">
		                            	<a href="{{ url('') }}">Dashboard</a>
		                            </li>
	                            	<li class="breadcrumb-item active text-capitalize">View {{ $package_no }}</li>
	                            </ol>
	                        </div>
                      	</div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                        	<div class="card card-transaction">
                        		<div class="card-body">
										<ul class="nav nav-tabs" role="tablist">
								            <li class="nav-item">
								              	<a class="nav-link active" id="homeIcon-tab" data-bs-toggle="tab" href="#pkg-detail" aria-controls="home" role="tab" aria-selected="true"><i data-feather="file"></i> Package Details</a>
								            	
								            </li>
								            <li class="nav-item">
								              	<a class="nav-link" id="profileIcon-tab" data-bs-toggle="tab"href="#more-actions" aria-controls="profile" role="tab" aria-selected="false"><i data-feather="package"></i> More Actions</a>
								            </li>
							          	</ul>
							          	<div class="tab-content">
								            <div class="tab-pane active" id="pkg-detail" aria-labelledby="homeIcon-tab" role="tabpanel">
					                            <div class="">
									              	<div class="transaction-item">
						                                <div class="d-flex">
						                                  	<div class="avatar bg-light-primary rounded float-start">
							                                    <div class="avatar-content">
							                                      	<i data-feather='package' class="list-icon"></i>
							                                    </div>
						                                  	</div>
						                                  	<div class="transaction-percentage">
						                                    	<h6 class="transaction-title">Package No</h6>
						                                    	<small>အထုပ်အမှတ်</small>
						                                  	</div>
						                                </div>
				                                		<div class="font-medium-4 text-primary">{{ $package->package_no }}</div>
					                              	</div>
					                              	<div class="transaction-item">
						                                <div class="d-flex">
						                                  	<div class="avatar bg-light-primary rounded float-start">
							                                    <div class="avatar-content">
							                                      	<i data-feather='shopping-cart' class="list-icon"></i>
							                                    </div>
						                                  	</div>
						                                  	<div class="transaction-percentage">
						                                    	<h6 class="transaction-title">CTF</h6>
						                                    	<small>စာရင်းအမှတ်</small>
						                                  	</div>
						                                </div>
					                                	<div class="font-medium-4 text-warning text--right">
					                                		@if($package->completed == 1)
					                                		<a href="{{ url('ctfs/view/'.$ctf->id) }}" class="btn btn-sm btn-success waves-effect waves-float waves-light add-btn" >
			                                                    {{ $ctf->ctf_no }}
			                                                </a>
			                                                @endif
					                                	</div>
					                              	</div>
					                              	<div class="transaction-item">

						                                <div class="d-flex">
						                                  	<div class="avatar bg-light-primary rounded float-start">
							                                    <div class="avatar-content">
							                                      	<i data-feather='map-pin' class="list-icon"></i>
							                                    </div>
						                                  	</div>
						                                  	<div class="transaction-percentage">
						                                    	<h6 class="transaction-title">From Branch/City</h6>
						                                    	<small>အထုပ်ပို့ထားသည့်ရုံး/မြို့</small>
						                                  	</div>
						                                </div>
					                                	<div class="text-primary text--right">
					                                		<span>{{ $branches->from_branch }}</span><br>
					                                		<span>{{ $branches->from_city }}</span>
					                                	</div>
					                              	</div>
					                              	<div class="transaction-item">
						                                <div class="d-flex">
						                                  	<div class="avatar bg-light-primary rounded float-start">
							                                    <div class="avatar-content">
							                                      	<i data-feather='map-pin' class="list-icon"></i>
							                                    </div>
						                                  	</div>
						                                  	<div class="transaction-percentage">
						                                    	<h6 class="transaction-title">To Branch/City</h6>
						                                    	<small>အထုပ်လက်ခံမည့်ရုံး/မြို့</small>
						                                  	</div>
						                                </div>
					                                	<div class="text-primary text--right">
					                                		<span>{{ $branches->to_branch }}</span><br>
					                                		<span>{{ $branches->to_city }}</span>
					                                	</div>
					                              	</div>
					                              	<div class="transaction-item">
						                                <div class="d-flex">
						                                  	<div class="avatar bg-light-primary rounded float-start">
							                                    <div class="avatar-content">
							                                      	<i data-feather='shopping-cart' class="list-icon"></i>
							                                    </div>
						                                  	</div>
						                                  	<div class="transaction-percentage">
						                                    	<h6 class="transaction-title">Waybills (Qty)</h6>
						                                    	<small>စာအရေအတွက်</small>
						                                  	</div>
						                                </div>
					                                	<div class="font-medium-4 text-primary text--right">
					                                		<strong id="quantity">0</strong><br>
					                                		<span class="text-primary font-small-3">(-<span id="removed">0</span>)</span>
					                                	</div>
					                              	</div>
					                              	<div class="transaction-item">
						                                <div class="d-flex">
						                                  	<div class="avatar bg-light-primary rounded float-start">
							                                    <div class="avatar-content">
							                                      	<i data-feather='shopping-cart' class="list-icon"></i>
							                                    </div>
						                                  	</div>
						                                  	<div class="transaction-percentage">
						                                    	<h6 class="transaction-title">Products (Qty)</h6>
						                                    	<small>ပစ္စည်းအရေအတွက်</small>
						                                  	</div>
						                                </div>
					                                	<div class="font-medium-4 text-warning text--right">
					                                		<strong id="product-qty">0</strong>
					                                	</div>
					                              	</div>
					                              	<div class="transaction-item">
						                                <div class="d-flex">
						                                  	<div class="avatar bg-light-primary rounded float-start">
							                                    <div class="avatar-content">
							                                      	<i data-feather='package' class="list-icon"></i>
							                                    </div>
						                                  	</div>
						                                  	<div class="transaction-percentage">
						                                    	<h6 class="transaction-title">Current Status</h6>
						                                    	<small>-</small>
						                                  	</div>
						                                </div>
					                                	<div class="text-primary text--right">
					                                		<span class="{{ $package->completed==0? 'text-warning':'text-success' }}">{{ $package->completed==0? 'Progress':'Completed' }}</span><br>
					                                		<span>{{ $package->completed_at }}</span>
					                                	</div>
					                              	</div>
					                              	<div class="transaction-item">
						                                <div class="d-flex">
						                                  	<div class="transaction-percentage">
						                                    	<h6 class="transaction-title">Package Type</h6>
						                                    	<small>-{{ $package->package_type }}</small>
						                                  	</div>
						                                </div>
					                                	<div class="font-medium-4 text-primary"><span id="quantity"></span></div>
					                              	</div>
					                              	<div class="transaction-item">
						                                <div class="d-flex">
						                                  	
						                                  	<div class="transaction-percentage">
						                                    	<h6 class="transaction-title">Remark</h6>
						                                    	<small>-{{ $package->remark }}</small>
						                                  	</div>
						                                </div>
					                                	<div class="font-medium-4 text-primary"><span id="quantity"></span></div>
					                              	</div>
						                            <div class="row">
						                                <div class="col-md-6">
						                                    <a href="{{ url('print/package/'.$package->package_no) }}" target="_blank" class="btn btn-wide btn-relief-primary waves-effect">အထုပ် Print ထုတ်မည်</a>
						                                </div>
						                                <div class="col-md-6">
						                                    <a href="{{ url('print/'.$package->package_no.'/waybills') }}" target="_blank" class="btn btn-wide btn-relief-primary waves-effect">စာများ Print ထုတ်မည်</a>
						                                </div>
						                            </div>
				                              	</div>
								        	</div>
							            	<div class="tab-pane" id="more-actions" aria-labelledby="profileIcon-tab" role="tabpanel">
							              		<div class="waybill-box hide mb-1" >
										            	<label class="form-label text-primary" for="select2-icons">အသစ်ထည့်မည့်စာအမှတ်</label>
										                <input type="text" id="waybill" class="form-control" placeholder="Z1234566789">
										        </div>

						      					<div class="row">
					                                <div class="col-md-6">
					                                    <button class="btn btn-wide btn-relief-primary waves-effect btn-new">စာအသစ်ထပ်ထည့်မည်</button>
					                                </div>
					                                <div class="col-md-6">
					                                    <button class="btn btn-wide btn-relief-danger waves-effect" data-bs-toggle="modal" data-bs-target="#transferred" data-bs-toggle="tooltip" data-bs-placement="bottom" value="{{ $package->id }}">ရုံးပြန်ပြောင်းမည်</button>
					                                </div>
					                            </div>
							            	</div>
							          	</div>
							    </div>
                          	
                        </div>
                        </div>
                        <div class="col-md-8">
                        	<div class="card card-transaction">
							    <div class="card-body scroll">
							    	<div class="row">
							    		<div class="col-md-6">
							    			<ul class="nav nav-tabs" role="tablist">
								            <li class="nav-item">
								              	<a class="nav-link active" id="homeIcon-tab" data-bs-toggle="tab" href="#homeIcon" aria-controls="home" role="tab" aria-selected="true"><i data-feather="edit-2"></i> Waybills</a>
								            	
								            </li>
								            <li class="nav-item">
								              	<a class="nav-link show-related-packages" id="profileIcon-tab" data-bs-toggle="tab"href="#profileIcon" aria-controls="profile" role="tab" aria-selected="false"><i data-feather="package"></i> Related Package</a>
								            </li>
							          	</ul>
							    		</div>
							    		<div class="col-md-6">
							    			<div class="w-100 mb-1 text--right">
				                                <label>စာအမှတ်ရှာရန်</label>
				                                <input type="search" class="form-control inline-search" id="search-waybill" placeholder="" >
				                            </div>
							    		</div>
							    	</div>
										

							          	<div class="tab-content">
								            <div class="tab-pane active" id="homeIcon" aria-labelledby="homeIcon-tab" role="tabpanel">
								              	
								          		@if(!$waybills->isEmpty())
                                    <div class="card card-transaction " style="max-height: 600px;">
                                        @foreach($waybills as $key => $waybill)
                                        @php 
                                        	++$qty;
                                        	if($waybill->active == 0){
                                            	++$remove;
                                            }
                                       	@endphp
                                          <div class="transaction-item list-item p-0 item-{{ $waybill->waybill_no }} id-{{ $waybill->waybill_id }} {{ ($waybill->active==0? 'border-danger border-rounded':($waybill->completed == 1? 'border-success':'border-warning')) }}">
                                            <div class=" drag-item">
	                                            <div class="d-flex">
	                                              	<div class="avatar {{ ($waybill->active==0? 'bg-light-danger':($waybill->completed == 1? 'bg-light-success':'bg-light-warning')) }} rounded item-bg-{{ $waybill->waybill_id }}">
		                                                <div class="avatar-content">
		                                                    <i data-feather='file' class="list-icon"></i>
		                                                </div>
	                                              	</div>
	                                              	<div class="transaction-percentage {{ ($waybill->completed == 1? 'text-success':'text-warning') }}">
	                                                	<h6 class="transaction-title font-medium-4 m-0 {{ ($waybill->active==0? 'text-danger':($waybill->completed == 1? 'text-success':'text-warning')) }} item-color-{{ $waybill->waybill_id }}"><span class="waybill-no">{{ $waybill->waybill_no }}</span><span class="badge badge-xs bg-danger waybill-duplicate-{{ $waybill->waybill_no }} hide">Duplicate</span></h6>
	                                                	<small><small class="text-muted">စာပို့သမား/ကောင်တာ : </small>{{ $waybill->courier }},</small>
	                                                	<small><small class="text-muted">ပူးတွဲပစ္စည်း : </small><span class="product-qty">{{ $waybill->qty }}</span></small>
	                                              	</div>
	                                            </div>
                                            </div>
                                            <div class="fw-bolder">
                                                @if($waybill->active==0)
                                                <button class="btn btn-danger waves-effect mx-03" disabled="">
	                                                 ၎င်းစာအား ဤအထုပ်ထဲမှပယ်ဖျတ်ထားပါသည်။
	                                            </button>
                                                @else
                                                <div class="removed-msg-{{ $waybill->waybill_id }} hide">
	                                                <button class="btn btn-danger waves-effect mx-03" disabled="">
	                                                  ၎င်းစာအား ဤအထုပ်ထဲမှပယ်ဖျတ်ထားပါသည်။
	                                                </button>
                                                </div>
                                                <div class="btn-actions-{{ $waybill->waybill_id }} me-1">
	                                                <a href="{{ url('waybills/view/'.$waybill->waybill_no) }}" class="btn btn-sm btn-success waves-effect waves-float waves-light add-btn" >
	                                                    <i data-feather='eye'></i>
	                                                </a>
	                                                <button type="button" class="btn btn-sm waves-effect waves-float waves-light remove-btn {{ $waybill->completed == 1? 'disabled btn-secondary':'btn-danger' }}" data-bs-toggle="modal" data-bs-target=".danger" id="{{ $waybill->waybill_id }}">
	                                                    <i data-feather='x-circle'></i>
	                                                </button>
													<button type="button" class="btn btn-sm btn-danger waves-effect waves-float waves-light duplicate-btn duplicate-{{ $waybill->waybill_no }} hide" data-bs-toggle="modal" data-bs-target=".duplicate-modal" id="{{ $waybill->id }}">
	                                                    <i data-feather='trash-2'></i>
	                                                </button>
                                                </div>
                                                @endif
                                            </div>
                                          </div>
                                        @endforeach
                                    </div>
                                 @else
                                    <div class="demo-spacing-0">
                                        <div class="alert alert-warning alert-validation-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                <span>စာများ မရှိသေးပါ</span>
                                            </div>
                                      	</div>
                                    </div>
                                @endif 
								            </div>
							            	<div class="tab-pane" id="profileIcon" aria-labelledby="profileIcon-tab" role="tabpanel">
							              		<div class="transaction-item fetched-reloated-pkgs">
										            
								          		</div>
							            	</div>
							          	</div>
							        </div>
	      					</div>
                        </div>
                    </div>
                </section>
            </div>
            @else
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      	<div class="col-12">
	                        <h3 class="content-header-title float-start mb-0">Package</h3>
	                        <div class="breadcrumb-wrapper">
	                          	<ol class="breadcrumb">
		                            <li class="breadcrumb-item">
		                            	<a href="{{ url('') }}">Dashboard</a>
		                            </li>
	                            	<li class="breadcrumb-item active text-capitalize">View {{ $package_no }}</li>
	                          	</ol>
	                        </div>
                      	</div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
	                <div class="alert alert-warning cancelled-msg" role="alert">
	              		<div class="alert-body"><strong>ဝမ်းနည်းပါတယ်...</strong> ၎င်းအထုပ်အမှတ်အား systemထဲတွင် ရှာမတွေ့ပါ။</div>
	            	</div>
	                <div class="row match-height">
	                    
	                </div>
                </section>
            </div>
            @endif
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="qty" value="{{ $qty }}">
    <input type="hidden" id="remove" value="{{ $remove }}">
    @if($package)
	    @if($package->completed == 1)
	    <input type="hidden" id="ctf_id" value="{{ $ctf->id }}">
	    @endif
	@endif
	
	@if($package)
	<div class="modal fade modal-danger text-start danger" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel120">အသိပေးခြင်း</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    စာအမှတ် အား အထုပ်အမှတ် <strong>{{ $package->package_no }}</strong> ထဲမှပယ်ဖျတ်ရန် အတည်ပြုပေးပါ။
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="package_id" value="{{ $package->id }}">
                    <input type="hidden" id="selected_item" value="">
                    <button type="button" class="btn btn-danger confirm-btn" data-bs-dismiss="modal">ဖျတ်မည်</button>
                </div>
            </div>
        </div>
    </div>

	<div class="modal fade modal-danger text-start duplicate-modal" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel120">ထပ်နေသော စာအမှတ်အားဖျတ်ရန်</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    စာအမှတ် ထပ်နေသဖြင့် အထုပ်ထဲမှ ပယ်ဖျတ်ရန် အတည်ပြုပေးပါ။
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="package_id" value="{{ $package->id }}">
                    <input type="hidden" id="duplicate_item" value="">
                    <button type="button" class="btn btn-danger confirm-duplicate-btn" data-bs-dismiss="modal">ဖျတ်မည်</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade modal-danger text-start" id="transferred" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary" id="myModalLabel120">Change Branch</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>မိမိပြောင်းလဲလိုသော ရုံးရွေးပါ</p>
                    <label class="form-label text-primary" for="select2-icons">Choose Branch</label>
                    <select data-placeholder="Select a branch..." class="select2-icons form-select" id="to_branch">
                        @foreach(App\Branch::get() as $key => $branch)
                        <option value="{{ $branch->id }}" data-icon="home">{{ $branch->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary change-branch btn-branch" data-bs-dismiss="modal">Change</button>
                </div>
            </div>
        </div>
    </div>
    @endif

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <!-- END: Page JS-->
    <script>
      	$(window).on('load',  function(){
        	if (feather) {
          		feather.replace({ width: 14, height: 14 });
        	}
      	});

      	$(document).ready(function(){
        	var url      = $("#url").val();
        	var packages = [];
        	var added    = 0;
        	var products = 0;
        	var ctf_id   = $("#ctf_id").val();
			var duplicates = [];

        	$("#quantity").text($("#qty").val());
        	$("#removed").text($("#remove").val());

	        $(".add-btn").on("click",function search(e) {
	            $('.export-btn-box').removeClass('hide');
	            $('.show-ctf').addClass('hide');

	            $('.show-msg').hide();
	            id = this.id;
	            $('.item-'+id).hide();
	            ++added;

	            //alert(id);
	            packages.push(id);

	            data = $('.drag-item-'+id).html();
	            $("#scanned-lists").prepend('<div class="transaction-item">'+data+'</div>');
	            
	            $('#listed').text(added);
	        });

	      	$('#filtered').on("change",function search(e) {
	        	branch = $("#filtered option:selected").text();
	        	if(branch == 'Select Branch'){
	            	$('.list-item').removeClass('hidden'); 
	        	}else{
	            	$('.list-item').addClass('hidden');
	            	$('.'+branch).removeClass('hidden');
	        	}
	        
	        	console.log(branch);
	        });

	      	$(".ctf-btn").on("click",function search(e) {
	            user_id         = $("#user_id").val();
	            username        = $("#username").val();
	            user_city_id    = $("#user_city_id").val();
	            user_branch_id  = $("#user_branch_id").val();
	                
	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	                       
	            $.ajax({
	                type: 'post',
	                //url: url+'/api/action/ctf-export',
	                url: url+'/api/action/outbound',
	                dataType:'json',
	                data: {
	                    'packages'      :packages,
	                    'user_id'       :user_id,
	                    'username'      :username,
	                    'user_city_id'  :user_city_id,
	                    'user_branch_id':user_branch_id,
	                    'action_id'     :3,
	                },
	                success: function(data) { 
	                    console.log(data);
	                    if(data.success == 1){
	                        $(".show-ctf").removeClass('hide');
	                        $("#ctf_no").text(data.ctf_no);
	                        $(".print-url").attr("href", url+"/print/cft?qr="+data.ctf_no)
	                    }

	                    packages= [];
	                    $("#scanned-lists").empty(); 
	                    $("#failed-lists").empty();
	                    $(".export-btn-box").addClass('hide');
	                },
	            });
	            $(this).val(''); 
	                
	            added = 0;
	            $('#listed').text(added);
	        });

      		$('body').delegate(".remove-btn","click",function () {
               	var id = $(this).attr('id');
               	console.log(id); 
               	$("#selected_item").val(id);
        	});

			$('body').delegate(".duplicate-btn","click",function () {
               	var id = $(this).attr('id');
               	console.log(id); 
               	$("#duplicate_item").val(id);
        	});

      		$('body').delegate(".btn-new","click",function () {
            	$('.waybill-box').toggle();
        	});


        	$('body').delegate(".confirm-btn","click",function () {
            	var item            = $("#selected_item").val();
            	var package_id      = $("#package_id").val();
            	var username        = $("#username").val();
            	var user_id         = $("#user_id").val();
            	var user_city_id    = $("#user_city_id").val();
            	var user_branch_id  = $("#user_branch_id").val();

	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
                       
	            $.ajax({
	                type: 'post',
	                url: url+'/api/removed-waybill',
	                dataType:'json',
	                data: {
	                    'item'      :item,
	                    'package_id':package_id,
	                    'username'  :username,
	                    'user_id'  :user_id,
	                    'user_city_id'  :user_city_id,
	                    'user_branch_id'  :user_branch_id,
	                },
	                success: function(data) { 
	                    console.log(data);
	                    
	                    if(data.success == 1){
	                    	$('.id-'+data.item).addClass('border-danger border-rounded');
	                    	$('.removed-msg-'+data.item).show();
							$('.btn-actions-'+data.item).hide();

							$('.item-bg-'+data.item).removeClass('bg-light-primary');
							$('.item-bg-'+data.item).addClass('bg-light-danger');

							$('.item-color-'+data.item).removeClass('text-primary');
							$('.item-color-'+data.item).addClass('text-danger');
	                    }
	                },
	            });   
            
        	});

			$('body').delegate(".confirm-duplicate-btn","click",function () {
            	var item  		= $("#duplicate_item").val();
				var package_id  = $("#package_id").val();
				var _token     	= $("#_token").val();

	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
                       
	            $.ajax({
	                type: 'post',
	                url: url+'/delete-package-waybill',
	                dataType:'json',
	                data: {
	                    'item'      :item,
						'package_id':package_id,
						'_token'   	:_token
	                },
	                success: function(data) { 
						if(data.success == 1){
							location.reload();
						}
	                },
	            });   
            
        	});

	      	$('#search-waybill').on("keyup",function search(e) {
	        	var term = $(this).val();
	        	$('.list-item').hide();
	        	$(".transaction-item:contains('"+term+"')").show();
	    	});
	    

		$('.product-qty').each(function() {
		  products += +$(this).text();
		});


		
		$('body').delegate(".show-related-packages","click",function () {
            var ctf_id  = $("#ctf_id").val();

	        $.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
                       
	        $.ajax({
	            type: 'post',
	            url: url+'/api/fetched-ctf-packages',
	            dataType:'json',
	            data: {
	                'ctf_id':ctf_id,
	            },
	            success: function(data) { 
	                console.log(data);

	                $.each(data,function(key,value) {
	                    //++count
	                    $(".fetched-reloated-pkgs").append(
	                    	'<div class="transaction-item list-item  border-primary border-rounded">'
	                            +'<div class=" drag-item">'
		                            +'<div class="d-flex">'
		                                +'<div class="avatar bg-light-primary rounded">'
			                                +'<div class="avatar-content">'
			                                    +'//'
			                                +'</div>'
		                                +'</div>'
		                                +'<div class="transaction-percentage ">'
		                                    +'<h6 class="transaction-title font-medium-3 m-0 text-primary">'+value+'</h6>'
		                                        +'<small class="text-primary"><small class="text-muted">အထုပ်ထုပ်ထားသူ - </small></small>'
		                                    +'</div>'
		                                +'</div>'
	                                +'</div>'
	                                +'<div class="fw-bolder">'
	                                    +'<a href="#" class="btn btn-relief-primary waves-effect add-btn btn-md mx-1" >'
		                                    //
		                                +'</a>'
	                                +'</div>'
	                            +'</div>'
	                        +'</div>'
	                    );
	                });
	        	}
	        });
        });

		$('.waybill-no').each(function() {
			duplicates.push($(this).text());
		});

		findDuplicates = duplicates => duplicates.filter((item, index) => duplicates.indexOf(item) != index);
		hilights = findDuplicates(duplicates);


		$.each(hilights,function(key,value) {
			$(".duplicate-"+value).removeClass('hide');
			$(".waybill-duplicate-"+value).removeClass('hide');
			
			console.log(value);
		});

		$("#product-qty").text(products);
	});
    </script>
</body>
</html>