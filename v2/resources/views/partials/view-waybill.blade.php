<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - View Waybill</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    @php $count = 0; @endphp
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
        	@if($waybill)
          	<div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      	<div class="col-12">
	                        <h3 class="content-header-title float-start mb-0">Waybill</h3>
	                        <div class="breadcrumb-wrapper">
	                          	<ol class="breadcrumb">
		                            <li class="breadcrumb-item">
		                            	<a href="{{ url('') }}">Dashboard</a>
		                            </li>
	                            	<li class="breadcrumb-item active text-capitalize">View {{ $waybill->waybill_no }} {!! $waybill->action_id==10? '<span class="text-success"><i data-feather="check-circle"></i></span>':'' !!}</li>
	                          	</ol>
	                        </div>
                      	</div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                      	@if($waybill->active > 0 && $waybill->action_id < 6)  
                      	<button type="button" class="btn btn-relief-danger waves-effect cancel-btn btn-md" data-bs-toggle="modal" data-bs-target=".cancelled" id="404">Cancel</button>
                      	@endif

                      	@if($waybill->action_id == 2 || $waybill->action_id == 5 || $waybill->action_id == 8)  
                      	<button type="button" class="btn btn-relief-danger waves-effect remove-btn btn-md" data-bs-toggle="modal" data-bs-target=".danger" id="404">Remove</button>
                      	@endif
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
	                <div class="alert alert-danger {{ ($waybill->active==0 ? '':'hide') }} cancelled-msg" role="alert">
	              		<div class="alert-body"><strong>Sorry!</strong> We found that waybill is cancelled by someone.</div>
	            	</div>
	                <div class="row match-height {{ ($waybill->active==0 ? 'denied':'') }}">
	                    <div class="col-xl-4 col-md-6 col-12">
	                        <div class="card card-transaction">
		                        <div class="card-header">
		                            <h4 class="card-title">Waybill Details</h4>     
		                        </div>
		                        <div class="card-body">
		                            <div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
		                                    	<div class="avatar-content">
		                                      		<i data-feather="pocket" class="avatar-icon font-medium-3"></i>
		                                    	</div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Waybill No</h6>
		                                    	<small>စာအမှတ်</small>
		                                  	</div>
		                                </div>
		                                <div class="fw-bolder text-primary">{{ $waybill->waybill_no }}</div>
		                            </div>
		                            <div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather="map-pin" class="avatar-icon font-medium-3"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Origin</h6>
		                                    	<small>စာပို့သည့်မြို့</small>
		                                  	</div>
		                                </div>
		                                <div class="fw-bolder text-primary">{{ $waybill->origin }}</div>
		                            </div>
		                            <div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-warning rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather="map-pin" class="avatar-icon font-medium-3"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Transit 1</h6>
		                                    	<small>စာဝင်ခဲ့သည့်မြို့ ၁</small>
		                                  	</div>
		                                </div>
		                                <div class="fw-bolder text-warning">{{ $waybill->transit_1 }}</div>
		                            </div>
		                            <div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-warning rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather="map-pin" class="avatar-icon font-medium-3"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Transit 2</h6>
		                                    	<small>စာဝင်ခဲ့သည့်မြို့ ၂</small>
		                                  	</div>
		                                </div>
		                                <div class="fw-bolder text-warning">{{ $waybill->transit_2 }}</div>
		                            </div>
		                            <div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather="map-pin" class="avatar-icon font-medium-3"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Destination</h6>
		                                    	<small>စာလက်ခံမည့်မြို့</small>
		                                  	</div>
		                                </div>
		                                <div class="fw-bolder text-primary">{{ $waybill->destination }}</div>
		                            </div>
		                            <div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
		                                    	<div class="avatar-content">
		                                      		<i data-feather="pocket" class="avatar-icon font-medium-3"></i>
		                                    	</div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Current Status</h6>
		                                    	<small>လက်ရှိအခြေအနေ</small>
		                                  	</div>
		                                </div>
		                                <div class="fw-bolder text-primary">{!! current_status($waybill->action_id) !!}</div>
		                            </div>
		                            <div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
		                                    	<div class="avatar-content">
		                                      		<i data-feather="user" class="avatar-icon font-medium-3"></i>
		                                    	</div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Delivery/Counter</h6>
		                                    	<small>စာပို့သမား/ကောင်တာ</small>
		                                  	</div>
		                                </div>
		                                <div class="fw-bolder text-primary">{{ $waybill->courier!=null? $waybill->courier:'Anonymous' }}</div>
		                            </div>
		                            <div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather="clock" class="avatar-icon font-medium-3"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Same Day</h6>
		                                    	<small>နေ့ချင်းပို့ဝန်ဆောင်မှု</small>
		                                  	</div>
		                                </div>
		                                <div class="fw-bolder text-primary">{!! $waybill->same_day==0? '-':'<span class="badge rounded-pill badge-light-warning">Same Day</span>' !!}</div>
		                            </div>
		                            <div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather="shopping-cart" class="avatar-icon font-medium-3"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Qty</h6>
		                                    	<small>ပါဝင်ပစ္စည်းအရေအတွက်</small>
		                                  	</div>
		                                </div>
		                                <div class="fw-bolder text-primary">{{ $waybill->qty }}</div>
		                            </div>
		                            <div class="alert alert-danger" role="alert">
						              	<div class="alert-body">
						              		<div class="row">
							              		<div class="col-md-9">
							              			<strong>End Point:</strong> 
							              			<span id="end_point">{{ $waybill->end_point }}</span>
							              		</div>
							              		<div class="col-md-3">
							              			<button class="btn btn-primary btn-xs btn-rounded waves-effect waves-light sync-odoo pull-right" data-bs-toggle="modal" data-bs-target="#sync-odoo"><i data-feather='map-pin'></i></button>
							              		</div>
							              	</div>
						              	</div>
						            </div>
		                        </div>
	                        </div>
	                    </div>
	                    <div class="col-xl-8 col-md-6 col-12">
	                        <div class="card card-transaction">
							       <div class="card-body">
										<ul class="nav nav-tabs" role="tablist">
								            <li class="nav-item">
								              	<a class="nav-link active" id="homeIcon-tab" data-bs-toggle="tab" href="#homeIcon" aria-controls="home" role="tab" aria-selected="true"><i data-feather="check-circle"></i> Action Logs</a>
								            </li>
								            <li class="nav-item">
								              	<a class="nav-link" id="profileIcon-tab" data-bs-toggle="tab"href="#profileIcon" aria-controls="profile" role="tab" aria-selected="false"><i data-feather="package"></i> Package History</a>
								            </li>
							          	</ul>
							          	<div class="tab-content">
								            <div class="tab-pane active" id="homeIcon" aria-labelledby="homeIcon-tab" role="tabpanel">
								              	@foreach($logs as $key => $log)
								              	@php ++$count; @endphp
								          		<div class="transaction-item">
										            <div class="w-100">
										              	<div class="avatar bg-light-{{ $log->action_id > 6? 'success':'primary' }} rounded float-start">
										                	<div class="avatar-content">
										                  		<i data-feather="check-circle" class="avatar-icon font-medium-3"></i>
										                	</div>
										              	</div>
										              	<div class="">
										                	<h6 class="transaction-title">
										                		<strong class="text-{{ $log->action_id > 6? 'success':'primary' }}">{{ $log->branch }}</strong>
										                		<span class="text-danger pull-right fw-normal">{{ $log->created_at }}</span>
										                	</h6>
										                	<span>{!! $log->log !!}</span>
										              	</div>
										            </div>
								          		</div>
								            	@endforeach
								            	@if($count == 0)
													<div class="response-log">
									          			<div class="alert alert-grey show-alert font-size-16 text-center" role="alert">
					                                        <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
					                                            <div class="alert-body d-flex align-items-center">
					                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
					                                                <strong>လုပ်ဆောင်မှုမှတ်တမ်းများအား မသိမ်းဆည်းရသေးပါ။ အောက်မှ button ကိုနှိပ်ပြီး သိမ်းဆည်းနိုင်ပါသည်။</strong>
					                                            </div>
					                                        </div>
					                                    </div>
									            		<button class="btn btn-primary btn-sm btn-rounded waves-effect waves-light sync-log" data-bs-toggle="modal" data-bs-target="#sync-log"><i data-feather='download'></i> သိမ်းဆည်းမည်</button>
									            	</div>
								          		@endif
								            </div>
							            	<div class="tab-pane" id="profileIcon" aria-labelledby="profileIcon-tab" role="tabpanel">
							              		@foreach($packages as $package)
							                    <a href="{{ url('packages/view/'.$package->package_no) }}" class="transaction-item package-item">
							                        <div class="d-flex">
							                            <div class="avatar bg-light-primary rounded float-start">
							                                <div class="avatar-content">
							                                    <i data-feather="package" class="avatar-icon font-medium-3"></i>
							                                </div>
							                            </div>
							                            <div class="transaction-percentage">
							                                <h5 class="transaction-title">{{ $package->package_no }}</h5>
							                                <span><span class="text-muted">Remark:</span>{{ $package->remark }}</span>
							                            </div>
							                        </div>
							                        <div class="fw-light text-danger">{{ $package->package_type_id }}</div>
							                    </a>
							                    @endforeach
							            	</div>
							          	</div>
							        </div>
	      					</div>
	                    </div>
	                </div>
                </section>
            </div>
            @else
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      	<div class="col-12">
	                        <h3 class="content-header-title float-start mb-0">Waybill</h3>
	                        <div class="breadcrumb-wrapper">
	                          	<ol class="breadcrumb">
		                            <li class="breadcrumb-item">
		                            	<a href="{{ url('') }}">Dashboard</a>
		                            </li>
	                            	<li class="breadcrumb-item active text-capitalize">View {{ $_GET['waybill'] }}</li>
	                          	</ol>
	                        </div>
                      	</div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
	                <div class="alert alert-warning cancelled-msg" role="alert">
	              		<div class="alert-body"><strong>ဝမ်းနည်းပါတယ်...</strong> ၎င်းစာအမှတ်အား systemထဲတွင် ရှာမတွေ့ပါ။</div>
	            	</div>
	                <div class="row match-height">
	                    
	                </div>
                </section>
            </div>
            @endif
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="waybill_no" value="{{ $waybill? $waybill->waybill_no:0 }}">

	@if($waybill)
		<input type="hidden" id="current_status" value="{{ $waybill->action_id }}">

		<div class="modal fade modal-danger text-start cancelled" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h5 class="modal-title" id="myModalLabel120">အသိပေးခြင်း</h5>
	                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	                </div>
	                <div class="modal-body">
	                    <div class="mb-1">
	                		<label class="form-label" for="reason">Cancelled လုပ်ရသည့်အကြောင်းအရာရေးပါ</label>
	                		<textarea class="form-control" id="reason" rows="3" placeholder="ဝန်ဆောင်မှုပိတ်ထားသည်"></textarea>
	              		</div>
	                </div>
	                <div class="modal-footer">
	                    <input type="hidden" id="selected_item" value="{{ $waybill->id }}">
	                    <button type="button" class="btn btn-danger cancelled-btn" data-bs-dismiss="modal">လုပ်ဆောင်မည်</button>
	                </div>
	            </div>
	        </div>
	    </div>

		@if($waybill->action_id > 1) 
			@if(!empty($package)) 
			<div class="modal fade modal-danger text-start danger" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
		        <div class="modal-dialog modal-dialog-centered">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <h5 class="modal-title" id="myModalLabel120">အသိပေးခြင်း</h5>
		                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		                </div>
		                <div class="modal-body">
		                    စာအမှတ် အား အထုပ်အမှတ် <strong>{{ $package->package_no }}</strong> ထဲမှပယ်ဖျတ်ရန် အတည်ပြုပေးပါ။
		                </div>
		                <div class="modal-footer">
		                    <input type="hidden" id="package_id" value="{{ $package->id }}">
		                    <input type="hidden" id="selected_item" value="{{ $waybill->id }}">
		                    <button type="button" class="btn btn-danger confirm-btn" data-bs-dismiss="modal">ဖျတ်မည်</button>
		                </div>
		            </div>
		        </div>
		    </div>
		    @endif
		@endif
	@endif

	<div class="modal fade text-start modal-success" id="sync-odoo" tabindex="-1" aria-labelledby="myModalLabel110" aria-hidden="true" >
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel110">စာလမ်းကြောင်းစစ်ဆေးခြင်း</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="alert-body text-center py-1">
                        <h5 class="mb-1">စာပို့ထားသည့်မြို့အား စစ်ဆေးနေသည် ...</h5>
                        <div class="spinner-grow text-warning me-1" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                        <div class="spinner-grow text-danger me-1" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                        <div class="spinner-grow text-success me-1" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade text-start modal-success" id="sync-log" tabindex="-1" aria-labelledby="myModalLabel110" aria-hidden="true" >
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel110">လုပ်ဆောင်မှုစစ်ဆေးခြင်း</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="alert-body text-center py-1">
                        <h5 class="mb-1">လုပ်ဆောင်မှုမှတ်တမ်းများ စစ်ဆေးနေပါပြီ ...</h5>
                        <div class="spinner-grow text-warning me-1" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                        <div class="spinner-grow text-danger me-1" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                        <div class="spinner-grow text-success me-1" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script>
      	$(window).on('load',  function(){
	        if (feather) {
	          	feather.replace({ width: 14, height: 14 });
	        }
      	});

      	$(document).ready(function(){
            var _token  = $("#_token").val();

            if(status > 1){
              $(".package-item:last-child").addClass("pkg-active");
            }
            

            $("#form").submit(function(event){
                event.preventDefault();  
            });

            

            var data_send = function(){
                waybills        = $('#multi_scanned_waybills').val();
                user_id         = $("#user_id").val();
                username        = $("#username").val();
                package_id      = $("#package_id").val();
                user_city_id    = $("#user_city_id").val();
                user_branch_id  = $("#user_branch_id").val();
                delivery_id     = $("#delivery").val();
                delivery_name   = $("#delivery option:selected").text();
                service_point   = $("#service_point").val();
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                       
                $.ajax({
                    type: 'post',
                    url: url+'/api/action/branch-in',
                    dataType:'json',
                    data: {
                        'waybills'      :waybills,
                        'user_id'       :user_id,
                        'username'      :username,
                        'user_city_id'  :user_city_id,
                        'user_branch_id':user_branch_id,
                        'package_id'    :package_id,
                        'delivery_id'   :delivery_id,
                        'delivery_name' :delivery_name,
                        'service_point' :service_point,
                        'action'        :1,
                        'type'          :'branch-in'
                    },
                    success: function(data) { 
                        setTimeout(function(){
                            $('.bs-example-modal-center-loading').modal('hide');
                        },1000);
                        $("#scanned-lists").removeClass('scanned-panel');
                        $("#scanned-lists").empty(); 
                        $("#failed-lists").empty();

                        success = $("#scanned").text() - data.failed.length;
                        failed  = data.failed.length;

                        //add scroll max size for item > 10
                        if(failed > 10){
                            $("#failed-lists").addClass('scanned-panel'); 
                        }

                        $("#success").text(success);
                        $("#failed").text(failed);

                        if(data.failed.length > 0 ){
                            for (i = 0; i < data.failed.length; i++) {
                                $("#failed-lists").prepend('<li class="list-group-item sm-item"><i class="bx bx-x text-danger v-middle"></i> '+data.failed[i]+'</li>');
                            }
                        }else{
                            $("#failed-lists").prepend('<li class="list-group-item sm-item"><i class="bx bx-check text-success v-middle"></i> စာရင်းအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                        }
                    },
                });
                $(this).val(''); 


                $('#multi_scanned_waybills').empty();
                $('.scan-btn').attr('disabled',true);
                $('.continue-action').hide();
                $("#waybill").attr("disabled", false);
                $('#waybill').trigger('focus');
                scanned = 0;
            }
        
            

            $(".fetched-count").on("click",function search(e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                       
                $.ajax({
                    type: 'post',
                    url: url+'/api/fetched-count',
                    dataType:'json',
                    data: {
                        'status'      :'branch-in',
                        'type'        :'inbound'
                    },
                    success: function(data) { 
                        $("#fetched-count").text(data);
                    },
                });
            });

            $('body').delegate(".confirm-btn","click",function () {
              var item            = $("#selected_item").val();
              var package_id      = $("#package_id").val();
              var username        = $("#username").val();
              var user_id         = $("#user_id").val();
              var user_city_id    = $("#user_city_id").val();
              var user_branch_id  = $("#user_branch_id").val();

              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
                       
              $.ajax({
                  type: 'post',
                  url: url+'/api/removed-waybill',
                  dataType:'json',
                  data: {
                      'item'      		:item,
                      'package_id'		:package_id,
                      'username'  		:username,
                      'user_id'  		:user_id,
                      'user_city_id'  	:user_city_id,
                      'user_branch_id'  :user_branch_id,
                  },
                  success: function(data) { 
                      console.log(data);
                      if(data.success == 1){
                      	location.reload();
                      }
                  },
              });   
            
          });

            $('body').delegate(".cancelled-btn","click",function () {
              var item            = $("#selected_item").val();
              var username        = $("#username").val();
              var user_id         = $("#user_id").val();
              var user_city_id    = $("#user_city_id").val();
              var user_branch_id  = $("#user_branch_id").val();
              var reason  = $("#reason").val();

              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
                       
              $.ajax({
                  type: 'post',
                  url: url+'/api/cancelled-waybill',
                  dataType:'json',
                  data: {
                      'item'      :item,
                      'username'  :username,
                      'user_id'  :user_id,
                      'user_city_id'  :user_city_id,
                      'user_branch_id'  :user_branch_id,
                      'reason'  :reason,
                  },
                  success: function(data) { 
                      console.log(data);
                      if(data.success == 1){
                      	$(".response-log").append(
                      		'<div class="d-flex">'
									+'<div class="avatar bg-light-primary rounded float-start">'
										+'<div class="avatar-content">'
										    +'<svg class="avatar-icon font-medium-3" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>'
										+'</div>'
									+'</div>'
									+'<div class="transaction-percentage">'
										+'<h6 class="transaction-title">'+data.branch+'</h6>'
										+'<span>'+data.log+'</span>'
									+'</div>'
								+'</div>'
								+'<div class="fw-light text-danger">'+data.date+'</div>'
						)}
                      
                      $('.cancel-btn').addClass('hidden');
                      $('.match-height').addClass('denied');
                      $('.cancelled-msg').removeClass('hide');
                      
                  },
              	});  
            });

            $('body').delegate(".sync-odoo","click",function () {
                var waybill_no = $("#waybill_no").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                 
                $.ajax({
                    type: 'post',
                    url: url+'/api/check-end-point',
                    dataType:'json',
                    data: {
                        'waybill_no' : waybill_no
                    },
                    success: function(data) {     
                        console.log(data);
                        $("#end_point").text(data.end_point);
                        setTimeout(function(){$('#sync-odoo').modal('hide')},1000);
                    },
                });
                
            });

            $('body').delegate(".sync-log","click",function () {
                var waybill_no = $("#waybill_no").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                 
                $.ajax({
                    type: 'post',
                    url: url+'/api/manual-sync-logs',
                    dataType:'json',
                    data: {
                        'waybill_no' : waybill_no
                    },
                    success: function(data) {     
                        if(data.success){
                        	location.reload();
                        }
                    },
                });
                
            });
        }); 
    </script>
</body>
</html>