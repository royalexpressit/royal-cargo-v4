<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Search Data</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
	@include('operator.header')

    <div class="app-content content ">
      	<div class="content-overlay"></div>
      	<div class="header-navbar-shadow"></div>
      	<div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Search Data</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item active">Search Data</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                </div>
            </div>
	        <div class="content-body">
				<section class="basic-select2">
					<div class="row">
					    <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">စာအမှတ်(waybill) ရှာရန်</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <form action="{{ route('search-data','form-1') }}" method="GET">
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">စာအမှတ်</label>
                                        <input type="text" name="waybill" class="form-control" placeholder="Z1234566789" autofocus />
                                    </div>
                                    <div class="mb-0">
                                        <button type="submit" class="btn btn-relief-primary waves-effect scan-btn">ရှာမည်</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
					    <div class="col-md-4">
					      	<div class="card">
						        <div class="card-header">
						          	<h4 class="card-title text-primary">အထုပ်(package) ရှာရန်</h4>
						        </div>
						        <hr>
					        	<div class="card-body">
					            	<form action="{{ route('search-data','form-2') }}" method="GET">
					            	<div class="mb-1">
						                <label class="form-label text-primary" for="select2-icons">အထုပ်အမှတ်</label>
						                <input type="text" name="package_no" class="form-control" placeholder="PG000012" />
					            	</div>
						            <div class="mb-0">
                            			<button type="submit" class="btn btn-relief-primary waves-effect scan-btn">ရှာမည်</button>
						            </div>
                                    </form>
					        	</div>
					      	</div>
					    </div>
					    <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">စာရင်းအမှတ်(CTF) ရှာရန်</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <form action="{{ route('search-data','form-3') }}" method="GET">
                                        <div class="mb-1">
                                            <label class="form-label text-primary" for="select2-icons">စာရင်းအမှတ်</label>
                                            <input type="text" name="ctf_no" class="form-control" placeholder="AA123456" />
                                        </div>
                                        <div class="mb-0">
                                            <button type="submit" class="btn btn-relief-primary waves-effect scan-btn">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
					</div>
				</section>
	        </div>
      	</div>
    </div>

	@include('customizer')
  	@include('footer')
	<input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="service_point" value="{{ city(user()->city_id)['is_service_point'] }}">
    <input type="hidden" id="to_branch" value="{{ main_branch(user()->city_id)->id }}">

	<!-- Modal -->
    <div class="modal fade" id="fullscreenModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalFullTitle">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>
                        Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel
                        augue laoreet rutrum faucibus dolor auctor.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-label-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script>
      	$(window).on('load',  function(){
        	if (feather) {
          		feather.replace({ width: 14, height: 14 });
        	}
      	});

      	$(document).ready(function(){
            var url     = $("#url").val();
            var scanned = 0;
            var success = 0;
            var failed  = 0;
            var waybills= [];

            var voice   = 'alert-1.mp3';
            var city    = $("#city").val();
            var _token  = $("#_token").val();

            $("#form").submit(function(event){
                event.preventDefault();  
            });

            $("#waybill").on("keydown",function search(e){
                if(e.keyCode == 13) {
                    waybill   = $("#waybill").val().toUpperCase();
                    qty       = 1;//$("#qty").val();

                    if(waybill.length > 10){
                        //added item into array
                        waybills.push({waybill_no: waybill,qty:qty});

                        console.log(waybills);

                        $(".get-started").hide();
                        //valid length && continue
                        ++scanned;
                        $("#scanned").text(scanned);
                        $("#success").text(0);
                        $("#failed").text(0);
                                    
                        $("#failed-lists").empty();
                        $("#scanned-lists").show();
           
                        $(".scan-btn").removeAttr('disabled');
                        $("#scanned-lists").prepend('<li class="list-group-item align-items-center"><svg class="list-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text me-1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg><span class="fs-18">'+waybill+'</span></span><input type="text" id="qty" class="form-control pull-right qty-box border-warning" value="1" >');
                        
                        if(scanned > 10){
                        	$("#scanned-lists").addClass('content-scroll'); 
                        	$(".scroll-msg").removeClass('hide');
                        }

                        $(this).val('');
                        $(".check-number").addClass('hide');     

                        //$('#multi_scanned_waybills').prepend(raw); 
                            
                        //limit scanned count with 25
                        if(scanned == 25){
                            $("#waybill").attr("disabled", true);
                            audioElement.play();
                            $('.bs-example-modal-center').modal('show');
                            setTimeout(function(){
                                $('.bs-example-modal-center').modal('hide');
                            },5000);
                            console.log(voice);
                        }
                    }else{
                        //invalid length && try again
                        $(".check-number").removeClass('hide');
                        $("#waybill").val('');
                    }

                    //removed fixed height for error lists
                    $("#failed-lists").removeClass('scanned-panel');
                }
            });
        }); 
    </script>
</body>
</html>