<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Outbound</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Failed Logs</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item active text-capitalize">Failed Logs
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                  
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                </div>
            </div>
              
            <div class="content-body">
                <section id="block-level-buttons">
                    <div class="row">
                        <div class="col-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Outbound Failed Logs</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="d-grid col-lg-12 col-md-12 mb-1 mb-lg-0 ">
                                            <a href="{{ url('_dist/logs/outbound-received-failed.txt') }}" class="btn btn-primary mb-1" download="outbound-received-failed-{{ date('ymd') }}.txt">Received Failed Logs</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="d-grid col-lg-12 col-md-12 mb-1 mb-lg-0 ">
                                            <a href="{{ url('_dist/logs/outbound-branch-out-failed.txt') }}" class="btn btn-primary mb-1" download="outbound-branch-out-failed-{{ date('ymd') }}.txt">Branch Out Failed Logs</a>
                                        </div>
                                    </div>
                                    @if(user()->branch_id == 11)
                                    <div class="row">
                                        <div class="d-grid col-lg-12 col-md-12 mb-1 mb-lg-0 ">
                                            <a href="{{ url('_dist/logs/scan-branch-out-ygn.txt') }}" class="btn btn-primary mb-1" download="scan-branch-out-ygn-{{ date('ymd') }}.txt">Scan Branch Out Logs (YGN)</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Inbound Failed Logs</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="d-grid col-lg-12 col-md-12 mb-1 mb-lg-0 ">
                                            <a href="{{ url('_dist/logs/outbound-branch-out-failed.txt') }}" class="btn btn-success mb-1" download="inbound-branch-in-failed-{{ date('ymd') }}.txt">Branch In Failed Logs</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="d-grid col-lg-12 col-md-12 mb-1 mb-lg-0 ">
                                            <a href="{{ url('_dist/logs/outbound-branch-out-failed.txt') }}" class="btn btn-success mb-1" download="inbound-handover-failed-{{ date('ymd') }}.txt">Handover Failed Logs</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="d-grid col-lg-12 col-md-12 mb-1 mb-lg-0 ">
                                            <a href="{{ url('_dist/logs/outbound-branch-out-failed.txt') }}" class="btn btn-success mb-1" download="inbound-received-failed-{{ date('ymd') }}.txt">Received Failed Logs</a>
                                        </div>
                                    </div>
                                    @if(user()->branch_id == 11)
                                    <div class="row">
                                        <div class="d-grid col-lg-12 col-md-12 mb-1 mb-lg-0 ">
                                            <a href="{{ url('_dist/logs/scan-handover-ygn.txt') }}" class="btn btn-success mb-1" download="scan-handover-ygn-{{ date('ymd') }}.txt">Scan Handover Logs (YGN)</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="city_id" value="{{ user()->city_id }}">

    <!-- Modal -->
              <div
                class="modal fade modal-danger text-start"
                id="duplicate"
                tabindex="-1"
                aria-labelledby="myModalLabel120"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="myModalLabel120">Danger Modal</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <ul class="list-group" id="waybill-listing">
                        
                        </ul>
                    </div>
                    <div class="modal-footer">
                      
                    </div>
                  </div>
                </div>
              </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>

    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });
    </script>
</body>
</html>