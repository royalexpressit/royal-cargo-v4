<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Branches</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.main-city.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Create New Branch</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">City</label>
                                        <select data-placeholder="Select a state..." class="select2 form-select" id="city_id">
                                            @foreach($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->shortcode.' ('.$city->name.')' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">Branch Name</label>
                                        <input type="text" id="branch_name" class="form-control" placeholder="YGN-SOKA" id="branch_name" autofocus />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">Office</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="office" id="office_1" value="1" checked />
                                                    <label class="form-check-label" for="office_1">Main</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="office" id="office_2" value="2" />
                                                    <label class="form-check-label" for="office_2">Normal</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-1 additional hide">
                                        <label class="form-label text-primary" for="select2-icons">Type</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="type" id="type_1" value="2" />
                                                    <label class="form-check-label" for="type_1">Sorting</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="type" id="type_2" value="3" />
                                                    <label class="form-check-label" for="type_2">Transit</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        <button type="button" class="btn btn-relief-primary waves-effect create-btn">Create</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    @include('customizer')
    @include('footer')

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
    <!-- END: Page JS-->

    <script>
        

      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      });

    $(document).ready(function(){
        
             

   
        $('.create-btn').on("click",function search(e) {
            var city_id     = $("#city_id").val();
            var branch_name = $("#branch_name").val();
            var office      = 0;
            var sorting     = 0;
            var transit     = 0;

            if($("#office_1").is(":checked")){
                office  = 1;
            }else{
                office = 2;
                if($("#type_1").is(":checked")){
                    sorting = 1;
                }

                if($("#type_2").is(":checked")){
                    transit = 1;
                }
            }
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                   
            $.ajax({
                type: 'post',
                url: url+'/api/branch/create',
                dataType:'json',
                data: {
                    'city_id':city_id,
                    'branch' :branch_name,
                    'office' :office,
                    'sorting':sorting,
                    'transit':transit,
                },
                success: function(data) {
                    if(data.success == 1){
                    Swal.fire({
                        position:"top-end",
                        title:"Good Job",
                        icon:"success",
                        text:"You have been created new branch.",
                        showConfirmButton:!1,
                        timer:3000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    }).then(function(){
                        location.href = url+"/branches";
                    });
                }else{
                    Swal.fire({
                        position:"top-end",
                        title:"Sorry",
                        icon:"error",
                        text:"Branch is already exists.",
                        showConfirmButton:!1,
                        timer:3000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    });
                }
                },
            });     
        }); 

        $('#office_1,#office_2').click(function(){
            if($("#office_1").is(":checked")){
                $(".additional").addClass('hide');
            }else{
                $(".additional").removeClass('hide');
            }
        });      


    });



    </script>
</body>
</html>