<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Deleted Data</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('admin.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Delete Data</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item active text-capitalize">{{ $type }}</li>
                                    <li class="breadcrumb-item active text-capitalize">{{ $result }}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <button type="button" class="btn btn-relief-danger" data-bs-toggle="modal" data-bs-target="#delete">
                ဖျတ်မည်
              </button>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="alert alert-danger mt-1 alert-validation-msg msg-box hide" role="alert">
                    <div class="alert-body d-flex align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                        <span>The value is <strong>invalid</strong>. You can only enter numbers.</span>
                    </div>
                </div>

                @if($type == 'ctf')
                <section class="basic-select2">
                    @if($ctf)
                    <div class="card bg-danger text-white">
                        <div class="card-body">
                            
                            <h3 class="text-white">Delete Type : {{ $type }}</h3>

                            <h5 class="text-white">Listing To Delete:</h5>
                            <span># {{ $ctf_no }}</span>
                            
                        </div>
                    </div>

                    <div class="card bg-light-danger border-danger">
                        <div class="card-body">
                            <h4 class="card-title text-danger">Affected Packages</h4>
                            <ul class="list-group list-group-numbered " id="package-items">
                                
                            </ul>
                        </div>
                    </div>

                    <div class="card bg-light-danger border-danger">
                        <div class="card-body mxh">
                            <h4 class="card-title text-danger">Affected Waybills</h4>
                            <ul class="list-group list-group-numbered waybill-items">
                                @foreach($waybills as $waybill)
                                <li class="list-group-item waybill-item">
                                  <span>{{ $waybill->waybill_no }}</span>
                                  <span class="package_no badge bg-light-warning pull-right">{{ $waybill->package_no }}</span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @else
                    <div class="card bg-danger text-white">
                        <div class="card-body">
                           <strong>No record found.</strong>
                        </div>
                    </div>
                        
                    @endif
                </section>
                @elseif($type == 'package')
                <section class="basic-select2">
                    <div class="card bg-danger text-white">
                        <div class="card-body">
                            <h1>{{ $type }}</h1>
                            <h4 class="card-title text-white">Delete Package</h4>
                            <strong># {{ $package_no }}</strong>

                        </div>
                    </div>

                    <div class="card bg-light-danger border-danger">
                        <div class="card-body">
                            <h4 class="card-title text-danger">Affected Waybills</h4>
                            <ul class="list-group list-group-numbered waybill-items">
                                @foreach($waybills as $waybill)
                                <li class="list-group-item waybill-item">
                                  <span>{{ $waybill->waybill_no }}</span>
                                  <span class="package_no badge bg-light-warning pull-right">{{ $waybill->package_no }}</span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </section>
                @else
                <section class="basic-select2">
                    <div class="card bg-danger text-white">
                        <div class="card-body">
                            @if($waybill)
                            <h3 class="text-white">Delete Type : {{ $type }}</h3>

                            <h5 class="text-white">Listing To Delete:</h5>
                            <span># {{ $waybill->waybill_no }}</span>
                            @else
                                <strong>No record found.</strong>
                            @endif
                        </div>
                    </div>
                </section>
                @endif
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="" id="type" value="{{ $type }}">
    <input type="" id="ctf_no" value="{{ $ctf_no }}">
    <input type="" id="package_no" value="{{ $package_no }}">
    <input type="" id="waybill_no" value="{{ $waybill_no }}">
    <div
                class="modal fade modal-danger text-start"
                id="delete"
                tabindex="-1"
                aria-labelledby="myModalLabel120"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="myModalLabel120">Danger Modal</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      Tart lemon drops macaroon oat cake chocolate toffee chocolate bar icing. Pudding jelly beans
                      carrot cake pastry gummies cheesecake lollipop. I love cookie lollipop cake I love sweet gummi
                      bears cupcake dessert.
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger btn-delete" data-bs-dismiss="modal">လုပ်ဆောင်မည်</button>
                    </div>
                  </div>
                </div>
              </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>

    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>


    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });

        $(document).ready(function(){
            var url     = $("#url").val();
            var scanned = 0;
            var success = 0;
            var failed  = 0;
            var packages= [];
            var key     = 0;

            var voice   = 'alert-1.mp3';
            var city    = $("#city").val();
            var _token  = $("#_token").val();

            $("#form").submit(function(event){
                event.preventDefault();  
            });


            $.each($('.package_no').data(),function(i, e) {
                alert('worked');
               //alert('name='+$('.package_no').text()+ ' value=' +e);
            });

           //  $(".waybill-item").each(function() {
           //      var id = $('.package_no').text();

           //      packages.push(id);
           // });

            var item = $(".waybill-items li")
                item.each(function() {
                  var package =  $(".package_no",this).text();
                  packages.push(package); 
            });

            var unique = packages.filter(function(item, i, packages) {
                return i == packages.indexOf(item);
            });

            $.each(unique, function (i, item) {
                $('#package-items').append('<li class="list-group-item "><span>'+item+'</span></li>' );
            });

            $(".btn-delete").on("click",function search(e) {
                var type = $('#type').val();
                var ctf  = $('#ctf_no').val();
                var pkg  = $('#package_no').val();
                var waybill  = $('#waybill_no').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                       
                $.ajax({
                    type: 'post',
                    url: url+'/api/deleted-data',
                    dataType:'json',
                    data: {
                        'type'  :type,
                        'ctf'   :ctf,
                        'pkg'   :pkg,
                        'waybill':waybill
                    },
                    success: function(data) {
                        if(data.success == 1){
                            $('.msg-box').removeClass('hide');
                        }
                    },
                });
            });

        }); 
    </script>
</body>
</html>