<!doctype html>
<html lang="en">
<head> 
    <meta charset="utf-8" />
    <title>Royal Cargo - Outbound</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <link rel="shortcut icon" href="{{ asset('dist/images/favicon.png') }}">
        
    <!-- plugin css -->
    <link href="{{ asset('dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/spectrum.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('dist/css/datepicker.min.css') }}">
        
    <link href="{{ asset('dist/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/app.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/custom.css') }}"  rel="stylesheet" type="text/css" />
</head>
<body data-layout="horizontal" data-topbar="colored">
    <!-- Begin page -->
    <div id="layout-wrapper">
        @include('operator.header')
    
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h5 class="mb-0">
                                    <a href="{{ url('') }}">Dashboard</a>
                                    > <a href="{{ url('outbound') }}"> Outbound</a> 
                                    > Scan
                                </h5>

                                <div class="page-title-right">
                                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                            <div class="col-lg-3">
                                <h5 class="card-header bg-danger text-white">Outbound <span class="pull-right">Step - 1</span></h5>
                                <div class="card card-body">
                                    <a href="{{ url('outbound/scan/collected') }}" class="btn btn-danger btn-md waves-effect waves-light mb-1">
                                        <div class="corner">
                                            <i class="mdi mdi-qrcode-scan h1 text-white"></i>
                                            <h5 class="mt-1 text-white">Scan Collected</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            @if(check_service_point(user()->city_id) != 1)
                            <div class="col-lg-3">
                                <h5 class="card-header bg-warning text-white">Outbound <span class="pull-right">Step - 2</span></h5>
                                <div class="card card-body">
                                    <a href="{{ url('outbound/scan/handover') }}" class="btn btn-warning btn-md waves-effect waves-light mb-1">
                                        <div class="corner">
                                            <i class="mdi mdi-qrcode-scan h1 text-white"></i>
                                            <h5 class="mt-1 text-white">Scan Handover</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            @if(branch(user()->branch_id)['is_main_office'] == 1)
                            <div class="col-lg-3">
                                <h5 class="card-header bg-success text-white">Outbound <span class="pull-right">Step - 3</span></h5>
                                <div class="card card-body">
                                    <a href="{{ url('outbound/scan/received') }}" class="btn btn-success btn-md waves-effect waves-light mb-1">
                                        <div class="corner">
                                            <i class="mdi mdi-qrcode-scan h1 text-white"></i>
                                            <h5 class="mt-1 text-white">Scan Received</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <h5 class="card-header bg-primary text-white">Outbound <span class="pull-right">Step - 4</span></h5>
                                <div class="card card-body">
                                    <a href="{{ url('outbound/scan/branch-out') }}" class="btn btn-primary btn-md waves-effect waves-light mb-1">
                                        <div class="corner">
                                            <i class="mdi mdi-qrcode-scan h1 text-white"></i>
                                            <h5 class="mt-1 text-white">Scan Branch Out</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @else
                            <div class="col-lg-3">
                                <h5 class="card-header bg-primary text-white">Outbound <span class="pull-right">Step - 2</span></h5>
                                <div class="card card-body">
                                    <a href="{{ url('outbound/scan/branch-out') }}" class="btn btn-primary btn-md waves-effect waves-light mb-1">
                                        <div class="corner">
                                            <i class="mdi mdi-qrcode-scan h1 text-white"></i>
                                            <h5 class="mt-1 text-white">Scan Branch Out</h5>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            @endif
                        </div>
                        <!-- end row -->

                        <div class="card">
                            <h4 class="card-header ">About Outbound</h4>
                            <div class="card-body">
                                <div class="mb-3">
                                    <h4 class="card-title text-danger">1.1 Outbound Collected</h4>
                                    <p class="card-text">Outbound Collected သည် outbound တွင်ပထမဦးဆုံး လုပ်ဆောင်ရမည့်အဆင့် ဖြစ်သည်။ မိမိရုံးမှ
                                        စာပို့သမား(သို့)ကောင်တာမှ ကောက်ယူထားသော စာများအား cargo တွင် စတင်စာရင်သွင်းခြင်းကို collected လုပ်သည်ဟု
                                        ဆိုလိုပါသည်။ collected အဆင့်အား <strong class="text-danger">ရုံးအများရှိသောမြို့နှင့် တစ်ရုံးတည်းရှိသောမြို့ ၂မြို့လုံး</strong> မဖြစ်မနေ လုပ်ဆောင်ရပါမည်။
                                    </p>
                                    <button class="btn btn-success btn-sm cities" data-bs-toggle="modal" data-bs-target="#modal-3"><i class="mdi mdi-check"></i> လုပ်ဆောင်ရန်လိုအပ်သောမြို့များ</button>
                                    <button class="btn btn-danger btn-sm" disabled=""><i class="mdi mdi-close"></i> လုပ်ဆောင်ရန်မလိုအပ်သောမြို့များ</button>
                                </div>
                                <div class="mb-3">
                                    <h4 class="card-title text-warning">1.2 Outbound Handover</h4>
                                    <p class="card-text">Outbound Handover သည် outbound တွင်ဒုတိယမြောက် လုပ်ဆောင်ရမည့်အဆင့် ဖြစ်သည်။ မိမိရုံးမှ
                                        ကောက်ယူထားသော စာများအား သက်ဆိုင်ရာ စာလက်ခံမည့်ဌာနသို့ စာလွှဲပြောင်းပေးခြင်းကို handover လုပ်သည်ဟု
                                        ဆိုလိုပါသည်။ handover အဆင့်အား ရုံးအများရှိသောမြို့တွင်သာ အသုံးပြုရန်လိုအပ်ပြီး <strong class="text-danger">တစ်ရုံးတည်းရှိသောမြို့ဖြစ်ပါက ထိုအဆင့်အားလုပ်ဆောင်ရန်မလို</strong> အပ်ပါ။
                                        တစ်ရုံးတည်းရှိသောမြို့များသည် outbound collected လုပ်လိုက်သည်နှင့် outbound received အဆင့်ထိ ရောက်ရှိသွားမည်ဖြစ်သည်။
                                    </p>
                                    <button class="btn btn-success btn-sm main-cities" data-bs-toggle="modal" data-bs-target="#modal-1"><i class="mdi mdi-check"></i> လုပ်ဆောင်ရန်လိုအပ်သောမြို့များ</button>
                                    <button class="btn btn-danger btn-sm service-points" data-bs-toggle="modal" data-bs-target="#modal-2"><i class="mdi mdi-close"></i> လုပ်ဆောင်ရန်မလိုအပ်သောမြို့များ</button>
                                </div>
                                <div class="mb-3">
                                    <h4 class="card-title text-success">1.3 Outbound Received</h4>
                                    <p class="card-text">Outbound received သည် outbound တွင်တတိယမြောက် လုပ်ဆောင်ရမည့်အဆင့် ဖြစ်သည်။ သက်ဆိုင်ရာရုံးများမှ
                                        မိမိရုံး(သို့)မိမိဌာနသို့ လွှဲပြောင်းထားသောစာများအာ လက်ခံခြင်းကို received လုပ်သည်ဟု
                                        ဆိုလိုပါသည်။ received အဆင့်အား ရုံးအများရှိသောမြို့ နှင့် စာစီစစ်ဌာန(sorting)ရှိသောမြို့များတွင်သာ အသုံးပြုရန်လိုအပ်ပြီး <strong class="text-danger">တစ်ရုံးတည်းရှိသောမြို့ဖြစ်ပါက ထိုအဆင့်အားလုပ်ဆောင်ရန်မလို</strong> အပ်ပါ။
                                        တစ်ရုံးတည်းရှိသောမြို့များသည် outbound collected လုပ်လိုက်သည်နှင့် outbound received အဆင့်ထိ ရောက်ရှိနေပြီးသားဖြစ်သည်။
                                    </p>
                                    <button class="btn btn-success btn-sm main-cities" data-bs-toggle="modal" data-bs-target="#modal-1"><i class="mdi mdi-check"></i> လုပ်ဆောင်ရန်လိုအပ်သောမြို့များ</button>
                                    <button class="btn btn-danger btn-sm service-points" data-bs-toggle="modal" data-bs-target="#modal-2"><i class="mdi mdi-close"></i> လုပ်ဆောင်ရန်မလိုအပ်သောမြို့များ</button>
                                </div>
                                <div class="mb-3">
                                    <h4 class="card-title text-primary">1.4 Outbound Branch Out</h4>
                                    <p class="card-text">Outbound branch out သည် မိမိရုံး(သို့)မိမိဌာနမှ လက်ခံထားသော စာများအား
                                        စာလက်ခံမည့် သက်ဆိုင်ရာမြို့သို့ စာပို့ဆောင်ပြီဖြစ်ကြောင်း စာထွက်လုပ်ခြင်းကို branch out လုပ်သည်ဟုဆိုလိုပါသည်။ branch out အဆင့်အား <strong class="text-danger">ရုံးအများရှိသောမြို့နှင့် တစ်ရုံးတည်းရှိသောမြို့ ၂မြို့လုံး</strong> မဖြစ်မနေ လုပ်ဆောင်ရပါမည်။
                                    </p>
                                    <button class="btn btn-success btn-sm cities" data-bs-toggle="modal" data-bs-target="#modal-3"><i class="mdi mdi-check"></i> လုပ်ဆောင်ရန်လိုအပ်သောမြို့များ</button>
                                    <button class="btn btn-danger btn-sm" disabled=""><i class="mdi mdi-close"></i> လုပ်ဆောင်ရန်မလိုအပ်သောမြို့များ</button>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <h4 class="card-header ">About Transit</h4>
                            <div class="card-body">
                                <div class="mb-3">
                                    <h4 class="card-title text-success">1.3 Transit Received</h4>
                                    <p class="card-text">Outbound received သည် transit လုပ်ဆောင်ရာတွင်တတိယမြောက် လုပ်ဆောင်ရမည့်အဆင့် ဖြစ်သည်။ သက်ဆိုင်ရာရုံးများမှ
                                        transit branchအား လွှဲပြောင်းထားသောစာများအာ လက်ခံခြင်းကို transit received လုပ်သည်ဟု
                                        ဆိုလိုပါသည်။ received အဆင့်အား transit branch ရှိသောမြို့များတွင်သာ အသုံးပြုနိင်မည်ဖြစ်သည်။
                                    </p>
                                </div>
                                <div class="mb-3">
                                    <h4 class="card-title text-primary">1.4 Transit Branch Out</h4>
                                    <p class="card-text">Transit branch out သည် transit branchမှ လက်ခံထားသော စာများအား
                                        စာလက်ခံမည့် သက်ဆိုင်ရာမြို့သို့ စာပို့ဆောင်ပြီဖြစ်ကြောင်း စာထွက်လုပ်ခြင်းကို branch out လုပ်သည်ဟုဆိုလိုပါသည်။
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                
                @include('footer')
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            <div data-simplebar class="h-100">

                <div class="rightbar-title d-flex align-items-center px-3 py-4">
            
                    <h5 class="m-0 me-2">Settings</h5>

                    <a href="javascript:void(0);" class="right-bar-toggle ms-auto">
                        <i class="mdi mdi-close noti-icon"></i>
                    </a>
                </div>



                <!-- Settings -->
                <hr class="mt-0" />
                <h6 class="text-center mb-0">Choose Layouts</h6>

                <div class="p-4">
                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-1.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="light-mode-switch" checked />
                        <label class="form-check-label" for="light-mode-switch">Light Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-2.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="dark-mode-switch" />
                        <label class="form-check-label" for="dark-mode-switch">Dark Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-3.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="rtl-mode-switch" />
                        <label class="form-check-label" for="rtl-mode-switch">RTL Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-4.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-5">
                        <input class="form-check-input theme-choice" type="checkbox" id="dark-rtl-mode-switch">
                        <label class="form-check-label" for="dark-rtl-mode-switch">Dark RTL Mode</label>
                    </div>

            
                </div>

            </div> <!-- end slimscroll-menu-->
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>
            <div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-scrollable">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title text-success" id="exampleModalScrollableTitle">လုပ်ဆောင်ရန်လိုအပ်သောမြို့များ</h5>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>
                                                                            <ul class="list-unstyled mb-0" id="main-cities">

                                                                            </ul>
                                                                        </p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                                        </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
            </div>
            <div class="modal fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-scrollable">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title text-danger" id="exampleModalScrollableTitle">လုပ်ဆောင်ရန်မလိုအပ်သောမြို့များ</h5>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>
                                                                            <ul class="list-unstyled mb-0" id="service-points">

                                                                            </ul>
                                                                        </p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                                        </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
            </div>

            <div class="modal fade" id="modal-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-scrollable">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title text-success" id="exampleModalScrollableTitle">လုပ်ဆောင်ရန်လိုအပ်သောမြို့များ</h5>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>
                                                                            <ul class="list-unstyled mb-0" id="cities">

                                                                            </ul>
                                                                        </p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                                        </div>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        <!-- JAVASCRIPT -->
        <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('dist/js/metisMenu.min.js') }}"></script>
        <script src="{{ asset('dist/js/simplebar.min.js') }}"></script>
        <script src="{{ asset('dist/js/waves.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.counterup.min.js') }}"></script>

        <!-- plugins -->
        <script src="{{ asset('dist/js/select2.min.js') }}"></script>
        <script src="{{ asset('dist/js/spectrum.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.bootstrap-touchspin.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-maxlength.min.js') }}"></script>
        <script src="{{ asset('dist/js/datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/form-advanced.init.js') }}"></script>
        <!-- App js -->
        <script src="{{ asset('dist/js/app.js') }}"></script>
        <script src="{{ asset('dist/js/custom.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var url         = $("#url").val();

                $('body').delegate(".cities","click",function () {
                    $.ajax({
                        url: url+'/api/v4/cities',
                        type: 'GET',
                        data: {},
                        success: function(data){
                            console.log(data);
                            $.each( data, function( key, value ) {
                                $("#cities").append('<li class="text-success"><i class="mdi mdi-check"></i> '+value.name+' ('+value.shortcode+')</li>');
                            })
                        }
                    });     
                });

                $('body').delegate(".main-cities","click",function () {
                    $.ajax({
                        url: url+'/api/v4/main-cities',
                        type: 'GET',
                        data: {},
                        success: function(data){
                            console.log(data);
                            $.each( data, function( key, value ) {
                                $("#main-cities").append('<li class="text-success"><i class="mdi mdi-check"></i> '+value.name+' ('+value.shortcode+')</li>');
                            })
                        }
                    });     
                });

                $('body').delegate(".service-points","click",function () {
                    $.ajax({
                        url: url+'/api/v4/service-points',
                        type: 'GET',
                        data: {},
                        success: function(data){
                            console.log(data);
                            $.each( data, function( key, value ) {
                                $("#service-points").append('<li class="text-danger"><i class="mdi mdi-close"></i> '+value.name+' ('+value.shortcode+')</li>');
                            })
                        }
                    });     
                });
            });
        </script>
    </body>
</html>
