<!doctype html>
<html lang="en">
    <head>
        
    <meta charset="utf-8" />
        <title>Horizontal Layout | Minible - Admin & Dashboard Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesbrand" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- plugin css -->
        <link href="{{ asset('dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dist/css/spectrum.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('dist/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('dist/css/datepicker.min.css') }}">
        <!-- Bootstrap Css -->
        <link href="{{ asset('dist/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{ asset('dist/css/app.min.css') }}"  rel="stylesheet" type="text/css" />

    </head>

    <body data-layout="horizontal" data-topbar="colored">

        <!-- Begin page -->
        <div id="layout-wrapper">

            @include('admin.header')
    


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">Outbound</h4>

                                    <div class="page-title-right">
                                        <div class="form-check form-switch form-switch-md mb-3" dir="ltr">
                                            <input type="checkbox" class="form-check-input" id="customSwitchsizemd">
                                            <label class="form-check-label" for="customSwitchsizemd">Sync To Odoo</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                        <div class="row">
                            <div class="col-lg-12">
                                
                                        <form>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <h5 class="card-header ">Scan Collections</h5>
                                                    <div class="card card-body">
                                                        <a href="{{ url('outbound/scan/collected') }}" class="btn btn-danger waves-effect waves-light mb-1">Scan Collected</a>
                                                        <a href="{{ url('outbound/scan/handover') }}" class="btn btn-warning waves-effect waves-light mb-1">Scan Handover</a>
                                                        <a href="{{ url('outbound/scan/received') }}" class="btn btn-success waves-effect waves-light mb-1">Scan Received</a>
                                                        <a href="{{ url('outbound/scan/branch-out') }}" class="btn btn-primary waves-effect waves-light mb-1">Scan Branch Out</a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="mb-3">
                                                        <h5 class="card-header ">Scan Received</h5>
                                                        <div class="card card-body">
                                                            <div>
                                                                <label class="form-label">Your Branch</label>
                                                                <select class="form-control select2">
                                                                    <option>Select</option>
                                                                    <optgroup label="Alaskan/Hawaiian Time Zone">
                                                                        <option value="AK">Alaska</option>
                                                                        <option value="HI">Hawaii</option>
                                                                    </optgroup>
                                                                    <optgroup label="Pacific Time Zone">
                                                                        <option value="CA">California</option>
                                                                        <option value="NV">Nevada</option>
                                                                        <option value="OR">Oregon</option>
                                                                        <option value="WA">Washington</option>
                                                                    </optgroup>
                                                                    <optgroup label="Mountain Time Zone">
                                                                        <option value="AZ">Arizona</option>
                                                                        <option value="CO">Colorado</option>
                                                                        <option value="ID">Idaho</option>
                                                                        <option value="MT">Montana</option>
                                                                        <option value="NE">Nebraska</option>
                                                                        <option value="NM">New Mexico</option>
                                                                        <option value="ND">North Dakota</option>
                                                                        <option value="UT">Utah</option>
                                                                        <option value="WY">Wyoming</option>
                                                                    </optgroup>
                                                                    <optgroup label="Central Time Zone">
                                                                        <option value="AL">Alabama</option>
                                                                        <option value="AR">Arkansas</option>
                                                                        <option value="IL">Illinois</option>
                                                                        <option value="IA">Iowa</option>
                                                                        <option value="KS">Kansas</option>
                                                                        <option value="KY">Kentucky</option>
                                                                        <option value="LA">Louisiana</option>
                                                                        <option value="MN">Minnesota</option>
                                                                        <option value="MS">Mississippi</option>
                                                                        <option value="MO">Missouri</option>
                                                                        <option value="OK">Oklahoma</option>
                                                                        <option value="SD">South Dakota</option>
                                                                        <option value="TX">Texas</option>
                                                                        <option value="TN">Tennessee</option>
                                                                        <option value="WI">Wisconsin</option>
                                                                    </optgroup>
                                                                    <optgroup label="Eastern Time Zone">
                                                                        <option value="CT">Connecticut</option>
                                                                        <option value="DE">Delaware</option>
                                                                        <option value="FL">Florida</option>
                                                                        <option value="GA">Georgia</option>
                                                                        <option value="IN">Indiana</option>
                                                                        <option value="ME">Maine</option>
                                                                        <option value="MD">Maryland</option>
                                                                        <option value="MA">Massachusetts</option>
                                                                        <option value="MI">Michigan</option>
                                                                        <option value="NH">New Hampshire</option>
                                                                        <option value="NJ">New Jersey</option>
                                                                        <option value="NY">New York</option>
                                                                        <option value="NC">North Carolina</option>
                                                                        <option value="OH">Ohio</option>
                                                                        <option value="PA">Pennsylvania</option>
                                                                        <option value="RI">Rhode Island</option>
                                                                        <option value="SC">South Carolina</option>
                                                                        <option value="VT">Vermont</option>
                                                                        <option value="VA">Virginia</option>
                                                                        <option value="WV">West Virginia</option>
                                                                    </optgroup>
                                                                </select>
                                                            </div>
                                                            <div class="mb-1">
                                                                <label class="form-label">Waybill No</label>
                                                                <input type="text" class="form-control" maxlength="25" name="defaultconfig" id="defaultconfig">
                                                            </div>
                                                            <div>
                                                                <button type="button" class="btn btn-success waves-effect waves-light">
                                                                    <i class="uil uil-check me-2"></i> Received
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <h5 class="card-header ">Scanned: 0, Success: 0, Failed: 0</h5>
                                                    
                                                    <div class="card">
                                            <div class="card-body">
                                                <ol class="activity-feed mb-0 ps-2" data-simplebar style="max-height: 336px;">
                                                    <li class="feed-item">
                                                        <div class="feed-item-list">
                                                            <p class="text-muted mb-1 font-size-13">Today<small class="d-inline-block ms-1">12:20 pm</small></p>
                                                           
                                                        </div>
                                                    </li>
                                                    <li class="feed-item">
                                                        <p class="text-muted mb-1 font-size-13">22 Jul, 2020 <small class="d-inline-block ms-1">12:36 pm</small></p>
                                                    </li>

                                                </ol>

                                            </div>
                                            </div>
                                                </div>
                                            </div>
        
                                        </form>
        
                                   
                                <!-- end select2 -->

                            </div>


                        </div>
                        <!-- end row -->
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <script>document.write(new Date().getFullYear())</script> © Minible.
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-end d-none d-sm-block">
                                    Crafted with <i class="mdi mdi-heart text-danger"></i> by <a href="https://themesbrand.com/" target="_blank" class="text-reset">Themesbrand</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            <div data-simplebar class="h-100">

                <div class="rightbar-title d-flex align-items-center px-3 py-4">
            
                    <h5 class="m-0 me-2">Settings</h5>

                    <a href="javascript:void(0);" class="right-bar-toggle ms-auto">
                        <i class="mdi mdi-close noti-icon"></i>
                    </a>
                </div>



                <!-- Settings -->
                <hr class="mt-0" />
                <h6 class="text-center mb-0">Choose Layouts</h6>

                <div class="p-4">
                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-1.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="light-mode-switch" checked />
                        <label class="form-check-label" for="light-mode-switch">Light Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-2.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="dark-mode-switch" />
                        <label class="form-check-label" for="dark-mode-switch">Dark Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-3.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="rtl-mode-switch" />
                        <label class="form-check-label" for="rtl-mode-switch">RTL Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-4.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-5">
                        <input class="form-check-input theme-choice" type="checkbox" id="dark-rtl-mode-switch">
                        <label class="form-check-label" for="dark-rtl-mode-switch">Dark RTL Mode</label>
                    </div>

            
                </div>

            </div> <!-- end slimscroll-menu-->
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('dist/js/metisMenu.min.js') }}"></script>
        <script src="{{ asset('dist/js/simplebar.min.js') }}"></script>
        <script src="{{ asset('dist/js/waves.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.counterup.min.js') }}"></script>

        <!-- plugins -->
        <script src="{{ asset('dist/js/select2.min.js') }}"></script>
        <script src="{{ asset('dist/js/spectrum.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.bootstrap-touchspin.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-maxlength.min.js') }}"></script>
        <script src="{{ asset('dist/js/datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/form-advanced.init.js') }}"></script>
        <!-- App js -->
        <script src="{{ asset('dist/js/app.js') }}"></script>

    </body>
</html>
