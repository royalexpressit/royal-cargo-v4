<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Royal Cargo - Outbound</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <link rel="shortcut icon" href="{{ asset('dist/images/favicon.png') }}">

    <link href="{{ asset('dist/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/app.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/custom.css') }}"  rel="stylesheet" type="text/css" />
</head>
<body data-layout="horizontal" data-topbar="colored">
    <div id="layout-wrapper">
        @include('operator.header')
    
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h5 class="mb-0">
                                    <a href="{{ url('') }}">Dashboard</a>
                                    > <a href="{{ url('outbound') }}">Outbound</a>
                                    > <span class="text-capitalize">Cancelled Waybills</span> 
                                </h5>

                                <div class="page-title-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                            Filtered By Status:<i class="mdi mdi-chevron-down"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-primary">
                                            <a class="dropdown-item text-primary" href="{{ url('outbound/status/collected') }}">Collected Waybills</a>
                                            <a class="dropdown-item text-primary" href="{{ url('outbound/status/handover') }}">Handover Waybills</a>
                                            <a class="dropdown-item text-primary" href="{{ url('outbound/status/received') }}">Received Waybills</a>
                                            <a class="dropdown-item text-primary" href="{{ url('outbound/status/branch-out') }}">Branch Out Waybills</a>
                                        </div>
                                    </div>
                                    <div class="btn-group block" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-primary pagination-btn" id="prev-btn"><i class="bx bx-caret-left"></i></button>
                                        <button type="button" class="btn btn-primary pagination-btn" id="next-btn"><i class="bx bx-caret-right"></i></button>
                                        <button type="button" class="btn btn-outline-primary waves-effect waves-light">Records: <span id="to-records">0</span> of <span id="total-records">0</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="success-box hide">
                                <div class="alert bg-success text-white alert-dismissible fade show" role="alert">
                                    <span id="success-num"></span> approved to cancel waybill.
                                    <button type="button" class="btn-close text-white" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            </div>
                            <div class="error-box hide">
                                <div class="alert bg-danger text-white alert-dismissible fade show" role="alert">
                                    <span id="error-num"></span> is already approved.
                                    <button type="button" class="btn-close text-white" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            </div>
                        </div>
                        <div id="fetched-data">
                            
                        </div>
                        <div class="data-loading text-center mt-10">
                                                <h5>Loading ...</h5>
                                                <div class="spinner-grow text-warning m-1" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                                <div class="spinner-grow text-success m-1" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                                <div class="spinner-grow text-danger m-1" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                            <div class="alert alert-warning show-alert font-size-16 m-2" role="alert">
                                                No data available...
                                            </div>
                    </div>
                </div>
                
                @include('footer')
                <input type="hidden" id="json" value="{{ $json }}">
            </div>
        </div>
    </div>

    <!-- Right Sidebar -->
    @include('right-side')  
    <!-- /Right-bar -->
<!-- sample modal content -->
                                                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Approved Waybill</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h5 class="font-size-16">Are you sure approve waybill?</h5>
                                                                <input type="" id="cancelled_id">
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-light waves-effect" data-bs-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary waves-effect waves-light approve-btn" data-bs-dismiss="modal">Approve</button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- JAVASCRIPT -->
    <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
    <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/js/metisMenu.min.js') }}"></script>
    <script src="{{ asset('dist/js/simplebar.min.js') }}"></script>
    <script src="{{ asset('dist/js/waves.min.js') }}"></script>
    <script src="{{ asset('dist/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('dist/js/jquery.counterup.min.js') }}"></script>

    <!-- plugins -->
    <script src="{{ asset('dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('dist/js/spectrum.min.js') }}"></script>
    <script src="{{ asset('dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('dist/js/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('dist/js/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{ asset('dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('dist/js/form-advanced.init.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('dist/js/app.js') }}"></script>
    <script src="{{ asset('dist/js/custom.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".data-loading").show();
            var url         = $("#url").val();
            var json        = $("#json").val();

            //declared first loaded json data
            var load_json   = url+'/'+json;
            var waybills = [];
            var item     = 0;
            var _token   = $("#_token").val();
            

            $.ajax({
                url: load_json,
                type: 'GET',
                data: {},
                success: function(data){
                    console.log(json);
                    if(data.total > 0){
                        $(".show-alert").hide();
                        $.each( data.data, function( key, value ) {
                            ++item;
                            waybills.push(value.waybill_no);
                            $("#fetched-data").append(
                                '<div class="col-lg-12"><div class="card card-body"><div class="row">'
                                    +'<div class="col-lg-4"><h5 class="'+(value.status == 1? 'text-success':'text-warning')+'">'+value.waybill_no+' <i class="mdi '+(value.status == 1? 'mdi-check-circle':'mdi-bell-circle')+'"></i></h5><span>'+value.username+', '+value.branch+'<span><br>'+(value.status == 1? '':'<button type="button" class="btn btn-danger btn-sm waves-effect waves-light cancelled-btn" data-bs-toggle="modal" data-bs-target="#myModal" id="'+value.id+'"><i class="mdi mdi-send"></i> Approved</button>')+'</div>'
                                    +'<div class="col-lg-8"><h6 class="text-right">'+value.cancelled_date+'</h6><span class="text-danger font-size-18">'+value.reason+'</span></div>'
                                +'</div></div></div>'
                            );
                            
                        });
                        $(".data-loading").hide();

                        $("#to-records").text(data.to);
                        $("#total-records").text(data.total);
                                    
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }else{
                        $(".show-alert").show();
                        $(".pagination-btn").attr('disabled',true);
                        $(".data-loading").hide();
                    }
                }
            });

            $('.pagination-btn').click(function(){
                //clicked url json data
                $(".data-loading").show();
                $("#fetched-data").empty();
                var clicked_url = $(this).val();
                        

                $(this).siblings().removeClass('active')
                $(this).addClass('active');
                $.ajax({
                    url: clicked_url,
                    type: 'GET',
                    data: {},
                    success: function(data){

                        $.each( data.data, function( key, value ) {
                            ++item;
                            $("#fetched-data").append(
                                '<div class="col-lg-12"><div class="card card-body"><div class="row">'
                                    +'<div class="col-lg-4"><h5 class="'+(value.status == 1? 'text-success':'text-warning')+'">'+value.waybill_no+' <i class="mdi '+(value.status == 1? 'mdi-check-circle':'mdi-bell-circle')+'"></i></h5><span>'+value.username+', '+value.branch+'<span><br>'+(value.status == 1? '':'<button type="button" class="btn btn-danger btn-sm waves-effect waves-light cancelled-btn" data-bs-toggle="modal" data-bs-target="#myModal" id="'+value.id+'"><i class="mdi mdi-send"></i> Approved</button>')+'</div>'
                                    +'<div class="col-lg-8"><h6 class="text-right">'+value.cancelled_date+'</h6><span class="text-danger font-size-18">'+value.reason+'</span></div>'
                                +'</div></div></div>'
                            );
                        });
                        $(".data-loading").hide();
                                

                        $("#to-records").text(data.to);
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }
                });
            });

            $('body').delegate(".load-modal","click",function () {
                var id = $(this).attr('id');
                        
                $.ajax({
                    url: url+'/inbound/'+id+'/view',
                    type: 'POST',
                    data: {
                        'id':id,
                        '_token': _token
                    },
                    success: function(data){
                        $("#waybill_label").text(data.waybill_no);
                        $("#action_date").text(data.action_date);
                        $("#from_city").text('_ '+data.origin);
                        $("#transit_city").text('_ '+data.transit);
                        $("#to_city").text('_ '+data.destination);
                        $("#branch_in_by").text('_ '+data.branch_in_by);
                        $("#current_status").html(data.current_status);
                        $(".view-logs").attr("href",url+'/inbound/view/'+data.id+'/logs')   
                    }
                });     
            });

            $('body').delegate(".cancelled-btn","click",function () {
                var id = $(this).attr('id');
                $("#cancelled_id").val(id);
            });

            $('body').delegate(".approve-btn","click",function () {
                var cancell_id = $("#cancelled_id").val();

                $.ajax({
                    url: 'api/approved-waybill',
                    type: 'POST',
                    data: {
                        'id':cancell_id
                    },
                    success: function(data){
                        console.log(json);
                        if(data.success == 1){
                            $("#success-num").text(data.waybill_no);
                            $(".success-box").show();
                            $(".error-box").hide();

                            $.ajax({
                                url: load_json,
                                type: 'GET',
                                data: {},
                                success: function(data){
                                    $("#fetched-data").empty();

                                    if(data.total > 0){
                                        $(".show-alert").hide();
                                        $.each( data.data, function( key, value ) {
                                            ++item;
                                            waybills.push(value.waybill_no);
                                            $("#fetched-data").append(
                                                '<div class="col-lg-12"><div class="card card-body"><div class="row">'
                                                    +'<div class="col-lg-4"><h5 class="'+(value.status == 1? 'text-success':'text-warning')+'">'+value.waybill_no+' <i class="mdi '+(value.status == 1? 'mdi-check-circle':'mdi-bell-circle')+'"></i></h5><span>'+value.username+', '+value.branch+'<span><br>'+(value.status == 1? '':'<button type="button" class="btn btn-danger btn-sm waves-effect waves-light cancelled-btn" data-bs-toggle="modal" data-bs-target="#myModal" id="'+value.id+'"><i class="mdi mdi-send"></i> Approved</button>')+'</div>'
                                                    +'<div class="col-lg-8"><h6 class="text-right">'+value.cancelled_date+'</h6><span class="text-danger font-size-18">'+value.reason+'</span></div>'
                                                +'</div></div></div>'
                                            );
                                            
                                        });
                                        $(".data-loading").hide();

                                        $("#to-records").text(data.to);
                                        $("#total-records").text(data.total);
                                                    
                                        if(data.prev_page_url === null){
                                            $("#prev-btn").attr('disabled',true);
                                        }else{
                                            $("#prev-btn").attr('disabled',false);
                                        }
                                        if(data.next_page_url === null){
                                            $("#next-btn").attr('disabled',true);
                                        }else{
                                            $("#next-btn").attr('disabled',false);
                                        }
                                        $("#prev-btn").val(data.prev_page_url);
                                        $("#next-btn").val(data.next_page_url);
                                    }else{
                                        $(".show-alert").show();
                                        $(".pagination-btn").attr('disabled',true);
                                        $(".data-loading").hide();
                                    }
                                }
                            });
                        }else{
                            $("#error-num").text(data.waybill_no);
                            $(".error-box").show(); 
                            $(".success-box").hide();  
                        }
                        
                    }
                });
            });
        });
    </script>
</body>
</html>
