
<!doctype html>
<html lang="en">
    
    <head>
        
        <meta charset="utf-8" />
        <title>Horizontal Layout | Minible - Admin & Dashboard Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesbrand" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- Bootstrap Css -->
        <link href="{{ asset('dist/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{ asset('dist/css/app.min.css') }}"  rel="stylesheet" type="text/css" />
    <style type="text/css">
        .pull-left{
            float: left;
        }
        .pull-right{
            float: right;
        }
    </style>
    </head>

    <body data-layout="horizontal" data-topbar="colored">

        <!-- Begin page -->
        <div id="layout-wrapper">

            @include('admin.header')
    


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">Collected By Branches</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Layout</a></li>
                                            <li class="breadcrumb-item active">Horizontal</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

               

                        <div class="row">
                            <div class="col-xl-8">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="accordion" id="accordionExample">
                                            @foreach(collected_by_branches(102) as $branch)
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingTwo">
                                                    <div class="accordion-button collapsed" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#branch-{{ $branch->name }}"
                                                        aria-expanded="false" aria-controls="collapseTwo">
                                                        <span>{{ $branch->name }}</span>
                                                        <span class="badge rounded-pill bg-primary">{{ outbound_collected_by_branch($branch->id) }}</span>
                                                
                                                    </div>
                                                </h2>
                                                <div id="branch-{{ $branch->name }}" class="accordion-collapse collapse"
                                                    aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                        <div class="row">
                                                            <div class="col-xl-4">
                                                                <div class="card">
                                                                    <div class="card-body d-grid gap-2">
                                                                        <button type="button" class="btn btn-danger btn-md waves-effect waves-light mb-1">
                                                                            <span class="pull-left">Collected</span> 
                                                                            <span class="pull-right">10</span>
                                                                        </button>
                                                                        <button type="button" class="btn btn-warning btn-md waves-effect waves-light mb-1">
                                                                            <span class="pull-left">Handover</span> 
                                                                            <span class="pull-right">10</span>
                                                                        </button>
                                                                        <button type="button" class="btn btn-success btn-md waves-effect waves-light mb-1">
                                                                            <span class="pull-left">Received</span> 
                                                                            <span class="pull-right">10</span>
                                                                        </button>
                                                                        <button type="button" class="btn btn-primary btn-md waves-effect waves-light mb-1">
                                                                            <span class="pull-left">Branch Out</span> 
                                                                            <span class="pull-right">10</span>
                                                                        </button>
                                                                    </div><!-- end card-body-->
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-8">
                                                                <div class="card">
                                                                    <div class="card-body d-grid gap-2"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4 font-size-20 text-primary">Today Outbound Status</h4>
                                        <div data-simplebar >
                                            <div class="table-responsive">
                                                <table class="table table-borderless table-centered table-nowrap">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 20px;"><img src="{{ asset('dist/images/icons/city.png') }}" class="avatar-xs rounded-circle " alt="..."></td>
                                                            <td>
                                                                <h6 class="font-size-18 mb-1 fw-normal text-danger right">Collected Status</h6>
                                                                <p class="text-muted font-size-20 mb-0">{{ fetched_outbound_status(102)['collected'] }}</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><img src="{{ asset('dist/images/icons/house.png') }}" class="avatar-xs rounded-circle " alt="..."></td>
                                                            <td>
                                                                <h6 class="font-size-18 mb-1 fw-normal text-warning">Handover Status</h6>
                                                                <p class="text-muted font-size-20 mb-0">{{ fetched_outbound_status(102)['handover'] }}</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><img src="{{ asset('dist/images/icons/report.png') }}" class="avatar-xs rounded-circle " alt="..."></td>
                                                            <td>
                                                                <h6 class="font-size-18 mb-1 fw-normal text-success">Received Status</h6>
                                                                <p class="text-muted font-size-20 mb-0">{{ fetched_outbound_status(102)['received'] }}</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><img src="{{ asset('dist/images/icons/marketing.png') }}" class="avatar-xs rounded-circle " alt="..."></td>
                                                            <td>
                                                                <h6 class="font-size-18 mb-1 fw-normal text-primary">Branch Out Status</h6>
                                                                <p class="text-muted font-size-20 mb-0">{{ fetched_outbound_status(102)['branch_out'] }}</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> 
                                            <!-- enbd table-responsive-->
                                        </div> <!-- data-sidebar-->
                                    </div><!-- end card-body-->
                                </div>
                            </div>
                        </div>
            
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <script>document.write(new Date().getFullYear())</script> © Minible.
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-end d-none d-sm-block">
                                    Crafted with <i class="mdi mdi-heart text-danger"></i> by <a href="https://themesbrand.com/" target="_blank" class="text-reset">Themesbrand</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            <div data-simplebar class="h-100">

                <div class="rightbar-title d-flex align-items-center px-3 py-4">
            
                    <h5 class="m-0 me-2">Settings</h5>

                    <a href="javascript:void(0);" class="right-bar-toggle ms-auto">
                        <i class="mdi mdi-close noti-icon"></i>
                    </a>
                </div>



                <!-- Settings -->
                <hr class="mt-0" />
                <h6 class="text-center mb-0">Choose Layouts</h6>

                <div class="p-4">
                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-1.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="light-mode-switch" checked />
                        <label class="form-check-label" for="light-mode-switch">Light Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-2.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="dark-mode-switch" />
                        <label class="form-check-label" for="dark-mode-switch">Dark Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-3.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="rtl-mode-switch" />
                        <label class="form-check-label" for="rtl-mode-switch">RTL Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-4.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-5">
                        <input class="form-check-input theme-choice" type="checkbox" id="dark-rtl-mode-switch">
                        <label class="form-check-label" for="dark-rtl-mode-switch">Dark RTL Mode</label>
                    </div>

            
                </div>

            </div> <!-- end slimscroll-menu-->
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('dist/js/metisMenu.min.js') }}"></script>
        <script src="{{ asset('dist/js/simplebar.min.js') }}"></script>
        <script src="{{ asset('dist/js/waves.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.counterup.min.js') }}"></script>

        <!-- apexcharts -->
        <script src="{{ asset('dist/js/apexcharts.min.js') }}"></script>

        <script src="{{ asset('dist/js/dashboard.init.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('dist/js/app.js') }}"></script>

    </body>
</html>
