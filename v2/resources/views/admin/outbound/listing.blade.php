<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Royal Cargo - Outbound</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <link rel="shortcut icon" href="{{ asset('dist/images/favicon.png') }}">

    <link href="{{ asset('dist/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/app.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/custom.css') }}"  rel="stylesheet" type="text/css" />
</head>
<body data-layout="horizontal" data-topbar="colored">
    <div id="layout-wrapper">
        @include('admin.header')
    
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h5 class="mb-0">
                                    <a href="{{ url('') }}">Dashboard</a>
                                    > <a href="{{ url('outbound') }}">Outbound</a>
                                    > <span class="text-capitalize">{{ $status }}</span> Waybills
                                </h5>

                                <div class="page-title-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                            Filtered By Status:<i class="mdi mdi-chevron-down"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-primary">
                                            <a class="dropdown-item text-primary" href="{{ url('outbound/status/collected') }}">Collected Waybills</a>
                                            <a class="dropdown-item text-primary" href="{{ url('outbound/status/handover') }}">Handover Waybills</a>
                                            <a class="dropdown-item text-primary" href="{{ url('outbound/status/received') }}">Received Waybills</a>
                                            <a class="dropdown-item text-primary" href="{{ url('outbound/status/branch-out') }}">Branch Out Waybills</a>
                                        </div>
                                    </div>
                                    <div class="btn-group block" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-primary pagination-btn" id="prev-btn"><i class="bx bx-caret-left"></i></button>
                                        <button type="button" class="btn btn-primary pagination-btn" id="next-btn"><i class="bx bx-caret-right"></i></button>
                                        <button type="button" class="btn btn-outline-primary waves-effect waves-light">Records: <span id="to-records">0</span> of <span id="total-records">0</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-centered table-nowrap mb-0">
                                                <thead class="table bg-primary text-white">
                                                    <tr>
                                                        <th>Collected Date</th>
                                                        <th>Waybill No</th>
                                                        <th>Origin</th>
                                                        <th>Destination</th>
                                                        <th>Delivery/Counter</th>
                                                        <th>Status</th>
                                                        <th>View Details</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="fetched-data">
                                                </tbody>
                                            </table>
                                            <div class="data-loading text-center mt-10">
                                                <h5>Loading ...</h5>
                                                <div class="spinner-grow text-warning m-1" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                                <div class="spinner-grow text-success m-1" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                                <div class="spinner-grow text-danger m-1" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                            <div class="alert alert-warning show-alert font-size-16 m-2" role="alert">
                                                No data available...
                                            </div>
                                        </div>
                                        <!-- end table-responsive -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                @include('footer')
                <input type="hidden" id="json" value="{{ $json }}">
            </div>
        </div>
    </div>

    <!-- Right Sidebar -->
    @include('right-side')  
    <!-- /Right-bar -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- JAVASCRIPT -->
    <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
    <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/js/metisMenu.min.js') }}"></script>
    <script src="{{ asset('dist/js/simplebar.min.js') }}"></script>
    <script src="{{ asset('dist/js/waves.min.js') }}"></script>
    <script src="{{ asset('dist/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('dist/js/jquery.counterup.min.js') }}"></script>

    <!-- plugins -->
    <script src="{{ asset('dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('dist/js/spectrum.min.js') }}"></script>
    <script src="{{ asset('dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('dist/js/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('dist/js/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{ asset('dist/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('dist/js/form-advanced.init.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('dist/js/app.js') }}"></script>
    <script src="{{ asset('dist/js/custom.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".data-loading").show();
            var url         = $("#url").val();
            var json        = $("#json").val();

            //declared first loaded json data
            var load_json   = url+'/'+json;
            var waybills = [];
            var item     = 0;
            var _token   = $("#_token").val();
            

            $.ajax({
                url: load_json,
                type: 'GET',
                data: {},
                success: function(data){
                    console.log(json);
                    if(data.total > 0){
                        $(".show-alert").hide();
                        $.each( data.data, function( key, value ) {
                            ++item;
                            waybills.push(value.waybill_no);
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td>'+value.outbound_date+'</td>'
                                    +'<td class="font-size-16 color-'+value.current_status+' ">'+value.waybill_no+(value.same_day==1? '<i class="uil-clock-three text-danger"></i>':'')+'</td>'
                                    +'<td><span class="btn-sm border-primary btn-rounded h6 text-primary">'+value.origin+'</span></td>'
                                    +'<td>'+(value.destination == null? '-':'<span class="btn-sm border-success btn-rounded h6 text-success">'+value.destination+'</span>')+'</td>'
                                    +'<td>'+value.delivery+'</td>'
                                    +'<td>'+status(value.current_status)+'</td>'
                                    +'<td><a href="'+url+'/view-waybill/'+value.waybill_no+'" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">View Details</a></td>'
                                +'</tr>'
                            );
                            
                        });
                        $(".data-loading").hide();

                        $("#to-records").text(data.to);
                        $("#total-records").text(data.total);
                                    
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }else{
                        $(".show-alert").show();
                        $(".pagination-btn").attr('disabled',true);
                        $(".data-loading").hide();
                    }
                }
            });

            $('.pagination-btn').click(function(){
                //clicked url json data
                $(".data-loading").show();
                $("#fetched-data").empty();
                var clicked_url = $(this).val();
                        

                $(this).siblings().removeClass('active')
                $(this).addClass('active');
                $.ajax({
                    url: clicked_url,
                    type: 'GET',
                    data: {},
                    success: function(data){

                        $.each( data.data, function( key, value ) {
                            ++item;
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td>'+value.outbound_date+'</td>'
                                    +'<td class="font-size-16 color-'+value.current_status+' ">'+value.waybill_no+(value.same_day==1? '<i class="uil-clock-three text-danger"></i>':'')+'</td>'
                                    +'<td><span class="btn-sm border-primary btn-rounded h6 text-primary">'+value.origin+'</span></td>'
                                    +'<td>'+(value.destination == null? '-':'<span class="btn-sm border-success btn-rounded h6 text-success">'+value.destination+'</span>')+'</td>'
                                    +'<td>'+value.delivery+'</td>'
                                    +'<td>'+status(value.current_status)+'</td>'
                                    +'<td><a href="'+url+'/view-waybill/'+value.waybill_no+'" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">View Details</a></td>'
                                +'</tr>'
                            );
                        });
                        $(".data-loading").hide();
                                

                        $("#to-records").text(data.to);
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }
                });
            });

            $('body').delegate(".load-modal","click",function () {
                var id = $(this).attr('id');
                        
                $.ajax({
                    url: url+'/inbound/'+id+'/view',
                    type: 'POST',
                    data: {
                        'id':id,
                        '_token': _token
                    },
                    success: function(data){
                        $("#waybill_label").text(data.waybill_no);
                        $("#action_date").text(data.action_date);
                        $("#from_city").text('_ '+data.origin);
                        $("#transit_city").text('_ '+data.transit);
                        $("#to_city").text('_ '+data.destination);
                        $("#branch_in_by").text('_ '+data.branch_in_by);
                        $("#current_status").html(data.current_status);
                        $(".view-logs").attr("href",url+'/inbound/view/'+data.id+'/logs')   
                    }
                });     
            });
        });
    </script>
</body>
</html>
