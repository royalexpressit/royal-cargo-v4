<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Royal Cargo - Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('dist/images/favicon.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <link href="{{ asset('dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('dist/css/datepicker.min.css') }}">

    <!-- plugin css -->
    <link href="{{ asset('dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/spectrum.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('dist/css/datepicker.min.css') }}">
        
    <!-- Bootstrap Css -->
    <link href="{{ asset('dist/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('dist/css/app.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/custom.css') }}"  rel="stylesheet" type="text/css" />
</head>
<body data-layout="horizontal" data-topbar="colored">
    <!-- Begin page -->
    <div id="layout-wrapper">
        @include('operator.header')
    
       <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h5 class="mb-0">
                                    <a href="{{ url('') }}">Dashboard</a>
                                    > <a href="{{ url('users') }}">Users</a>
                                    > Profile
                                </h5>
                                <div class="page-title-right">
                                    
                                </div>
                            </div>
                        </div>
                    </div>


                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4 font-size-20 text-primary">View Profile</h4>
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <img class="rounded-circle avatar-xl" alt="200x200" src="{{ asset('dist/images/'.user()->image) }}" data-holder-rendered="true">
                                                <p>
                                                    <h6>User Role</h6>
                                                    <p class="text-primary">{{ $user->role }}</p>

                                                    <h6>Collected Waybills</h6>
                                                    <p class="text-primary">0</p> 
                                                </p>
                                            </div>
                                            <div class="col-xl-8">
                                                <div>
                                                    <h5>Username</h5>
                                                    <p class="text-primary">- {{ $user->name }}</p>
                                                </div>
                                                <div>
                                                    <h5>Email</h5>
                                                    <p class="text-primary">- {{ $user->email }}</p>
                                                </div>
                                                <div>
                                                    <h5>Current City</h5>
                                                    <p class="text-primary">- {{ city($user->city_id)['name'] }}</p>
                                                </div>
                                                <div>
                                                    <h5>Current Branch</h5>
                                                    <p class="text-primary">- {{ branch($user->branch_id)['name'] }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4 font-size-20 text-primary">Change Password</h4>
                                        <div class="alert bg-warning alert-dismissible fade show mb-2 text-white warning-alert hide msg-box" role="alert">
                                            Please fill input field(s).
                                            <button type="button" class="btn-close"  aria-label="Close"></button>
                                        </div>
                                        <div class="alert bg-success alert-dismissible fade show mb-2 text-white success-alert hide msg-box" role="alert">
                                            New password has been changed.
                                            <button type="button" class="btn-close"  aria-label="Close"></button>
                                        </div>
                                        <div class="alert bg-danger alert-dismissible fade show mb-2 text-white failed-alert hide msg-box" role="alert">
                                            Old password is invalid.
                                            <button type="button" class="btn-close"  aria-label="Close"></button>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Old Password</label>
                                            <div class="input-group">
                                                <input class="form-control" type="password" value="" id="old">
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">New Password</label>
                                            <div class="input-group">
                                                <input class="form-control" type="password" value="" id="new">
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <button type="button" class="btn btn-primary w-lg waves-effect waves-light change-password">Change</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                            
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4 font-size-20 text-primary">Change City & Branch</h4>
                                        <div class="user-form">
                                            <div class="alert bg-success alert-dismissible fade show mb-2 text-white change-alert hide msg-box" role="alert">
                                                You have been changed city and branch.
                                                <button type="button" class="btn-close"  aria-label="Close"></button>
                                            </div>
                                            <div class="alert bg-danger alert-dismissible fade show mb-2 text-white error-alert hide msg-box" role="alert">
                                                Something wrong.
                                                <button type="button" class="btn-close"  aria-label="Close"></button>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-xl-6">
                                                    <label class="form-label">City</label>
                                                    <select class="form-control select2 form-data" disabled id="city">
                                                        @foreach($cities as $city)
                                                        <option value="{{ $city->id }}" {{ ($city->id == $user->city_id? 'selected':'') }}>{{ $city->shortcode }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-xl-6">
                                                    <label class="form-label">Branch</label>
                                                    <select class="form-control select2 form-data" disabled id="branches">
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <button type="button" class="btn btn-primary w-lg waves-effect waves-light btn-edit">Change</button>
                                                <button type="button" class="btn btn-success w-lg waves-effect waves-light hide btn-change">Save</button>
                                                <button type="button" class="btn btn-danger w-lg waves-effect waves-light hide btn-cancel">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>

                        
                    </div>
                </div>

                <input type="" id="user_id" value="{{ Auth::user()->id }}">
                <input type="" id="current_city" value="{{ Auth::user()->city_id }}">
                <input type="" id="current_branch" value="{{ Auth::user()->branch_id }}">
                @include('footer')
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right Sidebar -->
       
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('dist/js/metisMenu.min.js') }}"></script>
        <script src="{{ asset('dist/js/simplebar.min.js') }}"></script>
        <script src="{{ asset('dist/js/waves.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.counterup.min.js') }}"></script>

        <!-- plugins -->
        <script src="{{ asset('dist/js/select2.min.js') }}"></script>
        <script src="{{ asset('dist/js/spectrum.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.bootstrap-touchspin.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-maxlength.min.js') }}"></script>
        <script src="{{ asset('dist/js/datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/form-advanced.init.js') }}"></script>

        <!-- apexcharts -->
        <script src="{{ asset('dist/js/apexcharts.min.js') }}"></script>

        <script src="{{ asset('dist/js/dashboard.init.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('dist/js/app.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var url  = $("#url").val();
                var _token  = $("#_token").val();

                var current_city = $("#current_city").val();
                var current_branch = $("#current_branch").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
               
                $.ajax({
                    type: 'post',
                    url: url+'/api/fetched-branches',
                    dataType:'json',
                    data: {
                        'current_city' :current_city,
                    },
                    success: function(data) { 
                        $.each(data, function (i, item) {
                            $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                        });
                        $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                    },
                });

                $(".btn-edit").on("click",function search(e) {
                    $('.btn-edit').hide();
                    $('.form-data').prop("disabled", false);
                    $('.btn-change').show();
                    $('.btn-cancel').show();
                });

                $(".btn-close").on("click",function search(e) {
                    $('.msg-box').hide();
                });
                

                $(".btn-cancel").on("click",function search(e) {
                    $('.btn-edit').show();
                    $('.form-data').prop("disabled", true);
                    $('.btn-change').hide();
                    $('.btn-cancel').hide();
                });
                
                $('#city').on("change",function search(e) {
                    var id = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                   
                    $.ajax({
                        type: 'post',
                        url: url+'/api/fetched-branches',
                        dataType:'json',
                        data: {
                            'current_city' :id,
                        },
                        success: function(data) {
                            $('#branches').empty(); 
                            $.each(data, function (i, item) {
                                $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                            })

                            $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                        },
                    });
                    
                });

                $(".change-password").on("click",function search(e) {
                    var old_pass = $("#old").val();
                    var new_pass = $("#new").val();
                    

                    if(old_pass == '' && new_pass == ''){
                        $('.msg-box').hide();
                        $('.warning-alert').show();
                    }else{
                        $('.msg-box').hide();
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type: 'post',
                            url: url+'/changed-password',
                            dataType:'json',
                            data: {
                                'old_pass' :old_pass,
                                'new_pass' :new_pass
                            },
                            success: function(data) {
                                if(data.success == 1){
                                    $('.success-alert').show();
                                }else{
                                    $('.failed-alert').show();
                                }
                                $("#old").val('');
                                $("#new").val('')
                            },
                        }); 
                    }
                });

                $(".btn-change").on("click",function search(e) {
                    var user_id     = $("#user_id").val();
                    var city_id     = $("#city").val();
                    var branch_id   = $("#branches").val();
                
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                   
                    $.ajax({
                        type: 'post',
                        url: url+'/api/change-city-and-branch',
                        dataType:'json',
                        data: {
                            'user_id'   :user_id,
                            'city_id'   :city_id,
                            'branch_id' :branch_id,
                        },
                        success: function(data) { 
                            if(data.success == 1){
                                $('.change-alert').show();
                            }else{
                                $('.error-alert').show();
                            }
                        },
                    });
                });
            });
        </script>

    </body>
</html>
