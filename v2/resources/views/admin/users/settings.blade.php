<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Settings</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
        
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row hide">
                        <div class="col-md-6">
                            @if(change_cities(Auth::id()))
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">မြို့ နှင့် ရုံးခွဲပြောင်းရန်</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">မြို့</label>
                                                <select data-placeholder="Select a state..." class="select2-icons form-select form-data" id="city" disabled >
                                                    @foreach(change_cities(Auth::id()) as $city)
                                                    <option value="{{ $city }}" {{ ($city == $user->city_id? 'selected':'') }} data-icon="map-pin">{{ city($city)['shortcode'] }} ({{ city($city)['name'].'-'.city($city)['mm_name'] }})</option>
                                                    @endforeach
                                                 </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">ရုံး</label>
                                                <select data-placeholder="Select a city..." class="form-control select2 form-data" id="branches" disabled>
                                                   
                                                 </select>
                                                 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        <button type="button" class="btn btn-relief-primary waves-effect btn-edit">ပြင်မည်</button>
                                        <button type="button" class="btn btn-relief-success waves-effect hide btn-change">သိမ်းမည်</button>
                                        <button type="button" class="btn btn-relief-danger waves-effect hide btn-cancel">ပိတ်မည်</button>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Reset Password</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="alert alert-danger alert-validation-msg msg-box" role="alert">
                                        <div class="alert-body d-flex align-items-center">
                                            <ul class="unstyled-list">
                                                <li class="character1">✶ အနည်းဆုံး စာလုံးအရေအတွက် ၆ လုံးဖြစ်ရပါမည်။</li>
                                                <li class="character2">✶ စကားဝှက်ရိုက်ထည့်သည့် နေရာ၂ခုလုံး တူရပါမည်။</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">New Password <span class="text-danger">✶</span></label>
                                        <input type="password" id="new_pass" class="form-control" placeholder="New password" />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">Confirmed Password <span class="text-danger">✶</span></label>
                                        <input type="password" id="confirmed_pass" class="form-control" placeholder="Confirmed password" />
                                    </div>
                                    <div class="mb-0">
                                        <input type="hidden" id="user_id" value="{{ user()->id }}">
                                        <button type="button" class="btn btn-relief-primary waves-effect scan-btn btn-reset disabled">ပြောင်းမည်</button>
                                    </div>
                                    <div class="mb-0">
                                        <small><span class="text-danger">✶</span> is required fields.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-transaction">
                                <div class="card-header">
                                    <h4 class="card-title w-100">CTFs <small>(Primary Layer)</small>
                                        <small class="pull-right">
                                            <span class="badge badge-light-success">{{ number_format($data['waybills']) }}</span> WB ,
                                            <span class="badge badge-light-primary">{{ number_format($data['packages']) }}</span> PG ,
                                            <span class="badge badge-light-info">{{ number_format($data['ctfs']) }}</span> CTF
                                        </small>
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-title w-100 mb-1">
                                        <small class="">
                                            <span class="badge badge-light-success">{{ number_format($data['logs']) }}</span> Action Logs 
                                        </small>
                                    </p>
                                    @foreach(to_sycn_ctfs() as $ctf)
                                  <div class="transaction-item ctf-item-{{ $ctf->id }}">
                                    <div class="d-flex">
                                      <div class="transaction-percentage">
                                        <h6 class="transaction-title">{{ $ctf->ctf_no }}</h6>
                                        <small>{{ $ctf->created_at }}</small>
                                      </div>
                                    </div>
                                    <div class="fw-bolder text-danger">
                                        <button type="button" class="btn btn-primary btn-sm waves-effect waves-float waves-light sync-ctf" id="{{ $ctf->id }}"><i data-feather='upload-cloud'></i></button>
                                    </div>
                                  </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-transaction">
                                <div class="card-header">
                                <h4 class="card-title w-100">CTFs <small>(Secondary Layer)</small>
                                    <small class="pull-right">
                                        <span class="badge badge-light-success">{{ number_format($data['bk_waybills']) }}</span> WB ,
                                        <span class="badge badge-light-primary">{{ number_format($data['bk_packages']) }}</span> PG ,
                                        <span class="badge badge-light-info">{{ number_format($data['bk_ctfs']) }}</span> CTF
                                    </small>
                                </h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-title w-100 mb-1">
                                        <small class="">
                                            <span class="badge badge-light-danger">{{ number_format($data['bk_pending_waybills']) }}</span> Pending ,
                                            <span class="badge badge-light-success">{{ number_format($data['bk_completed_waybills']) }}</span> Completed ,
                                            <span class="badge badge-light-info">{{ number_format($data['bk_logs']) }}</span> Action Logs
                                        </small>
                                    </p>
                                    @foreach(synced_ctfs() as $ctf)
                                  <div class="transaction-item">
                                    <div class="d-flex">
                                      <div class="transaction-percentage">
                                        <h6 class="transaction-title">{{ $ctf->ctf_no }}</h6>
                                        <small>{{ $ctf->created_at }}</small>
                                      </div>
                                    </div>
                                    <div class="fw-bolder text-danger">
                                        @if($ctf->marked == 0)
                                        <button type="button" class="btn btn-success btn-sm waves-effect waves-float waves-light btn-check-ctf marked-item-{{ $ctf->ctf_id }}" id="{{ $ctf->ctf_id }}" value="{{ $ctf->ctf_no }}" data-bs-toggle="modal" data-bs-target="#success"><i data-feather='check'></i></button>
                                        @else
                                        <button type="button" class="btn btn-danger btn-sm waves-effect waves-float waves-light disabled"><i data-feather='check'></i></button>
                                        @endif
                                        <button type="button" class="btn btn-primary btn-sm waves-effect waves-float waves-light sync-pkg" id="{{ $ctf->ctf_id }}"><i data-feather='eye'></i></button>
                                    </div>
                                  </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-transaction">
                                <div class="card-header">
                                  <h4 class="card-title">Package Lists</h4>
                                </div>
                                <div class="card-body">
                                  <div id="fetched-packages">
                                        
                                    </div>
                                  
                                </div>
                            </div>

                            <div class="card card-transaction">
                                <div class="card-header">
                                  <h4 class="card-title w-100">Waybill Lists
                                    <span class="pull-right">
                                        <span class="text-primary">Total</span> 
                                        <span id="qty" class="badge badge-light-primary me-1 count-badge">0</span> 
                                    </span>
                                  </h4>
                                </div>
                                <div class="card-body">
                                  <div id="fetched-waybills">
                                        
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </section>
            </div>
        </div>
    </div>

     <div
                class="modal fade text-start modal-success"
                id="success"
                tabindex="-1"
                aria-labelledby="myModalLabel110"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="myModalLabel110">Completed Backup</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      ၎င်း <strong id="item-title">---</strong> နှင့်ဆိုင်သော အချက်အလက်များအား
                      သိမ်းဆည်းထားပြီးကြောင်း အသိအမှတ်ပြုပါသည်။
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="marked-item" value="0">
                      <button type="button" class="btn btn-success" data-bs-dismiss="modal">အတည်ပြုသည်</button>
                    </div>
                  </div>
                </div>
              </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="service_point" value="{{ city(user()->city_id)['is_service_point'] }}">
    <input type="hidden" id="data-limit" value="25">
    <input type="hidden" id="selected-pkg" value="0">

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });

        $(document).ready(function(){
            var temp = [];
            var wayill_qty = 0;

                $('.loading-waybill').hide();

                var _token  = $("#_token").val();

                var current_city = $("#user_city_id").val();
                var current_branch = $("#user_branch_id").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
               
                $.ajax({
                    type: 'post',
                    url: url+'/api/fetched-branches',
                    dataType:'json',
                    data: {
                        'city_id' :current_city,
                    },
                    success: function(data) { 
                        $.each(data, function (i, item) {
                            $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                        });
                        $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                    },
                });

                $(".btn-edit").on("click",function search(e) {
                    $('.btn-edit').hide();
                    $('.form-data').prop("disabled", false);
                    $('.btn-change').show();
                    $('.btn-cancel').show();
                });

                $(".btn-close").on("click",function search(e) {
                    $('.msg-box').hide();
                });
                

                $(".btn-cancel").on("click",function search(e) {
                    $('.btn-edit').show();
                    $('.form-data').prop("disabled", true);
                    $('.btn-change').hide();
                    $('.btn-cancel').hide();
                });
                
                $('#city').on("change",function search(e) {
                    var id = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                   
                    $.ajax({
                        type: 'post',
                        url: url+'/api/fetched-branches',
                        dataType:'json',
                        data: {
                            'city_id' :id,
                        },
                        success: function(data) {
                            $('#branches').empty(); 
                            $.each(data, function (i, item) {
                                $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                            })

                            $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                        },
                    });
                    
                });

                $(".change-password").on("click",function search(e) {
                    var old_pass = $("#old").val();
                    var new_pass = $("#new").val();
                    

                    if(old_pass == '' && new_pass == ''){
                        $('.msg-box').hide();
                        $('.warning-alert').show();
                    }else{
                        $('.msg-box').hide();
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type: 'post',
                            url: url+'/changed-password',
                            dataType:'json',
                            data: {
                                'old_pass' :old_pass,
                                'new_pass' :new_pass
                            },
                            success: function(data) {
                                if(data.success == 1){
                                    $('.success-alert').show();
                                }else{
                                    $('.failed-alert').show();
                                }
                                $("#old").val('');
                                $("#new").val('')
                            },
                        }); 
                    }
                });

                $(".btn-change").on("click",function search(e) {
                    var user_id     = $("#user_id").val();
                    var city_id     = $("#city").val();
                    var branch_id   = $("#branches").val();
                
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                   
                    $.ajax({
                        type: 'post',
                        url: url+'/api/change-city-and-branch',
                        dataType:'json',
                        data: {
                            'user_id'   :user_id,
                            'city_id'   :city_id,
                            'branch_id' :branch_id,
                        },
                        success: function(data) { 
                            if(data.success == 1){
                                var branch  = $("#branches option:selected").text();
                                var city    = $("#city option:selected").text();

                                $('.change-alert').show();
                                $('#current-branch').text(branch);
                                $('.btn-edit').show();
                                $('.form-data').prop("disabled", true);
                                $('.btn-change').hide();
                                $('.btn-cancel').hide();

                                Swal.fire({
                                    position:"top-end",
                                    title:"အသိပေးခြင်း",
                                    icon:"success",
                                    text:"သင့် လုပ်ဆောင်ရမည့် ရုံးအား "+branch+' သို့ပြောင်းလိုက်ပါပြီ',
                                    showConfirmButton:!1,
                                    timer:3000,
                                    customClass:{confirmButton:"btn btn-primary"},
                                    buttonsStyling:!1
                                });
                            }else{
                                $('.error-alert').show();
                            }
                        },
                    });
                });

        $("#new_pass,#confirmed_pass").on("keyup",function search(e){
            $new       = $("#new_pass").val().length;
            $confirmed = $("#confirmed_pass").val().length;
            //.msg-box,

            if($new >= 6 && $confirmed >= 6){
                if($("#new_pass").val() == $("#confirmed_pass").val()){
                    $(".btn-reset").removeClass('disabled'); 
                    $('.confirmed').hide();
                    $('.msg-box').hide();
                }else{
                    $('.msg-box').show();
                    $(".btn-reset").addClass('disabled');
                    $('.confirmed').show();
                    $('.character1,.character2').show();
                }
            }else if($new >= 6 && $confirmed < 6){
                $('.msg-box').show();
                $('.character1').hide();
                $('.character2').show();
            }else if($new < 6 && $confirmed >= 6){
                $('.msg-box').show();
                $('.character1').hide();
                $('.character2').show();
            }else{
                $('.msg-box').show();
                $('.character1,.character2').show();
                $(".btn-reset").addClass('disabled');
            }
        });

        $(".btn-reset").on("click",function search(e) {
            $(".data-loading").show();
            $(".user-form").hide();

            new_pass        = $('#new_pass').val();
            confirmed_pass  = $("#confirmed_pass").val();
            user_id         = $('#user_id').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                   
            $.ajax({
                type: 'post',
                url: url+'/api/users/reset-password',
                dataType:'json',
                data: {
                    'new_pass'      :new_pass,
                    'confirmed_pass':confirmed_pass,
                    'user_id'       :user_id
                },
                success: function(data) { 
                    if(data.success == 1){
                        Swal.fire({
                            position:"top-end",
                            title:"အသိပေးခြင်း",
                            icon:"success",
                            html:"သင့်စကားဝှက်အသစ် ပြောင်းလည်းလိုက်ပါပြီ",
                            showConfirmButton:!1,
                            timer:3000,
                            customClass:{confirmButton:"btn btn-primary"},
                            buttonsStyling:!1
                        });

                        $('#new_pass').val('');
                        $("#confirmed_pass").val('');
                        $(".btn-reset").addClass('disabled');
                    }
                },
            });
        });

        $(".btn-limit").on("click",function search(e) {
            var count = $(this).val();
            var limit = $('#data-limit').val(count);

            $('.btn-limit').removeClass('bg-primary text-white');
            $(this).addClass('bg-primary text-white');

        });

        $(".btn-synced-one").on("click",function search(e) {
            $('.loading-waybill').show();

            var limit = $('#data-limit').val();

            $.ajax({
                type: 'post',
                url: url+'/api/settings/synced-backup-waybills',
                dataType:'json',
                data: {
                    'limit'      :limit,
                },
                success: function(data) { 
                    $('#w_primary').text(data.w_primary);
                    $('#w_secondary').text(data.w_secondary);
                    $('#w_date').text(data.w_date);

                    $('#a_primary').text(data.a_primary);
                    $('#a_secondary').text(data.a_secondary);
                    $('#a_date').text(data.a_date);

                    $('#pw_primary').text(data.pw_primary);
                    $('#pw_secondary').text(data.pw_secondary);
                    $('#pw_date').text(data.pw_date);

                    $(".loading-waybill").hide();
                }
            });
        });

        $(".btn-synced-two").on("click",function search(e) {
            $('.loading-waybill').show();

            var limit = $('#data-limit').val();

            $.ajax({
                type: 'post',
                url: url+'/api/settings/synced-backup-packages',
                dataType:'json',
                data: {
                    'limit'      :limit,
                },
                success: function(data) { 
                    $('#p_primary').text(data.p_primary);
                    $('#p_secondary').text(data.p_secondary);
                    $('#p_date').text(data.p_date);

                    $('#cp_primary').text(data.cp_primary);
                    $('#cp_secondary').text(data.cp_secondary);
                    $('#cp_date').text(data.cp_date);

                    $('#c_primary').text(data.c_primary);
                    $('#c_secondary').text(data.c_secondary);
                    $('#c_date').text(data.c_date);

                    $(".loading-waybill").hide();
                }
            });
        });

        $(".sync-ctf").on("click",function search(e) {
            id = this.id;
            $("#fetched-packages").empty();
            $(".show-pkg").removeClass('btn-relief-danger');
            $(".package-block").removeClass('hide');
            $(".ctf-items").addClass('hide');
            $(".ctf-"+id).addClass('btn-relief-danger');

            ctf_id = id;


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                               
            $.ajax({
                type: 'post',
                url: url+'/api/fetched-completed-ctf-packages',
                dataType:'json',
                data: {
                    'ctf_id'      :id,
                },
                success: function(data) { 
                    console.log(data);

                    if(data.success == 1){
                        $('.ctf-item-'+id).remove();
                        location.reload();
                    }
                    
                },
            });
            $(this).val(''); 
                        
            added = 0;
        });

        $('body').delegate(".view-btn","click",function () {
            
            var id = this.id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                               
            $.ajax({
                type: 'post',
                url: url+'/api/fetched-completed-package-waybills',
                dataType:'json',
                data: {
                    'package_id'      :id,
                },
                success: function(data) { 
                    
                    $.each( data, function( key, value ) {
                        $("#fetched-waybills").append(
                        '<div class="transaction-item list-item item-'+value.id+' border-warning">'
                                    +'<div class="drag-item-'+value.waybill_no+' w-100">'
                                        +'<div class="">'
                                            +'<div class="transaction-percentage ">'
                                                +'<h6 class="transaction-title font-medium-5 m-0 text-warning">'+value.waybill_no+'</h6>'
                                                +'<div class="text-primary">'
                                                    +'<div class="font-small-4 m--t-4 w-100">'
                                                        +'<span class="badge badge-light-primary">Waybills</span>, <span class="badge badge-light-primary">Action Logs</span>, <span class="badge badge-light-primary">Package Waybills</span> <button type="button" class="btn btn-sm btn-success sync-btn pull-right" id="'+value.id+'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-upload-cloud"><polyline points="16 16 12 12 8 16"></polyline><line x1="12" y1="12" x2="12" y2="21"></line><path d="M20.39 18.39A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.3"></path><polyline points="16 16 12 12 8 16"></polyline></svg></button>'
                                                    +'</div>'
                                                +'</div>'
                                            +'</div>'
                                        +'</div>'
                                    +'</div>'
                        +'</div>'
                        );
                    });
                    
                }
            });
        });

        
        $('body').delegate(".sync-pkg","click",function () {
            var ctf_id = this.id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                           
            $.ajax({
                type: 'post',
                url: url+'/api/synced-packages',
                dataType:'json',
                data: {
                    'ctf_id'      :ctf_id
                },
                success: function(data) {
                    $("#fetched-packages").empty();
                    console.log(data);
                    $.each( data, function( key, value ) {
                        $("#fetched-packages").append(
                        '<div class="transaction-item list-item item-'+value.package_id+' border-warning">'
                                    +'<div class="drag-item-'+value.package_no+' w-100">'
                                        +'<div class="">'
                                            +'<div class="transaction-percentage ">'
                                                +'<h6 class="transaction-title font-medium-5 m-0 text-warning">'+value.package_no+'</h6>'
                                                +'<div class="text-primary">'
                                                    +'<div class="font-small-4 m--t-4 w-100">'
                                                        +'<span class="badge badge-light-success">CTFs</span>, <span class="badge badge-light-success">CTF Packages</span>, <span class="badge badge-light-success">Packages</span> <button type="button" class="btn btn-sm btn-success view-waybills-btn pull-right" id="'+value.package_id+'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></button>'
                                                    +'</div>'
                                                +'</div>'
                                            +'</div>'
                                        +'</div>'
                                    +'</div>'
                        +'</div>'
                        );
                    });

                },
            });

        });

        
        $('body').delegate(".btn-check-ctf","click",function () {
            id = this.id;
            title = $(this).val();
            console.log(id);
            $("#item-title").text(title);
            $("#marked-item").val(id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                           
            $.ajax({
                type: 'post',
                url: url+'/api/marked-ctf',
                dataType:'json',
                data: {
                    'ctf_id'      :id
                },
                success: function(data) {
                    console.log(data);
                    $(".marked-item-"+id).addClass('disabled btn-danger');
                },
            });
        });


        $('body').delegate(".view-waybills-btn","click",function () {
            //call api sent to server function
            id = this.id;
            $('#selected-pkg').val(id);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                           
            $.ajax({
                type: 'post',
                url: url+'/api/view-package-waybills',
                dataType:'json',
                data: {
                    'package_id'      :id
                },
                success: function(data) {
                    wayill_qty = data.length
                    $("#qty").text(wayill_qty);
                    console.log(data);
                    $.each( data, function( key, value ) {
                        $("#fetched-waybills").append(
                        '<div class="transaction-item list-item item-'+value.id+' border-warning">'
                                    +'<div class="waybill-item-'+value.id+' w-100">'
                                        +'<div class="">'
                                            +'<div class="transaction-percentage ">'
                                                +'<h6 class="transaction-title font-medium-5 m-0 text-warning">'+value.waybill_no+'</h6>'
                                                +'<div class="text-primary">'
                                                    +'<div class="font-small-4 m--t-4 w-100">'
                                                        +'<span class="badge badge-light-primary">Waybills</span>, <span class="badge badge-light-primary">Action Logs</span>, <span class="badge badge-light-primary">Package Waybills</span> <button type="button" class="btn btn-sm btn-success sync-waybills-btn pull-right" id="'+value.id+'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-upload-cloud"><polyline points="16 16 12 12 8 16"></polyline><line x1="12" y1="12" x2="12" y2="21"></line><path d="M20.39 18.39A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.3"></path><polyline points="16 16 12 12 8 16"></polyline></svg></button>'
                                                    +'</div>'
                                                +'</div>'
                                            +'</div>'
                                        +'</div>'
                                    +'</div>'
                        +'</div>'
                        );
                    });
                },
            });

        });

        $('body').delegate(".sync-waybills-btn","click",function () {
            //call api sent to server function
            id = this.id;
            pid = $('#selected-pkg').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                           
            $.ajax({
                type: 'post',
                url: url+'/api/synced-waybills',
                dataType:'json',
                data: {
                    'waybill_id'      :id,
                    'package_id'      :pid
                },
                success: function(data) {
                    console.log(data);
                    if(data.success == 1){
                        --wayill_qty;
                        $(".item-"+id).remove();
                    }
                    $("#qty").text(wayill_qty);
                },
            });
        });

    });
    </script>
</body>
</html>