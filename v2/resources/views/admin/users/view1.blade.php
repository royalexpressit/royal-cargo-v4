<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Royal Cargo - Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">


    <link href="{{ asset('dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('dist/css/datepicker.min.css') }}">

    <!-- plugin css -->
    <link href="{{ asset('dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/spectrum.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('dist/css/datepicker.min.css') }}">
        
    <!-- Bootstrap Css -->
    <link href="{{ asset('dist/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('dist/css/app.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/custom.css') }}"  rel="stylesheet" type="text/css" />
</head>
<body data-layout="horizontal" data-topbar="colored">
    <!-- Begin page -->
    <div id="layout-wrapper">
        @include('admin.header')
    
       <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h5 class="mb-0">Dashboard</h5>

                                <div class="page-title-right">
                                    <a href="{{ url('status/cities') }}" class="btn btn-success btn-sm"><i class="mdi mdi-map-marker-multiple"></i> တစ်နိုင်ငံလုံးအခြေအနေ</a>
                                </div>
                            </div>
                        </div>
                    </div>


                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4 font-size-20 text-primary">View Profile</h4>
                                        <div class="data-loading text-center mt-10">
                                            <h4>လုပ်ဆောင်နေပါသည်။ ခဏစောင့်ပါ .....</h4>
                                            <div class="spinner-grow text-warning m-1" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                            <div class="spinner-grow text-success m-1" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                            <div class="spinner-grow text-danger m-1" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                        </div>
                                        <div class="user-form">
                                            <div class="mb-3">
                                                <label class="form-label">Username</label>
                                                <div class="input-group">
                                                    <input class="form-control form-data" type="text" value="{{ $user->name }}" id="username" disabled>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Email</label>
                                                <div class="input-group">
                                                    <input class="form-control form-data" type="text" value="{{ $user->email }}" id="email" disabled>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-xl-6">
                                                    <label class="form-label">City</label>
                                                    <select class="form-control select2 form-data" disabled id="city">
                                                        @foreach(fetched_all_cities() as $city)
                                                        <option value="{{ $city->id }}" {{ ($city->id == $user->city_id? 'selected':'') }}>{{ $city->shortcode }} ({{ $city->name.'-'.$city->mm_name }})</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-xl-6">
                                                    <label class="form-label">Branch</label>
                                                    <select class="form-control select2 form-data" disabled id="branches">
                                                        @foreach(fetched_all_branches() as $branch)
                                                        <option value="{{ $branch->id }}" {{ ($branch->id == $user->branch_id? 'selected':'') }}>{{ $branch->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-xl-6">
                                                    <div class="form-check mb-3">
                                                        <input class="form-check-input form-data role" type="radio" name="role"
                                                            id="formRadios1" {{ ($user->role == 2 ? 'checked':'') }} value="2" disabled >
                                                        <label class="form-check-label" for="formRadios1">
                                                            Operator
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input form-data role" type="radio" name="role"
                                                            id="formRadios2" {{ ($user->role == 3 ? 'checked':'') }} value="3" disabled >
                                                        <label class="form-check-label" for="formRadios2">
                                                            Delivery 
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <button type="button" class="btn btn-primary w-lg waves-effect waves-light btn-edit">Edit</button>
                                                <button type="button" class="btn btn-success w-lg waves-effect waves-light hide btn-save">Save</button>
                                                <button type="button" class="btn btn-danger w-lg waves-effect waves-light hide btn-cancel">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4 font-size-20 text-success">Change Password</h4>
                                        <div class="mb-3">
                                            <label class="form-label">New Password</label>
                                            <div class="input-group">
                                                <input class="form-control" type="password" value="" id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <button type="button" class="btn btn-primary w-lg waves-effect waves-light">Change</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                        </div>

                        
                    </div>
                </div>

                
                @include('footer')
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right Sidebar -->
        
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('dist/js/metisMenu.min.js') }}"></script>
        <script src="{{ asset('dist/js/simplebar.min.js') }}"></script>
        <script src="{{ asset('dist/js/waves.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.counterup.min.js') }}"></script>

        <!-- plugins -->
        <script src="{{ asset('dist/js/select2.min.js') }}"></script>
        <script src="{{ asset('dist/js/spectrum.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.bootstrap-touchspin.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-maxlength.min.js') }}"></script>
        <script src="{{ asset('dist/js/datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/form-advanced.init.js') }}"></script>

       

        <!-- App js -->
        <script src="{{ asset('dist/js/app.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var url  = $("#url").val();

                var current_city = $("#current_city").val();
                var current_branch = $("#current_branch").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
               
                $.ajax({
                    type: 'post',
                    url: url+'/api/fetched-branches',
                    dataType:'json',
                    data: {
                        'current_city' :current_city,
                    },
                    success: function(data) { 
                        $.each(data, function (i, item) {
                            $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                        });
                        $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                    },
                });

                $(".btn-edit").on("click",function search(e) {
                    $('.btn-edit').hide();
                    $('.form-data').prop("disabled", false);
                    $('.btn-save').show();
                    $('.btn-cancel').show();
                });

                $(".btn-cancel").on("click",function search(e) {
                    $('.btn-edit').show();
                    $('.form-data').prop("disabled", true);
                    $('.btn-save').hide();
                    $('.btn-cancel').hide();
                });

                $('#city').on("change",function search(e) {
                    var id = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                   
                    $.ajax({
                        type: 'post',
                        url: url+'/api/fetched-branches',
                        dataType:'json',
                        data: {
                            'current_city' :id,
                        },
                        success: function(data) {
                            $('#branches').empty(); 
                            $.each(data, function (i, item) {
                                $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                            })

                            $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                        },
                    });
                    
                });

                $(".btn-save").on("click",function search(e) {
                    $(".data-loading").show();
                    $(".user-form").hide();

                    username    = $('#username').val();
                    email       = $("#email").val();
                    city        = $("#city").val();
                    branch      = $("#branch").val();
                    role        = $("input[type='radio']:checked").val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
               
                    $.ajax({
                        type: 'post',
                        url: url+'/api/users/update',
                        dataType:'json',
                        data: {
                            'username' :username,
                            'email'    :email,
                            'city'     :city,
                            'branch'   :branch,
                            'role'     :role,
                        },
                        success: function(data) { 
                            //finished();
                            $(".user-form").hide()
                        },
                    });

                });
            });
        </script>

    </body>
</html>
