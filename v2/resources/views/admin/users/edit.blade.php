<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Users</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.main-city.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Users</h3>
                            <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{ url('users/') }}">Users</a>
                                </li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="profile-info">
                    <div class="row">
                        <div class="col-lg-5 col-12 order-2 order-lg-1">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Edit User</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div>
                                        <label class="form-label text-primary" for="select2-icons">Username <span class="text-danger">✶</span></label>
                                        <input type="text" class="form-control" id="username" value="{{ $user->name }}">
                                    </div>
                                    <div class="mt-2">
                                        <label class="form-label text-primary" for="select2-icons">Email <span class="text-danger">✶</span></label>
                                        <input type="text" class="form-control" id="email" value="{{ $user->email }}" disabled>
                                    </div>
                                    <div class="mt-2">
                                        <div class="row">
                                            <div class="col-lg-6 col-12 order-2 order-lg-1">
                                                <label class="form-label text-primary" for="select2-icons">Current City <span class="text-danger">✶</span></label>
                                                <select data-placeholder="Select a state..." class="select2 form-select form-data" disabled id="city">
                                                    @foreach(fetched_all_cities() as $city)
                                                    <option value="{{ $city->id }}" {{ ($city->id == $user->city_id? 'selected':'') }}>{{ $city->shortcode }} ({{ $city->name.'-'.$city->mm_name }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-6 col-12 order-2 order-lg-1">
                                                <label class="form-label text-primary" for="select2-icons">Current Branch <span class="text-danger">✶</span></label>
                                                <select data-placeholder="Select a state..." class="select2 form-select form-data" disabled id="branch">
                                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mt-2">
                                        <div class="row">
                                            <div class="col-lg-6 col-12 order-2 order-lg-1">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="2" {{ ($user->role == 2 ? 'checked':'') }}/>
                                                    <label class="form-check-label" for="inlineRadio1">Operator</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12 order-2 order-lg-1">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="3" {{ ($user->role == 3 ? 'checked':'') }} />
                                                    <label class="form-check-label" for="inlineRadio1">Courier</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mt-2 form-check form-check-success">
                                        <input type="checkbox" class="form-check-input" id="active" {{ $user->active==1 ? 'checked':'' }} />
                                        <label class="form-check-label" for="active">Active </label>
                                    </div>

                                    <div class="mt-2">
                                        <button type="button" class="btn btn-relief-primary btn-edit">Edit</button>
                                        <button type="button" class="btn btn-relief-success hide btn-save">Save</button>
                                        <button type="button" class="btn btn-relief-danger hide btn-cancel">Cancel</button>
                                    </div>
                                    <div class="mb-0">
                                        <small><span class="text-danger">✶</span> is required fields.</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5 col-12 order-2 order-lg-1">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Reset Password</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div>
                                        <label class="form-label text-primary w-100" for="new_pass">New Password 
                                            <span class="text-danger">✶</span>
                                            <code class="text-danger pull-right character1">min : 6</code>
                                            <code class="text-danger pull-right confirmed hide">confirmed password does not match</code>
                                        </label>
                                        <input type="password" class="form-control password" id="new_pass" placeholder="********" min="6">
                                    </div>
                                    <div class="mt-2">
                                        <label class="form-label text-primary w-100" for="confirmed_pass">Confirmed Password 
                                            <span class="text-danger">✶</span>
                                            <code class="text-danger pull-right character2">min : 6</code>
                                        </label>
                                        <input type="password" class="form-control password" id="confirmed_pass" placeholder="********">
                                    </div>
                                    <div class="mt-2">
                                        <button type="button" class="btn btn-relief-primary btn-reset disabled">Change Password</button>
                                    </div>
                                    <div class="mb-0">
                                        <small><span class="text-danger">✶</span> is required fields.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')
    <input type="hidden" id="user_city_id" value="{{ user_info($id)->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user_info($id)->branch_id }}">
    <input type="hidden" id="selected_user" value="{{ $id }}">


    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/bundles/admin/a-432370.js') }}"></script>
</body>
</html>