<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Users</title>

    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" href="{{ asset('dist/images/favicon.png') }}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.main-city.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                  <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      <div class="col-12">
                        <h3 class="content-header-title float-start mb-0">Users</h3>
                        <div class="breadcrumb-wrapper">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Users
                            </li>
                          </ol>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                  </div>
              </div>
              <div class="content-body">
<section id="profile-info">
    <div class="row">
      <div class="col-lg-6 col-12 order-2 order-lg-1">
        <!-- about -->
        <div class="card">
          <div class="card-body">
            <h5 class="mb-75">Username</h5>
            <p class="card-text">
              {{ $user->name }}
            </p>
            <div class="mt-2">
              <h5 class="mb-75">Email</h5>
              <p class="card-text">{{ $user->email }}</p>
            </div>
            <div class="mt-2">
              <h5 class="mb-75">Current City</h5>
              <p class="card-text">{{ city($user->city_id)['name'] }}</p>
            </div>
            <div class="mt-2">
              <h5 class="mb-75">Current Branch</h5>
              <p class="card-text">{{ branch($user->branch_id)['name'] }}</p>
            </div>
            <div class="mt-2">
              <h5 class="mb-50">User Role</h5>
              <p class="card-text mb-0">{{ $user->role }}</p>
            </div>
            <div class="mt-2">
              <h5 class="mb-50">Status</h5>
              <p class="card-text mb-0">{{ $user->active }}</p>
            </div>
          </div>
        </div>
      </div>
 
    </div>
  </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')



    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script>
      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      });
      $(document).ready(function(){
                var url  = $("#url").val();

                var current_city = $("#current_city").val();
                var current_branch = $("#current_branch").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
               
                $.ajax({
                    type: 'post',
                    url: url+'/api/fetched-branches',
                    dataType:'json',
                    data: {
                        'current_city' :current_city,
                    },
                    success: function(data) { 
                        $.each(data, function (i, item) {
                            $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                        });
                        $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                    },
                });

                $(".btn-edit").on("click",function search(e) {
                    $('.btn-edit').hide();
                    $('.form-data').prop("disabled", false);
                    $('.btn-save').show();
                    $('.btn-cancel').show();
                });

                $(".btn-cancel").on("click",function search(e) {
                    $('.btn-edit').show();
                    $('.form-data').prop("disabled", true);
                    $('.btn-save').hide();
                    $('.btn-cancel').hide();
                });

                $('#city').on("change",function search(e) {
                    var id = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                   
                    $.ajax({
                        type: 'post',
                        url: url+'/api/fetched-branches',
                        dataType:'json',
                        data: {
                            'current_city' :id,
                        },
                        success: function(data) {
                            $('#branches').empty(); 
                            $.each(data, function (i, item) {
                                $('#branches').append('<option value="'+item['id']+'" data-icon="user">'+item['name']+'</option>' );
                            })

                            $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                        },
                    });
                    
                });

                $(".btn-save").on("click",function search(e) {
                    $(".data-loading").show();
                    $(".user-form").hide();

                    username    = $('#username').val();
                    email       = $("#email").val();
                    city        = $("#city").val();
                    branch      = $("#branch").val();
                    role        = $("input[type='radio']:checked").val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
               
                    $.ajax({
                        type: 'post',
                        url: url+'/api/users/update',
                        dataType:'json',
                        data: {
                            'username' :username,
                            'email'    :email,
                            'city'     :city,
                            'branch'   :branch,
                            'role'     :role,
                        },
                        success: function(data) { 
                            //finished();
                            $(".user-form").hide()
                        },
                    });

                });
            });
    </script>
</body>
</html>