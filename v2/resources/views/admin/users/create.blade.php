<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Inbound Actions</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Users</h3>
                            <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{ url('users/') }}">Users</a>
                                </li>
                                <li class="breadcrumb-item active">Create</li>
                            </ol>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Create New User</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">Username <span class="text-danger">✶</span></label>
                                        <input type="text" id="username" class="form-control" placeholder="Mg Mg" autofocus />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">Email <span class="text-danger">✶</span></label>
                                        <input type="text" id="email" class="form-control" placeholder="mgmg@royalx.net" />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">Password <span class="text-danger">✶</span></label>
                                        <input type="password" id="password" class="form-control" placeholder="********" />
                                    </div>
                                    <div class="mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-label text-primary" for="select2-icons">City <span class="text-danger">✶</span></label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" id="city">
                                                    @foreach($cities as $city)
                                                    <option value="{{ $city->id }}">{{ $city->shortcode.' ('.$city->name.')' }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label text-primary" for="select2-icons">Branch <span class="text-danger">✶</span></label>
                                                <select data-placeholder="Select a branch..." class="select2 form-select" id="branch">
                                                    <option value="" >----</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">User Role <span class="text-danger">✶</span></label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="option_11" id="inlineRadio1" value="2" checked />
                                                    <label class="form-check-label" for="same_day">Operator</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="option_11" id="inlineRadio1" value="3" />
                                                    <label class="form-check-label" for="same_day">Courier</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        <button type="button" class="btn btn-relief-primary waves-effect btn-create">Create</button>
                                    </div>
                                    <div class="mb-0">
                                        <small><span class="text-danger">✶</span> is required fields.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    @include('customizer')
    @include('footer')

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/bundles/admin/a-812270.js') }}"></script>
</body>
</html>