<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Royal Cargo - View Waybill # {{ $waybill->waybill_no }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('dist/images/favicon.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <link href="{{ asset('dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('dist/css/datepicker.min.css') }}">

    <!-- plugin css -->
    <link href="{{ asset('dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/spectrum.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('dist/css/datepicker.min.css') }}">
        
    <!-- Bootstrap Css -->
    <link href="{{ asset('dist/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('dist/css/app.min.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/css/custom.css') }}"  rel="stylesheet" type="text/css" />
</head>
<body data-layout="horizontal" data-topbar="colored">
    <!-- Begin page -->
    <div id="layout-wrapper">
        @include('operator.header')
    
        <div class="main-content">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h5 class="mb-0">
                                    <a href="{{ url('') }}">Dashboard</a>
                                    > View Waybill # {{ $waybill->waybill_no }}
                                </h5>
                                <div class="page-title-right">
                                    <div class="btn-group block" role="group" aria-label="Basic example">
                                        <a href="{{ url()->previous() }}" class="btn btn-primary">Previous Page</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4 font-size-20 text-primary">View Waybill</h4>
                                        <div class="table-responsive">
                                                <table class="table table-borderless table-centered table-nowrap">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 20px;">
                                                                <img src="{{ asset('dist/images/icons/city.png') }}" class="avatar-xs rounded-circle " alt="...">
                                                            </td>
                                                            <td>
                                                                <a href="{{ url('/outbound/collected-by-branches') }}">
                                                                    <p class="text-muted font-size-14 mb-0">Origin</p>
                                                                    <h6 class="font-size-18 mb-1 fw-normal text-primary">-{{ $waybill->origin }}</h6>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><img src="{{ asset('dist/images/icons/delivery-man.png') }}" class="avatar-xs rounded-circle " alt="..."></td>
                                                            <td>
                                                                <a href="{{ url('/outbound/collected-by-deliveries') }}">
                                                                    <p class="text-muted font-size-14 mb-0">Transit</p>
                                                                    <h6 class="font-size-18 mb-1 fw-normal text-primary">-{{ $waybill->transit }}</h6>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><img src="{{ asset('dist/images/icons/house.png') }}" class="avatar-xs rounded-circle " alt="..."></td>
                                                            <td>
                                                                <a href="{{ url('/outbound/handover-by-branches') }}">
                                                                    <p class="text-muted font-size-14 mb-0">Destination</p>
                                                                    <h6 class="font-size-18 mb-1 fw-normal text-primary">-{{ $waybill->destination }}</h6>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><img src="{{ asset('dist/images/icons/city.png') }}" class="avatar-xs rounded-circle " alt="..."></td>
                                                            <td>
                                                                <a href="{{ url('/outbound/branch-out-by-cities') }}">
                                                                    <p class="text-muted font-size-14 mb-0">Status</p>
                                                                    <h6 class="font-size-18 mb-1 fw-normal text-primary">{{ $waybill->current_status }}</h6>
                                                                <a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><img src="{{ asset('dist/images/icons/city.png') }}" class="avatar-xs rounded-circle " alt="..."></td>
                                                            <td>
                                                                <a href="{{ url('/outbound/branch-out-by-cities') }}">
                                                                    <p class="text-muted font-size-14 mb-0">Transit Status</p>
                                                                    <h6 class="font-size-18 mb-1 fw-normal text-primary">{{ $waybill->current_status }}</h6>
                                                                <a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><img src="{{ asset('dist/images/icons/delivery-man.png') }}" class="avatar-xs rounded-circle " alt="..."></td>
                                                            <td>
                                                                <a href="{{ url('/outbound/branch-out-by-cities') }}">
                                                                    <p class="text-muted font-size-14 mb-0">Delivery/Counter</p>
                                                                    <h6 class="font-size-18 mb-1 fw-normal text-primary">{{ $waybill->user_id }}</h6>
                                                                <a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-primary">Edit Waybill</a> 
                                    </div>
                                </div> 
                            </div>
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4 font-size-20 text-primary">Branch Action Logs</h4>
                                        <div class="table-responsive">
                                            <table class="table table-borderless table-centered table-nowrap">
                                                <tbody>
                                                    @foreach($branch_logs as $branch)
                                                    <tr>
                                                        <td>
                                                            <h6 class="text-primary mb-1 fw-normal font-size-18 text-capitalize">
                                                                {{ $branch->action_type }} 
                                                            </h6>
                                                            <p class="text-muted mb-0">
                                                                <span class="text-capitalize">{{ $branch->from_branch }}</span>
                                                                -
                                                                <span class="text-capitalize">{{ $branch->to_branch }}</span>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div> 
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4 font-size-20 text-primary">View Logs</h4>
                                       	<div class="table-responsive">
                                            <table class="table table-borderless table-centered table-nowrap">
                                                <tbody>
                                                	@foreach($action_logs as $log)
                                                	<tr>
                                                        <td>
                                                            <h6 class="mb-1 fw-normal">
                                                            	<span class="font-size-18 text-primary text-capitalize">{{ $log->action_type }}</span>
                                                            	<small class="font-size-12 pull-right text-danger">{{ $log->action_date }}</small>
                                                            </h6>
                                                            <p class="text-muted font-size-16 mb-0">{{ $log->action_log }}</p>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div> 
                                    </div>
                                </div> 
                            </div>

                            
                        </div>

                        
                    </div>
                </div>

               @include('footer')
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right Sidebar -->
       
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('dist/js/metisMenu.min.js') }}"></script>
        <script src="{{ asset('dist/js/simplebar.min.js') }}"></script>
        <script src="{{ asset('dist/js/waves.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.counterup.min.js') }}"></script>

        <!-- plugins -->
        <script src="{{ asset('dist/js/select2.min.js') }}"></script>
        <script src="{{ asset('dist/js/spectrum.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.bootstrap-touchspin.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-maxlength.min.js') }}"></script>
        <script src="{{ asset('dist/js/datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/form-advanced.init.js') }}"></script>

        <!-- apexcharts -->
        <script src="{{ asset('dist/js/apexcharts.min.js') }}"></script>

        <script src="{{ asset('dist/js/dashboard.init.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('dist/js/app.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var url  = $("#url").val();
                var _token  = $("#_token").val();


            });
        </script>

    </body>
</html>
