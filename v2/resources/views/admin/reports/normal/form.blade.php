<!doctype html>
<html lang="en">
    <head>
        
    <meta charset="utf-8" />
        <title>Royal Cargo - Report</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesbrand" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('dist/images/favicon.png') }}">

        <!-- plugin css -->
        <link href="{{ asset('dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dist/css/spectrum.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('dist/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('dist/css/datepicker.min.css') }}">
        <!-- Bootstrap Css -->
        <link href="{{ asset('dist/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{ asset('dist/css/app.min.css') }}"  rel="stylesheet" type="text/css" />
        <link href="{{ asset('dist/css/custom.css') }}"  rel="stylesheet" type="text/css" />
    </head>

    <body data-layout="horizontal" data-topbar="colored">

        <!-- Begin page -->
        <div id="layout-wrapper">
            @include('admin.header')

            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h5 class="mb-0">
                                        <a href="{{ url('') }}">Dashboard</a>
                                        > <a href="{{ url('reports') }}">Reports</a>
                                        > Normal Reports
                                    </h5>

                                    <div class="page-title-right">
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                        <div class="row">
                            <div class="col-lg-6">
                                <h5 class="card-header bg-primary text-white">1.Search Outbound By Date</h5>
                                <div class="card card-body">
                                    <form action="{{ route('normal-report-1') }}" method="GET">
                                        <div class="mb-3">
                                            <label class="form-label">Outbound Collected Date</label>
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" class="form-control" value="{{ date('Y-m-d') }}"
                                                    data-date-format="yyyy-mm-dd" data-date-container='#datepicker2' data-provide="datepicker"
                                                    data-date-autoclose="true" name="form_date_1">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <button type="submit" class="btn btn-primary w-lg waves-effect waves-light">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <h5 class="card-header bg-success text-white">2.Search Inbound By Date</h5>
                                <div class="card card-body">
                                    <form action="{{ route('normal-report-2') }}" method="GET">
                                        <div class="mb-3">
                                            <label class="form-label">Inbound Branch In Date</label>
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" class="form-control" value="{{ date('Y-m-d') }}"
                                                    data-date-format="yyyy-mm-dd" data-date-container='#datepicker2' data-provide="datepicker"
                                                    data-date-autoclose="true" name="form_date_2">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <button type="submit" class="btn btn-success w-lg waves-effect waves-light">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <h5 class="card-header bg-primary text-white">3.Search Branch Out By City </h5>
                                <div class="card card-body">
                                    <form action="{{ route('normal-report-3') }}" method="GET">
                                        <div class="mb-3">
                                            <label class="form-label">Outbound Collected Date</label>
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" class="form-control" value="{{ date('Y-m-d') }}"
                                                    data-date-format="yyyy-mm-dd" data-date-container='#datepicker2' data-provide="datepicker"
                                                    data-date-autoclose="true" name="form_date_3">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Select To City</label>
                                            <select class="form-control select2" name="form_city_3">
                                                @foreach(fetched_all_cities() as $city)
                                                <option value="{{ $city->id }}">{{ $city->shortcode }} ({{ $city->name.'-'.$city->mm_name }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <button type="submit" class="btn btn-primary w-lg waves-effect waves-light">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <h5 class="card-header bg-success text-white">4.Search Inbound By City</h5>
                                <div class="card card-body">
                                    <form action="{{ route('normal-report-4') }}" method="GET">
                                        <div class="mb-3">
                                            <label class="form-label">Inbound Branch In Date</label>
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" class="form-control" value="{{ date('Y-m-d') }}"
                                                    data-date-format="yyyy-mm-dd" data-date-container='#datepicker2' data-provide="datepicker"
                                                    data-date-autoclose="true" name="form_date_4">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Select From City</label>
                                            <select class="form-control select2" name="form_city_4">
                                                @foreach(fetched_all_cities() as $city)
                                                <option value="{{ $city->id }}">{{ $city->shortcode }} ({{ $city->name.'-'.$city->mm_name }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <button type="submit" class="btn btn-success w-lg waves-effect waves-light">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h5 class="card-header bg-primary text-white">5.Search Outbound Handover By Branch</h5>
                                <div class="card card-body">
                                    <form action="{{ route('normal-report-5') }}" method="GET">
                                        <div class="mb-3">
                                            <label class="form-label">Outbound Collected Date</label>
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" class="form-control" value="{{ date('Y-m-d') }}"
                                                    data-date-format="yyyy-mm-dd" data-date-container='#datepicker2' data-provide="datepicker"
                                                    data-date-autoclose="true" name="form_date_5">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Select Collected Branch</label>
                                            <select class="form-control select2" name="form_branch_5">
                                                @foreach($branches as $branch)
                                                <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <button type="submit" class="btn btn-primary w-lg waves-effect waves-light">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h5 class="card-header bg-success text-white">6.Search Inbound Handover To Branch</h5>
                                <div class="card card-body">
                                    <form action="{{ route('normal-report-6') }}" method="GET">
                                        <div class="mb-3">
                                            <label class="form-label">Inbound Branch In Date</label>
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" class="form-control" value="{{ date('Y-m-d') }}"
                                                    data-date-format="yyyy-mm-dd" data-date-container='#datepicker2' data-provide="datepicker"
                                                    data-date-autoclose="true" name="form_date_6">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">To Branch</label>
                                            <select class="form-control select2" name="form_branch_6">
                                                @foreach($branches as $branch)
                                                <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <button type="submit" class="btn btn-success w-lg waves-effect waves-light">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h5 class="card-header bg-primary text-white">7.Search Outbound Handover By Branch</h5>
                                <div class="card card-body">
                                    <form action="{{ route('normal-report-7') }}" method="GET">
                                        <div class="mb-3">
                                            <label class="form-label">Outbound Collected Date</label>
                                            <div class="input-group" id="datepicker2">
                                                <input type="text" class="form-control" value="{{ date('Y-m-d') }}"
                                                    data-date-format="yyyy-mm-dd" data-date-container='#datepicker2' data-provide="datepicker"
                                                    data-date-autoclose="true" name="form_date_7">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Select Collected Branch</label>
                                            <select class="form-control select2" name="form_branch_7">
                                                @foreach($branches as $branch)
                                                <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Select To City</label>
                                            <select class="form-control select2" name="to_city_7">
                                                @foreach(fetched_all_cities() as $city)
                                                <option value="{{ $city->id }}">{{ $city->shortcode }} ({{ $city->name.'-'.$city->mm_name }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <button type="submit" class="btn btn-primary w-lg waves-effect waves-light">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h5 class="card-header bg-primary text-white">Print Outbound By City</h5>
                                <div class="card card-body">
                                    
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                
                @include('footer')
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            <div data-simplebar class="h-100">

                <div class="rightbar-title d-flex align-items-center px-3 py-4">
            
                    <h5 class="m-0 me-2">Settings</h5>

                    <a href="javascript:void(0);" class="right-bar-toggle ms-auto">
                        <i class="mdi mdi-close noti-icon"></i>
                    </a>
                </div>



                <!-- Settings -->
                <hr class="mt-0" />
                <h6 class="text-center mb-0">Choose Layouts</h6>

                <div class="p-4">
                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-1.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="light-mode-switch" checked />
                        <label class="form-check-label" for="light-mode-switch">Light Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-2.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="dark-mode-switch" />
                        <label class="form-check-label" for="dark-mode-switch">Dark Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-3.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input type="checkbox" class="form-check-input theme-choice" id="rtl-mode-switch" />
                        <label class="form-check-label" for="rtl-mode-switch">RTL Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="assets/images/layouts/layout-4.jpg" class="img-thumbnail" alt="layout images">
                    </div>
                    <div class="form-check form-switch mb-5">
                        <input class="form-check-input theme-choice" type="checkbox" id="dark-rtl-mode-switch">
                        <label class="form-check-label" for="dark-rtl-mode-switch">Dark RTL Mode</label>
                    </div>

            
                </div>

            </div> <!-- end slimscroll-menu-->
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>
           
            

            
        <!-- JAVASCRIPT -->
        <script src="{{ asset('dist/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('dist/js/metisMenu.min.js') }}"></script>
        <script src="{{ asset('dist/js/simplebar.min.js') }}"></script>
        <script src="{{ asset('dist/js/waves.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.counterup.min.js') }}"></script>

        <!-- plugins -->
        <script src="{{ asset('dist/js/select2.min.js') }}"></script>
        <script src="{{ asset('dist/js/spectrum.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/jquery.bootstrap-touchspin.min.js') }}"></script>
        <script src="{{ asset('dist/js/bootstrap-maxlength.min.js') }}"></script>
        <script src="{{ asset('dist/js/datepicker.min.js') }}"></script>
        <script src="{{ asset('dist/js/form-advanced.init.js') }}"></script>
        <!-- App js -->
        <script src="{{ asset('dist/js/app.js') }}"></script>
        <script src="{{ asset('dist/js/custom.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var url         = $("#url").val();

                
            });
        </script>
    </body>
</html>
