<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Create Route</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @if(user()->role_id == 1)
        @include('admin.header')
    @else
        @include('operator.header')
    @endif

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Routes</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ url('config-routes') }}">Routes</a>
                                    </li>
                                    <li class="breadcrumb-item active">Create Route
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                  </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Create New Route</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">From City:</label>
                                        <select data-placeholder="Select a state..." class="select2 form-select" id="from_city">
                                            @foreach($cities as $from_city)
                                            <option value="{{ $from_city->id }}">{{ $from_city->shortcode.' ('.$from_city->name.')' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">To City:</label>
                                        <select data-placeholder="Select a state..." class="select2 form-select" id="to_city">
                                            @foreach($cities as $to_city)
                                            <option value="{{ $to_city->id }}">{{ $to_city->shortcode.' ('.$to_city->name.')' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-0">
                                        @if(user()->id == 880 || user()->id == 1)
                                            <button type="submit" class="btn btn-relief-primary waves-effect create-btn">Create</button>
                                        @else
                                            <button type="submit" class="btn btn-relief-secondary waves-effect disabled">Create</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-body mxh-480">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th>Routes <span class="badge rounded-pill badge-light-primary fs-16" id="total">0</span></th>
                                              <th>Status</th>
                                              <th>Action</th>
                                          </thead>
                                          <tbody id="fetched-data">
                                            
                                          </tbody>
                                        </table>
                                    </div>
                                    <div class="data-loading text-center mt-10">
                                        <h5>Loading ...</h5>
                                        <div class="spinner-grow text-primary me-1" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                        <div class="spinner-grow text-danger me-1" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                        <div class="spinner-grow text-success me-1" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <div class="alert alert-grey show-alert font-size-16 text-center hide" role="alert">
                                        <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
                                          <div class="alert-body d-flex align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                            <strong>လမ်းကြောင်း မရှိသေးပါ။</strong>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    @include('customizer')
    @include('footer')

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
    <!-- END: Page JS-->

    <script>
        $(document).ready(function(){
            var current_city = $("#from_city").val();
            var _token       = $("#_token").val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
               
            $.ajax({
                type: 'post',
                url: url+'/api/fetched-routes',
                dataType:'json',
                data: {
                    'city_id' :current_city,
                },
                success: function(data) {
                    $("#total").text(data.length);
                    if(data.length > 0){
                        $("#fetched-data").empty();
                        $.each(data, function (i, item) {
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td><span class="badge rounded-pill badge-light-primary me-1">'+item['from_city']+' - '+item['to_city']+'</span></td>'
                                    +'<td><span class="route-'+item['id']+'">'+route_active(item['active'])+'</span></td>'
                                    +'<td><div class="form-check form-switch form-check-success">'
                                        +'<input type="checkbox" class="form-check-input action-btn" id="customSwitch1" value="'+item['id']+'" '+(item['active']==1? 'checked':'')+'/>'
                                        +'<label class="form-check-label" for="customSwitch1"></label>'
                                        +'</div>'
                                    +'</td>'
                                +'</tr>'
                            );
                            $(".show-alert").hide();
                            $(".data-loading").hide();
                        });
                    }else{
                        $(".show-alert").show();
                        $("#fetched-data").empty();
                        $(".data-loading").hide();
                    } 
                },
            });

   
            $('#from_city').on("change",function search(e) {
                var id = $(this).val();
                $(".data-loading").show();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                   
                $.ajax({
                    type: 'post',
                    url: url+'/api/fetched-routes',
                    dataType:'json',
                    data: {
                        'city_id' :id,
                    },
                    success: function(data) { 
                        console.log(data.length);
                        $("#total").text(data.length);
                        if(data.length > 0){
                            $("#fetched-data").empty();
                            $.each(data, function (i, item) {
                                $("#fetched-data").append(
                                    '<tr>'
                                        +'<td><span class="badge rounded-pill badge-light-primary me-1">'+item['from_city']+' - '+item['to_city']+'</span></td>'
                                        +'<td><span class="route-'+item['id']+'">'+route_active(item['active'])+'</span></td>'
                                        +'<td><div class="form-check form-switch form-check-info">'
                                            +'<input type="checkbox" class="form-check-input action-btn" id="customSwitch1" value="'+item['id']+'" '+(item['active']==1? 'checked':'')+'/>'
                                            +'<label class="form-check-label" for="customSwitch1">'
                                            +'</div>'
                                        +'</td>'
                                    +'</tr>'
                                );
                                $(".show-alert").hide();
                                $(".data-loading").hide();
                            });
                        }else{
                            $(".show-alert").show();
                            $("#fetched-data").empty();
                            $(".data-loading").hide();
                        }
                        //$('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                    },
                });
            });  

            //action-btn
            //$('.action-btn').on("change",function search(e) {
            $('body').delegate(".action-btn","change",function () {
                if ($(this).is(":checked") ) {
                    route_id = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                       
                    $.ajax({
                        type: 'post',
                        url: url+'/api/change-route',
                        dataType:'json',
                        data: {
                            'route_id' :route_id,
                            'active':1
                        },success: function(data) { 
                            if(data.active == 1){
                                $(".route-"+data.route_id).html('<span class="badge bg-success">ဖွင့်ထား</span>');
                            }else{
                                $(".route-"+data.route_id).html('<span class="badge bg-danger">ပိတ်ထား</span>');
                            }
                        }
                    });
                }else{
                    route_id = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                       
                    $.ajax({
                        type: 'post',
                        url: url+'/api/change-route',
                        dataType:'json',
                        data: {
                            'route_id' :route_id,
                            'active':0
                        },success: function(data) { 
                            if(data.active == 1){
                                $(".route-"+data.route_id).html('<span class="badge bg-success">ဖွင့်ထား</span>');
                            }else{
                                $(".route-"+data.route_id).html('<span class="badge bg-danger">ပိတ်ထား</span>');
                            }
                        }
                    });
                }

            });     

            $('body').delegate(".create-btn","click",function () {
                var from_city   = $("#from_city").val();
                var to_city     = $("#to_city").val();
                var from_lbl   = $("#from_city option:selected").text().substring(0,3);
                var to_lbl     = $("#to_city option:selected").text().substring(0,3);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                       
                $.ajax({
                    type: 'post',
                    url: url+'/create-route',
                    dataType:'json',
                    data: {
                        'from_city' :from_city,
                        'to_city'   :to_city,
                        '_token'    :_token
                    },success: function(data) { 
                        if(data.success == 1){
                            Swal.fire({
                                position:"center",
                                title:"အသိပေးခြင်း",
                                icon:"success",
                                html:"<strong>စာဝင်/စာထွက် လမ်းကြောင်းထည့်လိုက်ပါပြီ။</strong> ",
                                showConfirmButton:!1,
                                timer:3000,
                                customClass:{confirmButton:"btn btn-primary"},
                                buttonsStyling:!1
                            }); 

                            total = parseInt($("#total").text());
                            $("#fetched-data").prepend(
                                '<tr>'
                                    +'<td><span class="badge rounded-pill badge-light-primary me-1">'+from_lbl+' - '+to_lbl+'</span></td>'
                                    +'<td><span class="route-'+data.id+'">'+route_active(1)+'</span></td>'
                                    +'<td><div class="form-check form-switch form-check-success">'
                                        +'<input type="checkbox" class="form-check-input action-btn" id="customSwitch1" value="'+data.id+'" checked/>'
                                        +'<label class="form-check-label" for="customSwitch1"></label>'
                                        +'</div>'
                                    +'</td>'
                                +'</tr>'
                            );
                            $("#total").text(total+1);
                        }else{
                            Swal.fire({
                                position:"center",
                                title:"အသိပေးခြင်း",
                                icon:"warning",
                                html:"<strong>လမ်းကြောင်းရှိပြီးသားဖြစ်သည်။</strong> ",
                                showConfirmButton:!1,
                                timer:3000,
                                customClass:{confirmButton:"btn btn-primary"},
                                buttonsStyling:!1
                            }); 
                        }
                    }
                });
            }); 
        });
    </script>
</body>
</html>