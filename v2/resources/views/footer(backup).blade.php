

<!-- Full screen Content -->
                                                        <div class="modal fade" id="exampleModalFullscreen" tabindex="-1" aria-labelledby="exampleModalFullscreenLabel"
                                                            aria-hidden="true">
                                                            <div class="modal-dialog modal-fullscreen">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title h4" id="exampleModalFullscreenLabel">Full screen modal</h5>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>Cras mattis consectetur purus sit amet fermentum.
                                                                            Cras justo odio, dapibus ac facilisis in,
                                                                            egestas eget quam. Morbi leo risus, porta ac
                                                                            consectetur ac, vestibulum at eros.</p>
                                                                        <p>Praesent commodo cursus magna, vel scelerisque
                                                                            nisl consectetur et. Vivamus sagittis lacus vel
                                                                            augue laoreet rutrum faucibus dolor auctor.</p>
                                                                        <p>Aenean lacinia bibendum nulla sed consectetur.
                                                                            Praesent commodo cursus magna, vel scelerisque
                                                                            nisl consectetur et. Donec sed odio dui. Donec
                                                                            ullamcorper nulla non metus auctor
                                                                            fringilla.</p>
                                                                        <p>Cras mattis consectetur purus sit amet fermentum.
                                                                            Cras justo odio, dapibus ac facilisis in,
                                                                            egestas eget quam. Morbi leo risus, porta ac
                                                                            consectetur ac, vestibulum at eros.</p>
                                                                        <p>Praesent commodo cursus magna, vel scelerisque
                                                                            nisl consectetur et. Vivamus sagittis lacus vel
                                                                            augue laoreet rutrum faucibus dolor auctor.</p>
                                                                        <p>Aenean lacinia bibendum nulla sed consectetur.
                                                                            Praesent commodo cursus magna, vel scelerisque
                                                                            nisl consectetur et. Donec sed odio dui. Donec
                                                                            ullamcorper nulla non metus auctor
                                                                            fringilla.</p>
                                                                        <p>Cras mattis consectetur purus sit amet fermentum.
                                                                            Cras justo odio, dapibus ac facilisis in,
                                                                            egestas eget quam. Morbi leo risus, porta ac
                                                                            consectetur ac, vestibulum at eros.</p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
<footer class="footer">
    <!--
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalFullscreen">
                                                                Full screen
                                                            </button>
                                                        -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> © Royal Express Cargo. <small class="text-danger">(version 4.1)</small>
            </div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block">
                    Powered by <i class="mdi mdi-heart text-danger"></i>  <a href="https://themesbrand.com/" target="_blank" class="text-reset">Royal IT Business Support</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<input type="hidden" id="url" value="{{ url('') }}">
<input type="hidden" id="_token" value="{{ csrf_token() }}">
<!-- staticBackdrop Modal -->
                                                            <div class="modal fade" id="date-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="staticBackdropLabel">Change Dashboard Date</h5>
                                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>Dashboard အား <span id="set_date" class="font-size-16 text-danger"></span> နေ့အား ပြောင်းလဲရန် အတည်ပြုပေးပါ။</p>
                                                                            <input type="hidden" id="config_date">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">ပိတ်မည်</button>
                                                                            <button type="button" class="btn btn-primary set-date" >ပြောင်းလဲမည်</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>