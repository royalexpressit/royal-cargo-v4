<section class="marquee">
  <h4 class="text">
    @foreach(DB::table('announcements')->select('content')->get() as $announcement)
    {{ $announcement->content }}
    @endforeach
  </h4>
</section>