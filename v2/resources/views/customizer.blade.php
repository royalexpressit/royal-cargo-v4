<!-- BEGIN: Customizer-->
<div class="customizer d-none d-md-block">
	<a class="customizer-toggle d-flex align-items-center justify-content-center" href="#">
		<i class="" data-feather="package"></i>
	</a>
	<div class="customizer-content">
	  	<div class="customizer-header px-2 pt-1 pb-0 position-relative">
	    	<h4 class="mb-0">Package Types</h4>
	    	<p class="m-0">အထုပ်အမျိုးအစားများ</p>

	    	<a class="customizer-close" href="#"><i data-feather="x"></i></a>
	  	</div>
	  	<hr />

	  	<!-- Styling & Text Direction -->
	  	<div class="customizer-styling-direction">
		    <div class="card card-transaction">
		        <div class="card-body">
		        	@foreach(DB::table('package_types')->orderBy('name','asc')->get() as $type)
		          	<div class="transaction-item">
			            <div class="d-flex">
			              	<div class="transaction-percentage">
			                	<h6 class="transaction-title text-primary"><i data-feather="{{ $type->icon }}" class="avatar-icon"></i> <strong>{{ $type->name }}</strong></h6>
			                	<small>{{ $type->remark }}</small>
			              	</div>
			            </div>
		          	</div>
		          	@endforeach
		        </div>
		    </div>
	  	</div>
		<!-- Footer -->
	</div>
</div>
<!-- End: Customizer-->