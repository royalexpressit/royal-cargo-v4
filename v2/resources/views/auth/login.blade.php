<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Login</title>
    
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-validation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/authentication.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
    <!-- END: Custom CSS-->
    <style type="text/css">
      .panda {
        position: relative;
        width: 200px;
        margin: 0px auto;
      }
      .face {
  width: 200px;
  height: 200px;
  background: #fff;
  border-radius: 100%;
  margin: 20px auto;
  box-shadow: 0 10px 15px rgba(0, 0, 0, 0.15);
  z-index: 50;
  position: relative;
}

.ear, .ear:after {
  position: absolute;
  width: 80px;
  height: 80px;
  background: #ff9800;
  z-index: 5;
  border: 10px solid #fff;
  left: -15px;
  top: -15px;
  border-radius: 100%;
}
.ear:after {
  content: '';
  left: 125px;
}

.eye-shade {
  background: #ffaa2c;
  width: 50px;
  height: 50px;
  margin: 10px;
  position: absolute;
  top: 60px;
  left: 20px;
  border-radius: 50%;
  //top: 35px;
  //left: 25px;
  //transform: rotate(220deg);
  //border-radius: 25px/20px 30px 35px 40px;
}
.eye-shade.rgt {
  transform: rotate(140deg);
  left: 110px;
}

.eye-white {
  position: absolute;
  width: 30px;
  height: 30px;
  border-radius: 100%;
  background: #fff;
  z-index: 500;
  left: 40px;
  top: 80px;
  overflow: hidden;
}
.eye-white.rgt {
  right: 40px;
  left: auto;
}

.eye-ball {
  position: absolute;
  width: 0px;
  height: 0px;
  left: 20px;
  top: 20px;
  max-width: 10px;
  max-height: 10px;
  transition: 0.1s;
}
.eye-ball:after {
  content: '';
  background: #ff9800;
  position: absolute;
  border-radius: 100%;
  right: 0;
  bottom: 0px;
  width: 20px;
  height: 20px;
}

.nose {
  position: absolute;
  height: 20px;
  width: 35px;
  bottom: 40px;
  left: 0;
  right: 0;
  margin: auto;
  border-radius: 50px 20px/30px 15px;
  transform: rotate(15deg);
  background: #ff9800;
}
.layer{
  padding: 2px;
  background:
    linear-gradient(to right, #ff5722 2px, transparent 2px) 0 0,
    linear-gradient(to right, #ff5722 2px, transparent 2px) 0 100%,
    linear-gradient(to left, #ff5722 2px, transparent 2px) 100% 0,
    linear-gradient(to left, #ff5722 2px, transparent 2px) 100% 100%,
    linear-gradient(to bottom, #ff5722 2px, transparent 2px) 0 0,
    linear-gradient(to bottom, #ff5722 2px, transparent 2px) 100% 0,
    linear-gradient(to top, #ff5722 2px, transparent 2px) 0 100%,
    linear-gradient(to top, #ff5722 2px, transparent 2px) 100% 100%;

  background-repeat: no-repeat;
  background-size: 30px 30px;
}
form {
  display: none;
  background: #f8f8f8;
  margin: auto;
  display: block;
  transition: 0.3s;
  position: relative;
  z-index: 500;
  padding: 20px 10px;
  border: 1px solid #f0e9e9;
  
}
form.up{
  transform: translateY(-140px);

}

.hand, .hand:after, .hand:before {
  width: 40px;
  height: 30px;
  border-radius: 50px;
  box-shadow: 0 2px 3px rgba(0, 0, 0, 0.15);
  background: #ff9800;
  margin: 5px;
  position: absolute;
  top: 70px;
  left: -40px;
}
.hand:after, .hand:before {
  content: '';
  left: -5px;
  top: 11px;
}
.hand:before {
  top: 26px;
}
.hand.rgt, .rgt.hand:after, .rgt.hand:before {
  left: auto;
  right: -40px;
}
.hand.rgt:after, .hand.rgt:before {
  left: auto;
  right: -5px;
}
    </style>
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="horizontal-layout horizontal-menu blank-page navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="blank-page">
    <!-- BEGIN: Content-->
    <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
          <div class="auth-wrapper auth-cover">
            <div class="auth-inner row m-0">
              <a class="brand-logo" href="#">
                <img src="{{ asset('_dist/images/logo.png') }}" class="logo">
                <h2 class="brand-text text-danger ms-1 display-5">Cargo</h2></a>
              <!-- /Brand logo-->
              <!-- Left Text-->
              <div class="d-none d-lg-flex col-lg-4 align-items-center">
                <div class="w-100 d-lg-flex align-items-center justify-content-center"><img class="img-fluid" src="{{ asset('_dist/images/img1.png') }}" alt="Login V2"/></div>
              </div>
              <!-- /Left Text-->
              <!-- Login-->
              <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-4">
                
                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                  <h3 class="fw-bold text-center">Welcome to Cargo! 👋</h3>
                  
                  <div class="text-center">
                    <img src="{{ asset('_dist/images/rocket.gif') }}" class="rocket mb-1">
                  </div>
                  <div class="layer">
                  <form class="auth-login-form" action="{{ route('login') }}" method="POST">
                    
                    @csrf
                    <div class="mb-1">
                      <label class="form-label" for="email">Login Name</label>
                      <input class="form-control" id="login_name" type="text" placeholder="kyawkyaw" aria-describedby="email" autofocus="" tabindex="1"/>
                      <input class="form-control" id="email" type="hidden" name="email" />
                    	@error('email')
                                                <span class="text-danger" role="alert">
                                                    <span>{{ $message }}</span>
                                                </span>
                                            @enderror
                    </div>
                    <div class="mb-1">
                      <div class="d-flex justify-content-between">
                        <label class="form-label" for="password">Password</label><a href="auth-forgot-password-cover.html"><small>Forgot Password?</small></a>
                      </div>
                      <div class="input-group input-group-merge form-password-toggle">
                        <input class="form-control form-control-merge" id="password" type="password" name="password" placeholder="········" aria-describedby="password" tabindex="2"/>
                        <span class="input-group-text cursor-pointer">
                        	<i data-feather="eye"></i>
                        </span>
                      	@error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                      </div>
                    </div>
                    <div class="mb-1">
                      <div class="form-check">
                        <input class="form-check-input" id="remember-me" type="checkbox" tabindex="3"/>
                        <label class="form-check-label" for="remember-me"> Remember Me</label>
                      </div>
                    </div>
                    <button class="btn btn-danger w-100" tabindex="4">Sign in</button>
                  </form>
                  </div>
                  <div class="divider my-2">
                    <div class="divider-text">or</div>
                  </div>
                  <div class="auth-footer-btn d-flex justify-content-center">
                    <img  src="{{ asset('_dist/images/playstore.png') }}" height="60px"/>
                  </div>
                </div>
              </div>
              <div class="d-none d-lg-flex col-lg-4 align-items-center">
                <div class="w-100 d-lg-flex align-items-center justify-content-center"><img class="img-fluid" src="{{ asset('_dist/images/img2.png') }}" alt="Login V2"/></div>
              </div>
              <!-- /Login-->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('_dist/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('_dist/js/auth-login.js') }}"></script>
    <script>
      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      });

      $("#login_name").keyup(function(){
        var name = $("#login_name").val()+'@royalx.net';

        $("#email").val(name);
      });


      // Panda Eye move
$(document).on("mousemove", function( event ) {

  var dw = $(document).width() / 15;
  var dh = $(document).height() / 15;
  var x = event.pageX/ dw;
  var y = event.pageY/ dh;
  $('.eye-ball').css({
    width : x,
    height : y
  });

/*
  var div = document.getElementById('move');
      document.addEventListener('mousemove',function(e) {     
        div.style.left = e.pageX+"px";
        div.style.top = e.pageY+"px";
      });
    */
});
    </script>
</body>
</html>