<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Blog List - Vuexy - Bootstrap HTML admin template</title>
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/page-blog.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
  <body class="horizontal-layout horizontal-menu content-detached-right-sidebar navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="content-detached-right-sidebar">
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->

    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
          <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Blog List</h2>
                <div class="breadcrumb-wrapper">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Pages</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Blog</a>
                    </li>
                    <li class="breadcrumb-item active">List
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
              <div class="dropdown">
                <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="content-detached">
          <div class="content-body">
          	<!-- Blog List -->
			<div class="blog-list-wrapper">
			  <!-- Blog List Items -->
			  <div class="row">

			    <div class="col-md-4 col-12">
			      <div class="card">
			        <a href="page-blog-detail.html">
			          <img class="card-img-top img-fluid" src="../../../app-assets/images/slider/02.jpg" alt="Blog Post pic" />
			        </a>
			        <div class="card-body">
			          <h4 class="card-title">
			            <a href="page-blog-detail.html" class="blog-title-truncate text-body-heading"
			              >The Best Features Coming to iOS and Web design</a
			            >
			          </h4>
			          <div class="d-flex">
			            <div class="avatar me-50">
			              <img src="../../../app-assets/images/portrait/small/avatar-s-7.jpg" alt="Avatar" width="24" height="24" />
			            </div>
			            <div class="author-info">
			              <small class="text-muted me-25">by</small>
			              <small><a href="#" class="text-body">Ghani Pradita</a></small>
			              <span class="text-muted ms-50 me-25">|</span>
			              <small class="text-muted">Jan 10, 2020</small>
			            </div>
			          </div>
			          <div class="my-1 py-25">
			            <a href="#">
			              <span class="badge rounded-pill badge-light-info me-50">Quote</span>
			            </a>
			            <a href="#">
			              <span class="badge rounded-pill badge-light-primary">Fashion</span>
			            </a>
			          </div>
			          <p class="card-text blog-content-truncate">
			            Donut fruitcake soufflé apple pie candy canes jujubes croissant chocolate bar ice cream.
			          </p>
			          <hr />
			          <div class="d-flex justify-content-between align-items-center">
			            <a href="page-blog-detail.html#blogComment">
			              <div class="d-flex align-items-center">
			                <i data-feather="message-square" class="font-medium-1 text-body me-50"></i>
			                <span class="text-body fw-bold">76 Comments</span>
			              </div>
			            </a>
			            <a href="page-blog-detail.html" class="fw-bold">Read More</a>
			          </div>
			        </div>
			      </div>
			    </div>

			    <div class="col-md-4 col-12">
			      <div class="card">
			        <a href="page-blog-detail.html">
			          <img class="card-img-top img-fluid" src="../../../app-assets/images/slider/02.jpg" alt="Blog Post pic" />
			        </a>
			        <div class="card-body">
			          <h4 class="card-title">
			            <a href="page-blog-detail.html" class="blog-title-truncate text-body-heading"
			              >The Best Features Coming to iOS and Web design</a
			            >
			          </h4>
			          <div class="d-flex">
			            <div class="avatar me-50">
			              <img src="../../../app-assets/images/portrait/small/avatar-s-7.jpg" alt="Avatar" width="24" height="24" />
			            </div>
			            <div class="author-info">
			              <small class="text-muted me-25">by</small>
			              <small><a href="#" class="text-body">Ghani Pradita</a></small>
			              <span class="text-muted ms-50 me-25">|</span>
			              <small class="text-muted">Jan 10, 2020</small>
			            </div>
			          </div>
			          <div class="my-1 py-25">
			            <a href="#">
			              <span class="badge rounded-pill badge-light-info me-50">Quote</span>
			            </a>
			            <a href="#">
			              <span class="badge rounded-pill badge-light-primary">Fashion</span>
			            </a>
			          </div>
			          <p class="card-text blog-content-truncate">
			            Donut fruitcake soufflé apple pie candy canes jujubes croissant chocolate bar ice cream.
			          </p>
			          <hr />
			          <div class="d-flex justify-content-between align-items-center">
			            <a href="page-blog-detail.html#blogComment">
			              <div class="d-flex align-items-center">
			                <i data-feather="message-square" class="font-medium-1 text-body me-50"></i>
			                <span class="text-body fw-bold">76 Comments</span>
			              </div>
			            </a>
			            <a href="page-blog-detail.html" class="fw-bold">Read More</a>
			          </div>
			        </div>
			      </div>
			    </div>

			  </div>
			  <!--/ Blog List Items -->

			  <!-- Pagination -->
			  <div class="row">
			    <div class="col-12">
			      <nav aria-label="Page navigation">
			        <ul class="pagination justify-content-center mt-2">
			          <li class="page-item prev-item"><a class="page-link" href="#"></a></li>
			          <li class="page-item"><a class="page-link" href="#">1</a></li>
			          <li class="page-item"><a class="page-link" href="#">2</a></li>
			          <li class="page-item"><a class="page-link" href="#">3</a></li>
			          <li class="page-item active" aria-current="page"><a class="page-link" href="#">4</a></li>
			          <li class="page-item"><a class="page-link" href="#">5</a></li>
			          <li class="page-item"><a class="page-link" href="#">6</a></li>
			          <li class="page-item"><a class="page-link" href="#">7</a></li>
			          <li class="page-item next-item"><a class="page-link" href="#"></a></li>
			        </ul>
			      </nav>
			    </div>
			  </div>
			  <!--/ Pagination -->
			</div>
			<!--/ Blog List -->

          </div>
        </div>

        
      </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script>
      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      })
    </script>
  </body>
  <!-- END: Body-->
</html>