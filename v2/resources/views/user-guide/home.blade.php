<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Dashboard</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
        	<div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">User Guide</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item active">User Guide</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                </div>
            </div>
            <div class="content-body">
				<section id="knowledge-base-content">
					<h4 class="card-title text-danger">Documents</h4>
				  	<div class="row kb-search-content-info match-height">
					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="{{ url('user-guide/outbound-process') }}">
						          	<div class="card-body text-center">
							            <h4>မိမိမြို့မှစာထွက် လုပ်ဆောင်မှုများ</h4>
							            <p class="text-body mt-1 mb-0">
							               မိမိမြို့မှ စာထွက်လုပ်ဆောင်ချက်များနှင့်ပတ်သတ်သော
							               အသေးစိပ် လုပ်ဆောင်ချက်များ
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <!-- marketing -->
					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="{{ url('user-guide/inbound-process') }}">
						          	<div class="card-body text-center">
							            <h4>မိမိမြို့သို့စာဝင် လုပ်ဆောင်မှုများ</h4>
							            <p class="text-body mt-1 mb-0">
							              မိမိမြို့သို့ စာဝင်လုပ်ဆောင်ချက်များနှင့်ပတ်သတ်သော
							               အသေးစိပ် လုပ်ဆောင်ချက်များ 
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="{{ url('user-guide/transit-process') }}">
						          	<div class="card-body text-center">
							            <h4>နယ်စာဝင်-နယ်စာထွက် လုပ်ဆောင်မှုများ</h4>
							            <p class="text-body mt-1 mb-0">
							              မိမိမြို့သို့ စာဝင်လုပ်ဆောင်ချက်များနှင့်ပတ်သတ်သော
							               အသေးစိပ် လုပ်ဆောင်ချက်များ  
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="{{ url('user-guide/rejected-process') }}">
						          	<div class="card-body text-center">
							            <h4>Rejectedစာများ အတွက် လုပ်ဆောင်မှုများ</h4>
							            <p class="text-body mt-1 mb-0">
							              Rejected ဖြစ်သွားသောစာများနှင့်ပတ်သတ်သော
							              အသေးစိပ် လုပ်ဆောင်ချက်များ 
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

				  	</div>
				</section>

				<section id="knowledge-base-content">
					<h4 class="card-title text-danger">Video Collections</h4>
				  	<div class="row kb-search-content-info match-height">
					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="#" class="view-clip" alt="https://dl.dropboxusercontent.com/s/xamb04ofnh923vg/01.Outbound%20%28YGN-BTHG%29.mov?dl=0" title="01.Outbound (YGN-BTHG)" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
						          	<div class="card-body text-center">
							            <h4 class="text-left">01.Outbound (YGN-BTHG)</h4>
							            <p class="text-body mt-1 mb-0">
							              <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="#" class="view-clip" alt="https://dl.dropboxusercontent.com/s/w0dyni2f7y4yprz/02.Outbound%20%28YGN-Sorting%29.mov?dl=0" title="02.Outbound (YGN-Sorting)" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
						          	<div class="card-body text-center">
							            <h4 class="text-left">02.Outbound (YGN-Sorting)</h4>
							            <p class="text-body mt-1 mb-0">
							              <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="#" class="view-clip" alt="https://dl.dropboxusercontent.com/s/owagimnkc03kjqd/03.Inbound%20%28MDY-Sorting%29.mov?dl=0" title="03.Inbound (MDY-Sorting)" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
						          	<div class="card-body text-center">
							            <h4 class="text-left">03.Inbound (MDY-Sorting)</h4>
							            <p class="text-body mt-1 mb-0">
							              <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="#" class="view-clip" alt="https://dl.dropboxusercontent.com/s/etkexp2bihldtxp/04.Inbound%20%28MDY-C1%20Office%29.mov?dl=0" title="04.Inbound (MDY-C1 Office)" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
						          	<div class="card-body text-center">
							            <h4 class="text-left">04.Inbound (MDY-C1 Office)</h4>
							            <p class="text-body mt-1 mb-0">
							              <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="#" class="view-clip" alt="https://dl.dropboxusercontent.com/s/kel5xjhi2jl1z46/05.Inbound%20%28MDY-C1%20Office%20%3E%20MDY-COD%29%20Transferred%20%26%20Rejected.mov?dl=0" title="05.Inbound (MDY-C1 Office > MDY-COD) Transferred & Rejected" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
						          	<div class="card-body text-center">
							            <h4 class="text-left">05.Inbound (MDY-C1 Office > MDY-COD) Transferred & Rejected</h4>
							            <p class="text-body mt-1 mb-0">
							              <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="#" class="view-clip" alt="https://dl.dropboxusercontent.com/s/syyeuyez0zid1t3/06.Inbound%20%28Received%20COD%29.mov?dl=0" title="06.Inbound (Received COD)" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
						          	<div class="card-body text-center">
							            <h4 class="text-left">06.Inbound (Received COD)</h4>
							            <p class="text-body mt-1 mb-0">
							              <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="#" class="view-clip" alt="https://dl.dropboxusercontent.com/s/vvcyg9d31dmvfxf/07.Inbound%20%28MDY-Sorting%29%20Transit.mov?dl=0" title="07.Inbound (MDY-Sorting) Transi" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
						          	<div class="card-body text-center">
							            <h4 class="text-left">07.Inbound (MDY-Sorting) Transit</h4>
							            <p class="text-body mt-1 mb-0">
							              <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="#" class="view-clip" alt="https://dl.dropboxusercontent.com/s/aosg3ad6y556p88/08.Inbound%20%28DWI-MDY-TGI%29%20Transit.mov?dl=0" title="08.Inbound (DWI-MDY-TGI) Transit" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
						          	<div class="card-body text-center">
							            <h4 class="text-left">08.Inbound (DWI-MDY-TGI) Transit</h4>
							            <p class="text-body mt-1 mb-0">
							              <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

					    <div class="col-md-4 col-sm-6 col-12 kb-search-content">
					      	<div class="card">
						        <a href="#" class="view-clip" alt="https://dl.dropboxusercontent.com/s/55i0ue5mvbh2tum/Additional%20Features%20%28Cancelled%20and%20Removed%29.mov?dl=0" title="Additional Features (Cancelled and Removed)" data-bs-toggle="modal" data-bs-target="#fullscreenModal">
						          	<div class="card-body text-center">
							            <h4 class="text-left">Additional Features (Cancelled and Removed)</h4>
							            <p class="text-body mt-1 mb-0">
							              <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
							            </p>
						          	</div>
						        </a>
					      	</div>
					    </div>

				  	</div>
				</section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    @include('customizer')
    @include('footer')

    <!-- Modal -->
    <div class="modal fade" id="fullscreenModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title clip-title" id="modalFullTitle">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                	<video id="view-video" controls="controls" width="100%" height="100%" name="Video Name">
					  	
					</video>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>

    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/page-knowledge-base.min.js') }}"></script>
    <script>
      	$(window).on('load',  function(){
        	if (feather) {
          	feather.replace({ width: 14, height: 14 });
        	}
      	});

      	$('body').delegate(".view-clip","click",function () {
      		$('#view-video').attr('src','');
            var title = $(this).attr('title');
            var link = $(this).attr('alt');
            $('.clip-title').text(title);
            $('#view-video').attr('src',link);
            console.log(link);
        });
    </script>
</body>
</html>