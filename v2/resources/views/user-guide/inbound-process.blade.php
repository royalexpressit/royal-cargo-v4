<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - User Guide</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-body">
                <section>
				  	<div class="card">
					    <div class="card-header">
					      	<h4 class="card-title">Inbound Process</h4>
					    </div>
					    <div class="card-body">
					    	<h4>Main City (Sorting ရှိသောမြို့)</h4>
					    	<div class="mb-2">
						      	<h5 class="text-success text-bold mb-1">01.စာထွက်(Outbound)နှင့်စာဝင်(Inbound) လုပ်ဆောင်ရန်</h5>
						      	<div class="row">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/001.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/001.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
						              	<ul class="list-unstyled fs-16 guideline-items">
						              		<li><span class="badge badge-light-danger count-badge">1</span> <code>စာထွက်(outbound)</code> နှင့် <code>စာဝင်(inbound)</code> စတင်လုပ်ဆောင်မည်ဆိုပါက <code>QR Code</code> icon လေးကိုနှိပ်ပါ။</li>
							      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိမြို့မှ <code>စာထွက်(outbound)</code>နှင့် သက်ဆိုင်သော လုပ်ဆောင်ချက်များကို တစ်စုတစ်စည်းတည်း လုပ်ဆောင်နိင်ပါသည်။</li>
							      			<li><span class="badge badge-light-success count-badge">3</span> မိမိမြို့မှ <code>စာဝင်(inbound)</code>နှင့် သက်ဆိုင်သော လုပ်ဆောင်ချက်များကို တစ်စုတစ်စည်းတည်း လုပ်ဆောင်နိင်ပါသည်။</li>
							      		</ul>
							      	</div>
						      	</div>
						    </div>
						    <div class="link-break"></div>
						    <div class="mb-1">
						      	<h5 class="text-success text-bold mb-1">02(1).မိမိမြို့သို့ပိုထားသောစာ စာဝင်(inbound)စာရင်းသွင်းခြင်း (စာရင်းအမှတ်ဖြင့်စာရင်းသွင်းခြင်း)</h5>
					      		<div class="row">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/021.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/021.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
							      		<ul class="list-unstyled fs-16 guideline-items">
							      			<li><span class="badge badge-light-success count-badge">1</span> သက်ဆိုင်ရာမြို့မှ <code>မိမိမြို့သို့</code>ပို့ထားသော <code>စာရင်းအမှတ်(CTF)</code>အား <code>ပေးပို့သည့်မြို့</code>အလိုက် ရှာနိင်ပါသည်။</li>
							      			<li><span class="badge badge-light-success count-badge">2</span> သက်ဆိုင်ရာမြို့မှ <code>မိမိမြို့သို့</code>ပို့ထားသော <code>စာရင်းအမှတ်(CTF)</code>အား <code>စာရင်းအမှတ်</code>ဖြင့်လည်း ရှာနိင်ပါသည်။</li>
							      			<li><span class="badge badge-light-success count-badge">3</span> လွှဲပြောင်းထားသည့် <code>စာရင်း</code>အား <span class="badge bg-success"><i data-feather='truck'></i></span>(button) အားနှိပ်ပြီး ပါဝင်သည့် <code>အထုပ်စာရင်း</code> များကြည့်ပါ။</li>
							      			<li><span class="badge badge-light-success count-badge">4</span> မိမိ စာများလက်ခံမည့် အထုပ်နံပါတ်အား <span class="badge bg-success"><i data-feather='eye'></i></span>(button) အားနှိပ်ပြီး ပါဝင်သည့်<code>စာ</code>များကြည့်ပါ။</li>
							      			<li><span class="badge badge-light-success count-badge">5</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာလက်ခံပြီးကြောင်း စာရင်းသွင်းပေးပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
							      		</ul>
							      		
							      	</div>
						      	</div>
						    </div>
						    <div class="link-break"></div>
						    <div class="mb-1">
						      	<h5 class="text-success text-bold">02(2).မိမိမြို့သို့ စာဝင်စာရင်းသွင်းခြင်း (စာအမှတ်ဖြင့်စာရင်းသွင်းခြင်း)</h5>
					      		<div class="row">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/022.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/022.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
							      		<ul class="list-unstyled fs-16 guideline-items mt-2">
							      			<li><span class="badge badge-light-success count-badge">1</span> စာဝင်လာသည့် <code>မြို့</code> အား ရွေးပါ။ <em class="text-warning ">(Unknown waybill အတွက်သာအသုံးပြုသွားမည်ဖြစ်ပြီး အထုပ်/စာရင်းအမှတ်ဖြင့် ပါလာသော စာဖြစ်ပါက ၎င်းမြို့အား ထည့်စဉ်းစားသွားမည်မဟုတ်ပါ။)</em> </li>
								      		<li><span class="badge badge-light-success count-badge">2</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      		<li><span class="badge badge-light-success count-badge">3</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      	</ul>
							      	</div>
						      	</div>
						    </div>
						    <div class="link-break"></div>
						    <div class="mb-1">
						      	<h5 class="text-success text-bold">03.သက်ဆိုင်ရာရုံး(သို့)ဌာနသို့ပို့ရန် အထုပ်(package)ထုပ်ခြင်း</h5>
						      	<div class="row">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/023.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/023.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
							      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      		<li><span class="badge badge-light-success count-badge">1</span> မိမိမြို့မှ ဝေမည့်စာအား သက်ဆိုင်ရာ<code>ရုံး</code>(သို့)<code>ဌာန</code>သို့ စာများလွှဲပြောင်းပေးရန် ၎င်း <code>button</code> အား နှိပ်ပါ။</li>
								      		<li><span class="badge badge-light-success count-badge">2</span> မိမိစာလွှဲပြောင်းပေးမည့် <code>ရုံး</code>(သို့)<code>ဌာန</code> အား ရွေးပါ။ </li>
								      		<li><span class="badge badge-light-success count-badge">3</span> မိမိလွှဲပြောင်းပေးမည့် <code>အထုပ်အမျိုးအစား</code>အား ရွေးပါ။</li>
								      		<li><span class="badge badge-light-success count-badge">4</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      		<li><span class="badge badge-light-success count-badge">5</span> အထုပ်နှင့်ပတ်သတ်သော <code>မှတ်ချက်</code> ရေးပါ။(မထည့်လည်းရသည်။)</li>
								      		<li><span class="badge badge-light-success count-badge">6</span>  စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      	</ul>
							      	</div>
						      	</div>
						    </div>
						    <div class="link-break"></div>
						    <div class="mb-1">
						      	<h5 class="text-success text-bold">04.အထုပ်အမှတ်အား အထုပ်တွင်ရေးခြင်း</h5>
						      	<div class="row">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/024.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/024.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
							      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      		<li><span class="badge badge-light-success count-badge">1</span> အထုပ်စာရင်းသွင်းပါက system မှ <code>အထုပ်အမှတ်စဉ်(package no)</code>အား ရရှိပါမည်။
								      			ရရှိလာသော <code>အမှတ်စဉ်</code>အား အထုပ်တွင်ကပ်ထားသော <code>label</code> တွင်ရေးပါ။
								      		</li>
								      	</ul>
								      	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		              						<div class="alert-body">
									      		အထုပ်(packages)များအား သက်ဆိုင်ရုံးလို့ လွှဲပြောင်းပါက မိမိလွှဲပြောင်းလိုသောအထုပ်များအားစုပြီး
									      		စာရင်းအမှတ်(CTF)ထုတ်ပြီးမှ လွှဲပြောင်းပေးရပါမည်။ အထုပ်များအား တိုက်ရိုက်လွှဲပြောင်းခြင်းမပြုရန် သတိပြုရပါမည်။
									      	</div>
								      	</div>
							      	</div>
						      	</div>
						    </div>
						    <div class="mb-1">
							      	<h5 class="text-primary text-bold">05.ထုပ်ထားသော အထုပ်(package)ထဲသို့ စာထပ်ထည့်ခြင်း</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/005.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/005.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_branches as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
								      		<p class="mt-2 fs-16">အသစ်စာရင်းသွင်းထားသော စာများအား အထုပ်(package)အသစ် မထုတ်ပဲ ရှိပြီးသား အထုပ်တွင် ထပ်ဖြည့်နိင်ပါသည်။</p>
								      		<ul class="list-unstyled fs-16 guideline-items">
								      			<li><span class="badge badge-light-success count-badge">1</span> အထုပ်မထုတ်ရသေးသော <code>စာ</code>အား <code>အထုပ်အမှတ်ထုတ်ပြီးသားအထုပ်</code> ထဲသို့ထပ်ဖြည့်ရန် ၎င်း<code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-success count-badge">2</span> မိမိစာထပ်ဖြည့်လိုသော <code>အထုပ်အမှတ်</code>အား ရွေးပါ။ </li>
								      			<li><span class="badge badge-light-success count-badge">4</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-success count-badge">4</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
								      			</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
						    <div class="link-break"></div>
						    <div class="mb-1">
						      	<h5 class="text-success text-bold">06.အထုပ်(package)ကို သက်ဆိုင်ရာရုံးသို့ CTFထုတ်ပြီး လွှဲပြောင်းပေးခြင်း</h5>
						      	<div class="row">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/015.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/015.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
							      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-success count-badge">1</span> အထုပ်များအား <code>သက်ဆိုင်ရာရုံး</code>သို့ <code>စာရင်းအမှတ်(CTF)</code>ထုတ်ပြီး လွှဲပြောင်းပေးရန် ၎င်း<code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-success count-badge">2</span> မိမိရုံးမှ သက်ဆိုင်ရာ <code>ရုံး</code>သို့ လွှဲပြောင်းမည့်အထုပ်အား <code>လွှဲပြောင်းမည့်ရုံး</code>အလိုက် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-success count-badge">3</span> မိမိရုံးမှ သက်ဆိုင်ရာ <code>ရုံး</code>သို့ လွှဲပြောင်းမည့်အထုပ်အား <code>အထုပ်အမှတ်</code>ဖြင့် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-success count-badge">4</span> လွှဲပြောင်းမည့်အထုပ်များအား <span class="badge bg-success"><i data-feather='truck'></i></span>(button) အားနှိပ်ပြီး ထည့်ပါ။</li>
								      			<li><span class="badge badge-light-success count-badge">5</span> လွှဲပြောင်းမည့်အထုပ်များအား <code>ရုံးပြန်ပြောင်း</code>လိုပါက <span class="badge bg-warning"><i data-feather='repeat'></i></span>(button) အားနှိပ်ပြီး <code>ရုံးပြန်ရွေး</code>ပြီး ပြောင်းနိုင်ပါသည်။</li>
								      			<li><span class="badge badge-light-success count-badge">6</span> <code>စာရင်း(CTF)</code>ထုတ်ရန် အဆင်သင့်ဖြစ်ပါက <code>CTF ထုတ်မည်</code> (button) နှိပ်ပြီး စာရင်းအမှတ်ထုတ်ပါ။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
								      			</div>
								      		</div>
							      	</div>
						      	</div>
						    </div>
				    	</div>
				  	</div>
				  	<div class="card">
					    <div class="card-body">
					    	<h4>Single City (တစ်ရုံးတည်းရှိသောမြို့)</h4>
					      	<h5 class="text-success text-bold">01(1).မိမိမြို့သို့ စာဝင်စာရင်းသွင်းခြင်း (စာရင်းအမှတ်ဖြင့်စာရင်းသွင်းခြင်း) </h5>
					      	<div class="row mb-1">
						      	<div class="col-xl-4 col-md-6 col-12">
						      		<img src="{{ asset('_dist/images/screenshots/030.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/030.png') }}">
						      	</div>
						      	<div class="col-xl-8 col-md-6 col-12">
					              	<ul class="list-unstyled fs-16">
							      		<li><span class="badge badge-light-success count-badge">1</span> မိမိမြို့သို့ပို့ထားသော မြို့အလိုက် ရှာနိင်ပါသည်။</li>
							      		<li><span class="badge badge-light-success count-badge">2</span> မိမိမြို့သို့ပို့ထားသော စာရင်းအမှတ်(CTF)ဖြင့် ရှာနိင်ပါသည်။</li>
							      		<li><span class="badge badge-light-success count-badge">3</span> စာရင်းအမှတ်ကိုကြည့်ရန်နှိပ်ပါ။စာရင်းအမှတ်နှင့်ပါလာသော အထုပ်များတွေ့ရပါမည်။</li>
							      		<li><span class="badge badge-light-success count-badge">4</span> မိမိစာရင်းသွင်းလိုသော အထုပ်အမှတ်ကိုကြည့်ရန်နှိပ်ပါ။ အထုပ်တွင်ပါလာသော စာများတွေ့ရပါမည်။</li>
							      		<li><span class="badge badge-light-success count-badge">5</span> မိမိစာလက်ခံလုပ်မည့် စာအမှတ်များကို တစ်စောင်ချင်းဖတ်ပြီး စာလက်ခံလုပ်ပေးပါ။</li>
							      	</ul>
							      	<div class="alert alert-success alert-dismissible fade show" role="alert">
              							<div class="alert-body">
							      			တစ်ရုံးတည်းရှိသောမြို့ဖြစ်သောကြောင့် ဤအဆင့်လုပ်ပြီးပါက စာဝင်(inbound) ပြီးဆုံးပါပြီ။
						      			</div>
						      		</div>
						      	</div>
					      	</div>
					      	<div class="link-break"></div>
				      		<h5 class="text-success text-bold">01(2).မိမိမြို့သို့ စာဝင်စာရင်းသွင်းခြင်း (စာအမှတ်ဖြင့်စာရင်းသွင်းခြင်း)</h5>
					      	<div class="row mb-1">
						      	<div class="col-xl-4 col-md-6 col-12">
						      		<img src="{{ asset('_dist/images/screenshots/031.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/031.png') }}">
						      	</div>
						      	<div class="col-xl-8 col-md-6 col-12">
					              	<ul class="list-unstyled fs-16">
							      		<li><span class="badge badge-light-success count-badge">1</span> မိမိစာလက်ခံလုပ်မည့် စာတွင်ပါသော QR(သို့)Barcode အားဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။(manual လည်းရိုက်ထည့်နိင်ပါသည်။)</li>
							      		<li><span class="badge badge-light-success count-badge">2</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
							      	</ul>
							      	<div class="alert alert-success alert-dismissible fade show" role="alert">
              							<div class="alert-body">
							      			တစ်ရုံးတည်းရှိသောမြို့ဖြစ်သောကြောင့် ဤအဆင့်လုပ်ပြီးပါက စာဝင်(inbound) ပြီးဆုံးပါပြီ။
						      			</div>
						      		</div>
						      	</div>
					      	</div>
				    	</div>
				  	</div>
				</section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    @include('customizer')
    @include('footer')

    <div class="modal fade" id="fullscreenModal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-fullscreen" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="modalFullTitle">View Image</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <div class="p-2">
                      	<img src="" id="image-viewer" class="img-fluid rounded border-primary">
                      </div>
                    </div>
                  </div>
                </div>
    </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>

    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/page-knowledge-base.min.js') }}"></script>
    <script>
      	$(window).on('load',  function(){
        	if (feather) {
          	feather.replace({ width: 14, height: 14 });
        	}
      	});

      	//view-img
      	$('body').delegate(".view-img","click",function () {
      		var img = $(this).attr('alt');
      		$("#image-viewer").attr("src", img);
      		console.log(img);
      	});
    </script>
</body>
</html>