<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - User Guide</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
        	<div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Outbound Process</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('user-guide') }}">User Guide</a>
                                    </li>
                                    <li class="breadcrumb-item active">Rejected Process</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section>
                	<div class="sortable">
					  	<div class="card {{ $city_type==1? 'denied':'' }} doc-content" data-index="1">
						    <div class="card-header">
						      	<h4 class="card-title">Outbound Process</h4>
						    </div>
						    <div class="card-body">
						    	@if($city_type==1)
								<div class="card bg-danger text-white">
									<div class="card-body">
										<h4 class="text-white">မိမိမြို့အတွက် အောက်ပါလုပ်ဆောင်ချက်များနှင့် ကိုက်ညီမှုမရှိပါ</h4>
									</div>
								</div>
								@endif
						    	<h4>Main City (Sorting ရှိသောမြို့)</h4>
						    	<div class="mb-2">
							      	<h5 class="text-danger text-bold mb-1">01.စာထွက်(Outbound)နှင့်စာဝင်(Inbound) လုပ်ဆောင်ရန်</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/001.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/001.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
							              	<ul class="list-unstyled fs-16 guideline-items">
							              		<li><span class="badge badge-light-danger count-badge">1</span> <code>စာထွက်(outbound)</code> နှင့် <code>စာဝင်(inbound)</code> စတင်လုပ်ဆောင်မည်ဆိုပါက <code>QR Code</code> icon လေးကိုနှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိမြို့မှ <code>စာထွက်(outbound)</code>နှင့် သက်ဆိုင်သော လုပ်ဆောင်ချက်များကို တစ်စုတစ်စည်းတည်း လုပ်ဆောင်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-success count-badge">3</span> မိမိမြို့မှ <code>စာဝင်(inbound)</code>နှင့် သက်ဆိုင်သော လုပ်ဆောင်ချက်များကို တစ်စုတစ်စည်းတည်း လုပ်ဆောင်နိင်ပါသည်။</li>
								      		</ul>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
							      	<h5 class="text-primary text-bold">02.Cargo ထဲသို့စတင်စာရင်းသွင်းခြင်း</h5>
						      		<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/002.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/002.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
								      			@if($city_type !=1 )
										      		@foreach($only_branches as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
								      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> <code>စာထွက်(outbound)</code>အတွက် ပထမဦးဆုံး စာရင်း စတင်သွင်းရန် ၎င်း <code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> ကောင်တာမှ ရရှိသည့်စာဖြစ်ပါက <code>ကောင်တာ</code>(သို့)စာပို့သမားမှ ရရှိသည့်စာဖြစ်ပါက <code>စာပို့သမား</code>အမည် ရွေးပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">3</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာနှင့်တွဲပါလာသည့် <code>ပစ္စည်းအရေအတွက်</code> ကိုထည့်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">5</span> <code>နေချင်းပို့ဝန်ဆောင်မှု</code>ဖြစ်ပါက အမှန်ခြစ်ပါ။ မဟုတ်ပါက အမှန်ခြစ်စရာမလိုပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">6</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
								      			</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
							      	<h5 class="text-primary text-bold">03.မိမိရုံးမှ သက်ဆိုင်ရာရုံး(သို့)ဌာနသို့ပို့ရန် အထုပ်(package)ထုပ်ခြင်း</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/003.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/003.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_branches as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
								      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> မိမိရုံးမှ<code>စာထွက်(outbound)</code>အတွက်  သက်ဆိုင်ရာ<code>ရုံး</code>(သို့)<code>ဌာန</code>သို့ စာများလွှဲပြောင်းပေးရန် ၎င်း <code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိစာလွှဲပြောင်းပေးမည့် <code>ရုံး</code>(သို့)<code>ဌာန</code> အား ရွေးပါ။ </li>
								      			<li><span class="badge badge-light-primary count-badge">3</span> မိမိလွှဲပြောင်းပေးမည့် <code>အထုပ်အမျိုးအစား</code>အား ရွေးပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">5</span> အထုပ်နှင့်ပတ်သတ်သော <code>မှတ်ချက်</code> ရေးပါ။(မထည့်လည်းရသည်။)</li>
								      			<li><span class="badge badge-light-primary count-badge">6</span>  စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
									      		</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
							      	<h5 class="text-primary text-bold">04.ရရှိလာသော အထုပ်အမှတ်စဉ်အား အထုပ်တွင်ရေးခြင်း</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/004.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/004.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_branches as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
								      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> အထုပ်စာရင်းသွင်းပါက system မှ <code>အထုပ်အမှတ်စဉ်(package no)</code>အား ရရှိပါမည်။
								      				ရရှိလာသော <code>အမှတ်စဉ်</code>အား အထုပ်တွင်ကပ်ထားသော <code>label</code> တွင်ရေးပါ။
								      			</li>
								      		</ul>
								      		<div class="alert alert-danger alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			အထုပ်(packages)များအား သက်ဆိုင်ရုံးလို့ လွှဲပြောင်းပါက မိမိလွှဲပြောင်းလိုသောအထုပ်များအားစုပြီး
									      			စာရင်းအမှတ်(CTF)ထုတ်ပြီးမှ လွှဲပြောင်းပေးရပါမည်။ အထုပ်များအား တိုက်ရိုက်လွှဲပြောင်းခြင်းမပြုရန် သတိပြုရပါမည်။
									      		</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
							      	<h5 class="text-primary text-bold">05.ထုပ်ထားသော အထုပ်(package)ထဲသို့ စာထပ်ထည့်ခြင်း</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/005.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/005.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_branches as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
								      		<p class="mt-2 fs-16">အသစ်စာရင်းသွင်းထားသော စာများအား အထုပ်(package)အသစ် မထုတ်ပဲ ရှိပြီးသား အထုပ်တွင် ထပ်ဖြည့်နိင်ပါသည်။</p>
								      		<ul class="list-unstyled fs-16 guideline-items">
								      			<li><span class="badge badge-light-primary count-badge">1</span> အထုပ်မထုတ်ရသေးသော <code>စာ</code>အား <code>အထုပ်အမှတ်ထုတ်ပြီးသာအထုပ်</code> ထဲသို့ထပ်ဖြည့်ရန် ၎င်း<code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိစာထပ်ဖြည့်လိုသော <code>အထုပ်အမှတ်</code>အား ရွေးပါ။ </li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
								      			</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
							      	<h5 class="text-primary text-bold">06.အထုပ်(package)ကို သက်ဆိုင်ရာရုံးသို့ စာရင်းအမှတ်(CTF) ထုတ်ပြီး လွှဲပြောင်းပေးခြင်း</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/006.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/006.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_branches as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
								      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> အထုပ်များအား <code>သက်ဆိုင်ရာရုံး</code>သို့ <code>စာရင်းအမှတ်(CTF)</code>ထုတ်ပြီး လွှဲပြောင်းပေးရန် ၎င်း<code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိရုံးမှ သက်ဆိုင်ရာ <code>ရုံး</code>သို့ လွှဲပြောင်းမည့်အထုပ်အား <code>လွှဲပြောင်းမည့်ရုံး</code>အလိုက် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">3</span> မိမိရုံးမှ သက်ဆိုင်ရာ <code>ရုံး</code>သို့ လွှဲပြောင်းမည့်အထုပ်အား <code>အထုပ်အမှတ်</code>ဖြင့် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> လွှဲပြောင်းမည့်အထုပ်များအား <span class="badge bg-success"><i data-feather='truck'></i></span>(button) အားနှိပ်ပြီး ထည့်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">5</span> လွှဲပြောင်းမည့်အထုပ်များအား <code>ရုံးပြန်ပြောင်း</code>လိုပါက <span class="badge bg-warning"><i data-feather='repeat'></i></span>(button) အားနှိပ်ပြီး <code>ရုံးပြန်ရွေး</code>ပြီး ပြောင်းနိုင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">6</span> <code>စာရင်း(CTF)</code>ထုတ်ရန် အဆင်သင့်ဖြစ်ပါက <code>CTF ထုတ်မည်</code> (button) နှိပ်ပြီး စာရင်းအမှတ်ထုတ်ပါ။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
								      			</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
								    <h5 class="text-primary text-bold">07.စာရင်းအမှတ်ဖြင့်လွှဲပြောင်းပေးခြင်း</h5>
							      	<div class="row mb-1">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/007.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/007.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_branches as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
							              	<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> <code>စာရင်းအမှတ်(CTF)</code> ထုတ်ပါက system မှ <code>စာရင်းအမှတ်စဉ်(ctf no)</code>အား ရရှိပါမည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> <code>အထုပ်</code>များလွှဲပြောင်းရန်အတွက် ရရှိလာသော <code>စာရင်းအမှတ်စဉ်</code>အား <code>print</code> ထုတ်ပါ။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
								      			</div>
								      		</div>
								      		<div class="alert alert-danger alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			အထုပ်(packages)များအား သက်ဆိုင်ရုံးလို့ လွှဲပြောင်းပါက ၎င်းအထုပ်များနှင့်သက်ဆိုင်သော 
									      			စာရင်းအမှတ်(CTF)နှင့်ပူတွဲ၍ သက်ဆိုင်ရာ ရုံး(သို့)ဌာနသို့ လွှဲပြောင်းပေးရပါမည်။ အထက်ဖော်ပြပါ
									      			အဆင့်များမှ ယခုအဆင့်ထိသည် <strong>ရုံးခွဲများတွင်</strong>သာ အသုံးပြုရမည့် အဆင့်များဖြစ်သည်။ <strong>စာစီဌာန</strong>
									      			ဖြစ်ပါက ဤအဆင့်များအား အသုံးပြုရန်မလိုအပ်ပါ။
									      		</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
								    <h5 class="text-primary text-bold">08(1).မိမိရုံး(သို့)ဌာန သို့ပိုထားသောစာများအား လက်ခံခြင်း(စာရင်းအမှတ်ဖြင့်စာလက်ခံခြင်း)</h5>
							      	<div class="row mb-1">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/008.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/008.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($all_branches as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
							              	<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> သက်ဆိုင်ရာ ရုံးများမှ <code>မိမိရုံးသို့</code> လွှဲပြောင်းထားသော စာများအား လက်ခံရန် ၎င်း <code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> သက်ဆိုင်ရာ ရုံးများမှ <code>မိမိရုံးသို့</code> လွှဲပြောင်းထားသည့် စာရင်းများအား <code>လွှဲပြောင်းထားသည့်ရုံး</code>အလိုက် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">3</span> သက်ဆိုင်ရာ ရုံးများမှ <code>မိမိရုံးသို့</code> လွှဲပြောင်းထားသည့် စာရင်းများအား <code>စာရင်းအမှတ်</code>ဖြင့် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> လွှဲပြောင်းထားသည့် စာရင်းအား <span class="badge bg-success"><i data-feather='eye'></i></span>(button) အားနှိပ်ပြီး ပါဝင်သည့် <code>အထုပ်စာရင်း</code> များကြည့်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">5</span> မိမိ စာများလက်ခံမည့် အထုပ်နံပါတ်အား <span class="badge bg-success"><i data-feather='eye'></i></span>(button) အားနှိပ်ပြီး ပါဝင်သည့်<code>စာ</code>များကြည့်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">6</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာလက်ခံပြီးကြောင်း စာရင်းသွင်းပေးပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      		</ul>
								      		<div class="alert alert-danger alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			<strong>ရုံးခွဲများ အချင်းချင်း</strong> လွှဲပြောင်းထားသော <strong>နေ့ချင်းပို့ဝန်ဆောင်မှု</strong> စာများအား ဤနေရာတွင်လက်ခံရမည်ဖြစ်သည်။<br>
									      			<strong>ရုံးခွဲများမှ စာစီဌာန</strong>သို့လွှဲပြောင်းထားသော စာများအား ဤနေရာတွင်လက်ခံရမည်ဖြစ်သည်။
									      		</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
								    <h5 class="text-primary text-bold">08(2).မိမိရုံး(သို့)ဌာန သို့ပိုထားသောစာများအား လက်ခံခြင်း(စာအမှတ်ဖြင့်စာလက်ခံခြင်း)</h5>
							      	<div class="row mb-1">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/009.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/009.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($all_branches as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
							              	<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
								      			</div>
								      		</div>
								      		<div class="alert alert-danger alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			<strong>ရုံးခွဲများ အချင်းချင်း</strong> လွှဲပြောင်းထားသော <strong>နေ့ချင်းပို့ဝန်ဆောင်မှု</strong> စာများအား ဤနေရာတွင်လက်ခံရမည်ဖြစ်သည်။<br>
									      			<strong>ရုံးခွဲများမှ စာစီဌာန</strong>သို့လွှဲပြောင်းထားသော စာများအား ဤနေရာတွင်လက်ခံရမည်ဖြစ်သည်။
									      		</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
								    <h5 class="text-primary text-bold">09.မိမိဌာနမှ သက်ဆိုင်ရာမြို့များသို့ပို့ရန် အထုပ်(package)ထုပ်ခြင်း</h5>
							      	<div class="row mb-1">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/010.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/010.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_sorting as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
							              	<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> မိမိရုံးမှ<code>စာထွက်(outbound)</code>အတွက်  သက်ဆိုင်ရာ<code>မြို့</code>သို့ စာများလွှဲပြောင်းပေးရန် ၎င်း <code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိစာလွှဲပြောင်းပေးမည့် <code>မြို့</code> အား ရွေးပါ။ </li>
								      			<li><span class="badge badge-light-primary count-badge">3</span> မိမိလွှဲပြောင်းပေးမည့် <code>အထုပ်အမျိုးအစား</code>အား ရွေးပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">5</span> အထုပ်နှင့်ပတ်သတ်သော <code>မှတ်ချက်</code> ရေးပါ။(မထည့်လည်းရသည်။)</li>
								      			<li><span class="badge badge-light-primary count-badge">6</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">7</span> မိမိမြို့ရှိ<code>ရုံး</code>များသို့ ပြန်ပို့မည့်စာများဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး သက်ဆိုရာ ရုံးများသို့ အထက်ပါအဆင့်များအတိုင်း လွှဲပြောင်းနိုင်ပါသည်။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			အထုပ်(packages)များအား သက်ဆိုင်ရုံး(သို့)မြို့များသို့ လွှဲပြောင်းပါက ၎င်းအထုပ်များနှင့်သက်ဆိုင်သော 
									      			စာရင်းအမှတ်(CTF)နှင့်ပူတွဲ၍ သက်ဆိုင်ရာ ရုံး(သို့)မြို့များသို့ လွှဲပြောင်းပေးရပါမည်။ ယခုအဆင့်သည် <strong>စာစီဌာန</strong>တွင်သာ အသုံးပြုရာမည့် အဆင့်ဖြစ်သည်။ <strong>ရုံးခွဲ</strong>များ
									      			ဖြစ်ပါက ဤအဆင့်အား အသုံးပြုရန်မလိုအပ်ပါ။
									      		</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
							      	<h5 class="text-primary text-bold">10.ရရှိလာသော အထုပ်အမှတ်စဉ်အား အထုပ်တွင်ရေးခြင်း</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/011.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/011.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_sorting as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
								      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> အထုပ်စာရင်းသွင်းပါက system မှ <code>အထုပ်အမှတ်စဉ်(package no)</code>အား ရရှိပါမည်။
								      				ရရှိလာသော <code>အမှတ်စဉ်</code>အား အထုပ်တွင်ကပ်ထားသော <code>label</code> တွင်ရေးပါ။
								      			</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			အထုပ်(packages)များအား သက်ဆိုင်ရာ<strong>မြို့</strong>များသို့ လွှဲပြောင်းပါက မိမိလွှဲပြောင်းလိုသောအထုပ်များအားစုပြီး
									      			<strong>စာရင်းအမှတ်(CTF)</strong>ထုတ်ပြီးမှ လွှဲပြောင်းပေးရပါမည်။ အထုပ်များအား <strong>တိုက်ရိုက်လွှဲပြောင်းခြင်းမပြုရန်</strong> သတိပြုရပါမည်။
									      		</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
							      	<h5 class="text-primary text-bold">11.ထုပ်ထားသော အထုပ်(package)ထဲသို့ စာထပ်ထည့်ခြင်း</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/005.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/005.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_sorting as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
								      		<p class="mt-2">အသစ်စာရင်းသွင်းထားသော စာများအား အထုပ်(package)အသစ် မထုတ်ပဲ ရှိပြီးသား အထုပ်တွင် ထပ်ဖြည့်နိင်ပါသည်။</p>
								      		<ul class="list-unstyled fs-16 guideline-items">
								      			<li><span class="badge badge-light-primary count-badge">1</span> အထုပ်မထုတ်ရသေးသော <code>စာ</code>အား <code>အထုပ်အမှတ်ထုတ်ပြီးသာအထုပ်</code> ထဲသို့ထပ်ဖြည့်ရန် ၎င်း<code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိစာထပ်ဖြည့်လိုသော <code>အထုပ်အမှတ်</code>အား ရွေးပါ။ </li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      		</ul>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
							      	<h5 class="text-primary text-bold">12.အထုပ်(package)ကို သက်ဆိုင်ရာမြို့များသို့ စာရင်းအမှတ်(CTF) ထုတ်ပြီး လွှဲပြောင်းပေးခြင်း</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/012.png') }}" class="img-fluid rounded mb-75 border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/012.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_sorting as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
								      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> အထုပ်များအား <code>သက်ဆိုင်ရာမြို့</code>သို့ <code>စာရင်းအမှတ်(CTF)</code>ထုတ်ပြီး လွှဲပြောင်းပေးရန် ၎င်း<code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိရုံးမှ သက်ဆိုင်ရာ <code>မြို့</code>သို့ လွှဲပြောင်းမည့်အထုပ်အား <code>လွှဲပြောင်းမည့်မြို့</code>အလိုက် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">3</span> မိမိရုံးမှ သက်ဆိုင်ရာ <code>မြို့</code>သို့ လွှဲပြောင်းမည့်အထုပ်အား <code>အထုပ်အမှတ်</code>ဖြင့် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> လွှဲပြောင်းမည့်အထုပ်များအား <span class="badge bg-success"><i data-feather='truck'></i></span>(button) အားနှိပ်ပြီး ထည့်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">5</span> လွှဲပြောင်းမည့်အထုပ်များအား <code>မြို့ပြန်ပြောင်း</code>လိုပါက <span class="badge bg-warning"><i data-feather='repeat'></i></span>(button) အားနှိပ်ပြီး <code>မြို့ပြန်ရွေး</code>ပြီး ပြောင်းနိုင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">6</span> <code>စာရင်း(CTF)</code>ထုတ်ရန် အဆင်သင့်ဖြစ်ပါက <code>CTF ထုတ်မည်</code> (button) နှိပ်ပြီး စာရင်းအမှတ်ထုတ်ပါ။</li>
								      		</ul>
								      	</div>
							      	</div>
							    </div>
							    <div class="link-break"></div>
							    <div class="mb-1">
								    <h5 class="text-primary text-bold">13.စာရင်းအမှတ်စဉ်အား printထုတ်ခြင်း</h5>
							      	<div class="row mb-1">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/007.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/007.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type !=1 )
										      		@foreach($only_sorting as $branch)
										      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
										      		@endforeach
										      	@else
										      		မရှိပါ
										      	@endif
								      		</div>
							              	<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> <code>စာရင်းအမှတ်(CTF)</code> ထုတ်ပါက system မှ <code>စာရင်းအမှတ်စဉ်(ctf no)</code>အား ရရှိပါမည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> <code>အထုပ်</code>များလွှဲပြောင်းရန်အတွက် ရရှိလာသော <code>စာရင်းအမှတ်စဉ်</code>အား <code>print</code> ထုတ်ပါ။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			အထုပ်(packages)များအား <strong>သက်ဆိုင်ရာမြို့</strong>သို့ လွှဲပြောင်းပါက ၎င်းအထုပ်များနှင့်သက်ဆိုင်သော 
									      			<strong>စာရင်းအမှတ်(CTF)နှင့်ပူတွဲ</strong>၍ သက်ဆိုင်ရာ <strong>မြို့</strong>သို့ လွှဲပြောင်းပေးရပါမည်။ 
									      			ယခုအဆင့်သည် <strong>စာစီဌာန</strong>တွင်သာ အသုံးပြုရာမည့် အဆင့်ဖြစ်သည်။ <strong>ရုံးခွဲ</strong>များ ဖြစ်ပါက ဤအဆင့်အား အသုံးပြုရန်မလိုအပ်ပါ။
									      		</div>
								      		</div>
								      	</div>
							      	</div>
							    </div>
					    	</div>
					  	</div>

				  	
					  	<div class="card {{ $city_type!=1? 'denied':'' }} doc-content" data-index="2">
						    <div class="card-body">
						    	@if($city_type!=1)
								<div class="card bg-danger text-white">
									<div class="card-body">
										<h4 class="text-white">မိမိမြို့အတွက် အောက်ပါလုပ်ဆောင်ချက်များနှင့် ကိုက်ညီမှုမရှိပါ</h4>
									</div>
								</div>
								@endif
						    	<h4>Single City (တစ်ရုံးတည်းရှိသောမြို့)</h4>
						    	<div class="mb-2">
							      	<h5 class="text-danger text-bold mb-1">01.စာထွက်(Outbound)နှင့်စာဝင်(Inbound) လုပ်ဆောင်ရန်</h5>
							      	<div class="row">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/001.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/001.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
							              	<ul class="list-unstyled fs-16 guideline-items">
							              		<li><span class="badge badge-light-danger count-badge">1</span> <code>စာထွက်(outbound)</code> နှင့် <code>စာဝင်(inbound)</code> စတင်လုပ်ဆောင်မည်ဆိုပါက <code>QR Code</code> icon လေးကိုနှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိမြို့မှ <code>စာထွက်(outbound)</code>နှင့် သက်ဆိုင်သော လုပ်ဆောင်ချက်များကို တစ်စုတစ်စည်းတည်း လုပ်ဆောင်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-success count-badge">3</span> မိမိမြို့မှ <code>စာဝင်(inbound)</code>နှင့် သက်ဆိုင်သော လုပ်ဆောင်ချက်များကို တစ်စုတစ်စည်းတည်း လုပ်ဆောင်နိင်ပါသည်။</li>
								      		</ul>
								      	</div>
							      	</div>
							    </div>
						      	<h5 class="text-primary text-bold">02.Cargo ထဲသို့စတင်စာရင်းသွင်းခြင်း</h5>
						      	<div class="row mb-1">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/013.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/013.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
						              	<div>
						              		<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
								      		@if($city_type ==1 )
									      		@foreach($only_branches as $branch)
									      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
									      		@endforeach
									      	@else
									      		မရှိပါ
									      	@endif
								      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> <code>စာထွက်(outbound)</code>အတွက် ပထမဦးဆုံး စာရင်း စတင်သွင်းရန် ၎င်း <code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> ကောင်တာမှ ရရှိသည့်စာဖြစ်ပါက <code>ကောင်တာ</code>(သို့)စာပို့သမားမှ ရရှိသည့်စာဖြစ်ပါက <code>စာပို့သမား</code>အမည် ရွေးပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">3</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာနှင့်တွဲပါလာသည့် <code>ပစ္စည်းအရေအတွက်</code> ကိုထည့်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">5</span> <code>နေချင်းပို့ဝန်ဆောင်မှု</code>ဖြစ်ပါက အမှန်ခြစ်ပါ။ မဟုတ်ပါက အမှန်ခြစ်စရာမလိုပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">6</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
								      			</div>
								      		</div>
							      	</div>
						      	</div>
						      	<div class="link-break"></div>
					      		<h5 class="text-primary text-bold">03.မိမိရုံးမှ သက်ဆိုင်ရာမြို့များသို့ပို့ရန် အထုပ်(package)ထုပ်ခြင်း</h5>
						      	<div class="row mb-1">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/012.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/012.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
						              	<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
								      		@if($city_type ==1 )
									      		@foreach($only_branches as $branch)
									      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
									      		@endforeach
									      	@else
									      		မရှိပါ
									      	@endif
								      		</div>
								      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> မိမိရုံးမှ<code>စာထွက်(outbound)</code>အတွက်  သက်ဆိုင်ရာ<code>မြို့</code>သို့ စာများလွှဲပြောင်းပေးရန် ၎င်း <code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိစာလွှဲပြောင်းပေးမည့် <code>မြို့</code> အား ရွေးပါ။ </li>
								      			<li><span class="badge badge-light-primary count-badge">3</span> မိမိလွှဲပြောင်းပေးမည့် <code>အထုပ်အမျိုးအစား</code>အား ရွေးပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">5</span> အထုပ်နှင့်ပတ်သတ်သော <code>မှတ်ချက်</code> ရေးပါ။(မထည့်လည်းရသည်။)</li>
								      			<li><span class="badge badge-light-primary count-badge">6</span>  စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      		</ul>

								      	<div class="alert alert-warning alert-dismissible fade show" role="alert">
	              							<div class="alert-body">
								      		ပစ္စည်းပို့မည့်မြို့ လမ်းကြောင်းအား system မှသတ်မှတ်ထားသောကြောင့် မိမိပို့မည့်မြို့ မရှိပါက IT(or)Operation သို့အကြောင်းကြားပေးပါ။
							      			</div>
							      		</div>
							      	</div>
						      	</div>
						      	<div class="link-break"></div>
						      	<h5 class="text-primary text-bold">04.ထုပ်ထားသော အထုပ်(package)ထဲသို့ စာထပ်ထည့်ခြင်း</h5>
						      	<div class="row mb-1">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/005.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/005.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
						              	<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type ==1 )
									      		@foreach($only_branches as $branch)
									      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
									      		@endforeach
									      	@else
									      		မရှိပါ
									      	@endif
								      		</div>
								      		<p class="mt-2">အသစ်စာရင်းသွင်းထားသော စာများအား အထုပ်(package)အသစ် မထုတ်ပဲ ရှိပြီးသား အထုပ်တွင် ထပ်ဖြည့်နိင်ပါသည်။</p>
								      		<ul class="list-unstyled fs-16 guideline-items">
								      			<li><span class="badge badge-light-primary count-badge">1</span> အထုပ်မထုတ်ရသေးသော <code>စာ</code>အား <code>အထုပ်အမှတ်ထုတ်ပြီးသာအထုပ်</code> ထဲသို့ထပ်ဖြည့်ရန် ၎င်း<code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိစာထပ်ဖြည့်လိုသော <code>အထုပ်အမှတ်</code>အား ရွေးပါ။ </li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာတွင်ပါသော <code>QR</code>(သို့)<code>Barcode</code> အား Scan ဖတ်ပြီး စာအမှတ်အား ထည့်ပါ။ စာအမှတ်အား လက်ဖြင့် manual ရိုက်ပြီး enter ခေါက်၍လည်း ထည့်နိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> စာရင်းသွင်းရန် အဆင်သင့်ဖြစ်ပါက ၎င်း <code>button</code>အား နှိပ်ပြီး စာရင်းသွင်းနိုင်ပါသည်။</li>
								      		</ul>
								      		<div class="alert alert-warning alert-dismissible fade show" role="alert">
		              							<div class="alert-body">
									      			* ပြထားသော fields များသည် မဖြစ်မနေ အချက်အလက်များ ဖြည့်သွင်းပေးရပါမည်။
								      			</div>
								      		</div>
							      	</div>
						      	</div>
						      	<div class="link-break"></div>
						      	<h5 class="text-primary text-bold">05.အထုပ်(package)ကို သက်ဆိုင်ရာမြို့သို့ CTFထုတ်ပြီး လွှဲပြောင်းပေးခြင်း</h5>
						      	<div class="row mb-1">
							      	<div class="col-xl-4 col-md-6 col-12">
							      		<img src="{{ asset('_dist/images/screenshots/014.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/014.png') }}">
							      	</div>
							      	<div class="col-xl-8 col-md-6 col-12">
						              	<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type ==1 )
									      		@foreach($only_branches as $branch)
									      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
									      		@endforeach
									      	@else
									      		မရှိပါ
									      	@endif
								      		</div>
								      		<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> အထုပ်များအား <code>သက်ဆိုင်ရာမြို့</code>သို့ <code>စာရင်းအမှတ်(CTF)</code>ထုတ်ပြီး လွှဲပြောင်းပေးရန် ၎င်း<code>button</code> အား နှိပ်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> မိမိရုံးမှ သက်ဆိုင်ရာ <code>မြို့</code>သို့ လွှဲပြောင်းမည့်အထုပ်အား <code>လွှဲပြောင်းမည့်မြို့</code>အလိုက် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">3</span> မိမိရုံးမှ သက်ဆိုင်ရာ <code>မြို့</code>သို့ လွှဲပြောင်းမည့်အထုပ်အား <code>အထုပ်အမှတ်</code>ဖြင့် ရှာနိင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">4</span> လွှဲပြောင်းမည့်အထုပ်များအား <span class="badge bg-success"><i data-feather='truck'></i></span>(button) အားနှိပ်ပြီး ထည့်ပါ။</li>
								      			<li><span class="badge badge-light-primary count-badge">5</span> လွှဲပြောင်းမည့်အထုပ်များအား <code>မြို့ပြန်ပြောင်း</code>လိုပါက <span class="badge bg-warning"><i data-feather='repeat'></i></span>(button) အားနှိပ်ပြီး <code>မြို့ပြန်ရွေး</code>ပြီး ပြောင်းနိုင်ပါသည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">6</span> <code>စာရင်း(CTF)</code>ထုတ်ရန် အဆင်သင့်ဖြစ်ပါက <code>CTF ထုတ်မည်</code> (button) နှိပ်ပြီး စာရင်းအမှတ်ထုတ်ပါ။</li>
								      		</ul>
							      	</div>
						      	</div>
						      	<div class="link-break"></div>
						      	<div class="mb-1">
								    <h5 class="text-primary text-bold">06.စာရင်းအမှတ်စဉ်အား printထုတ်ခြင်း</h5>
							      	<div class="row mb-1">
								      	<div class="col-xl-4 col-md-6 col-12">
								      		<img src="{{ asset('_dist/images/screenshots/007.png') }}" class="img-fluid rounded border-warning view-img" data-bs-toggle="modal" data-bs-target="#fullscreenModal" alt="{{ asset('_dist/images/screenshots/007.png') }}">
								      	</div>
								      	<div class="col-xl-8 col-md-6 col-12">
								      		<div>
								      			<h5>လုပ်ဆောင်ရမည့်ရုံးခွဲများ</h5>
									      		@if($city_type ==1 )
									      		@foreach($only_branches as $branch)
									      			<span class="badge rounded-pill badge-light-primary">{{ $branch->name }}</span>
									      		@endforeach
									      	@else
									      		မရှိပါ
									      	@endif
								      		</div>
							              	<ul class="list-unstyled fs-16 guideline-items mt-2">
								      			<li><span class="badge badge-light-primary count-badge">1</span> <code>စာရင်းအမှတ်(CTF)</code> ထုတ်ပါက system မှ <code>စာရင်းအမှတ်စဉ်(ctf no)</code>အား ရရှိပါမည်။</li>
								      			<li><span class="badge badge-light-primary count-badge">2</span> <code>အထုပ်</code>များလွှဲပြောင်းရန်အတွက် ရရှိလာသော <code>စာရင်းအမှတ်စဉ်</code>အား <code>print</code> ထုတ်ပါ။</li>
								      		</ul>
								      	</div>
							      	</div>
							    </div>
					    	</div>
					  	</div>
					</div>
				</section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <input type="hidden" id="sort_content" value="{{ $city_type }}">
    @include('customizer')
    @include('footer')

    <div class="modal fade" id="fullscreenModal" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-fullscreen" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="modalFullTitle">View Image</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <div class="p-2">
                      	<img src="" id="image-viewer" class="img-fluid rounded border-primary">
                      </div>
                    </div>
                  </div>
                </div>
    </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>

    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/page-knowledge-base.min.js') }}"></script>
    <script>
      	$(window).on('load',  function(){
        	if (feather) {
          	feather.replace({ width: 14, height: 14 });
        	}
      	});

      	//view-img
      	$('body').delegate(".view-img","click",function () {
      		var img = $(this).attr('alt');
      		$("#image-viewer").attr("src", img);
      		console.log(img);
      	});

      	$('.sortable').each(function(){
      		var sort = $("#sort_content").val();

      		if(sort == 1){
      			var $this = $(this);
			    $this.append($this.find('.doc-content').get().sort(function(a, b) {
			        return $(b).data('index') - $(a).data('index');
			    }));
      		}
		});
    </script>
</body>
</html>