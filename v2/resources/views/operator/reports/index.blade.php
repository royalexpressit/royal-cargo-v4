<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Reports</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Reports</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item active text-capitalize"> Reports
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right pull-right">
                        <button type="button" class="btn btn-sm btn-primary re-1" data-bs-toggle="modal" data-bs-target="#cod">
                            <span class="limit-lbl">YGN-COD Report</span>
                        </button>
                        <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#inlineForm">
                            <span class="limit-lbl">Shop.com Report</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">1.Search Outbound CTFs <small>(Branch - Branch)</small></h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <form action="{{ route('report-1') }}" method="POST">
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                                        <input type="text" name="form1_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                    </div>
                                    <div class="mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-label text-primary" for="select2-icons">ထုတ်ထားသည့်ရုံးခွဲ</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form1_1_branch">
                                                    <option value="0">All Branches</option>
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label text-primary" for="select2-icons">လက်ခံမည့်ရုံးခွဲ</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form1_2_branch">
                                                    <option value="0">All Branches</option>
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        @csrf
                                        <button type="submit" class="btn btn-relief-primary waves-effect">ရှာမည်</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">2.Search Outbound Packages <small>(Branch - Branch)</small></h4>
                                </div>
                                <hr>
                               <div class="card-body">
                                    <form action="{{ route('report-2') }}" method="POST">
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                                        <input type="text" name="form2_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                    </div>
                                    <div class="mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-label text-primary" for="select2-icons">ထုတ်ထားသည့်ရုံးခွဲ</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form2_1_branch">
                                                    <option value="0">All Branches</option>
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label text-primary" for="select2-icons">လက်ခံမည့်ရုံးခွဲ</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form2_2_branch">
                                                    <option value="0">All Branches</option>
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        @csrf
                                            <button type="submit" class="btn btn-relief-primary waves-effect">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">3.Search Outbound Waybills</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <form action="{{ route('report-3') }}" method="post">
                                        <div class="mb-1">
                                            <label class="form-label text-primary" for="select2-icons">ရက်စွဲ</label>
                                            <input type="text" name="form3_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label text-primary" for="select2-icons">ရုံးခွဲ</label>
                                            <select data-placeholder="Select a state..." class="select2 form-select" id="form3_1_branch" name="form3_1_branch">
                                                <option value="0">All Branches</option>
                                                @foreach($branches as $b3)
                                                <option value="{{ $b3->id }}">{{ $b3->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-0">
                                            @csrf
                                            <button type="submit" class="btn btn-relief-primary waves-effect">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">4.Search Outbound CTFs <small>(City - City)</small></h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <form action="{{ route('report-4') }}" method="POST">
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                                        <input type="text" name="form4_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                    </div>
                                    <div class="mb-1">
                                        <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">လက်ခံမည့်မြို့</label>
                                            <select data-placeholder="Select a state..." class="select2 form-select" id="form4_city" name="form4_city">
                                                <option value="0">All Cities</option>
                                                @foreach($cities as $c1)
                                                <option value="{{ $c1->id }}">{{ $c1->name }} ({{ $c1->shortcode }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        @csrf
                                        <button type="submit" class="btn btn-relief-primary waves-effect">ရှာမည်</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">5.Search Outbound Packages <small>(City - City)</small></h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <form action="{{ route('report-5') }}" method="POST">
                                        <div class="mb-1">
                                            <label class="form-label text-primary" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                                            <input type="text" name="form5_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                        </div>
                                        <div class="mb-1">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">လက်ခံမည့်မြို့</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" id="form5_city" name="form5_city">
                                                    <option value="0">All Cities</option>
                                                    @foreach($cities as $c1)
                                                    <option value="{{ $c1->id }}">{{ $c1->name }} ({{ $c1->shortcode }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-0">
                                            @csrf
                                            <button type="submit" class="btn btn-relief-primary waves-effect">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">6.Search Outbound Waybills <small>(City - City)</small></h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <form action="{{ route('report-6') }}" method="POST">
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                                        <input type="text" name="form6_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                    </div>
                                    <div class="mb-1">
                                        <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">လက်ခံမည့်မြို့</label>
                                        <select data-placeholder="Select a state..." class="select2 form-select" id="form6_city" name="form6_city">
                                            <option value="0">All Cities</option>
                                            @foreach($cities as $c1)
                                                <option value="{{ $c1->id }}">{{ $c1->name }} ({{ $c1->shortcode }})</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    </div>
                                    <div class="mb-0">
                                        @csrf
                                        <button type="submit" class="btn btn-relief-primary waves-effect">ရှာမည်</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-success">7.Search Inbound CTFs <small>(City - City)</small></h4>
                                </div>
                                <hr>
                               <div class="card-body">
                                    <form action="{{ route('report-7') }}" method="POST">
                                        <div class="mb-1">
                                            <label class="form-label text-success" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ (Date)</label>
                                            <input type="text" name="form7_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label text-success" for="select2-icons">စာထွက်ထားသည့်မြို့ (Origin)</label>
                                            <select data-placeholder="Select a state..." class="select2 form-select" name="form7_city">
                                                <option value="0">All Cities</option>
                                                @foreach($cities as $c1)
                                                <option value="{{ $c1->id }}">{{ $c1->name }} ({{ $c1->shortcode }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-0">
                                            @csrf
                                            <button type="submit" class="btn btn-relief-success waves-effect">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-success">8.Search Inbound CTFs <small>(Branch - Branch)</small></h4>
                                </div>
                                <hr>
                               <div class="card-body">
                                    <form action="{{ route('report-8') }}" method="POST">
                                    <div class="mb-1">
                                        <label class="form-label text-success" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ (CTF Date)</label>
                                        <input type="text" name="form8_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                    </div>
                                    <div class="mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-label text-success" for="select2-icons">ထုတ်ထားသည့်ရုံးခွဲ (From)</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form8_1_branch">
                                                    <option value="0">All Branches</option>
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label text-success" for="select2-icons">လက်ခံမည့်ရုံးခွဲ (To)</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form8_2_branch">
                                                    <option value="0">All Branches</option>
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        @csrf
                                            <button type="submit" class="btn btn-relief-success waves-effect">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-success">9.Search Inbound Packages <small>(City - City)</small></h4>
                                </div>
                                <hr>
                               <div class="card-body">
                                    <form action="{{ route('report-9') }}" method="POST">
                                    <div class="mb-1">
                                        <label class="form-label text-success" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                                        <input type="text" name="form9_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-success" for="select2-icons">စာထွက်ထားသည့်မြို့ (Origin)</label>
                                        <select data-placeholder="Select a state..." class="select2 form-select" name="form9_city">
                                            <option value="0">All Cities</option>
                                            @foreach($cities as $c1)
                                            <option value="{{ $c1->id }}">{{ $c1->name }} ({{ $c1->shortcode }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-0">
                                        @csrf
                                            <button type="submit" class="btn btn-relief-success waves-effect">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-success">10.Search Inbound Packages <small>(Branch - Branch)</small></h4>
                                </div>
                                <hr>
                               <div class="card-body">
                                    <form action="{{ route('report-10') }}" method="POST">
                                    <div class="mb-1">
                                        <label class="form-label text-success" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                                        <input type="text" name="form10_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                    </div>
                                    <div class="mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-label text-success" for="select2-icons">ထုတ်ထားသည့်ရုံးခွဲ (From)</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form10_1_branch">
                                                    <option value="0">All Branches</option>
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label text-success" for="select2-icons">လက်ခံမည့်ရုံးခွဲ (To)</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form10_2_branch">
                                                    <option value="0">All Branches</option>
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        @csrf
                                            <button type="submit" class="btn btn-relief-success waves-effect">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-success">11.Search Inbound Waybills <small>(City - City)</small></h4>
                                </div>
                                <hr>
                               <div class="card-body">
                                    <form action="{{ route('report-11') }}" method="POST">
                                    <div class="mb-1">
                                        <label class="form-label text-success" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ (Date)</label>
                                        <input type="text" name="form11_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-success" for="select2-icons">စာထွက်ထားသည့်မြို့ (Origin)</label>
                                        <select data-placeholder="Select a state..." class="select2 form-select" name="form11_city">
                                            <option value="0">All Cities</option>
                                            @foreach($cities as $c1)
                                            <option value="{{ $c1->id }}">{{ $c1->name }} ({{ $c1->shortcode }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-success" for="select2-icons">စာဝင်အခြေအနေ</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="type_11" value="1" checked/>
                                                    <label class="form-check-label" for="inlineRadio1">မိမိမြို့သို့ တိုက်ရိုက်</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="type_11" value="2"/>
                                                    <label class="form-check-label" for="inlineRadio1">မိမိမြို့မှ တဆင့်</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        @csrf
                                            <button type="submit" class="btn btn-relief-success waves-effect">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        

                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-success">12.Search Inbound Waybills <small>(Branch - Branch)</small></h4>
                                </div>
                                <hr>
                               <div class="card-body">
                                    <form action="{{ route('report-12') }}" method="POST">
                                    <div class="mb-1">
                                        <label class="form-label text-success" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                                        <input type="text" name="form12_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                                    </div>
                                    <div class="mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-label text-success" for="select2-icons">ထုတ်ထားသည့်ရုံးခွဲ (From)</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form12_1_branch">
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-label text-success" for="select2-icons">လက်ခံမည့်ရုံးခွဲ (To)</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" name="form12_2_branch">
                                                    <option value="0">All Branches</option>
                                                    @foreach($branches as $b1)
                                                    <option value="{{ $b1->id }}">{{ $b1->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        @csrf
                                            <button type="submit" class="btn btn-relief-success waves-effect">ရှာမည်</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')

    <!-- Modal -->
    <div class="modal fade text-start" id="inlineForm" tabindex="-1" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Shop.com Report</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('report-shop') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="mb-1">
                            <label class="form-label text-primary" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                            <input type="text" name="shop_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                        </div>
                        <div class="mb-1">
                            <label>အမည် </label>
                            <input type="text" value="Shop.com" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >ရှာမည်</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade text-start" id="cod" tabindex="-1" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">COD Report <small>(Actually Received Waybills)</small></h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('report-shop') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="mb-1">
                            <label class="form-label text-primary" for="select2-icons">ထုတ်ထားသည့်ရက်စွဲ</label>
                            <input type="text" name="shop_date" id="fp-default" class="form-control flatpickr-basic" value="{{ date('Y-m-d') }}" />
                        </div>
                        <div class="mb-1">
                            <label class="form-label text-primary" for="select2-icons">လက်ခံထားသည့်ဌာန</label>
                            <input type="text" value="YGN-COD" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" >ရှာမည်</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>

    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>


    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });

        $(document).ready(function(){
            var url     = $("#url").val();
            var scanned = 0;
            var success = 0;
            var failed  = 0;
            var waybills= [];
            var key     = 0;

            var voice   = 'alert-1.mp3';
            var city    = $("#city").val();
            var _token  = $("#_token").val();

            $("#form").submit(function(event){
                event.preventDefault();  
            });

        }); 
    </script>
</body>
</html>