<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Outbound</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Outbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('reports') }}">Reports</a>
                                    </li>
                                    <li class="breadcrumb-item active text-capitalize"> Report 2</li>
                                </ol>
                                <p>
                                    <span class="text-muted">Date: <span class="badge badge-light-primary me-1">{{ $date }}</span></span>
                                    <span class="text-muted">From: <span class="badge badge-light-primary me-1">{{ branch($branch_1)['name']? branch($branch_1)['name']:'All' }}</span></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                  
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <button type="button" class="btn btn-warning export-csv" data-bs-toggle="modal" data-bs-target="#download" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Change Date">
                            <i data-feather='download'></i> Download
                        </button>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="prev-btn"><i data-feather='skip-back'></i></button>
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="next-btn"><i data-feather='skip-forward'></i></button>
                            <button type="button" class="btn btn-outline-primary waves-effect waves-float waves-light">Records: <span id="to-records">0</span> of <span id="total-records">0</span></button>
                        </div>
                    </div>
                </div>
            </div>
              
            <div class="content-body">
                <div class="row" id="table-head">
                    <div class="col-12">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="table-dark">
                                        <tr>
                                            <th>Created Date</th>
                                            <th>Waybill No</th>
                                            <th>Courier/Counter</th>
                                            <th>From Branch</th>
                                            <th>Current Status</th>
                                            <th>Qty</th>
                                            <th>Same Day</th>
                                            <th>End Point</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="fetched-data">
                                                                            
                                    </tbody>
                                </table>
                                <div class="data-loading text-center m-2">
                                    <h5>Loading ...</h5>
                                    <div class="spinner-grow text-primary me-1" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                    <div class="spinner-grow text-danger me-1" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                    <div class="spinner-grow text-success me-1" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div class="alert alert-grey show-alert font-size-16 mx-2 text-center hide" role="alert">
                                    <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
                                      <div class="alert-body d-flex align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                        <strong>စာများ မရှိသေးပါ။</strong>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="" id="json" value="{{ $json }}">
    <input type="" id="date" value="{{ $date }}">
    <input type="" id="branch_1" value="{{ $branch_1 }}">

    <!-- Modal -->
    <div class="modal fade modal-danger text-start" id="download" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary" id="myModalLabel120">Export CSV</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <textarea id="raw" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary confirmed" data-bs-dismiss="modal">Export</button>
                </div>
            </div>
        </div>
    </div>
    
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/export.js') }}"></script>

    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });

        $(document).ready(function(){
    var json        = $("#json").val();
    var date        = $("#date").val();
    var branch_1    = $("#branch_1").val();
    var params      = date+'&'+branch_1;

    //declared first loaded json data
    var load_json   = url+'/'+json+'/'+params+'&view';
    var print_json  = url+'/'+json+'/'+params+'&print';
    var waybills    = [];
    var item        = 0;
    var _token      = $("#_token").val();
          
    $.ajax({
        url: load_json,
        type: 'GET',
        data: {},
        success: function(data){
            console.log(data);
            if(data.total > 0){
                $(".show-alert").hide();
                $.each( data.data, function( key, value ) {
                    ++item;
                    waybills.push(value.waybill_no);
                    $("#fetched-data").append(
                                '<tr>'
                                    +'<td><span class="text-muted">'+value.created_at+'</span></td>'
                                    +'<td><span class="waybill-no text-danger">'+value.waybill_no+'</span></td>'
                                    +'<td>'+value.courier+'</td>'
                                    +'<td class="h6 text-primary">'+value.branch+'</td>'
                                    +'<td>'+outbound_status(value.action_id)+'</td>'
                                    +'<td class="h6">'+value.qty+'</td>'
                                    +'<td>'+same_day(value.same_day)+'</td>'
                                    +'<td><span class="end-point-'+value.id+'">'+(value.end_point ==null? '---':value.end_point)+'</span></td>'
                                    +'<td>'
                                       +'<a href="'+url+'/waybills/view/'+value.waybill_no+'" class="btn btn-success btn-sm btn-rounded waves-effect waves-light">'+replace_icon('eye')+'</a> '
                                       +'<button class="btn btn-danger btn-sm btn-rounded waves-effect waves-light sync-odoo" data-bs-toggle="modal" data-bs-target="#sync-odoo" value='+value.waybill_no+' id="'+value.id+'">'+replace_icon('map-pin')+'</button>'
                                    +'</td>'
                                +'</tr>'
                            );
                });
                $(".data-loading").hide();

                $("#to-records").text(data.to);
                $("#total-records").text(data.total);
                                    
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }else{
                $(".show-alert").show();
                $(".pagination").hide();
                $(".data-loading").hide();
            }
        }
    });

    $('.pagination-btn').click(function(){
        //clicked url json data
        $(".data-loading").show();
        $("#fetched-data").empty();
        var clicked_url = $(this).val();
                        
        $(this).siblings().removeClass('active')
        $(this).addClass('active');
        $.ajax({
            url: clicked_url,
            type: 'GET',
            data: {},
            success: function(data){
                $.each( data.data, function( key, value ) {
                    ++item;
                    $("#fetched-data").append(
                                '<tr>'
                                    +'<td><span class="text-muted">'+value.created_at+'</span></td>'
                                    +'<td><span class="waybill-no text-danger">'+value.waybill_no+'</span></td>'
                                    +'<td>'+value.courier+'</td>'
                                    +'<td class="h6 text-primary">'+value.branch+'</td>'
                                    +'<td>'+outbound_status(value.action_id)+'</td>'
                                    +'<td class="h6">'+value.qty+'</td>'
                                    +'<td>'+same_day(value.same_day)+'</td>'
                                    +'<td><span class="end-point-'+value.id+'">'+(value.end_point ==null? '---':value.end_point)+'</span></td>'
                                    +'<td>'
                                       +'<a href="'+url+'/waybills/view/'+value.waybill_no+'" class="btn btn-success btn-sm btn-rounded waves-effect waves-light">'+replace_icon('eye')+'</a> '
                                       +'<button class="btn btn-danger btn-sm btn-rounded waves-effect waves-light sync-odoo" data-bs-toggle="modal" data-bs-target="#sync-odoo" value='+value.waybill_no+' id="'+value.id+'">'+replace_icon('map-pin')+'</button>'
                                    +'</td>'
                                +'</tr>'
                            ); 
                });
                
                $(".data-loading").hide();
                $("#to-records").text(data.to);
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }
        });
    });

    $('.export-csv').click(function(){
        $("#raw").val('test');
        
        var params = $('#params').val();
        $.ajax({
            url: print_json,
            type: 'GET',
            data: {

            },
            success: function(data){
                console.log(data);
                if(data != ''){
                    json = JSON.stringify(data);
                    $("#raw").val(json);
                    $('.confirmed').show();
                }else{
                    $('.confirmed').hide();
                }
            }
        });
    });
});
    </script>
</body>
</html>