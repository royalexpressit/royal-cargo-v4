<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Config Routes</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @if(user()->role_id == 1)
        @include('admin.header')
    @else
        @include('operator.header')
    @endif
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Routes</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item active">Routes
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <a href="{{ url('config-routes/create') }}" type="button" class="btn btn-success">
                            Create
                        </a>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-danger waves-effect waves-float waves-light btn-clear" disabled><i data-feather='x'></i></button>
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light" data-bs-toggle="modal" data-bs-target="#transferred" data-bs-toggle="tooltip" data-bs-placement="bottom" ><i data-feather='search'></i></button>
                        </div>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="prev-btn"><i data-feather='skip-back'></i></button>
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="next-btn"><i data-feather='skip-forward'></i></button>
                            <button type="button" class="btn btn-outline-primary waves-effect waves-float waves-light">Records: <span id="to-records">0</span> of <span id="total-records">0</span></button>
                        </div>
                    </div>
                  </div>
            </div>

              <div class="content-body">
                <div class="row" id="table-head">
                  <div class="col-12">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="table-dark">
                                    <tr>
                                        <th>Route ID</th>
                                        <th>Label</th>
                                        <th>Form City</th>
                                        <th>To City</th>
                                        <th>Active</th>
                                    </tr>
                                </thead>
                                <tbody id="fetched-data">
                                                                        
                                </tbody>
                            </table>
                            <div class="data-loading text-center mt-10">
                                <h5>Loading ...</h5>
                                <div class="spinner-grow text-primary me-1" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                                <div class="spinner-grow text-danger me-1" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                                <div class="spinner-grow text-success me-1" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>
                            <div class="alert alert-grey show-alert font-size-16 mx-2 text-center hide" role="alert">
                                    <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
                                      <div class="alert-body d-flex align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                        <strong>လမ်းကြောင်းများ မထည့်ရသေးပါ</strong>
                                      </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')
    <input type="hidden" id="json" value="{{ $json }}">


    <!-- Modal -->
    <div class="modal fade modal-danger text-start" id="transferred" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-warning" id="myModalLabel120">စာထွက်လမ်းကြောင်းရှာရန်</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label class="form-label text-primary" for="select2-icons">မြို့ရွေးပါ</label>
                    <select data-placeholder="Select a branch..." class="select2 form-select" id="from_city_id">
                        @foreach($cities as $key => $city)
                        <option value="{{ $city->id }}" >{{ $city->name.' ('.$city->shortcode.')' }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning search-route" data-bs-dismiss="modal">ပြောင်းမည်</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });
        $(document).ready(function(){
            //$(".data-loading").show();
            var url         = $("#url").val();
            var json        = $("#json").val();

            //declared first loaded json data
            var load_json   = url+'/'+json;
            var waybills    = [];
            var item        = 0;
            var _token      = $("#_token").val();



            
                $.ajax({
                    url: load_json,
                    type: 'GET',
                    data: {},
                    success: function(data){
                        console.log(json);
                        if(data.total > 0){
                            $(".show-alert").hide();
                            $.each( data.data, function( key, value ) {
                                ++item;
                                waybills.push(value.waybill_no);
                                $("#fetched-data").append(
                                    '<tr>'
                                        +'<td>'+value.id+'</td>'
                                        +'<td><span class="badge badge-light-primary">'+value.from_city+'-'+value.to_city+'</span></td>'
                                        +'<td>'+value.from_city+' ('+value.from_city_name+')</td>'
                                        +'<td>'+value.to_city+' ('+value.to_city_name+')</td>'
                                        +'<td>'+route_active(value.active)+'</td>'
                                    +'</tr>'
                                );
                                
                            });
                            $(".data-loading").hide();

                            $("#to-records").text(data.to);
                            $("#total-records").text(data.total);
                                        
                            if(data.prev_page_url === null){
                                $("#prev-btn").attr('disabled',true);
                            }else{
                                $("#prev-btn").attr('disabled',false);
                            }
                            if(data.next_page_url === null){
                                $("#next-btn").attr('disabled',true);
                            }else{
                                $("#next-btn").attr('disabled',false);
                            }
                            $("#prev-btn").val(data.prev_page_url);
                            $("#next-btn").val(data.next_page_url);
                        }else{
                            $(".show-alert").show();
                            $(".pagination").hide();
                            $(".data-loading").hide();
                        }
                    }
                });

            

            $('.pagination-btn').click(function(){
                //clicked url json data
                $(".data-loading").show();
                $("#fetched-data").empty();
                var clicked_url = $(this).val();
                        

                $(this).siblings().removeClass('active')
                $(this).addClass('active');
                $.ajax({
                    url: clicked_url,
                    type: 'GET',
                    data: {},
                    success: function(data){

                        $.each( data.data, function( key, value ) {
                            ++item;
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td>'+value.id+'</td>'
                                    +'<td><span class="badge badge-light-primary">'+value.from_city+'-'+value.to_city+'</span></td>'
                                    +'<td>'+value.from_city+' ('+value.from_city_name+')</td>'
                                    +'<td>'+value.to_city+' ('+value.to_city_name+')</td>'
                                    +'<td>'+route_active(value.active)+'</td>'
                                +'</tr>'
                            );
                        });
                        $(".data-loading").hide();
                                

                        $("#to-records").text(data.to);
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }
                });
            });

            $('body').delegate(".search-route","click",function () {
                from_city_id    = $("#from_city_id").val();
                $(".btn-clear").attr('disabled',false);
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                          
                $.ajax({
                    type: 'get',
                    url: url+'/api/search-routes/'+from_city_id,
                    dataType:'json',
                    data: {},
                    success: function(data) { 
                      console.log(data);
                      $("#fetched-data").empty();
                      if(data.total > 0){
                        $(".show-alert").hide();
                        $.each( data.data, function( key, value ) {
                            ++item;
                            waybills.push(value.waybill_no);
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td>'+value.id+'</td>'
                                    +'<td><span class="badge badge-light-primary">'+value.from_city+'-'+value.to_city+'</span></td>'
                                    +'<td>'+value.from_city+' ('+value.from_city_name+')</td>'
                                    +'<td>'+value.to_city+' ('+value.to_city_name+')</td>'
                                    +'<td>'+route_active(value.active)+'</td>'
                                +'</tr>'
                            );
                            
                        });
                        $(".data-loading").hide();

                        $("#to-records").text(data.to);
                        $("#total-records").text(data.total);
                                    
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }else{
                        $(".show-alert").show();
                        $(".pagination").hide();
                        $(".data-loading").hide();
                    }
                    },
                });
            });

            $('body').delegate(".btn-clear","click",function () {
                $('.btn-clear').attr('disabled',true);

                $.ajax({
                    url: load_json,
                    type: 'GET',
                    data: {},
                    success: function(data){
                        console.log(json);
                        if(data.total > 0){
                            $(".show-alert").hide();
                            $.each( data.data, function( key, value ) {
                                ++item;
                                waybills.push(value.waybill_no);
                                $("#fetched-data").append(
                                    '<tr>'
                                        +'<td>'+value.id+'</td>'
                                        +'<td><span class="badge badge-light-primary">'+value.from_city+'-'+value.to_city+'</span></td>'
                                        +'<td>'+value.from_city+' ('+value.from_city_name+')</td>'
                                        +'<td>'+value.to_city+' ('+value.to_city_name+')</td>'
                                        +'<td>'+route_active(value.active)+'</td>'
                                    +'</tr>'
                                );
                                
                            });
                            $(".data-loading").hide();

                            $("#to-records").text(data.to);
                            $("#total-records").text(data.total);
                                        
                            if(data.prev_page_url === null){
                                $("#prev-btn").attr('disabled',true);
                            }else{
                                $("#prev-btn").attr('disabled',false);
                            }
                            if(data.next_page_url === null){
                                $("#next-btn").attr('disabled',true);
                            }else{
                                $("#next-btn").attr('disabled',false);
                            }
                            $("#prev-btn").val(data.prev_page_url);
                            $("#next-btn").val(data.next_page_url);
                        }else{
                            $(".show-alert").show();
                            $(".pagination").hide();
                            $(".data-loading").hide();
                        }
                    }
                });

            });
        });


    </script>
</body>
</html>