<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Settings</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Profile</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('profile') }}">Profile</a>
                                    </li>
                                    <li class="breadcrumb-item active">Settings</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-6">
                            @if(change_cities(Auth::id()))
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">မြို့ နှင့် ရုံးခွဲပြောင်းရန်</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">မြို့</label>
                                                <select data-placeholder="Select a state..." class="select2-icons form-select form-data" id="city" disabled >
                                                    @foreach(change_cities(Auth::id()) as $city)
                                                    <option value="{{ $city }}" {{ ($city == $user->city_id? 'selected':'') }} data-icon="map-pin">{{ city($city)['shortcode'] }} ({{ city($city)['name'].'-'.city($city)['mm_name'] }})</option>
                                                    @endforeach
                                                 </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">ရုံး</label>
                                                <select data-placeholder="Select a city..." class="form-control select2 form-data" id="branches" disabled>
                                                   
                                                 </select>
                                                 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-0">
                                        <button type="button" class="btn btn-relief-primary waves-effect btn-edit">ပြင်မည်</button>
                                        <button type="button" class="btn btn-relief-success waves-effect hide btn-change">သိမ်းမည်</button>
                                        <button type="button" class="btn btn-relief-danger waves-effect hide btn-cancel">ပိတ်မည်</button>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Reset Password</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="alert alert-danger alert-validation-msg msg-box" role="alert">
                                        <div class="alert-body d-flex align-items-center">
                                            <ul class="unstyled-list">
                                                <li class="character1">✶ အနည်းဆုံး စာလုံးအရေအတွက် ၆ လုံးဖြစ်ရပါမည်။</li>
                                                <li class="character2">✶ စကားဝှက်ရိုက်ထည့်သည့် နေရာ၂ခုလုံး တူရပါမည်။</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">New Password <span class="text-danger">✶</span></label>
                                        <input type="password" id="new_pass" class="form-control" placeholder="New password" />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label text-primary" for="select2-icons">Confirmed Password <span class="text-danger">✶</span></label>
                                        <input type="password" id="confirmed_pass" class="form-control" placeholder="Confirmed password" />
                                    </div>
                                    <div class="mb-0">
                                        <input type="hidden" id="user_id" value="{{ user()->id }}">
                                        <button type="button" class="btn btn-relief-primary waves-effect scan-btn btn-reset disabled">ပြောင်းမည်</button>
                                    </div>
                                    <div class="mb-0">
                                        <small><span class="text-danger">✶</span> is required fields.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="service_point" value="{{ city(user()->city_id)['is_service_point'] }}">
    <input type="hidden" id="data-limit" value="25">

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });

        $(document).ready(function(){
            var temp = [];
                $('.loading-waybill').hide();

                var _token  = $("#_token").val();

                var current_city = $("#user_city_id").val();
                var current_branch = $("#user_branch_id").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
               
                $.ajax({
                    type: 'post',
                    url: url+'/api/fetched-branches',
                    dataType:'json',
                    data: {
                        'city_id' :current_city,
                    },
                    success: function(data) { 
                        $.each(data, function (i, item) {
                            $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                        });
                        $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                    },
                });

                $(".btn-edit").on("click",function search(e) {
                    $('.btn-edit').hide();
                    $('.form-data').prop("disabled", false);
                    $('.btn-change').show();
                    $('.btn-cancel').show();
                });

                $(".btn-close").on("click",function search(e) {
                    $('.msg-box').hide();
                });
                

                $(".btn-cancel").on("click",function search(e) {
                    $('.btn-edit').show();
                    $('.form-data').prop("disabled", true);
                    $('.btn-change').hide();
                    $('.btn-cancel').hide();
                });
                
                $('#city').on("change",function search(e) {
                    var id = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                   
                    $.ajax({
                        type: 'post',
                        url: url+'/api/fetched-branches',
                        dataType:'json',
                        data: {
                            'city_id' :id,
                        },
                        success: function(data) {
                            $('#branches').empty(); 
                            $.each(data, function (i, item) {
                                $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                            })

                            $('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
                        },
                    });
                    
                });

                $(".change-password").on("click",function search(e) {
                    var old_pass = $("#old").val();
                    var new_pass = $("#new").val();
                    

                    if(old_pass == '' && new_pass == ''){
                        $('.msg-box').hide();
                        $('.warning-alert').show();
                    }else{
                        $('.msg-box').hide();
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type: 'post',
                            url: url+'/changed-password',
                            dataType:'json',
                            data: {
                                'old_pass' :old_pass,
                                'new_pass' :new_pass
                            },
                            success: function(data) {
                                if(data.success == 1){
                                    $('.success-alert').show();
                                }else{
                                    $('.failed-alert').show();
                                }
                                $("#old").val('');
                                $("#new").val('')
                            },
                        }); 
                    }
                });

                $(".btn-change").on("click",function search(e) {
                    var user_id     = $("#user_id").val();
                    var city_id     = $("#city").val();
                    var branch_id   = $("#branches").val();
                
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                   
                    $.ajax({
                        type: 'post',
                        url: url+'/api/change-city-and-branch',
                        dataType:'json',
                        data: {
                            'user_id'   :user_id,
                            'city_id'   :city_id,
                            'branch_id' :branch_id,
                        },
                        success: function(data) { 
                            if(data.success == 1){
                                var branch  = $("#branches option:selected").text();
                                var city    = $("#city option:selected").text();

                                $('.change-alert').show();
                                $('#current-branch').text(branch);
                                $('.btn-edit').show();
                                $('.form-data').prop("disabled", true);
                                $('.btn-change').hide();
                                $('.btn-cancel').hide();

                                Swal.fire({
                                    position:"top-end",
                                    title:"အသိပေးခြင်း",
                                    icon:"success",
                                    text:"သင့် လုပ်ဆောင်ရမည့် ရုံးအား "+branch+' သို့ပြောင်းလိုက်ပါပြီ',
                                    showConfirmButton:!1,
                                    timer:3000,
                                    customClass:{confirmButton:"btn btn-primary"},
                                    buttonsStyling:!1
                                });
                            }else{
                                $('.error-alert').show();
                            }
                        },
                    });
                });

            $("#new_pass,#confirmed_pass").on("keyup",function search(e){
            $new       = $("#new_pass").val().length;
            $confirmed = $("#confirmed_pass").val().length;
            //.msg-box,

            if($new >= 6 && $confirmed >= 6){
                if($("#new_pass").val() == $("#confirmed_pass").val()){
                    $(".btn-reset").removeClass('disabled'); 
                    $('.confirmed').hide();
                    $('.msg-box').hide();
                }else{
                    $('.msg-box').show();
                    $(".btn-reset").addClass('disabled');
                    $('.confirmed').show();
                    $('.character1,.character2').show();
                }
            }else if($new >= 6 && $confirmed < 6){
                $('.msg-box').show();
                $('.character1').hide();
                $('.character2').show();
            }else if($new < 6 && $confirmed >= 6){
                $('.msg-box').show();
                $('.character1').hide();
                $('.character2').show();
            }else{
                $('.msg-box').show();
                $('.character1,.character2').show();
                $(".btn-reset").addClass('disabled');
            }
        });

            $(".btn-reset").on("click",function search(e) {
                $(".data-loading").show();
                $(".user-form").hide();

                new_pass        = $('#new_pass').val();
                confirmed_pass  = $("#confirmed_pass").val();
                user_id         = $('#user_id').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                   
                $.ajax({
                    type: 'post',
                    url: url+'/api/users/reset-password',
                    dataType:'json',
                    data: {
                        'new_pass'      :new_pass,
                        'confirmed_pass':confirmed_pass,
                        'user_id'       :user_id
                    },
                    success: function(data) { 
                        if(data.success == 1){
                            Swal.fire({
                                position:"top-end",
                                title:"အသိပေးခြင်း",
                                icon:"success",
                                html:"သင့်စကားဝှက်အသစ် ပြောင်းလည်းလိုက်ပါပြီ",
                                showConfirmButton:!1,
                                timer:3000,
                                customClass:{confirmButton:"btn btn-primary"},
                                buttonsStyling:!1
                            });

                            $('#new_pass').val('');
                            $("#confirmed_pass").val('');
                            $(".btn-reset").addClass('disabled');
                        }
                    },
                });
            });
        });
    </script>
</body>
</html>