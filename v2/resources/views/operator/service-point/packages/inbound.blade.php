<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Inbound Packages</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      	<div class="col-12">
                        	<h3 class="content-header-title float-start mb-0 text-capitalize"><span id="status-lbl">{{ $status }}</span> Packages</h3>
                        	<div class="breadcrumb-wrapper">
	                          	<ol class="breadcrumb">
		                            <li class="breadcrumb-item">
		                            	<a href="{{ url('') }}">Dashboard</a>
		                            </li>
		                            <li class="breadcrumb-item">
		                            	<a href="{{ url('inbound') }}">Inbound</a>
		                            </li>
		                            <li class="breadcrumb-item active">Packages By Cities</li>
	                          	</ol>
	                        </div>
                      	</div>
                    </div>
                </div>

                <div class="text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="btn-group">
              			<button type="button" class="btn btn-primary">Filterd By:</button>
		              	<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
	                		<span class="visually-hidden">Toggle Dropdown</span>
	              		</button>
              			<div class="dropdown-menu dropdown-menu-end">
                			<button class="dropdown-item btn-filtered w-100" value="all">All Packages</button>
                			<button class="dropdown-item btn-filtered w-100" value="shipping">Shipping Packages</button>
                			<button class="dropdown-item btn-filtered w-100" value="arrived">Arrived Packages</button>
              			</div>
            		</div>
                </div>
            </div>

            <div class="content-body">
			    <div class="col-lg-12 col-12">
			      	<div class="card card-company-table">
				        <div class="card-body p-0">
				          	<div class="table-responsive">
					            <table class="table">
					              	<thead>
						                <tr>
						                  	<th>Form City</th>
						                  	<th>To City</th>
						                  	<th>Package (Qty)</th>
						                  	<th>Filtered</th>
						                  	<th>View</th>
						                </tr>
					              	</thead>
					              	<tbody id="fetched-data">
					              		
					              	</tbody>
					            </table>

					            <div class="data-loading text-center mt-10">
						            <h5>Loading ...</h5>
						            <div class="spinner-grow text-primary me-1" role="status">
						                <span class="visually-hidden">Loading...</span>
						            </div>
						            <div class="spinner-grow text-danger me-1" role="status">
						                <span class="visually-hidden">Loading...</span>
						            </div>
						            <div class="spinner-grow text-success me-1" role="status">
						                <span class="visually-hidden">Loading...</span>
						            </div>
						        </div>

						        <div class="alert alert-grey show-alert font-size-16 m-2 text-center hide" role="alert">
                                    <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
                                        <div class="alert-body d-flex align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                            <strong>အထုပ်စာရင်းများ မရှိသေးပါ။</strong>
                                        </div>
                                    </div>
                                </div>
				          	</div>
				        </div>
			      	</div>
			    </div>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="json" value="{{ $json }}">
    <input type="hidden" id="status" value="{{ $status }}">
    <input type="hidden" id="city" value="{{ city(user()->city_id)['name'] }}">


    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>

    <script>
      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      });
      $(document).ready(function(){
            var json        = $("#json").val();
            var status        = $("#status").val();
            var city_shortcode = $("#city").val();

            //declared first loaded json data
            var load_json   = url+'/'+json+'/'+status;
            var waybills    = [];
            var item        = 0;
            var _token      = $("#_token").val();
            
            fetched_package(status);

            $('.btn-filtered').click(function(){
                var filtered = $(this).attr('value');
                $(".data-loading").show();
                $("#fetched-data").empty();
                fetched_package(filtered);
            });

            function fetched_package(filtered){
                load_json = url+'/'+json+'/'+filtered;

                $("#status-lbl").text(filtered);
                $.ajax({
	                url: load_json,
	                type: 'GET',
	                data: {},
	                success: function(data){
	                    console.log(json);
	                    if(data.total > 0){
	                    	$(".show-alert").addClass('hide');
	                        $.each( data.data, function( key, value ) {
	                            ++item;
	                            waybills.push(value.waybill_no);
	                            $("#fetched-data").append(
	                                '<tr>'
	                                  	+'<td>'
		                                    +'<div class="d-flex align-items-center">'
		                                      	+'<div>'
		                                        	+'<div class="fw-bolder">'+value.from_city_name+'</div>'
		                                      	+'</div>'
		                                    +'</div>'
	                                  	+'</td>'
					                  	+'<td>'
					                    	+'<div class="d-flex align-items-center">'
						                      	+'<div class="avatar rounded bg-white">'
							                        +'<div class="avatar-content">'
							                          	+'<img src="'+url+'/_dist/images/logo.png" class="w-100" alt="Toolbar svg" />'
							                        +'</div>'
						                      	+'</div>'
						                      	+'<div>'
						                        	+'<div class="fw-bolder">'+value.to_city_name+'</div>'
						                        	+'<div class="font-small-2 text-danger">'+city_shortcode+'</div>'
						                      	+'</div>'
					                    	+'</div>'
					                  	+'</td>'
					                  	+'<td class="text-nowrap">'
					                    	+'<div class="d-flex flex-column">'
					                      		+'<span class="fw-bolder mb-25">'+value.count+'</span>'
					                    	+'</div>'
					                  	+'</td>'
					                  	+'<td class="text-nowrap">'
						                    +'<div class="d-flex flex-column">'
						                      	+'<span class="fw-bolder mb-25 text-capitalize"><span class="badge bg-'+filtered+'">'+filtered+'</span></span>'
						                    +'</div>'
					                  	+'</td>'
					                  	+'<td><a href="'+url+'/inbound/package/view/'+value.to_branch_id+'" class="btn btn-relief-primary waves-effect btn-sm">View</a></td>'
					                +'</tr>'
	                            ); 
	                        });
	                        $(".data-loading").hide();

	                        $("#to-records").text(data.to);
	                        $("#total-records").text(data.total);
	                                    
	                        if(data.prev_page_url === null){
	                            $("#prev-btn").attr('disabled',true);
	                        }else{
	                            $("#prev-btn").attr('disabled',false);
	                        }
	                        if(data.next_page_url === null){
	                            $("#next-btn").attr('disabled',true);
	                        }else{
	                            $("#next-btn").attr('disabled',false);
	                        }
	                        $("#prev-btn").val(data.prev_page_url);
	                        $("#next-btn").val(data.next_page_url);
	                    }else{
	                        $(".show-alert").removeClass('hide');
	                        $(".pagination").hide();
	                        $(".data-loading").hide();
	                    }
	                }
            	});
            }
        });
    </script>
</body>
</html>