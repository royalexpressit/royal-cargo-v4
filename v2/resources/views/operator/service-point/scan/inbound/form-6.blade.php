<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Inbound Actions</title>
    <!-- form 6 -->
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
 <body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Inbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('inbound') }}">Inbound</a>
                                    </li>
                                    <li class="breadcrumb-item active">Scan</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <button type="button" class="btn btn-sm btn-primary dropdown-toggle pull-right" 
                            data-bs-toggle="dropdown" aria-expanded="false" >
                            <span class="limit-lbl">Setting Limit: 20</span>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item btn-limit" href="#" alt="10">10 Waybills</a>
                            <a class="dropdown-item btn-limit" href="#" alt="20">20 Waybills</a>
                            <a class="dropdown-item btn-limit" href="#" alt="30">30 Waybills</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">စာဝင်လုပ်ဆောင်မှုများ</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                    <div class="mb-half">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=1') }}" class="btn btn-wide btn-relief-success waves-effect">1.စာရင်းအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=2') }}" class="btn btn-wide btn-relief-success waves-effect">2.စာအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                    <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=3&type=branch') }}" class="btn btn-wide btn-relief-success waves-effect">3.ရုံးခွဲ(သို့)နယ်မြို့သို့ <strong class="block">အထုပ်ပို့မည်</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=4') }}" class="btn btn-wide btn-relief-secondary waves-effect">4.ရှိပြီးသား အထုပ်ထဲ<strong class="block">စာထပ်ထည့်မည်</strong></a>
                                            </div>
                                          </div>
                                        </div>
                                    <div class="mb-half">
                                        <a href="{{ url('scan/inbound?action=3&type=rejected') }}" class="btn btn-wide btn-relief-danger waves-effect disabled">5.ငြင်ပယ်ထားသော စာများအား <strong class="block">COD ဌာန သို့ပြန်ပို့မည်</strong></a>
                                    </div>
                                    <div class="mb-half">
                                        <a href="{{ url('scan/inbound?action=5') }}" class="btn btn-wide btn-relief-success waves-effect">6.ထုပ်ထားသော အထုပ်များအား <strong class="block">စာရင်းဖြင့်ပို့မည်</strong></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title text-primary">
                                  ရုံးခွဲများသို့ပို့မည့်ပစ္စည်းထုပ်ရန်
                                
                              </h4>
                            </div>
                            <hr>
                            <div class="card-body">
                                <div class="mb-1">
                                    <label class="form-label text-primary w-100" for="select2-icons">အထုပ်ပို့မည့်ရုံးခွဲ <span class="text-danger">✶</span><span class="text-warning pull-right" data-bs-toggle="popover" data-bs-content="မိမိ ပြည်နယ်/တိုင်း အပေါ်တွင်မူတည်၍ systemမှ လွှဲပြောင်းရမည့် ဌာနအား အလိုအလျောက်ရွေးပေးထားပါသည်။" data-bs-trigger="hover" title="" data-bs-original-title="လွှဲပြောင်းရမည့် ဌာန">
                                          <i data-feather='help-circle'></i>
                                        </span>
                                    </label>
                                    <select data-placeholder="ရုံးရွေးပါ..." class="select2-icons form-select" id="to_branch">
                                        @foreach($branches as $branch)
                                        <option value="{{ $branch->id }}" data-icon="home">{{ $branch->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-1">
                                    <label class="form-label text-primary" for="select2-icons">အထုပ်အမျိုးအစား <span class="text-danger">✶</span></label>
                                    <select data-placeholder="Select a state..." class="select2-icons form-select" id="type">
                                        @foreach($types as $type)
                                        <option value="{{ $type->id }}" data-icon="{{ $type->icon }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-1">
                                    <label class="form-label text-primary" for="select2-icons">စာအမှတ် <span class="text-danger">✶</span></label>
                                    <input type="text" id="waybill" class="form-control" placeholder="Z1234566789" />
                                </div>
                                <div class="mb-1">
                                    <div class="continued-pkg hide">
                                        <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
                                          <div class="alert-body d-flex align-items-center">
                                            <span>Added waybills into current package</span>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="remark">
                                       <label class="form-label text-primary" for="select2-icons">မှတ်ချက်</label>
                                       <textarea class="form-control" id="remark" rows="3" placeholder="Something..."></textarea>
                                   </div>
                                </div>
                                <div class="mb-0">
                                    <div class="row">
                                        <input type="hidden" id="action" value="save">
                                        <input type="hidden" id="package_id" value="0">
                                        <input type="hidden" id="package_no" value="0">
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-relief-success waves-effect save-btn w-100 disabled" value="save">အထုပ်ပိတ်မည်</button>
                                        </div> 
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-relief-warning waves-effect continue-btn w-100 disabled" value="continue">ပစ္စည်းထပ်ထည့်မည်</button>
                                        </div>              
                                    </div>
                                    
                                    
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">
                                        <span class="text-primary">Scanned</span> <span id="scanned" class="badge badge-light-primary me-1 count-badge">0</span> 
                                        <span class="text-success">Success</span> <span id="success" class="badge badge-light-success me-1 count-badge">0</span> 
                                        <span class="text-danger">Failed</span> <span id="failed" class="badge badge-light-danger count-badge">0</span>
                                    </h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="alert alert-warning alert-validation-msg get-started" role="alert">
                                        <div class="alert-body d-flex align-items-center">
                                            <i data-feather='alert-circle'></i>&nbsp;
                                            <span>Scan ဖတ်ထားသောစာများ မရှိသေးပါ။</span>
                                        </div>
                                    </div>
                                    <div class="show-package hide">
                                        <div class="row">
                                            <div class="col-md-3"><img class="mt-6p bg-light-secondary border-rounded" src="{{ asset('_dist/images/label.png') }}" style="height:88px;text-align: center;display: block;padding:0px 4px"></div>
                                            <div class="col-md-9">
                                                <div class="card shadow-none bg-transparent border-success mt-6p success-pkg">
                                                    <div class="px-1 pt-1">
                                                        <span class="text-success">Package No:(အထုပ်တွင်ရေးပါ)</span>
                                                        <h2 id="pkg_no" class="fs-30 text-success">000000</h2>
                                                    </div>
                                                </div>
                                                <div class="card shadow-none bg-transparent border-danger mt-6p error-pkg">
                                                    <div class="px-1 pt-1">
                                                        <span class="text-danger">Package No:(နံပါတ်မရရှိပါ)</span>
                                                        <h2 id="pkg_no" class="fs-30 text-danger">▧▧▧▧▧▧</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <ul class="list-group" id="scanned-lists">
                                                                              
                                    </ul>
                                    <ul class="list-group" id="failed-lists">
                                                                                                            
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')
    <input type="hidden" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="cod_id" value="{{ fetched_reject_branch(user()->city_id)->id }}">
    
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/bundles/service-point/s-034302.js') }}"></script>
    <script src="{{ asset('/_dist/js/components-popovers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>   
</body>
</html>