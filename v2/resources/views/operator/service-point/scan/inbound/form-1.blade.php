<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Inbound Actions</title>
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Inbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('inbound') }}">Inbound</a>
                                    </li>
                                    <li class="breadcrumb-item active">Scan</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <a href="{{ url('_dist/logs/inbound-branch-in-failed.txt') }}" class="btn btn-sm btn-warning" download="inbound-branch-in-failed-{{ date('ymd') }}.txt">
                            <i data-feather='download'></i> Failed Logs
                        </a>&nbsp;
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">စာဝင်လုပ်ဆောင်မှုများ</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                    <div class="mb-half">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=1') }}" class="btn btn-wide btn-relief-success waves-effect disabled">1.စာရင်းအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=2') }}" class="btn btn-wide btn-relief-success waves-effect">2.စာအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                    <div class="mb-half">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=3&type=branch') }}" class="btn btn-wide btn-relief-success waves-effect">3.ရုံးခွဲ(သို့)နယ်မြို့သို့ <strong class="block">အထုပ်ပို့မည်</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=4') }}" class="btn btn-wide btn-relief-secondary waves-effect">4.ရှိပြီးသား အထုပ်ထဲ<strong class="block">စာထပ်ထည့်မည်</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-half">
                                        <a href="{{ url('scan/inbound?action=3&type=rejected') }}" class="btn btn-wide btn-relief-danger waves-effect">5.ငြင်ပယ်ထားသော စာများအား <strong class="block">COD ဌာန သို့ပြန်ပို့မည်</strong></a>
                                    </div>
                                    <div class="mb-half">
                                        <a href="{{ url('scan/inbound?action=5') }}" class="btn btn-wide btn-relief-success waves-effect">6.ထုပ်ထားသော အထုပ်များအား <strong class="block">စာရင်းဖြင့်ပို့မည်</strong></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">မိမိရုံးသို့ဝင်လာသော CTF များ</h4>
                                    <button type="button" class="btn btn-relief-success waves-effect show-ctf btn-sm">Show/Hide CTFs</button>
                                </div>
                                <hr>
                                <div class="card-body ctf-items">
                                    @if(!$ctfs->isEmpty())
                                    <?php 
                                        $attrs = [];
                                        foreach ($ctfs as $key => $value) {
                                            $attrs[$value->from_city_name][] = $value->value;
                                        }
                                    ?>
                                    <!-- Icons -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">မြို့အလိုက် ကြည့်ရန်</label>
                                                <select data-placeholder="Select a state..." class="select2-icons form-select" id="filtered">
                                                    <option value="1" data-icon="search">မြို့အားလုံး</option>
                                                    @foreach ($attrs as $key => $c)
                                                    <option value="1" data-icon="map-pin">{{ $key }}</option>
                                                    @endforeach
                                                </select>
                                            </div> 
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">စာအရင်းအမှတ်ဖြင့် ရှာရန်</label>
                                                <input type="text"  class="form-control" id="search-ctf" placeholder="AA123456" />
                                            </div>
                                        </div>
                                    </div>
                                        
    								<div class="card card-transaction mxh-400">
    									<span class="form-label text-primary">မိမိမြို့သို့ပို့ထားသော စာရင်းများ</span>
    									@foreach($ctfs as $ctf)
    									<div class="transaction-item ctf-item list-item ctf-item-{{ $ctf->id }} {{ $ctf->from_city_name }} border-warning">
    										<div class="drag-item-{{ $ctf->id }} w-100">
    											<div class="">
                                                        <div class="transaction-percentage ">
                                                            <h6 class="transaction-title font-medium-5 m-0 text-{{ $ctf->rejected==1? 'danger':'warning' }}">
                                                                {{ $ctf->ctf_no }}
                                                                <span class="font-small-3 pull-right">
                                                                    <span class="text-muted">ရက်စွဲ :</span> {{ $ctf->created_at }}
                                                                </span>
                                                            </h6>
                                                            <div class="text-primary ">
                                                                <span class="text-muted">မြို့မှ </span>  <span class="badge badge-light-warning">{{ $ctf->from_city_name }}</span> ,
                                                                <span class="text-muted">အထုပ် </span>  <span class="badge badge-light-warning">{{ $ctf->total }}</span> <span class="text-muted">ထုပ်</span> 
                                                                <div class="pull-right font-small-4 m--t-4">
                                                                    @if($ctf->rejected==1)
                                                                    <span data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="ပြန်ပြောင်းမည်"><button type="button" class="btn btn-sm btn-warning waves-effect waves-float waves-light select-ctf" data-bs-toggle="modal" data-bs-target="#transferred" data-bs-toggle="tooltip" data-bs-placement="bottom" value="{{ $ctf->id }}" ><i data-feather='repeat'></i></button></span>
                                                                    @endif
                                                                    <button type="button" class="btn btn-sm btn-success waves-effect waves-float waves-light show-pkg ctf-{{ $ctf->id }}" id="{{ $ctf->id }}"><i data-feather='eye'></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
    										</div>
    									</div>
    									@endforeach
          							</div>
                                    @else
                                    <div class="demo-spacing-0">
                                        <div class="alert alert-warning alert-validation-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                <span>No CTFs found.</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="card package-block hide">
                                <div class="card-header p--8">
                                    <h4 class="card-title text-primary w-100">
                                        <span class="lh--40">Included Packages</span>
                                        <input type="text" class="form-control inline-search pull-right" id="search-pkg" placeholder="Search Package..." />
                                    </h4>
                                </div>
                                <hr>
                                <div class="card-body">
									<div class="card card-transaction mxh-400">
										<div id="fetched-packages">
															         	
										</div>
      								</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Listed Waybills</h4>
                                    <span class="btn btn-outline-danger btn-sm waves-effect right">Qty: <span id="count">0</span></span>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="card card-transaction">
                                        <div class="mb-1 waybill-form hide">
                                            <label class="form-label text-primary" for="select2-icons">Scan Waybill No</label>
                                            <input type="text" id="waybill" class="form-control" placeholder="Z12345678" />
                                        </div>
                                        <ul class="list-group" id="fetched-package-waybills">
                                            
                                        </ul>
                                        <div class="mt-1">
                                            <textarea class="form-control hide" id="multi_scanned_waybills"></textarea>
                                            <button type="button" class="btn btn-relief-primary waves-effect ctf-btn hide">Export CTF</button>                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    @include('customizer')
    @include('footer')
    <input type="hidden" id="selected_package">
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="row" value="{{ $row }}">

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>

    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/bundles/service-point/s-427595.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
</body>
</html>