<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - View Inbound Package</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      	<div class="col-12">
                        	<h3 class="content-header-title float-start mb-0 text-capitalize">Packages</h3>
                        	<div class="breadcrumb-wrapper">
	                          	<ol class="breadcrumb">
		                            <li class="breadcrumb-item">
		                            	<a href="{{ url('') }}">Dashboard</a>
		                            </li>
		                            <li class="breadcrumb-item">
		                            	<a href="{{ url('outbound/package/all') }}">Packages</a>
		                            </li>
		                            <li class="breadcrumb-item active">
		                            	View Branch: <strong class="text-primary">{{ $branch['name'] }}</strong>
		                            </li>
	                          	</ol>
                        	</div>
                      	</div>
                    </div>
                </div>
                  
                <div class="text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                      	<div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="prev-btn"><i data-feather='skip-back'></i></button>
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="next-btn"><i data-feather='skip-forward'></i></button>
                            <button type="button" class="btn btn-outline-primary waves-effect waves-float waves-light">Records: <span id="to-records">0</span> of <span id="total-records">0</span></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-body">
			    <div class="col-lg-12 col-12">
			      	<div class="card card-company-table">
			        	<div class="card-body p-0">
				          	<div class="table-responsive">
				            	<table class="table">
					              	<thead>
					                	<tr>
						                  	<th>Package No</th>
						                  	<th>From Branch</th>
                                            <th>To Branch</th>
						                  	<th>Package Type</th>
						                  	<th>Package Stage</th>
						                  	<th>View</th>
					                	</tr>
					              	</thead>
				              		<tbody id="fetched-data">
				                
				              		</tbody>
				            	</table>
					            <div class="data-loading text-center mt-10">
						            <h5>Loading ...</h5>
						            <div class="spinner-grow text-primary me-1" role="status">
						                <span class="visually-hidden">Loading...</span>
						            </div>
						            <div class="spinner-grow text-danger me-1" role="status">
						                <span class="visually-hidden">Loading...</span>
						            </div>
						            <div class="spinner-grow text-success me-1" role="status">
						                <span class="visually-hidden">Loading...</span>
						            </div>
						        </div>
						        <div class="alert alert-grey show-alert font-size-16 m-2 text-center" role="alert">
                                    <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
                                        <div class="alert-body d-flex align-items-center">
                                            <img src="{{ asset('_dist/images/icons/icon-001.png') }}" class="list-icon me-1">
                                            <strong>စာများ မရှိသေးပါ။</strong>
                                        </div>
                                    </div>
                                </div>
          					</div>
        				</div>
      				</div>
    			</div>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')
    <input type="hidden" id="json" value="{{ $json }}">
    <input type="hidden" id="city" value="{{ city(user()->city_id)['name'] }}">
    @if(is_sorting($branch['id']) == 1)
    <input type="hidden" id="sorting" value="1">
    @else
    <input type="hidden" id="sorting" value="0">
    @endif

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script>
      	$(window).on('load',  function(){
	        if (feather) {
	          	feather.replace({ width: 14, height: 14 });
	        }
      	});
      $(document).ready(function(){
            var url         	= $("#url").val();
            var json        	= $("#json").val();
            var status        	= $("#status").val();
            var city_shortcode 	= $("#city").val();
            var sorting 		= $("#sorting").val();

            //declared first loaded json data
            var load_json   = url+'/'+json;
            var waybills    = [];
            var item        = 0;
            var _token      = $("#_token").val();
            
            fetched_package();

            function fetched_package(){
                load_json = url+'/'+json;
                $.ajax({
                	url: load_json,
                	type: 'GET',
                	data: {},
                	success: function(data){
                    if(data.total > 0){
                        $(".show-alert").hide();
                        $.each( data.data, function( key, value ) {
                            ++item;

                            $("#fetched-data").append(
                                '<tr>'
                                  	+'<td>'
                                    	+'<div class="d-flex align-items-center">'
	                                    	+'<div class="avatar rounded bg-white">'
	                                      		+'<div class="avatar-content">'
	                                        		+'<img src="'+url+'/_dist/images/logo.png" class="w-100" alt="Toolbar svg" />'
	                                      		+'</div>'
	                                    	+'</div>'
	                                    	+'<div>'
                                                +'<div class="fw-bolder font-medium-3 text-primary px-1">'+value.package_no+'</div>'
                                                +'<div class="font-small-3 text-muted ms-1">'+value.created_at+'</div>'
                                            +'</div>'
                                    	+'</div>'
                                  	+'</td>'
                    				+'<td>'
                      					+'<div class="d-flex align-items-center">'
	                        				+'<div>'
	                          					+'<div class="fw-bolder">'+value.from_branch+'</div>'
	                          					+'<div class="font-small-3 text-danger"><small class="font-small-2 text-muted">Created By</small>: '+value.created_by+'</div>'
	                        				+'</div>'
                      					+'</div>'
                    				+'</td>'
                                    +'<td class="text-nowrap">'
                                        +'<div class="d-flex flex-column">'
                                            +'<span class="fw-bolder mb-25">'+value.to_branch+'</span>'
                                        +'</div>'
                                    +'</td>'
				                    +'<td class="text-nowrap">'
				                      	+'<div class="d-flex flex-column">'
				                        	+'<span class="fw-bolder mb-25 text-capitalize"><span class="badge bg-light-primary">'+value.package_type+'</span></span>'
				                      	+'</div>'
				                    +'</td>'
			                      	+'<td class="text-nowrap">'
				                        +'<div class="d-flex flex-column">'
				                          	+'<span class="fw-bolder mb-25 text-capitalize">'+checked_package(value.completed)+'</span>'
				                        +'</div>'
			                      	+'</td>'
                      				+'<td><a href="'+url+'/packages/view/'+value.package_no+'" class="btn btn-relief-primary waves-effect btn-sm">View</a></td>'
                    			+'</tr>'
                        	); 
                        });
                        $(".data-loading").hide();

                        $("#to-records").text(data.to);
                        $("#total-records").text(data.total);
                                    
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }else{
                        $(".show-alert").show();
                        $(".pagination").hide();
                        $(".data-loading").hide();
                    }
                	}
            	});
            }

            $('.pagination-btn').click(function(){
                //clicked url json data
                $(".data-loading").show();
                $("#fetched-data").empty();
                var clicked_url = $(this).val();
                        

                $(this).siblings().removeClass('active')
                $(this).addClass('active');
                $.ajax({
                    url: clicked_url,
                    type: 'GET',
                    data: {},
                    success: function(data){

                        $.each( data.data, function( key, value ) {
                            ++item;
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td>'
                                        +'<div class="d-flex align-items-center">'
                                            +'<div class="avatar rounded bg-white">'
                                                +'<div class="avatar-content">'
                                                    +'<img src="'+url+'/_dist/images/logo.png" class="w-100" alt="Toolbar svg" />'
                                                +'</div>'
                                            +'</div>'
                                            +'<div>'
                                                +'<div class="fw-bolder font-medium-3 text-primary px-1">'+value.package_no+'</div>'
                                                +'<div class="font-small-3 text-muted ms-1">'+value.created_at+'</div>'
                                            +'</div>'
                                        +'</div>'
                                    +'</td>'
                                    +'<td>'
                                        +'<div class="d-flex align-items-center">'
                                            +'<div>'
                                                +'<div class="fw-bolder">'+value.from_branch+'</div>'
                                                +'<div class="font-small-3 text-danger"><small class="font-small-2 text-muted">Created By</small>: '+value.created_by+'</div>'
                                            +'</div>'
                                        +'</div>'
                                    +'</td>'
                                    +'<td class="text-nowrap">'
                                        +'<div class="d-flex flex-column">'
                                            +'<span class="fw-bolder mb-25">'+value.to_branch+'</span>'
                                        +'</div>'
                                    +'</td>'
                                    +'<td class="text-nowrap">'
                                        +'<div class="d-flex flex-column">'
                                            +'<span class="fw-bolder mb-25 text-capitalize"><span class="badge bg-light-primary">'+value.package_type+'</span></span>'
                                        +'</div>'
                                    +'</td>'
                                    +'<td class="text-nowrap">'
                                        +'<div class="d-flex flex-column">'
                                            +'<span class="fw-bolder mb-25 text-capitalize">'+checked_package(value.completed)+'</span>'
                                        +'</div>'
                                    +'</td>'
                                    +'<td><a href="'+url+'/packages/view/'+value.package_no+'" class="btn btn-relief-primary waves-effect btn-sm">View</a></td>'
                                +'</tr>'
                            ); 
                        });
                        $(".data-loading").hide();

                        $("#to-records").text(data.to);
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }
                });
            });
        });
    </script>
</body>
</html>