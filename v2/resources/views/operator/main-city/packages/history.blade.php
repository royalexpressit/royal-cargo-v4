<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Packages History</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                  <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      <div class="col-12">
                        <h3 class="content-header-title float-start mb-0">Packages</h3>
                        <div class="breadcrumb-wrapper">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active text-capitalize">
                                Packages Change History
                            </li>
                          </ol>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="prev-btn"><i data-feather='skip-back'></i></button>
                        <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="next-btn"><i data-feather='skip-forward'></i></button>
                        <button type="button" class="btn btn-outline-primary waves-effect waves-float waves-light">Records: <span id="to-records">0</span> of <span id="total-records">0</span></button>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="content-body">
<div class="row" id="table-head">
  <div class="col-12">
    <div class="">


        <section id="accordion-with-margin">
            <div class="accordion accordion-margin" id="accordionMargin">
                <div id="fetched-data"></div>
            </div>
            <div class="data-loading text-center mt-10">
            <h5>Loading ...</h5>
            <div class="spinner-grow text-primary me-1" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
            <div class="spinner-grow text-danger me-1" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
            <div class="spinner-grow text-success me-1" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>
        <div class="alert alert-grey show-alert font-size-16 m-2 text-center" role="alert">
            <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
              <div class="alert-body d-flex align-items-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                <strong>အချက်အလက်များ မရှိသေးပါ။</strong>
              </div>
            </div>
        </div>
        </section>
<!-- Accordion with margin end -->
      
      
    </div>
  </div>
</div>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')
<input type="hidden" id="json" value="{{ $json }}">



    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script>
      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      });
      $(document).ready(function(){
            //$(".data-loading").show();
            var url         = $("#url").val();
            var json        = $("#json").val();

            //declared first loaded json data
            var load_json   = url+'/'+json;
            var waybills    = [];
            var item        = 0;
            var _token      = $("#_token").val();
            
            $.ajax({
                url: load_json,
                type: 'GET',
                data: {},
                success: function(data){
                    console.log(json);
                    if(data.total > 0){
                        $(".show-alert").hide();
                        $.each( data.data, function( key, value ) {
                            ++item;
                            waybills.push(value.waybill_no);
                            $("#fetched-data").append(
                                '<div class="accordion-item">'
                                    +'<h2 class="accordion-header" id="headingMarginOne">'
                                        +'<button class="accordion-button collapsed view-btn" type="button" data-bs-toggle="collapse" data-bs-target="#'+value.package_no+'" aria-expanded="false" aria-controls="accordionMarginOne" value='+value.id+'>'
                                            +value.package_no
                                            +'<small class="px-1 text-warning">('+value.created_by+')</small>'
                                        +'</button>'
                                    +'</h2>'
                                    +'<div id="'+value.package_no+'" class="accordion-collapse collapse" aria-labelledby="headingMarginOne" data-bs-parent="#accordionMargin" >'
                                        +'<div class="accordion-body"><ul class="unstyled-list listing-item listing-item-'+value.id+'"></ul></div>'
                                    +'</div>'
                                +'</div>'
                            );
                            
                        });
                        $(".data-loading").hide();

                        $("#to-records").text(data.to);
                        $("#total-records").text(data.total);
                                    
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }else{
                        $(".show-alert").show();
                        $(".pagination").hide();
                        $(".data-loading").hide();

                        $("#prev-btn").attr('disabled',true);
                        $("#next-btn").attr('disabled',true);
                    }
                }
            });

            $('.pagination-btn').click(function(){
                //clicked url json data
                $(".data-loading").show();
                $("#fetched-data").empty();
                var clicked_url = $(this).val();
                        

                $(this).siblings().removeClass('active')
                $(this).addClass('active');
                $.ajax({
                    url: clicked_url,
                    type: 'GET',
                    data: {},
                    success: function(data){

                        $.each( data.data, function( key, value ) {
                            ++item;
                            $("#fetched-data").append(
                                '<div class="accordion-item">'
                                  +'<h2 class="accordion-header" id="headingMarginOne">'
                                    +'<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#'+value.package_no+'" aria-expanded="false" aria-controls="accordionMarginOne">'
                                      +value.package_no
                                      +'<small class="px-1 text-warning">('+value.created_by+')</small>'
                                    +'</button>'
                                  +'</h2>'
                                  +'<div id="'+value.package_no+'" class="accordion-collapse collapse" aria-labelledby="headingMarginOne" data-bs-parent="#accordionMargin" >'
                                    +'<div class="accordion-body">'
                                    +'</div>'
                                  +'</div>'
                                +'</div>'
                            );
                        });
                        $(".data-loading").hide();
                                

                        $("#to-records").text(data.to);
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }
                });
            });

            $('body').delegate(".view-btn","click",function () {
                var id = $(this).val();
                $(".listing-item").empty();

                $.ajax({
                    url: url+'/packages/changes-history/'+id,
                    type: 'GET',
                    data: {},
                    success: function(data){
                        
                        $.each( data, function( key, value ) {
                            $('.listing-item-'+id).append('<li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg> '+value.package_no+' '+value.log+'</li>');
                        });


                    }   
                });
            });
        });
    </script>
</body>
</html>