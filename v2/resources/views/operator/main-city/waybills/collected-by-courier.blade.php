<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Collected By Couriers</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
   
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      	<div class="col-12">
                        	<h3 class="content-header-title float-start mb-0 text-capitalize"> Outbound</h3>
                        	<div class="breadcrumb-wrapper">
	                          	<ol class="breadcrumb">
	                            	<li class="breadcrumb-item">
	                            		<a href="{{ url('') }}">Dashboard</a>
	                            	</li>
	                            	<li class="breadcrumb-item">
	                            		<a href="{{ url('outbound') }}">Outbound</a>
	                            	</li>
	                            	<li class="breadcrumb-item active">Collected By Couriers</li>
	                          	</ol>
	                        </div>
                      	</div>
                    </div>
                </div>
                 
                <div class="text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="btn-group">
	              		<button type="button" class="btn btn-outline-primary">Filterd By:</button>
			              <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split"data-bs-toggle="dropdown" aria-expanded="false" >
                			<span class="visually-hidden">Toggle Dropdown</span>
              			</button>
		              	<div class="dropdown-menu dropdown-menu-end" id="filtered-dropdown">
		                	
		              	</div>
            		</div>
                </div>
            </div>

            <div class="content-body">
			    <div class="col-lg-12 col-12">
			      	<div class="card card-company-table">
				        <div class="card-body p-0">
				          	<div class="table-responsive">
				          		<div id="test"></div>
					            <table class="table">
					              	<thead>
						                <tr>
						                  	<th>User ID</th>
						                  	<th>Courier/Counter</th>
						                  	<th>Waybills (Qty)</th>
						                  	<th>View</th>
						                </tr>
					              	</thead>
					              	<tbody id="fetched-data">
					                	<tr class="data-loading">
					                		<td colspan="4">
					                			<div class=" text-center mt-10">
									            	<h5>Loading ...</h5>
									            	<div class="spinner-grow text-primary me-1" role="status">
									                	<span class="visually-hidden">Loading...</span>
									            	</div>
									            	<div class="spinner-grow text-danger me-1" role="status">
									                	<span class="visually-hidden">Loading...</span>
									            	</div>
									            	<div class="spinner-grow text-success me-1" role="status">
									                	<span class="visually-hidden">Loading...</span>
									            	</div>
									        	</div>
					                		</td>
					                	</tr>
					              	</tbody>
					            </table>
				          	</div>
				        </div>
			      	</div>
			    </div>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="json" value="{{ $json }}">
    <input type="hidden" id="city" value="{{ city(user()->city_id)['name'] }}">


    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/bundles/main-city/m-578349.js') }}"></script>
</body>
</html>