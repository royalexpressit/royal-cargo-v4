<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="{{ url('') }}" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ asset('dist/images/logo.png') }}" alt="" height="80">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ asset('dist/images/logo.png') }}" alt="" height="60">
                    </span>
                </a>

                <a href="{{ url('') }}" class="logo logo-light mt-4">
                    <span class="logo-sm">
                        <img src="{{ asset('dist/images/logo.png') }}" alt="" height="80">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ asset('dist/images/logo.png') }}" alt="" height="60">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 d-lg-none header-item waves-effect waves-light" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                <i class="fa fa-fw fa-bars"></i>
            </button>

            <!-- App Search-->
            <form action="#" class="app-search d-none d-lg-block mt-4">
                <div class="position-relative">
                    <span class="uil-trash-alt delete-result hide"></span>
                    <input type="text" class="form-control" placeholder="Search waybill..." id="search" autocomplete="off">
                    <span class="uil-search"></span>
                </div>
                <div style="position: absolute;" class=" hide" id="searching-results">
                    <ul class="card" style="list-style: none;width:212px;max-height: 300px;overflow: scroll;position: absolute;z-index: 9;padding: 0px;margin:0px" id="results">
                        
                    </ul>
                </div>
                <div class="mt-1">
                    <h6 class="text-white current-branch">Your City: <strong>{{ city(user()->city_id)['name'] }}</strong></h6>
                </div>
            </form>

            <form class="app-search d-none d-lg-block mt-4">
                <div class="position-relative">
                    <div class="" id="datepicker2">
                        <input type="text" class="form-control" value="{{ get_date() }}"
                            data-date-format="yyyy-mm-dd" data-date-container='#datepicker2' data-provide="datepicker"
                            data-date-autoclose="true" id="dashboard-date">
                        <span class="uil-calendar-alt"></span>
                    </div> 
                    <div class="mt-1">
                        <h6 class="text-white current-branch">Your Branch: <strong>{{ branch(user()->branch_id)['name'] }}</strong></h6>
                    </div>
                </div>
            </form>   
        </div>

        <div class="d-flex">
            <div class="dropdown d-inline-block d-lg-none ms-2">
                <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="uil-search"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                    aria-labelledby="page-header-search-dropdown">
                    
                    <form class="p-3">
                        <div class="m-0">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="dropdown d-inline-block language-switch">
                <button type="button" class="btn header-item waves-effect"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="" src="{{ asset('dist/images/us.jpeg') }}" alt="Header Language" height="16">
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <img src="{{ asset('dist/images/spain.jpeg') }}" alt="user-image" class="me-1" height="12"> <span class="align-middle">Spanish</span>
                    </a>
                </div>
            </div>

            <div class="dropdown d-none d-lg-inline-block ms-1">
                <button type="button" class="btn header-item noti-icon waves-effect"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="mdi mdi-qrcode-scan"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end">
                    <div class="px-lg-2">
                        <div class="row g-0">
                            <div class="col">
                                <a class="dropdown-icon-item border-2 border-primary m-1" href="{{ url('scan/branch-out') }}">
                                    <i class="mdi mdi-qrcode-scan font-size-36 text-primary"></i>
                                    <span class="font-size-16">Branch Out Scan</span>
                                </a>
                            </div>
                            <div class="col">
                                <a class="dropdown-icon-item border-2 border-success m-1" href="{{ url('scan/branch-in') }}">
                                    <i class="mdi mdi-qrcode-scan font-size-36 text-success"></i>
                                    <span class="font-size-16">Branch In Scan</span>
                                </a>
                            </div>
                        </div>
                        
                        <div class="row g-0">
                            <div class="col">
                                <a class="dropdown-icon-item border-2-dashed border-primary m-1" href="{{ url('client-waybills/scan/collected') }}">
                                    <i class="mdi mdi-qrcode-scan font-size-36"></i>
                                    <span class="font-size-16">Client's Waybill Scan</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="dropdown d-none d-lg-inline-block ms-1">
                <button type="button" class="btn header-item noti-icon waves-effect" data-bs-toggle="fullscreen">
                    <i class="uil-minus-path"></i>
                </button>
            </div>

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="uil-bell"></i>
                    <span class="badge bg-danger rounded-pill">3</span>
                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0" aria-labelledby="page-header-notifications-dropdown">
                    <div class="p-3">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5 class="m-0 font-size-16"> Notifications </h5>
                            </div>
                            <div class="col-auto">
                                <a href="javascript:void(0);" class="small"> Mark all as read</a>
                            </div>
                        </div>
                    </div>
                    <div data-simplebar style="max-height: 230px;">
                        <a href="" class="text-reset notification-item">
                            <div class="d-flex align-items-start">
                                <div class="flex-shrink-0 me-3">
                                    <div class="avatar-xs">
                                        <span class="avatar-title bg-primary rounded-circle font-size-16">
                                            <i class="uil-shopping-basket"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="flex-grow-1">
                                    <h6 class="mb-1">Your order is placed</h6>
                                        <div class="font-size-12 text-muted">
                                            <p class="mb-1">If several languages coalesce the grammar</p>
                                            <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="p-2 border-top">
                            <div class="d-grid">
                                <a class="btn btn-sm btn-link font-size-14 text-center" href="javascript:void(0)">
                                    <i class="uil-arrow-circle-right me-1"></i> View More..
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="rounded-circle header-profile-user" src="{{-- asset('dist/images/'.user()->image) --}}"
                            alt="Header Avatar">
                        <span class="d-none d-xl-inline-block ms-1 fw-medium font-size-15">
                            {{ user()->name }}
                        </span>
                        <i class="uil-angle-down d-none d-xl-inline-block font-size-15"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <!-- item-->
                        <a class="dropdown-item" href="{{ url('profile') }}">
                            <i class="uil uil-user-circle font-size-18 align-middle text-muted me-1"></i> 
                            <span class="align-middle">View Profile</span>
                        </a>
                        <a class="dropdown-item" href="#"><i class="uil uil-wallet font-size-18 align-middle me-1 text-muted"></i> <span class="align-middle">My Wallet</span></a>
                        <a class="dropdown-item d-block" href="#"><i class="uil uil-cog font-size-18 align-middle me-1 text-muted"></i> <span class="align-middle">Settings</span> <span class="badge bg-soft-success rounded-pill mt-1 ms-2">03</span></a>
                        <a class="dropdown-item" href="#"><i class="uil uil-lock-alt font-size-18 align-middle me-1 text-muted"></i> <span class="align-middle">Lock screen</span></a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="uil uil-sign-out-alt font-size-18 align-middle me-1 text-muted"></i> <span class="align-middle">Logout</span></a>
                    </div>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                        <i class="uil-cog"></i>
                    </button>
                </div>
        </div>
    </div>
    <div class="container-fluid">
        
        <div class="topnav">
            <nav class="navbar navbar-light navbar-expand-lg topnav-menu">
                <div class="collapse navbar-collapse" id="topnav-menu-content">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('') }}">
                                <i class="uil-desktop text-danger font-size-18"></i>Dashboard
                            </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-pages" role="button">
                                <i class="uil-baby-carriage text-danger font-size-18"></i>Branch In <div class="arrow-down"></div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="topnav-pages">
                                <a href="{{ url('scan/branch-in') }}" class="dropdown-item">From Delivery/Counter</a>
                                <a href="{{ url('scan/branch-in') }}" class="dropdown-item">From Sorting/Branches</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-pages" role="button">
                                <i class="uil-truck text-danger font-size-18"></i>Branch Out <div class="arrow-down"></div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="topnav-pages">
                                <a href="{{ url('outbound/status/collected') }}" class="dropdown-item">To Sorting/Branches</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-pages" role="button">
                                <i class="uil-envelope-search text-danger font-size-18"></i>Reports <div class="arrow-down"></div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="topnav-pages">
                                <a href="{{ url('reports/types/normal-reports') }}" class="dropdown-item">Normal Reports</a>
                                <a href="{{ url('reports/types/same-day-delivery-reports') }}" class="dropdown-item">Same Day Delivery Reports</a>
                                <a href="{{ url('reports/types/transit-reports') }}" class="dropdown-item">Transit Reports</a>
                                <a href="{{ url('reports/types/client-waybills-reports') }}" class="dropdown-item">Client's Wyabills Reports</a>
                                <a href="{{ url('reports/types/transferred-reports') }}" class="dropdown-item">Transferred Waybills</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-pages" role="button">
                                <i class="uil-apps text-danger font-size-18"></i>More <div class="arrow-down"></div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="topnav-pages">
                                <a href="{{ url('advanced-search') }}" class="dropdown-item">Advanced Search</a>
                                <a href="{{ url('failed-waybill-logs') }}" class="dropdown-item">Failed Waybill Logs</a>
                                <a href="{{ url('users') }}" class="dropdown-item">Available Users</a>
                                <a href="{{ url('cities') }}" class="dropdown-item">Available Cities</a>
                                <a href="{{ url('branches') }}" class="dropdown-item">Available Branches</a>
                                <a href="{{ url('transferred') }}" class="dropdown-item">Transferred Waybill</a>
                                <a href="{{ url('submit/cancelled-waybills') }}" class="dropdown-item">Submit Cancelled Waybill <small class="btn-xs btn-danger btn-rounded">New</small></a>
                                <a href="{{ url('ctf') }}" class="dropdown-item">CTF Dashboard <small class="btn-xs btn-danger btn-rounded">New</small></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>