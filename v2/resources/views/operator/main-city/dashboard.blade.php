<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Dashboard</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
        	@include('announcement')

            <div class="content-body">
                <section class="basic-select2 mt-1">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-12">
                          	<div class="card">
	                            <div class="card-header d-flex justify-content-between align-items-center">
	                              	<h4 class="card-title">
	                              		CTF Status <br>
	                              		<span class="fs-16 text-warning">({{ branch(user()->branch_id)['name'] }} ရုံးနှင့်ဆိုင်သော)</span>
	                              	</h4>
	                              	<img src="{{ asset('_dist/images/folders.png') }}" height="120">
	                            </div>
	                            @if(is_sorting(user()->branch_id) == 0)
	                            <div class="card-body p-0">
	                            	<div class="row mx-0">
		                                <div class="col-6 py-1">
		                                  	<p class="card-text text-primary mb-0"></p>
		                                  	<h3 class="fw-bolder mb-0 text-primary"><button class="btn btn-sm btn-primary w-100 fs-16 waves-effect waves-float waves-light btn-b-tab disabled" value="b-out">Outbound</button></h3>
		                                </div>
		                                <div class="col-6 py-1">
		                                  	<p class="card-text text-success mb-0"></p>
		                                  	<h3 class="fw-bolder mb-0 text-success"><button class="btn btn-sm btn-success w-100 fs-16 waves-effect waves-float waves-light btn-b-tab" value="b-in">Inbound</button></h3>
		                                </div>
	                              	</div>
	                              	<div class="b-out-tab">
		                              	<div class="row border-top text-center mx-0">
			                                <div class="col-12 py-1">
			                                  	<p class="card-text mb-0 text-primary">Branch <i data-feather="repeat"></i> Branch 
			                                    	<span class="badge rounded-pill badge-light-primary me-1 pull-right">စုစုပေါင်း  <strong>{{ outbound_ctf_status(3)['total'] }}</strong></span>
			                                  	</p>
			                                </div>
		                              	</div>
		                              	<div class="row border-top text-center mx-0">
			                                <div class="col-12">
			                                  	<div class="row text-center">
				                                    <div class="col-6 border-end py-1">
				                                      	<h4 class="fw-bolder mb-0"> {{ outbound_ctf_status(3)['progress'] }}</h4>
	                                                    <small class="w-100"><i data-feather="clock" class="text-warning"></i> Shipping (ပို့ထားဆဲ)</small>
				                                    </div>
				                                    <div class="col-6 py-1">
				                                      	<h4 class="fw-bolder mb-0"> {{ outbound_ctf_status(3)['completed'] }}</h4>
	                                                    <small class="w-100"><i data-feather="check" class="text-success"></i> Arrived (ရောက်ရှိပြီး)</small>
	                                                </div>
			                                  	</div>
			                                </div>
		                              	</div>
		                            </div>
		                            <div class="b-in-tab hide">
		                              	<div class="row border-top text-center mx-0">
			                                <div class="col-12 py-1">
			                                  	<p class="card-text mb-0 text-success">Branch <i data-feather="repeat"></i> Branch 
			                                    	<span class="badge rounded-pill badge-light-success me-1 pull-right">စုစုပေါင်း  <strong>{{ inbound_ctf_status(9)['total'] }}</strong></span>
			                                  	</p>
			                                </div>
		                              	</div>
		                              	<div class="row border-top text-center mx-0">
			                                <div class="col-12">
			                                  	<div class="row text-center">
				                                    <div class="col-6 border-end py-1">
				                                      	<h4 class="fw-bolder mb-0"> {{ inbound_ctf_status(9)['progress'] }}</h4>
	                                                    <small class="w-100"><i data-feather="clock" class="text-warning"></i> Shipping (ပို့ထားဆဲ)</small>
				                                    </div>
				                                    <div class="col-6 py-1">
				                                      	<h4 class="fw-bolder mb-0"> {{ inbound_ctf_status(9)['completed'] }}</h4>
	                                                    <small class="w-100"><i data-feather="check" class="text-success"></i> Arrived (ရောက်ရှိပြီး)</small>
	                                                </div>
			                                  	</div>
			                                </div>
		                              	</div>
		                            </div>
	                            </div>
	                            @else
	                            <div class="card-body p-0">
	                              	<div class="row mx-0">
		                                <div class="col-6 py-1">
		                                  	<p class="card-text text-primary mb-0"></p>
		                                  	<h3 class="fw-bolder mb-0 text-primary"><button class="btn btn-sm btn-primary w-100 fs-16 waves-effect waves-float waves-light btn-tab disabled" value="out">Outbound</button></h3>
		                                </div>
		                                <div class="col-6 py-1">
		                                  	<p class="card-text text-success mb-0"></p>
		                                  	<h3 class="fw-bolder mb-0 text-success"><button class="btn btn-sm btn-success w-100 fs-16 waves-effect waves-float waves-light btn-tab" value="in">Inbound</button></h3>
		                                </div>
	                              	</div>

	                              	<div class="out-tab">
		                              	<div class="row border-top text-center mx-0">
			                                <div class="col-6 border-end py-1">
			                                  	<p class="card-text mb-0 text-primary">Branch <i data-feather="repeat"></i> Branch 
			                                    	<span class="badge rounded-pill badge-light-primary me-1 pull-right">{{ outbound_ctf_status(3)['total'] }}</span>
			                                  	</p>
			                                </div>
			                                <div class="col-6 py-1 ">
			                                  	<p class="card-text mb-0 text-primary">City <i data-feather="repeat"></i> City
			                                    	<span class="badge rounded-pill badge-light-primary me-1 pull-right">{{ outbound_ctf_status(6)['total'] }}</span>
			                                  	</p>
			                                </div>
		                              	</div>
		                              	<div class="row border-top text-center mx-0">
			                                <div class="col-6 border-end">
			                                  	<div class="row text-center">
				                                    <div class="col-6 border-end py-1">
				                                      	<h4 class="fw-bolder mb-0 text-primary"> {{ outbound_ctf_status(3)['progress'] }}</h4>
	                                                    <small class="w-100"><i data-feather="clock" class="text-warning"></i> Shipping</small>
				                                    </div>
				                                    <div class="col-6 py-1">
				                                      	<h4 class="fw-bolder mb-0 text-primary"> {{ outbound_ctf_status(3)['completed'] }}</h4>
	                                                    <small class="w-100"><i data-feather="check" class="text-success"></i> Arrived</small>
	                                                </div>
			                                  	</div>
			                                </div>
			                                <div class="col-6">
			                                  	<div class="row text-center">
				                                    <div class="col-6 border-end py-1">
				                                      	<h4 class="fw-bolder mb-0 text-primary"> {{ outbound_ctf_status(6)['progress'] }}</h4>
	                                                    <small class="w-100"><i data-feather="clock" class="text-warning"></i> Shipping</small>
				                                    </div>
				                                    <div class="col-6 py-1">
				                                      	<h4 class="fw-bolder mb-0 text-primary"> {{ outbound_ctf_status(6)['completed'] }}</h4>
	                                                    <small class="w-100"><i data-feather="check" class="text-success"></i> Arrived</small>
	                                                </div>
			                                  	</div>
			                                </div>
		                              	</div>
		                             </div>

	                              	<!-- -->
	                              	<div class="in-tab ">
		                              	<div class="row border-top text-center mx-0">
			                                <div class="col-6 py-1 ">
			                                  	<p class="card-text mb-0 text-success">City <i data-feather="repeat"></i> City
			                                    	<span class="badge rounded-pill badge-light-success me-1 pull-right">{{ inbound_ctf_status(6)['total'] }}</span>
			                                  	</p>
			                                </div>
			                                <div class="col-6 border-end py-1">
			                                  	<p class="card-text mb-0 text-success">Branch <i data-feather="repeat"></i> Branch 
			                                    	<span class="badge rounded-pill badge-light-success me-1 pull-right">{{ inbound_ctf_status(9)['total'] }}</span>
			                                  	</p>
			                                </div>
		                              	</div>
		                              	<div class="row border-top text-center mx-0">
			                                <div class="col-6 border-end">
			                                  	<div class="row text-center">
				                                    <div class="col-6 border-end py-1">
				                                      	<h4 class="fw-bolder mb-0 text-success"> {{ inbound_ctf_status(6)['progress'] }}</h4>
	                                                    <small class="w-100"><i data-feather="clock" class="text-warning"></i> Shipping</small>
				                                    </div>
				                                    <div class="col-6 py-1">
				                                      	<h4 class="fw-bolder mb-0 text-success"> {{ inbound_ctf_status(6)['completed'] }}</h4>
	                                                    <small class="w-100"><i data-feather="check" class="text-success"></i> Arrived</small>
	                                                </div>
			                                  	</div>
			                                </div>
			                                <div class="col-6">
			                                  	<div class="row text-center">
				                                    <div class="col-6 border-end py-1">
				                                      	<h4 class="fw-bolder mb-0 text-success"> {{ inbound_ctf_status(9)['progress'] }}</h4>
	                                                    <small class="w-100"><i data-feather="clock" class="text-warning"></i> Shipping</small>
				                                    </div>
				                                    <div class="col-6 py-1">
				                                      	<h4 class="fw-bolder mb-0 text-success"> {{ inbound_ctf_status(9)['completed'] }}</h4>
	                                                    <small class="w-100"><i data-feather="check" class="text-success"></i> Arrived</small>
	                                                </div>
			                                  	</div>
			                                </div>
		                              	</div>
		                            </div>
	                            </div>
	                            @endif
                          	</div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12">
                          	<div class="card">
	                            <div class="card-header d-flex justify-content-between align-items-center">
	                              	<h4 class="card-title">
	                              		Waybill Status <br>
	                              		<span class="fs-16 text-warning">({{ branch(user()->branch_id)['name'] }} ရုံးနှင့်ဆိုင်သော)</span>
	                              	</h4>
	                              	<img src="{{ asset('_dist/images/document.png') }}" height="120">
	                            </div>
	                            <div class="card-body p-0">
	                              	<div id="goal-overview-radial-bar-chart" class="my-2"></div>
	                              	<div class="row border-top text-center mx-0">
		                                <div class="col-6 border-end py-1">
		                                  	<p class="card-text mb-0 fs-18">Outbound <small>(ရုံးထွက်)</small></p>
		                                </div>
		                                <div class="col-6 py-1">
		                                  	<p class="card-text mb-0 fs-18">Inbound <small>(ရုံးဝင်)</small></p>
		                                </div>
	                              	</div>
	                              	<div class="row border-top text-center mx-0">
		                                <div class="col-6 border-end py-1">
		                                  	<p class="card-text text-primary mb-0"></p>
		                                  	<h3 class="fw-bolder mb-0 text-primary"><a href="{{ url('outbound/collected-by-branch/'.user()->branch_id) }}" class="btn btn-sm btn-primary w-100 fs-18">{{ outbound_status(user()->branch_id) }}</a></h3>
		                                </div>
		                                <div class="col-6 py-1">
		                                  	<p class="card-text text-success mb-0"></p>
		                                  	<h3 class="fw-bolder mb-0 text-success"><a href="#" class="btn btn-sm btn-success w-100 fs-18 disabled">{{ inbound_status(user()->branch_id) }}</a></h3>
		                                </div>
	                              	</div>
	                            </div>
                          	</div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12">
                          <div class="card">
	                            <div class="card-header d-flex justify-content-between align-items-center">
	                              	<h4 class="card-title">
	                              		Package Status <br>
	                              		<span class="fs-16 text-warning">({{ branch(user()->branch_id)['name'] }} ရုံးနှင့်ဆိုင်သော)</span>
	                              	</h4>
	                              	<img src="{{ asset('_dist/images/package.png') }}" height="120">
	                            </div>
                            	<div class="card-body p-0">
	                              	<div id="goal-overview-radial-bar-chart" class="my-2"></div>
	                              	<div class="row border-top text-center mx-0">
		                                <div class="col-6 border-end py-1">
		                                  	<p class="card-text mb-0 fs-18">Outbound <small>(ရုံးထွက်)</small></p>
		                                </div>
		                                <div class="col-6 py-1">
		                                  	<p class="card-text mb-0 fs-18">Inbound <small>(ရုံးဝင်)</small></p>
		                                </div>
	                              	</div>
	                              	<div class="row border-top text-center mx-0">
		                                <div class="col-6 border-end py-1">
		                                  	<p class="card-text text-primary mb-0"></p>
		                                  	<h3 class="fw-bolder mb-0 text-primary"><a href="#" class="btn btn-sm btn-primary w-100 fs-18 disabled">{{ package_status('outbound') }}</a></h3>
		                                </div>
		                                <div class="col-6 py-1">
		                                  	<p class="card-text text-success mb-0"></p>
		                                  	<h3 class="fw-bolder mb-0 text-success"><a href="#" class="btn btn-sm btn-success w-100 fs-18 disabled">{{ package_status('inbound') }}</a></h3>
		                                </div>
	                              	</div>
                            	</div>
                          	</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-12">
                          	<div class="card">
	                            <div class="card-body p-0">
	                              	<div class="table-responsive">
		                                <table class="table">
		                                  	<thead class="border-rouned">
			                                    <tr>
			                                      	<th>Courier/Counter</th>
			                                      	<th>Total</th>
			                                    </tr>
		                                  	</thead>
		                                  	<tbody>
			                                    @if(!collected_by_courier()->isEmpty())
			                                    @foreach(collected_by_courier() as $courier)
			                                    <tr>
			                                      	<td>
				                                        <div class="d-flex align-items-center">
				                                          	<div class="avatar me-1">
					                                            <div class="avatar-content">
					                                              	<img src="{{ asset('_dist/images/'.$courier->image) }}" width="34" height="34" alt="Toolbar svg" />
					                                            </div>
				                                          	</div>
				                                          	<div>
				                                            	<div class="fw-bolder text-primary">{{ $courier->name }}</div>
				                                            	<div class="font-small-2 text-muted">{{ $courier->branch }} &nbsp;</div>
				                                          	</div>
				                                        </div>
			                                      	</td>
			                                      	<td class="text-nowrap">
			                                        	<div class="d-flex flex-column">
			                                          		<span class="fw-bolder mb-25"><a href="{{ url('outbound/collected-by-courier/'.$courier->user_id) }}" class="btn btn-sm text-right btn-primary w-100 fs-18 waves-effect waves-float waves-light">{{ $courier->total }}</a></span>
			                                        	</div>
			                                      	</td>
			                                    </tr>
			                                    @endforeach
			                                    @else
			                                    <tr>
			                                    	<td colspan="2">
				                                        <div class="demo-spacing-0">
				                                          	<div class="alert alert-warning alert-dismissible fade show" role="alert">
					                                            <div class="alert-body">
					                                              	စာရင်းမရှိသေးပါ
					                                            </div>
				                                          	</div>
				                                        </div>
			                                      	</td>
			                                    </tr>
			                                    @endif
		                                  	</tbody>
		                                </table>
	                              	</div>
	                            </div>
                          	</div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card card-developer-meetup">
                                <div class="meetup-img-wrapper rounded-top text-center">
                                  	<img src="{{ asset('_dist/images/vector-004.jpeg') }}" alt="Meeting Pic" height="170">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title mb-25">Outbound Dashboard</h4>
                                    <div class="mt-1">
                                        <a href="{{ url('outbound') }}" class="btn btn-primary waves-effect waves-float waves-light w-100">View Dashboard</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card card-developer-meetup">
                                <div class="meetup-img-wrapper rounded-top text-center">
                                  	<img src="{{ asset('_dist/images/vector-006.jpeg') }}" alt="Meeting Pic" height="170">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title mb-25">Inbound Dashboard</h4>
                                    <div class="mt-1">
                                        <a href="{{ url('inbound') }}" class="btn btn-success waves-effect waves-float waves-light w-100 disabled">View Dashboard</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                @if(is_sorting(user()->branch_id) == 0)
                <div class="row match-height">
                   
                </div>
                @endif
            </div>
        </div>
    </div>
    <!-- END: Content-->

    @include('customizer')
    @include('footer')

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>

    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script>
      	$(window).on('load',  function(){
        	if (feather) {
          	feather.replace({ width: 14, height: 14 });
        	}
      	});

      	$(".btn-tab").on("click",function search(e) {
	        //call api sent to server function
	        var tab = $(this).val();
	        $(".btn-tab").removeClass('disabled');
	        $(this).addClass('disabled');
	        if(tab == 'out'){
	        	$(".in-tab").hide();
	        	$(".out-tab").show();

	        }else{
	        	$(".in-tab").show();
	        	$(".out-tab").hide();
	        }
	    });

	    $(".btn-b-tab").on("click",function search(e) {
	        //call api sent to server function
	        var tab = $(this).val();
	        $(".btn-b-tab").removeClass('disabled');
	        $(this).addClass('disabled');
	        if(tab == 'b-out'){
	        	$(".b-in-tab").hide();
	        	$(".b-out-tab").show();

	        }else{
	        	$(".b-in-tab").show();
	        	$(".b-out-tab").hide();
	        }
	    });
    </script>
</body>
</html>