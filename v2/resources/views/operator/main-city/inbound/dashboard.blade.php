<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Inbound</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
          	<div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Inbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item active text-capitalize">Inbound
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    
                </div>
            </div>
        	<div class="content-body">
                <section class="basic-select2">
                    <div class="row match-height">
                    	<div class="col-lg-4 col-md-6 col-12">
					      	<div class="card card-developer-meetup">
						        <div class="meetup-img-wrapper rounded-top text-center">
						          	<img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
						        </div>
					        	<div class="card-body">
					          
					          
					          
					        	</div>
					      	</div>
					    </div>

					    <div class="col-xl-8 col-md-6 col-12">
					      	<div class="card card-statistics">
						        <div class="card-header">
						          	<h4 class="card-title">Inbound Statistics</h4>
						          	<div class="d-flex align-items-center">
						            	<p class="card-text font-medium-3 me-25 mb-0">{{ get_date() }}</p>
						          	</div>
						        </div>
						        <div class="card-body statistics-body">
						          	<div class="row">
							            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
							              	<div class="d-flex flex-row">
								                <div class="avatar bg-light-primary me-2">
								                  	<div class="avatar-content">
								                    	<i data-feather="trending-up" class="avatar-icon"></i>
								                  	</div>
								                </div>
								                <div class="my-auto">
								                  	<h2 class="fw-bolder mb-0">{{ $statistics['waybills'] }}</h2>
								                  	<p class="card-text font-small-3 mb-0">Waybills</p>
								                </div>
							              	</div>
							            </div>
							            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
							              	<div class="d-flex flex-row">
								                <div class="avatar bg-light-info me-2">
								                  	<div class="avatar-content">
								                    	<i data-feather="user" class="avatar-icon"></i>
								                  	</div>
								                </div>
								                <div class="my-auto">
								                  	<h2 class="fw-bolder mb-0">{{ $statistics['packages'] }}</h2>
								                  	<p class="card-text font-small-3 mb-0">Packages</p>
								                </div>
							              	</div>
							            </div>
							            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
							              	<div class="d-flex flex-row">
								                <div class="avatar bg-light-danger me-2">
								                  	<div class="avatar-content">
								                    	<i data-feather="box" class="avatar-icon"></i>
								                  	</div>
								                </div>
								                <div class="my-auto">
								                  	<h2 class="fw-bolder mb-0">{{ $statistics['ctfs'] }}</h2>
								                  	<p class="card-text font-small-3 mb-0">CTFs</p>
								                </div>
							              	</div>
							            </div>
							            <div class="col-xl-3 col-sm-6 col-12">
							              	<div class="d-flex flex-row">
								                <div class="avatar bg-light-success me-2">
								                  	<div class="avatar-content">
								                    	<i data-feather="dollar-sign" class="avatar-icon"></i>
								                  	</div>
								                </div>
								                <div class="my-auto">
								                  	<h2 class="fw-bolder mb-0">{{ $statistics['branches'] }}</h2>
								                  	<p class="card-text font-small-3 mb-0">Branches</p>
								                </div>
							              	</div>
							            </div>
						          	</div>
						        </div>
					      	</div>
					    </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card card-developer-meetup">
                                <div class="meetup-img-wrapper rounded-top text-center">
                                    <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
                                </div>
                                <div class="card-body">
                                    <div class="meetup-header d-flex align-items-center">
                                        မြို့အလိုက်ဝင်လာသော အထုပ်များ
                                    </div>
                                    <div class="mt-1">
                                        <a href="{{ url('outbound/status/same-day') }}" class="btn btn-success waves-effect waves-float waves-light w-100">ကြည့်မည်</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      	<div class="col-lg-4 col-md-6 col-12">
                            <div class="card card-developer-meetup">
                                <div class="meetup-img-wrapper rounded-top text-center">
                                    <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
                                </div>
                                <div class="card-body">
                                    <div class="meetup-header d-flex align-items-center">
                                      	ရုံးခွဲအလိုက်ပို့ထားသော အထုပ်များ
                                    </div>
                                    <div class="mt-1">
                                        <a href="{{ url('outbound/collected-by-courier') }}" class="btn btn-success waves-effect waves-float waves-light w-100">ကြည့်မည်</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card card-developer-meetup">
                                <div class="meetup-img-wrapper rounded-top text-center">
                                    <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
                                </div>
                                <div class="card-body">
                                    <div class="meetup-header d-flex align-items-center">
                                        လက်ခံရန်ကျန်ရှိနေသော စာများ
                                    </div>
                                    <div class="mt-1">
                                        <a href="{{ url('outbound/package/draft') }}" class="btn btn-success waves-effect waves-float waves-light w-100">ကြည့်မည်</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card card-developer-meetup">
                                <div class="meetup-img-wrapper rounded-top text-center">
                                    <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
                                </div>
                                <div class="card-body">
                                    <div class="meetup-header d-flex align-items-center">
                                        နယ်ပြန်ပို့ထားသော အထုပ်များ
                                    </div>
                                    <div class="mt-1">
                                        <a href="{{ url('outbound/package/all') }}" class="btn btn-success waves-effect waves-float waves-light w-100">ကြည့်မည်</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card card-developer-meetup">
                                <div class="meetup-img-wrapper rounded-top text-center">
                                    <img src="{{ asset('_dist/images/email.svg') }}" alt="Meeting Pic" height="170">
                                </div>
                                <div class="card-body">
                                    <div class="meetup-header d-flex align-items-center">
                                        ငြင်းပယ်(Rejected)ထားသော စာများ
                                    </div>
                                    <div class="mt-1">
                                        <a href="{{ url('outbound/status/same-day') }}" class="btn btn-primary waves-effect waves-float waves-light w-100">ကြည့်မည်</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script>
      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      });
    </script>
</body>
</html>