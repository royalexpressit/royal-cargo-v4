<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - View Package</title>
    
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-transaction">
	                            <div class="card-header">
	                              	<h4 class="card-title">Waybill Details</h4>
	                            </div>
	                            <div class="card-body">
	                            	{{ $ctf }}
	                              	<div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather='package' class="list-icon"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">CTF No</h6>
		                                    	<small>စာရင်းအမှတ်</small>
		                                  	</div>
		                                </div>
	                                	<div class="font-medium-4 text-primary">{{ $ctf->ctf_no }}</div>
	                              	</div>
	                              	<div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather='home' class="list-icon"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">From Branch</h6>
		                                    	<small>စာရင်းပို့ထားသည့်ရုံး</small>
		                                  	</div>
		                                </div>
	                                	<div class="font-medium-4 text-primary">{{ $ctf->branch_name }}</div>
	                              	</div>
	                              	<div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather='home' class="list-icon"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Created By</h6>
		                                    	<small>စာရင်းပို့ထားသူ</small>
		                                  	</div>
		                                </div>
	                                	<div class="font-medium-4 text-primary">{{ $ctf->username }}</div>
	                              	</div>
	                              	<div class="transaction-item">
		                                <div class="d-flex">
		                                  	<div class="avatar bg-light-primary rounded float-start">
			                                    <div class="avatar-content">
			                                      	<i data-feather='shopping-cart' class="list-icon"></i>
			                                    </div>
		                                  	</div>
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Qty</h6>
		                                    	<small>အထုပ်အရေအတွက်</small>
		                                  	</div>
		                                </div>
	                                	<div class="font-medium-4 text-primary">
	                                		<strong id="quantity">0</strong><br>
	                                	</div>
	                              	</div>
	                              	<div class="transaction-item">
		                                <div class="d-flex">
		                                  	
		                                  	<div class="transaction-percentage">
		                                    	<h6 class="transaction-title">Current Status</h6>
		                                    	<small>{{ $ctf->completed }}</small>
		                                  	</div>
		                                </div>
	                                	<div class="font-medium-4 text-primary"><span id="quantity"></span></div>
	                              	</div>
	                            </div>
                          	</div>
                        </div>
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header">
                                  	<h4 class="card-title text-primary">CTF တွင်ပါဝင်သည့် အထုပ်များ</h4>
                                  	<span>
                                  		<label>အထုပ်အမှတ်ရှာရန်</label>
                                  		<input type="search" class="form-control inline-search" id="search-waybill" placeholder="" >
                                    </span>
                                </div>
                                <hr>
                                <div class="card-body">
                                @if(!$packages->isEmpty())
                                    <div class="card card-transaction " style="max-height: 600px;">
                                        @foreach($packages as $key => $package)
                                        @php 
                                        	++$qty;
                                        	if($package->active == 0){
                                            	
                                            }
                                       	@endphp
                                          <div class="transaction-item list-item item-{{ $package->packaged_no }} id-{{ $package->packaged_no }} {{ ($package->completed==0? 'border-danger border-rounded':'') }}">
                                            <div class=" drag-item">
	                                            <div class="d-flex">
	                                              	<div class="avatar {{ ($package->completed==0? 'bg-light-danger':'bg-light-primary') }} rounded item-bg-{{ $package->packaged_no }}">
		                                                <div class="avatar-content">
		                                                    <i data-feather='file' class="list-icon"></i>
		                                                </div>
	                                              	</div>
	                                              	<div class="transaction-percentage ">
	                                                	<h6 class="transaction-title font-medium-3 m-0 {{ ($package->active==0? 'text-danger':'text-primary') }} item-color-{{ $package->packaged_no }}">{{ $package->package_no }}</h6>
	                                                	<small class="text-primary"><small class="text-muted">အထုပ်ပို့ထားသည့်ရုံး - </small>{{ $package->from_branch_name }}</small>
	                                              	</div>
	                                            </div>
                                            </div>
                                            <div class="fw-bolder">
                                                <div class="btn-actions-{{ $package->waybill_id }}">
	                                                <a href="{{ url('packages/view/'.$package->package_no) }}" class="btn btn-relief-primary waves-effect add-btn btn-md" >
	                                                    အသေးစိပ်ကြည့်ရန်
	                                                </a>
                                                </div>
                                            </div>
                                          </div>
                                        @endforeach
                                    </div>
                                 @else
                                    <div class="demo-spacing-0">
                                        <div class="alert alert-warning alert-validation-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                <span>စာများ မရှိသေးပါ</span>
                                            </div>
                                      	</div>
                                    </div>
                                @endif 
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="qty" value="{{ $qty }}">
	
	<div class="modal fade modal-danger text-start danger" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel120">အသိပေးခြင်း</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    စာအမှတ် အား အထုပ်အမှတ် <strong></strong> ထဲမှပယ်ဖျတ်ရန် အတည်ပြုပေးပါ။
                </div>
                <div class="modal-footer">
                    <input type="" id="package_id" value="">
                    <input type="" id="selected_item" value="">
                    <button type="button" class="btn btn-danger confirm-btn" data-bs-dismiss="modal">ဖျတ်မည်</button>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <!-- END: Page JS-->
    <script>
      	$(window).on('load',  function(){
        	if (feather) {
          		feather.replace({ width: 14, height: 14 });
        	}
      	});

      	$(document).ready(function(){
        	var url     = $("#url").val();
        	var packages= [];
        	var added   = 0;

        	$("#quantity").text($("#qty").val());
        	$("#removed").text($("#remove").val());

	        $(".add-btn").on("click",function search(e) {
	            $('.export-btn-box').removeClass('hide');
	            $('.show-ctf').addClass('hide');

	            $('.show-msg').hide();
	            id = this.id;
	            $('.item-'+id).hide();
	            ++added;

	            //alert(id);
	            packages.push(id);

	            data = $('.drag-item-'+id).html();
	            $("#scanned-lists").prepend('<div class="transaction-item">'+data+'</div>');
	            
	            $('#listed').text(added);
	        });

	      	$('#filtered').on("change",function search(e) {
	        	branch = $("#filtered option:selected").text();
	        	if(branch == 'Select Branch'){
	            	$('.list-item').removeClass('hidden'); 
	        	}else{
	            	$('.list-item').addClass('hidden');
	            	$('.'+branch).removeClass('hidden');
	        	}
	        
	        	console.log(branch);
	        });

	      	$(".ctf-btn").on("click",function search(e) {
	            user_id         = $("#user_id").val();
	            username        = $("#username").val();
	            user_city_id    = $("#user_city_id").val();
	            user_branch_id  = $("#user_branch_id").val();
	                
	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
	                       
	            $.ajax({
	                type: 'post',
	                //url: url+'/api/action/ctf-export',
	                url: url+'/api/action/outbound',
	                dataType:'json',
	                data: {
	                    'packages'      :packages,
	                    'user_id'       :user_id,
	                    'username'      :username,
	                    'user_city_id'  :user_city_id,
	                    'user_branch_id':user_branch_id,
	                    'action_id'     :3,
	                },
	                success: function(data) { 
	                    console.log(data);
	                    if(data.success == 1){
	                        $(".show-ctf").removeClass('hide');
	                        $("#ctf_no").text(data.ctf_no);
	                        $(".print-url").attr("href", url+"/print/cft?qr="+data.ctf_no)
	                    }

	                    packages= [];
	                    $("#scanned-lists").empty(); 
	                    $("#failed-lists").empty();
	                    $(".export-btn-box").addClass('hide');
	                },
	            });
	            $(this).val(''); 
	                
	            added = 0;
	            $('#listed').text(added);
	        });

      		$('body').delegate(".remove-btn","click",function () {
               	var id = $(this).attr('id');
               	console.log(id); 
               	$("#selected_item").val(id);
        	});

      		$('body').delegate(".btn-new","click",function () {
            	$('.waybill-box').toggle();
        	});


        	$('body').delegate(".confirm-btn","click",function () {
            	var item            = $("#selected_item").val();
            	var package_id      = $("#package_id").val();
            	var username        = $("#username").val();
            	var user_id         = $("#user_id").val();
            	var user_city_id    = $("#user_city_id").val();
            	var user_branch_id  = $("#user_branch_id").val();

	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                }
	            });
                       
	            $.ajax({
	                type: 'post',
	                url: url+'/api/removed-waybill',
	                dataType:'json',
	                data: {
	                    'item'      :item,
	                    'package_id':package_id,
	                    'username'  :username,
	                    'user_id'  :user_id,
	                    'user_city_id'  :user_city_id,
	                    'user_branch_id'  :user_branch_id,
	                },
	                success: function(data) { 
	                    console.log(data);
	                    
	                    if(data.success == 1){
	                    	$('.id-'+data.item).addClass('border-danger border-rounded');
	                    	$('.removed-msg-'+data.item).show();
							$('.btn-actions-'+data.item).hide();

							$('.item-bg-'+data.item).removeClass('bg-light-primary');
							$('.item-bg-'+data.item).addClass('bg-light-danger');

							$('.item-color-'+data.item).removeClass('text-primary');
							$('.item-color-'+data.item).addClass('text-danger');
	                    }
	                },
	            });   
            
        	});

	      	$('#search-waybill').on("keyup",function search(e) {
	        	var term = $(this).val();
	        	$('.list-item').hide();
	        	$(".transaction-item:contains('"+term+"')").show();
	    	});
	  	});
    </script>
</body>
</html>