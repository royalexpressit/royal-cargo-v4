<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Packages</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Packages</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('outbound') }}">Packages</a>
                                    </li>
                                    <li class="breadcrumb-item active text-capitalize">Incomplete Package #
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="prev-btn"><i data-feather='skip-back'></i></button>
                            <button type="button" class="btn btn-primary waves-effect waves-float waves-light pagination-btn" id="next-btn"><i data-feather='skip-forward'></i></button>
                            <button type="button" class="btn btn-outline-primary waves-effect waves-float waves-light">Records: <span id="to-records">0</span> of <span id="total-records">0</span></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-body">
                <div class="row" id="table-head">
                        <div class="col-12">
                            <div class="card">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="table-dark">
                                            <tr>
                                                <th>Created Date</th>
                                                <th>Package No</th>
                                                <th>CTF No</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody id="fetched-data">
                                                                                
                                       </tbody>
                                    </table>
                                    <div class="data-loading text-center mt-10">
                                        <h5>Loading ...</h5>
                                        <div class="spinner-grow text-primary me-1" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                        <div class="spinner-grow text-danger me-1" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                        <div class="spinner-grow text-success me-1" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <div class="alert alert-grey show-alert font-size-16 m-2 text-center" role="alert">
                                        <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                <strong>စာများ မရှိသေးပါ။</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')
    <input type="hidden" id="json" value="{{ $json }}">


    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });
        
        $(document).ready(function(){
            var url         = $("#url").val();
            var json        = $("#json").val();

            //declared first loaded json data
            var load_json   = url+'/'+json;
            var waybills    = [];
            var item        = 0;
            var _token      = $("#_token").val();
            
            $.ajax({
                url: load_json,
                type: 'GET',
                data: {},
                success: function(data){
                    console.log(json);
                    if(data.total > 0){
                        $(".show-alert").hide();
                        $.each( data.data, function( key, value ) {
                            ++item;
                            waybills.push(value.waybill_no);
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td><span class="waybill-no text-danger">'+value.created_at+'</span></td>'
                                    +'<td><span class="waybill-no text-danger">'+value.package_no+'</span></td>'
                                    +'<td class="text-bold-500">'+value.ctf_no+'</td>'
                                    +'<td><span class="btn btn-outline-primary btn-xs round waves-effect">'+value.completed+'</span></td>'
                                    +'<td><a href="'+url+'/incomplete-package/view/'+value.id+'" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">View Waybill</a></td>'
                                +'</tr>'
                            );
                            
                        });
                        $(".data-loading").hide();

                        $("#to-records").text(data.to);
                        $("#total-records").text(data.total);
                                    
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }else{
                        $(".show-alert").show();
                        $(".pagination").hide();
                        $(".data-loading").hide();
                    }
                }
            });

            $('.pagination-btn').click(function(){
                //clicked url json data
                $(".data-loading").show();
                $("#fetched-data").empty();
                var clicked_url = $(this).val();
                        
                $(this).siblings().removeClass('active')
                $(this).addClass('active');
                $.ajax({
                    url: clicked_url,
                    type: 'GET',
                    data: {},
                    success: function(data){

                        $.each( data.data, function( key, value ) {
                            ++item;
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td><span class="waybill-no text-danger">'+value.waybill_no+'</span></td>'
                                    +'<td class="text-bold-500">'+value.courier+'</td>'
                                    +'<td><span class="btn btn-outline-primary btn-xs round waves-effect">'+value.origin+'</span></td>'
                                    +'<td><span class="btn btn-outline-success btn-xs round waves-effect">'+value.destination+'</span></td>'
                                    +'<td class="text-bold-500 h6 text-danger">'+outbound_status(value.action_id)+'</td>'
                                    +'<td>'+value.qty+'</td>'
                                    +'<td><a href="'+url+'/waybills/view/'+value.waybill_no+'" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">View Package</a></td>'
                                +'</tr>'
                            );
                        });
                        $(".data-loading").hide();
                                
                        $("#to-records").text(data.to);
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }
                });
            });
        });
    </script>
</body>
</html>