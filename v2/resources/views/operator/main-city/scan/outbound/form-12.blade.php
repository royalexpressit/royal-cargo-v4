<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Outbound Actions</title>
    <!-- form 12 -->
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    @include('operator.main-city.scan.outbound.global')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Outbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('outbound') }}">Outbound</a>
                                    </li>
                                    <li class="breadcrumb-item active">Scan</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="mb-1 breadcrumb-right">
                            <strong>စစ်ဆေးခြင်း - </strong>
                            ရုံးသို့ <span class="badge badge-light-danger filtered-pkg-branch">---</span> /
                            စုစုပေါင်း<span class="badge badge-light-danger filtered-pkg-qty">0</span>ထုပ်
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">စာထွက်လုပ်ဆောင်မှုများ</h4>
                                    <span data-bs-toggle="modal" data-bs-target="#fullscreenModal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-help-circle"><circle cx="12" cy="12" r="10"></circle><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                                    </span>
                                </div>
                                <hr>

                                @if($branch_type == 1)
                                <div class="card-body">
                                    <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                    <div class="mb-2">
                                        <a href="{{ url('scan/outbound') }}" class="btn btn-wide btn-relief-primary waves-effect">1.{!! $GLOBALS['shop_btn'] !!}</strong></a>
                                    </div>
                                    <div class="denied">
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">2.{!! $GLOBALS['outbound_btn_2'] !!}</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">3.{!! $GLOBALS['outbound_btn_3'] !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">4.{!! $GLOBALS['outbound_btn_4'] !!}</span>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="card-body">
                                    <h5 class="text-primary">Branch In Actions</h5>
                                    <div class="mb-2">
                                        <a href="{{ url('scan/outbound') }}" class="btn btn-wide btn-relief-primary waves-effect disabled">1.{!! $GLOBALS['outbound_btn_1'] !!}</a>
                                    </div>
                                    
                                    <h5 class="text-primary">Branch Out Actions</h5>
                                    <div class="mb-half">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=2') }}" class="btn btn-wide btn-relief-primary waves-effect">2.{!! $GLOBALS['outbound_btn_2'] !!}</a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=3') }}" class="btn btn-wide btn-relief-secondary waves-effect">3.{!! $GLOBALS['outbound_btn_3'] !!}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-half">
                                        <a href="{{ url('scan/outbound?action=4') }}" class="btn btn-wide btn-relief-primary waves-effect">4.{!! $GLOBALS['outbound_btn_4'] !!}</a>
                                    </div>
                                </div>
                                @endif
                                
                                <div class="card-body">
                                   	<h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                    <div class="mb-half">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=5') }}" class="btn btn-wide btn-relief-primary waves-effect">5.{!! $GLOBALS['outbound_btn_5'] !!}</a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=6') }}" class="btn btn-wide btn-relief-primary waves-effect">6.{!! $GLOBALS['outbound_btn_6'] !!}</a>
                                            </div>
                                        </div>
                                    </div>
                                    @if($branch_type == 1)
                                    <div>
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/outbound?action=7&type=city') }}" class="btn btn-wide btn-relief-primary waves-effect">7.{!! $GLOBALS['outbound_btn_7'] !!}</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/outbound?action=8') }}" class="btn btn-wide btn-relief-secondary waves-effect">8.{!! $GLOBALS['outbound_btn_8'] !!}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <a href="{{ url('scan/outbound?action=9') }}" class="btn btn-wide btn-relief-primary waves-effect disabled">9.{!! $GLOBALS['outbound_btn_9'] !!}</a>
                                        </div>  
                                    </div>
                                    @else
                                    <div class="denied">
                                        <h5 class="text-primary">Branch Out Actions</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">7.{!! $GLOBALS['outbound_btn_7'] !!}</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">8.{!! $GLOBALS['outbound_btn_8'] !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">9.{!! $GLOBALS['outbound_btn_9'] !!}</span>
                                        </div>     
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                @if(!$packages->isEmpty())
                                <?php 
                                    $attrs = [];
                                    foreach ($packages as $key => $value) {
                                        $attrs[$value->to_city][] = $value;
                                    }
                                ?>
                                <div class="card-header">
                                    <h4 class="card-title text-primary w-100">
                                        CTF မထုတ်ရသေးသော အထုပ်များ
                                        <span class="pull-right text-warning font-medium-1 fw-light">Draft:
                                            <strong class="badge badge-light-warning font-medium-1" id="draft">0</strong>
                                        </span>
                                    </h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">ပို့မည့်မြို့အလိုက် ကြည့်ရန်</label>
                                                <select data-placeholder="မြို့ရွေးပါ..." class="select2 form-select" id="filtered">
                                                    <option>မြို့အားလုံးကြည့်ရန်</option>
                                                    @foreach ($attrs as $key => $b)
                                                    <option value="1" data-icon="home">{{ $key }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">အထုပ်အမှတ်ဖြင့်ရှာရန်</label>
                                                <input type="text"  class="form-control" id="search-package" placeholder="PG123456" />
                                            </div>
                                        </div>
                                    </div>    
                                    <div class="card card-transaction package-list">
                                        <span class="form-label text-primary">အထုပ်စာရင်းများ</span>
                                        <div class="draft-pkg-lists">
                                            @foreach($packages as $key => $package)
                                            @php ++$row;++$key @endphp

                                            <div class="transaction-item items list-item item-{{ $package->id }} {{ $package->to_city }} border-warning">
                                                <div class="drag-item-{{ $package->id }} w-100">
                                                    <div class=" w-100">
                                                        <div class="transaction-percentage ">
                                                            <h6 class="transaction-title font-medium-5 m-0 text-warning">
                                                                {{ $package->package_no }}
                                                                <span class="font-small-3 pull-right">
                                                                    <span class="text-muted">ရက်စွဲ :</span> {{ $package->created_at }}
                                                                </span>
                                                            </h6>
                                                            <div class="text-primary ">
                                                                <span class="text-muted">မြို့သို့ </span>  <span class="badge badge-light-warning">{{ $package->to_city }}</span> ,
                                                                <span class="text-muted">စာ </span> <button type="button" class="btn btn-xss btn-danger waves-effect waves-float waves-light badge btn-count item-{{ $package->package_no }}" id="{{ $package->package_no }}">0</button> <span class="text-muted">ခု</span> 
                                                                <div class="pull-right font-small-4 m--t-4">
                                                                    <button type="button" class="btn btn-sm btn-success waves-effect waves-float waves-light add-btn" id="{{ $package->id }}" alt="{{ $package->to_city }}"><i data-feather='truck'></i></button>
                                                                    <a href="{{ url('packages/view/'.$package->package_no) }}" class="btn btn-sm btn-primary waves-effect waves-float waves-light" target="_blank"><i data-feather='eye'></i></a>
                                                                    <button type="button" class="btn btn-sm btn-warning waves-effect waves-float waves-light select-pkg" data-bs-toggle="modal" data-bs-target="#transferred" data-bs-toggle="tooltip" data-bs-placement="bottom" value="{{ $package->id }}" alt="{{ $package->to_city_id }}"><i data-feather='repeat'></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="bg-light-primary mt-6p border-rounded px-1 h-24">
                                                                <small><i data-feather='{{ $package->icon }}'></i> {{ $package->type }}</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            <input type="hidden" id="draft-total" value="{{ $key }}">
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="card-header">
                                    <h4 class="card-title text-primary w-100">
                                        CTF မထုတ်ရသေးသော အထုပ်များ
                                        
                                    </h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="demo-spacing-0">
                                        <div class="alert alert-warning alert-validation-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                <span>အထုပ်များ မရှိသေးပါ</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary w-100">CTF ထုတ်မည့် အထုပ်များ 
                                        <span class="pull-right text-success font-medium-1 fw-light">Added:
                                            <strong class="badge badge-light-success font-medium-1" id="listed">0</strong>
                                        </span>
                                    </h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="mb-1 export-btn-box hide">
                                        <textarea class="form-control hide" id="multi_scanned_waybills"></textarea>
                                        <button type="button" class="btn btn-sm btn-relief-primary waves-effect ctf-btn">CTF ထုတ်မည်</button> 
                                        <button type="button" class="btn btn-sm btn-relief-danger waves-effect clear-btn">စာရင်းရှင်းလင်းမည်</button>                                       
                                    </div>
                                    <div class="card-transaction">
                                        <div class="demo-spacing-0">
                                            @if(!$packages->isEmpty())
                                            <div class="alert alert-warning alert-validation-msg show-msg" role="alert">
                                                <div class="alert-body d-flex align-items-center">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                    <span>CTF ထုတ်ရန် အထုပ်များကိုထည့်ပါ</span>
                                                </div>
                                            </div>
                                            @else
                                            <div class="alert alert-warning alert-validation-msg show-msg" role="alert">
                                                <div class="alert-body d-flex align-items-center">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                    <span>ပစ္စည်းများအား အထုပ်အရင်ထုပ်ပါ။</span>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="show-ctf hide">
                                            <div class="row">
                                                <div class="col-md-3"><img class="mt-6p border-rounded" src="{{ asset('_dist/images/qr-code.png') }}" style="height:88px;text-align: center;display: block;padding:0px 4px"></div>
                                                <div class="col-md-9">
                                                    <div class="card shadow-none bg-transparent border-success mt-6p">
                                                        <div class="px-1 pt-1">
                                                            <span class="text-success">CTF No: (စာရင်းအမှတ်စဉ်)</span>
                                                            <h2 id="ctf_no" class="fs-30 text-success">000000</h2>
                                                        </div>
                                                    </div>     
                                                </div>
                                                <div class="col-md-12">
                                                    <a href="#" class="btn btn-relief-primary waves-effect print-url" target="_blank">စာရင်အမှတ်အား Print ထုတ်ပါ</a> 
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div  id="scanned-lists">
                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="row" value="{{ $row }}">
    <input type="hidden" id="selected_pkg" value="0">

    <!-- Modal -->
    <div class="modal fade modal-danger text-start" id="transferred" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-warning" id="myModalLabel120">မြို့ပြန်ပြောင်းမည်</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label class="form-label text-primary" for="select2-icons">မြို့ရွေးပါ</label>
                    <select data-placeholder="Select a branch..." class="select2 form-select" id="to_city">
                        @foreach($cities as $key => $city)
                        <option value="{{ $city->id }}" >{{ $city->shortcode.' - '.$city->mm_name.' ('.$city->state.')' }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning change-city btn-city" data-bs-dismiss="modal">ပြောင်းမည်</button>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/bundles/main-city/m-508558.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
</body>
</html>