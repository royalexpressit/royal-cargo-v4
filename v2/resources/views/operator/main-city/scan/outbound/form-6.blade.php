<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Outbound Actions</title>
    <!-- form 6 -->
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    @include('operator.main-city.scan.outbound.global')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Outbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('outbound') }}">Outbound</a>
                                    </li>
                                    <li class="breadcrumb-item active">Scan</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <button class="btn btn-sm btn-outline-danger unknow-btn" data-bs-toggle="modal" data-bs-target="#small">
                            <i data-feather='layers'></i> <strong id="unknow-total">0000</strong> Waybills
                        </button>&nbsp;
                        <a href="{{ url('_dist/logs/outbound-received-failed.txt') }}" class="btn btn-sm btn-warning" download="outbound-received-failed-{{ date('ymd') }}.txt">
                            <i data-feather='download'></i> Failed Logs
                        </a>&nbsp;
                        <button type="button" class="btn btn-sm btn-success">
                            <i data-feather='activity'></i> <span class="checker">ပြီးမြောက်မှု စစ်ဆေးရန်</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">စာထွက်လုပ်ဆောင်မှုများ</h4>
                                    <span data-bs-toggle="modal" data-bs-target="#fullscreenModal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-help-circle"><circle cx="12" cy="12" r="10"></circle><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                                    </span>
                                </div>
                                <hr>

                                @if($branch_type == 1)
                                <div class="card-body">
                                    <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်) </h5>
                                    <div class="mb-2">
                                        <a href="{{ url('scan/outbound') }}" class="btn btn-wide btn-relief-primary waves-effect">1.{!! $GLOBALS['shop_btn'] !!}</a>
                                    </div>
                                    <div class="denied">
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">2.{!! $GLOBALS['outbound_btn_2'] !!}</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">3.{!! $GLOBALS['outbound_btn_3'] !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">4.{!! $GLOBALS['outbound_btn_4'] !!}</span>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="card-body">
                                    <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                    <div class="mb-2">
                                        <a href="{{ url('scan/outbound') }}" class="btn btn-wide btn-relief-primary waves-effect">1.{!! $GLOBALS['outbound_btn_1'] !!}</a>
                                    </div>
                                    
                                    <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                    <div class="mb-half">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=2') }}" class="btn btn-wide btn-relief-primary waves-effect">2.{!! $GLOBALS['outbound_btn_2'] !!}</a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=3') }}" class="btn btn-wide btn-relief-secondary waves-effect">3.{!! $GLOBALS['outbound_btn_3'] !!}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-half">
                                        <a href="{{ url('scan/outbound?action=4') }}" class="btn btn-wide btn-relief-primary waves-effect">4.{!! $GLOBALS['outbound_btn_4'] !!}</a>
                                    </div>
                                </div>
                                @endif
                                <hr>
                                
                                <div class="card-body">
                                   <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                    <div class="mb-half">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=5') }}" class="btn btn-wide btn-relief-primary waves-effect">5.{!! $GLOBALS['outbound_btn_5'] !!}</a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=6') }}" class="btn btn-wide btn-relief-primary waves-effect disabled">6.{!! $GLOBALS['outbound_btn_6'] !!}</a>
                                            </div>
                                        </div>
                                    </div>

                                    @if($branch_type == 1)
                                    <div>
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/outbound?action=7&type=city') }}" class="btn btn-wide btn-relief-primary waves-effect">7.{!! $GLOBALS['outbound_btn_7'] !!}</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/outbound?action=8') }}" class="btn btn-wide btn-relief-secondary waves-effect">8.{!! $GLOBALS['outbound_btn_8'] !!}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <a href="{{ url('scan/outbound?action=9&type=city') }}" class="btn btn-wide btn-relief-primary waves-effect">9.{!! $GLOBALS['outbound_btn_9'] !!}</a>
                                        </div>    
                                    </div>
                                    @else
                                    <div class="denied">
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">7.{!! $GLOBALS['outbound_btn_7'] !!}</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">8.{!! $GLOBALS['outbound_btn_8'] !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">9.{!! $GLOBALS['outbound_btn_9'] !!}</span>
                                        </div>     
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card" id="card-block">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">မိမိရုံးသို့ဝင်လာသော CTF များ</h4>
                                    <button type="button" class="btn btn-relief-primary waves-effect show-ctf btn-sm">Show/Hide CTFs</button>
                                </div>
                                <hr>
                                <div class="card-body ctf-items">
                                    @if(!$ctfs->isEmpty())
                                    @php 
                                        $attrs = [];
                                        foreach ($ctfs as $key => $value) {
                                            $attrs[$value->from_branch_name][] = $value->value;
                                        }
                                    @endphp
                                    <div class="row">
                                        <div class="col-md-6">
                                           <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">ရုံးအလိုက် ကြည့်ရန်</label>
                                                <select data-placeholder="Select a state..." class="select2 form-select" id="filtered">
                                                    <option>ရုံးခွဲအားလုံးကြည့်ရန်</option>
                                                    @foreach ($attrs as $key => $b)
                                                    <option value="1">{{ $key }}</option>
                                                    @endforeach
                                                </select>
                                            </div> 
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label text-primary" for="select2-icons">စာရင်းအမှတ်ဖြင့်ရှာရန်</label>
                                                <input type="text"  class="form-control" id="search-ctf" placeholder="AA123456" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-transaction package-list">
    									<span class="form-label text-primary">CTF Lists</span>
                                        @foreach($ctfs as $ctf)
                                            @php ++$row @endphp
    										<div class="transaction-item list-item ctf-item-{{ $ctf->id }} {{ $ctf->from_branch_name }} border-warning">
    											<div class=" drag-item-{{ $ctf->id }} w-100">
    												<div class="">
    													<div class="transaction-percentage ">
    														<h6 class="transaction-title font-medium-5 m-0 text-warning">
                                                                {{ $ctf->ctf_no }}
                                                                <span class="font-small-3 pull-right">
                                                                    <span class="text-muted">ရက်စွဲ :</span> {{ $ctf->created_at }}
                                                                </span>
                                                            </h6>
    														<div class="text-primary ">
                                                                <span class="text-muted">ရုံးခွဲမှ </span>  <span class="badge badge-light-warning">{{ $ctf->from_branch_name }}</span> ,
                                                                <span class="text-muted">အထုပ် </span> <span class="badge badge-light-warning">{{ $ctf->total }}</span> <span class="text-muted">ထုပ်</span> 
                                                                <div class="pull-right font-small-4 m--t-4">
                                                                    @if(date('Y-m-d') > date('Y-m-d', strtotime($ctf->created_at)))
                                                                    <span class="badge badge-light-warning">ရက်ကျော်</span>
                                                                    @endif
                                                                    <span data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="အထုပ်များကြည့်မည်"><button type="button" class="btn btn-sm btn-relief-success waves-effect waves-float waves-light show-pkg ctf-{{ $ctf->id }}" id="{{ $ctf->id }}"><i data-feather='eye'></i></button></span>
                                                                </div>
                                                            </div>
    													</div>
    												</div>
    											</div>
    										</div>
    								    @endforeach
          							</div>
                                    @else
                                    <div class="demo-spacing-0">
                                        <div class="alert alert-warning alert-validation-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                <span>No CTFs found.</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="card package-block hide">
                                <div class="card-header p--8">
                                    <h4 class="card-title text-primary w-100">
                                        <span class="lh--40">Scan ဖတ်ရန်ကျန်ရှိသည့် အထုပ်များ</span>
                                        <span class="pull-right text-success font-medium-1 fw-light mt--6">Pkg(Qty):
                                            <strong id="total-pkg" class="badge badge-light-success font-medium-1" id="listed">0</strong>
                                        </span>
                                    </h4>
                                </div>
                                <hr>
                                <div class="card-body">
									<div class="card card-transaction mxh-400">
										<div id="fetched-packages">
										</div>
      								</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Listed Waybills</h4>
                                    <span class="pull-right text-success font-medium-1 fw-light">Waybills(Qty):
                                    <strong id="count" class="badge badge-light-success font-medium-1" id="listed">0</strong>
                                </span>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="alert alert-warning alert-validation-msg get-started" role="alert">
                                        <div class="alert-body d-flex align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                            <span>Scan ဖတ်ထားသောစာအမှတ်များ မရှိသေးပါ။</span>
                                        </div>
                                    </div>
                                    <div class="mt--10">
                                        <div class="mb-1 waybill-form hide">
                                            <label class="form-label text-primary w-100" for="select2-icons">
                                                စာအမှတ် (scan ဖတ်ပါ)
                                                <i data-feather='check-circle' class="pull-right text-success list-icon w-passed hide"></i>
                                                <i data-feather='x-circle' class="pull-right text-danger list-icon w-failed hide"></i>
                                            </label>
                                            <input type="text" id="waybill" class="form-control" placeholder="Z12345678" />
                                        </div>
                                        <div class="data-loading text-center hide mb-1">
                                            <div class="demo-spacing-0">
                                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                                    <div class="alert-body">
                                                        <h5>Loading ...</h5>
                                                        <div class="spinner-grow text-warning me-1" role="status">
                                                            <span class="visually-hidden">Loading...</span>
                                                        </div>
                                                        <div class="spinner-grow text-danger me-1" role="status">
                                                            <span class="visually-hidden">Loading...</span>
                                                        </div>
                                                        <div class="spinner-grow text-success me-1" role="status">
                                                            <span class="visually-hidden">Loading...</span>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-group" id="fetched-package-waybills">
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="row" value="{{ $row }}">
    <input type="hidden" id="selected_ctf" value="0">
    <input type="hidden" id="is_sorting" value="{{ branch(user()->branch_id)['is_sorting'] }}">
    <input type="hidden" id="date" value="{{ get_date() }}">
    <!-- Modal -->
    <div class="modal fade modal-danger text-start" id="transferred" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary" id="myModalLabel120">Change Branch</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>မိမိပြောင်းလဲလိုသော ရုံးရွေးပါ</p>
                    <label class="form-label text-primary" for="select2-icons">Choose Branch</label>
                    <select data-placeholder="Select a branch..." class="select2-icons form-select" id="to_branch">
                        @foreach($branches as $key => $branch)
                        <option value="{{ $branch->id }}" data-icon="user">{{ $branch->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary change-branch btn-branch" data-bs-dismiss="modal">Change</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade text-start" id="small" tabindex="-1" aria-labelledby="myModalLabel19" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel19">Unknow Waybills</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h1 class="text-center text-danger display-1"><span class="unknow-count">0000</span></h1>
                    <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
                        <div class="alert-body d-flex align-items-center">
                            <span>စာပို့သမားနှင့်ရုံးခွဲ အချက်အလက် မသိသောစာများ</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{ url('unknown/outbound') }}" class="btn btn-primary" target="_blank">စာများကြည့်ရန်</a>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/bundles/main-city/m-062980.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
</body>
</html>