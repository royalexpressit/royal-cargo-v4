<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - </title>

    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.main-city.header')

    <div class="app-content content ">
      	<div class="content-overlay"></div>
      	<div class="header-navbar-shadow"></div>
      	<div class="content-wrapper container-xxl p-0">
	        <div class="content-body">
				<section class="basic-select2">
				  	<div class="row">
				    	<div class="col-md-4">
					      	<div class="card">
					        	<div class="card-header">
					          		<h4 class="card-title text-primary">Outbound Actions</h4>
					        	</div>
					        	<div class="card-body">
					            	<h5 class="">Branch In Actions</h5>
					            	<div class="mb-2">
					                	<a href="{{ url('scan/outbound') }}"class="btn btn-wide btn-relief-primary waves-effect">Branch In From Courier/Counter</a>
					            	</div>

					            	<h5>Branch Out Actions</h5>
					            	<div class="mb-half">
					                	<a href="{{ url('scan/outbound?action=create-package') }}" class="btn btn-wide btn-relief-primary waves-effect">Create Waybill Package</a>
					            	</div>
					            	<div class="mb-half">
					                	<a href="{{ url('scan/outbound?action=generate-ctf') }}" class="btn btn-wide btn-relief-primary waves-effect">Export Package CTF</a>
					            	</div>
					        	</div>
					      	</div>
					    </div>
					    <div class="col-md-4">
					      <div class="card">
					        <div class="card-header">
					          <h4 class="card-title">Draft Packages</h4>
					        </div>
					        <div class="card-body">
					            <!-- Icons -->
					            <div class="mb-1">
					                <label class="form-label" for="select2-icons">Filtered By Branch</label>
					                <select data-placeholder="Select a state..." class="select2-icons form-select" id="select2-icons">
					                    <option value="1" data-icon="user">Branch</option>
					                </select>
					            </div>
					             Packages
					            <div class="mb-1">
					                <div class="row">
					                    <div class="col-md-10">
					                        <div class="btn-group w-100" role="group" aria-label="Basic radio toggle button group">
					                        <button type="button" class="btn btn-outline-danger waves-effect wd-200 ">
					                          Danger
					                        </button>
					                        <button type="button" class="btn btn-outline-danger waves-effect active">
					                          YGN-Sorting
					                        </button>
					                      </div>
					                    </div>
					                    <div class="col-md-2">
					                        <button type="reset" class="btn btn-relief-primary waves-effect">+</button>
					                    </div>
					                </div>
					            </div>
					            <div class="mb-1">
					                <div class="row">
					                    <div class="col-md-10">
					                        <div class="btn-group w-100" role="group" aria-label="Basic radio toggle button group">
					                        <button type="button" class="btn btn-outline-danger waves-effect wd-200 ">
					                          Danger
					                        </button>
					                        <button type="button" class="btn btn-outline-danger waves-effect active">
					                          YGN-Sorting
					                        </button>
					                      </div>
					                    </div>
					                    <div class="col-md-2">
					                        <button type="reset" class="btn btn-relief-primary waves-effect">+</button>
					                    </div>
					                </div>
					            </div>
					        </div>
					      </div>
					    </div>
					    <div class="col-md-4">
					      <div class="card">
					        <div class="card-header">
					          <h4 class="card-title">Listed Packages</h4>
					        </div>
					        <div class="card-body">
					          <ul class="list-group">
					            <li class="list-group-item d-flex align-items-center">
					              <i class="me-1" data-feather="file-text" class="font-medium-2"></i>
					              <span>Item 1</span>
					              <span class="badge bg-primary rounded-pill ms-auto">4</span>
					            </li>
					            <li class="list-group-item d-flex align-items-center">
					              <i class="me-1" data-feather="file-text" class="font-medium-2"></i>
					              <span>Item 1</span>
					              <span class="badge bg-primary rounded-pill ms-auto">2</span>
					            </li>
					            <li class="list-group-item d-flex align-items-center">
					              <i class="me-1" data-feather="file-text" class="font-medium-2"></i>
					              <span>Item 1</span>
					              <span class="badge bg-primary rounded-pill ms-auto">1</span>
					            </li>
					          </ul>
					          <div class="mt-2">
					                <button type="reset" class="btn btn-relief-primary waves-effect">Generate CTF</button>
					            </div>
					        </div>
					      </div>
					    </div>
				  	</div>
				</section>
	        </div>
      	</div>
    </div>

	@include('customizer')
  	@include('footer')


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <!-- END: Page JS-->

    <script>
      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      })
    </script>
  </body>
  <!-- END: Body-->
</html>