<!-- Modal -->
    <div class="modal fade" id="fullscreenModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalFullTitle">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Accordion with margin start -->

  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Outbound Action Collections</h4>
        </div>
        <div class="card-body">
          <p class="card-text">
            To create accordion with margin use <code>.accordion-margin</code> class as a wrapper for your accordion
            header.
          </p>
          <div class="accordion accordion-margin" id="accordionMargin">
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingMarginOne">
                <button
                  class="accordion-button collapsed text-primary"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#accordionMarginOne"
                  aria-expanded="false"
                  aria-controls="accordionMarginOne"
                >
                  <strong class="px-1">Action 1:</strong> Collected Waybill
                </button>
              </h2>
              <div
                id="accordionMarginOne"
                class="accordion-collapse collapse"
                aria-labelledby="headingMarginOne"
                data-bs-parent="#accordionMargin"
              >
                <div class="accordion-body">
                  Pastry pudding cookie toffee bonbon jujubes jujubes powder topping. Jelly beans gummi bears sweet roll
                  bonbon muffin liquorice. Wafer lollipop sesame snaps. Brownie macaroon cookie muffin cupcake candy
                  caramels tiramisu. Oat cake chocolate cake sweet jelly-o brownie biscuit marzipan. Jujubes donut
                  marzipan chocolate bar. Jujubes sugar plum jelly beans tiramisu icing cheesecake.
                    <p>
                        <h5 class="text-primary">လုပ်ဆောင်ရမည့် ရုံးခွဲ(သို့)ဌာနများ</h5>
                        @foreach(fetched_branches(user()->city_id,0) as $branch)
                        <span class="badge bg-primary right">{{ $branch->name }}</span>
                        @endforeach
                    </p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingMarginTwo">
                <button
                  class="accordion-button text-primary collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#accordionMarginTwo"
                  aria-expanded="false"
                  aria-controls="accordionMarginTwo"
                >
                  <strong class="px-1">Action 2:</strong> Create Package
                </button>
              </h2>
              <div
                id="accordionMarginTwo"
                class="accordion-collapse collapse"
                aria-labelledby="headingMarginTwo"
                data-bs-parent="#accordionMargin"
              >
                <div class="accordion-body">
                  Sweet pie candy jelly. Sesame snaps biscuit sugar plum. Sweet roll topping fruitcake. Caramels
                  liquorice biscuit ice cream fruitcake cotton candy tart. Donut caramels gingerbread jelly-o
                  gingerbread pudding. Gummi bears pastry marshmallow candy canes pie. Pie apple pie carrot cake.
                    
                    <p>
                        <h4>မှတ်ချက်</h4>
                        @foreach(fetched_branches(user()->city_id,0) as $branch)
                        <span class="badge bg-primary right">{{ $branch->name }}</span>
                        @endforeach
                    </p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingMarginThree">
                <button
                  class="accordion-button text-primary collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#accordionMarginThree"
                  aria-expanded="false"
                  aria-controls="accordionMarginThree"
                >
                  <strong class="px-1">Action 3:</strong> Edit Package
                </button>
              </h2>
              <div
                id="accordionMarginThree"
                class="accordion-collapse collapse"
                aria-labelledby="headingMarginThree"
                data-bs-parent="#accordionMargin"
              >
                <div class="accordion-body">
                  Tart gummies dragée lollipop fruitcake pastry oat cake. Cookie jelly jelly macaroon icing jelly beans
                  soufflé cake sweet. Macaroon sesame snaps cheesecake tart cake sugar plum. Dessert jelly-o sweet
                  muffin chocolate candy pie tootsie roll marzipan. Carrot cake marshmallow pastry. Bonbon biscuit
                  pastry topping toffee dessert gummies. Topping apple pie pie croissant cotton candy dessert tiramisu.
                    <p>
                        <h4>မှတ်ချက်</h4>
                        @foreach(fetched_branches(user()->city_id,0) as $branch)
                        <span class="badge bg-primary right">{{ $branch->name }}</span>
                        @endforeach
                    </p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingMarginFour">
                <button
                  class="accordion-button text-primary collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#accordionMarginFour"
                  aria-expanded="false"
                  aria-controls="accordionMarginFour"
                >
                  <strong class="px-1">Action 4:</strong>Create CTF
                </button>
              </h2>
              <div
                id="accordionMarginFour"
                class="accordion-collapse collapse"
                aria-labelledby="headingMarginFour"
                data-bs-parent="#accordionMargin"
              >
                <div class="accordion-body">
                  Cheesecake muffin cupcake dragée lemon drops tiramisu cake gummies chocolate cake. Marshmallow tart
                  croissant. Tart dessert tiramisu marzipan lollipop lemon drops. Cake bonbon bonbon gummi bears topping
                  jelly beans brownie jujubes muffin. Donut croissant jelly-o cake marzipan. Liquorice marzipan cookie
                  wafer tootsie roll. Tootsie roll sweet cupcake.
                  <p>
                        <h4>မှတ်ချက်</h4>
                        @foreach(fetched_branches(user()->city_id,0) as $branch)
                        <span class="badge bg-primary right">{{ $branch->name }}</span>
                        @endforeach
                    </p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingMarginFour">
                <button
                  class="accordion-button text-primary collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#accordionMarginFour"
                  aria-expanded="false"
                  aria-controls="accordionMarginFour"
                >
                  <strong class="px-1">Action 4:</strong>Create CTF
                </button>
              </h2>
              <div
                id="accordionMarginFour"
                class="accordion-collapse collapse"
                aria-labelledby="headingMarginFour"
                data-bs-parent="#accordionMargin"
              >
                <div class="accordion-body">
                  Cheesecake muffin cupcake dragée lemon drops tiramisu cake gummies chocolate cake. Marshmallow tart
                  croissant. Tart dessert tiramisu marzipan lollipop lemon drops. Cake bonbon bonbon gummi bears topping
                  jelly beans brownie jujubes muffin. Donut croissant jelly-o cake marzipan. Liquorice marzipan cookie
                  wafer tootsie roll. Tootsie roll sweet cupcake.
                  <p>
                        <h4>မှတ်ချက်</h4>
                        @foreach(fetched_branches(user()->city_id,0) as $branch)
                        <span class="badge bg-primary right">{{ $branch->name }}</span>
                        @endforeach
                    </p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingMarginFour">
                <button
                  class="accordion-button text-primary collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#accordionMarginFour"
                  aria-expanded="false"
                  aria-controls="accordionMarginFour"
                >
                  <strong class="px-1">Action 4:</strong>Create CTF
                </button>
              </h2>
              <div
                id="accordionMarginFour"
                class="accordion-collapse collapse"
                aria-labelledby="headingMarginFour"
                data-bs-parent="#accordionMargin"
              >
                <div class="accordion-body">
                  Cheesecake muffin cupcake dragée lemon drops tiramisu cake gummies chocolate cake. Marshmallow tart
                  croissant. Tart dessert tiramisu marzipan lollipop lemon drops. Cake bonbon bonbon gummi bears topping
                  jelly beans brownie jujubes muffin. Donut croissant jelly-o cake marzipan. Liquorice marzipan cookie
                  wafer tootsie roll. Tootsie roll sweet cupcake.
                  <p>
                        <h4>မှတ်ချက်</h4>
                        @foreach(fetched_branches(user()->city_id,0) as $branch)
                        <span class="badge bg-primary right">{{ $branch->name }}</span>
                        @endforeach
                    </p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingMarginFour">
                <button
                  class="accordion-button text-primary collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#accordionMarginFour"
                  aria-expanded="false"
                  aria-controls="accordionMarginFour"
                >
                  <strong class="px-1">Action 4:</strong>Create CTF
                </button>
              </h2>
              <div
                id="accordionMarginFour"
                class="accordion-collapse collapse"
                aria-labelledby="headingMarginFour"
                data-bs-parent="#accordionMargin"
              >
                <div class="accordion-body">
                  Cheesecake muffin cupcake dragée lemon drops tiramisu cake gummies chocolate cake. Marshmallow tart
                  croissant. Tart dessert tiramisu marzipan lollipop lemon drops. Cake bonbon bonbon gummi bears topping
                  jelly beans brownie jujubes muffin. Donut croissant jelly-o cake marzipan. Liquorice marzipan cookie
                  wafer tootsie roll. Tootsie roll sweet cupcake.
                  <p>
                        <h4>မှတ်ချက်</h4>
                        @foreach(fetched_branches(user()->city_id,0) as $branch)
                        <span class="badge bg-primary right">{{ $branch->name }}</span>
                        @endforeach
                    </p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingMarginFour">
                <button
                  class="accordion-button text-primary collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#accordionMarginFour"
                  aria-expanded="false"
                  aria-controls="accordionMarginFour"
                >
                  <strong class="px-1">Action 4:</strong>Create CTF
                </button>
              </h2>
              <div
                id="accordionMarginFour"
                class="accordion-collapse collapse"
                aria-labelledby="headingMarginFour"
                data-bs-parent="#accordionMargin"
              >
                <div class="accordion-body">
                  Cheesecake muffin cupcake dragée lemon drops tiramisu cake gummies chocolate cake. Marshmallow tart
                  croissant. Tart dessert tiramisu marzipan lollipop lemon drops. Cake bonbon bonbon gummi bears topping
                  jelly beans brownie jujubes muffin. Donut croissant jelly-o cake marzipan. Liquorice marzipan cookie
                  wafer tootsie roll. Tootsie roll sweet cupcake.
                  <p>
                        <h4>မှတ်ချက်</h4>
                        @foreach(fetched_branches(user()->city_id,0) as $branch)
                        <span class="badge bg-primary right">{{ $branch->name }}</span>
                        @endforeach
                    </p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingMarginFour">
                <button
                  class="accordion-button text-primary collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#accordionMarginFour"
                  aria-expanded="false"
                  aria-controls="accordionMarginFour"
                >
                  <strong class="px-1">Action 4:</strong>Create CTF
                </button>
              </h2>
              <div
                id="accordionMarginFour"
                class="accordion-collapse collapse"
                aria-labelledby="headingMarginFour"
                data-bs-parent="#accordionMargin"
              >
                <div class="accordion-body">
                  Cheesecake muffin cupcake dragée lemon drops tiramisu cake gummies chocolate cake. Marshmallow tart
                  croissant. Tart dessert tiramisu marzipan lollipop lemon drops. Cake bonbon bonbon gummi bears topping
                  jelly beans brownie jujubes muffin. Donut croissant jelly-o cake marzipan. Liquorice marzipan cookie
                  wafer tootsie roll. Tootsie roll sweet cupcake.
                  <p>
                        <h4>မှတ်ချက်</h4>
                        @foreach(fetched_branches(user()->city_id,0) as $branch)
                        <span class="badge bg-primary right">{{ $branch->name }}</span>
                        @endforeach
                    </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Accordion with margin end -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-label-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>