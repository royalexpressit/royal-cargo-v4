<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Outbound Actions</title>
    <!-- form 10 -->
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
 <body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Outbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('outbound') }}">Outbound</a>
                                    </li>
                                    <li class="breadcrumb-item active">Scan</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <button type="button" class="btn btn-sm btn-primary dropdown-toggle pull-right" 
                            data-bs-toggle="dropdown" aria-expanded="false" >
                            <span class="limit-lbl">Setting Limit: 20</span>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item btn-limit" href="#" alt="10">10 Waybills</a>
                            <a class="dropdown-item btn-limit" href="#" alt="20">20 Waybills</a>
                            <a class="dropdown-item btn-limit" href="#" alt="30">30 Waybills</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">စာထွက်လုပ်ဆောင်မှုများ</h4>
                                    <span data-bs-toggle="modal" data-bs-target="#fullscreenModal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-help-circle"><circle cx="12" cy="12" r="10"></circle><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                                    </span>
                                </div>
                                <hr>

                                @if(is_sorting(user()->branch_id) == 1)
                                <div class="card-body">
                                    <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                    <div class="mb-2">
                                        <a href="{{ url('scan/outbound') }}" class="btn btn-wide btn-relief-primary waves-effect">1.Shop.com မှရရှိထားသော <strong class="block">စာများ စာရင်းသွင်းမည်</strong></a>
                                    </div>
                                    <div class="denied">
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">2.ရုံးခွဲ(သို့)စာစီဌာနသို့ <strong class="block">အထုပ်ပို့မည်</strong></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">3.ရှိပြီးသား အထုပ်ထဲ<strong class="block">စာထပ်ထည့်မည်</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">4.ထုပ်ထားသော အထုပ်များအား <strong class="block">စာရင်းဖြင့်ပို့မည်</strong></span>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="card-body">
                                    <h5 class="text-primary">Branch In Actions</h5>
                                    <div class="mb-2">
                                        <a href="{{ url('scan/outbound') }}" class="btn btn-wide btn-relief-primary waves-effect disabled">1.Branch In From <strong class="block">Courier/Counter</strong></a>
                                    </div>
                                    
                                    <h5 class="text-primary">Branch Out Actions</h5>
                                    <div class="mb-half">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=2') }}" class="btn btn-wide btn-relief-primary waves-effect">2.Create Waybill <strong class="block">Package</strong> </a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=3') }}" class="btn btn-wide btn-relief-secondary waves-effect">3.Edit Waybill <strong class="block">Package</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-half">
                                        <a href="{{ url('scan/outbound?action=4') }}" class="btn btn-wide btn-relief-primary waves-effect">4.Export Package <strong class="block">CTF</strong></a>
                                    </div>
                                </div>
                                @endif

                                
                                <div class="card-body">
                                   <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                    <div class="mb-half">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=5') }}" class="btn btn-wide btn-relief-primary waves-effect">5.စာအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/outbound?action=6') }}" class="btn btn-wide btn-relief-primary waves-effect">6.စာရင်းအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                            </div>
                                        </div>
                                    </div>

                                    @if(is_sorting(user()->branch_id) == 1)
                                    <div>
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/outbound?action=7&type=city') }}" class="btn btn-wide btn-relief-primary waves-effect disabled">7.ရုံးခွဲ(သို့)အခြားမြို့သို့ <strong class="block">အထုပ်ပို့မည်</strong></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/outbound?action=8') }}" class="btn btn-wide btn-relief-secondary waves-effect">8.ရှိပြီးသား အထုပ်ထဲ<strong class="block">စာထပ်ထည့်မည်</strong></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <a href="{{ url('scan/outbound?action=9&type=city') }}" class="btn btn-wide btn-relief-primary waves-effect">9.ထုပ်ထားသော အထုပ်များအား <strong class="block">စာရင်းဖြင့်ပို့မည်</strong></a>
                                        </div>  
                                    </div>
                                    @else
                                    <div class="denied">
                                        <h5 class="text-primary">Branch Out Actions</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">7.Create Waybill <strong class="block">Package</strong></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">8.Edit Waybill <strong class="block">Package</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">9.Export Package <strong class="block">CTF</strong></span>
                                        </div>     
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title text-primary">
                                  ရုံးခွဲများသို့ပို့မည့်ပစ္စည်းထုပ်ရန်
                              </h4>
                              <a href="{{ url('scan/outbound?action=7&type=city') }}" class="btn btn-relief-primary waves-effect show-ctf btn-sm">အခြားမြို့များသို့ပို့ရန်</a>
                            </div>
                            <hr>
                            <div class="card-body">
                                <div class="mb-1">
                                    <label class="form-label text-primary w-100" for="select2-icons">အထုပ်ပို့မည့်ရုံးခွဲ <span class="text-danger">✶</span><span class="pull-right">ရုံး(<span id="to-branch">0</span>)</span></label>
                                    <select data-placeholder="Select a state..." class="select2 form-select" id="to_branch">
                                        @foreach($branches as $branch)
                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-1">
                                    <label class="form-label text-primary" for="select2-icons">အထုပ်အမျိုးအစား <span class="text-danger">✶</span></label>
                                    <select data-placeholder="အထုပ်အမျိုးအစား..." class="select2-icons form-select" id="type">
                                        @foreach($types as $type)
                                        <option value="{{ $type->id }}" data-icon="{{ $type->icon }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-1">
                                    <label class="form-label text-primary w-100" for="select2-icons">စာအမှတ် <span class="text-danger">✶</span><span class="check-number text-danger pull-right hide">နံပါတ်မှားနေသည်</span></label>
                                    <input type="text" id="waybill" class="form-control" placeholder="Z1234566789" />
                                </div>
                                <div class="mb-1">
                                    <div class="continued-pkg hide">
                                        <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
                                          <div class="alert-body d-flex align-items-center">
                                            <span>Added waybills into current package</span>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="remark">
                                       <label class="form-label text-primary" for="select2-icons">မှတ်ချက်</label>
                                       <textarea class="form-control" id="remark" rows="3" placeholder="Something..."></textarea>
                                   </div>
                                </div>
                                <div class="mb-0">
                                    <textarea class="form-control hide" id="multi_scanned_waybills"></textarea>
                                    <div class="row">
                                        <input type="hidden" id="action" value="save">
                                        <input type="hidden" id="package_id" value="0">
                                        <input type="hidden" id="package_no" value="0">
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-relief-primary waves-effect save-btn w-100 disabled" value="save">အထုပ်ပိတ်မည်</button>
                                        </div> 
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-relief-warning waves-effect continue-btn w-100 disabled" value="continue">ပစ္စည်းထပ်ထည့်မည်</button>
                                        </div>              
                                    </div>
                                    
                                    
                                </div>
                                <div class="mb-0">
                                    <small><span class="text-danger">✶</span> is required fields.</small>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">
                                        <span class="text-primary">Scanned</span> <span id="scanned" class="badge badge-light-primary me-1 count-badge">0</span> 
                                        <span class="text-success">Success</span> <span id="success" class="badge badge-light-success me-1 count-badge">0</span> 
                                        <span class="text-danger">Failed</span> <span id="failed" class="badge badge-light-danger count-badge">0</span>
                                    </h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="alert alert-warning alert-validation-msg get-started" role="alert">
                                        <div class="alert-body d-flex align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                            <span>Scan ဖတ်ထားသောစာများ မရှိသေးပါ။</span>
                                        </div>
                                    </div>
                                    <div class="show-package hide">
                                        <div class="alert alert-danger alert-validation-msg continue-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <i data-feather='folder-plus'></i>&nbsp;
                                                <span>ယခုအထုပ်ထဲ ပစ္စည်းထပ်ထည့်မည်။</span>
                                            </div>
                                        </div>
                                        <div class="alert alert-success alert-validation-msg close-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <i data-feather='folder-plus'></i>&nbsp;
                                                <span>ယခုအထုပ်အား ပိတ်လိုက်ပါပြီ။</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"><img class="mt-6p bg-light-secondary border-rounded" src="{{ asset('_dist/images/label.png') }}" style="height:88px;text-align: center;display: block;padding:0px 4px"></div>
                                            <div class="col-md-9">
                                                <div class="card shadow-none bg-transparent border-success mt-6p success-pkg">
                                                    <div class="px-1 pt-1">
                                                        <span class="text-success">Package No:(အထုပ်တွင်ရေးပါ)</span>
                                                        <h2 id="pkg_no" class="fs-30 text-success">000000</h2>
                                                    </div>
                                                </div>
                                                <div class="card shadow-none bg-transparent border-danger mt-6p error-pkg">
                                                    <div class="px-1 pt-1">
                                                        <span class="text-danger">Package No:(နံပါတ်မရရှိပါ)</span>
                                                        <h2 id="pkg_no" class="fs-30 text-danger">▧▧▧▧▧▧</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <ul class="list-group" id="scanned-lists">
                                    
                                    </ul>
                                    <ul class="list-group" id="failed-lists">
                                                                                                            
                                    </ul>
                                    <div class="alert alert-warning hide scroll-msg mt-1" role="alert">
                                        <div class="alert-body"><strong>Please scroll up/down to see more waybills.</strong></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')

    <!-- Modal -->
    <div class="modal fade modal-danger text-start" id="warning-msg" tabindex="-1" aria-labelledby="myModalLabel120" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel120">သတိပြုရမည့်အချက်</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p class="fs-16">Package ထုတ်ပါက system မှ ထုတ်ပေးသော package number <code>PGXXXXXX</code> အား
                    အထုပ်နှင့်တွဲထားသော label တွင် မဖြစ်မနေ ရေးရမည့်ကို သတိထားပေးပါ။</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">သိရှိပါပြီ</button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/bundles/main-city/m-681109.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>
</body>
</html>