<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Inbound Actions</title>
    <!-- form 11 -->
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">Inbound Actions</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    @if(is_sorting(user()->branch_id) == 1)
                                    <div>
                                        <h5 class="text-primary">Branch In Actions</h5>
                                        <div class="mb-half">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=1') }}" class="btn btn-wide btn-relief-success waves-effect">1.Branch In By <strong class="block">CTF</strong></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=2') }}" class="btn btn-wide btn-relief-success waves-effect">2.Branch In By <strong class="block">Waybill</strong></a>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-primary">Branch Out Actions</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=3&type=branch') }}" class="btn btn-wide btn-relief-success waves-effect">3.Create Waybill <strong class="block">Package</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=4') }}" class="btn btn-wide btn-relief-secondary waves-effect">4.Edit Waybill <strong class="block">Package</strong></a>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="mb-half">
                                            <a href="{{ url('scan/inbound?action=5') }}" class="btn btn-wide btn-relief-success waves-effect disabled">5.Export Package <strong class="block">CTF</strong></a>
                                        </div>
                                    </div>
                                    @else
                                    <div class="denied">
                                        <h5 class="text-primary">Branch In Actions</h5>
                                        <div class="mb-half">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">1.Branch In By <strong class="block">CTF</strong></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">2.Branch In By <strong class="block">Waybill</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-primary">Branch Out Actions</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">3.Create Waybill <strong class="block">Package</strong></span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">4.Edit Waybill <strong class="block">Package</strong></span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">5.Export Package <strong class="block">CTF</strong></span>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <hr>
                                <div class="card-body">
                                    @if(is_sorting(user()->branch_id) == 1)
                                    <div class="denied">
                                        <h5 class="text-primary">Branch In Actions</h5>
                                        <div class="mb-half">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">6.Branch In By <strong class="block">CTF</strong></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">7.Branch In By <strong class="block">Waybill</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-primary">Branch Out Actions</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-danger waves-effect btn-denied">8.Create Package <strong class="block">Package</strong></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-danger waves-effect btn-denied">9.Edit Waybill <strong class="block">Package</strong></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">10.Export Package <strong class="block">CTF</strong></span>
                                        </div>
                                    </div>
                                    @else
                                    <h5 class="text-primary">Branch In Actions</h5>
                                    <div class="mb-half">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=6') }}" class="btn btn-wide btn-relief-success waves-effect">6.Branch In By <strong class="block">CTF</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=7') }}" class="btn btn-wide btn-relief-success waves-effect">7.Branch In By <strong class="block">Waybill</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="text-primary">Branch Out Actions</h5>
                                    <div class="mb-half">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-success waves-effect">8.Create Package <strong class="block">Package</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-secondary waves-effect">9.Edit Waybill <strong class="block">Package</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-half">
                                        <span class="btn btn-wide btn-relief-success waves-effect">10.Export Package <strong class="block">CTF</strong></span>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                  <h4 class="card-title text-primary">CTF မထုတ်ရသေးသော အထုပ်များ</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                @if(!$packages->isEmpty())
                                <?php 
                                    $attrs = [];
                                    foreach ($packages as $key => $value) {
                                        $attrs[$value->to_city][] = $value->value;
                                    }
                                ?>
                                <div class="mb-1">
                                    <label class="form-label text-primary" for="select2-icons">ပို့မည့်မြို့အလိုက် ကြည့်ရန်</label>
                                    <select data-placeholder="Select a state..." class="select2-icons form-select" id="filtered">
                                        <option data-icon="search">မြို့အားလုံးကြည့်ရန်</option>
                                        @foreach ($attrs as $key => $b)
                                        <option value="1" data-icon="home">{{ $key }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                 
                                    <div class="card card-transaction mxh-400">
                                        <span class="form-label text-primary">အထုပ်စာရင်းများ</span>
               
                
                                        @foreach($packages as $package)
                                          <div class="transaction-item list-item item-{{ $package->id }} {{ $package->to_city }}">
                                            <div class=" drag-item-{{ $package->id }}">
                                            <div class="d-flex">
                                              <div class="avatar bg-light-warning rounded ">
                                                <div class="avatar-content">
                                                  
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-package avatar-icon font-medium-5"><line x1="16.5" y1="9.4" x2="7.5" y2="4.21"></line><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
                                                </div>
                                              </div>
                                              <div class="transaction-percentage ">
                                                <h6 class="transaction-title font-medium-5 m-0 text-warning">{{ $package->package_no }}</h6>
                                                <small class="text-primary"><small class="text-muted">ပို့မည့်မြို့ - </small>{{ $package->to_city }}</small>
                                              </div>
                                            </div>
                                            </div>
                                            <div class="fw-bolder">
                                                <button type="button" class="btn btn-relief-success waves-effect add-btn btn-sm font-medium-5" id="{{ $package->id }}">+</button>
                                            </div>
                                          </div>
                                        @endforeach
                                        
                
                                    </div>
                                 @else
                                            <div class="demo-spacing-0">
                                                <div class="alert alert-warning alert-validation-msg" role="alert">
                                                  <div class="alert-body d-flex align-items-center">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                    <span>အထုပ်များ မရှိသေးပါ</span>
                                                  </div>
                                                </div>
                                              </div>
                                        @endif   
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title text-primary">CTF ထုတ်မည့် အထုပ်များ 
                                </h4>
                                <span class="btn btn-outline-danger btn-sm waves-effect right">Qty: <strong id="listed">0</strong></span>
                              
                            </div>
                            <hr>
                            <div class="card-body">
                              
                                <div class="card-transaction">
                                    <div class="demo-spacing-0">
                                        @if(!$packages->isEmpty())
                                                <div class="alert alert-warning alert-validation-msg show-msg" role="alert">
                                                  <div class="alert-body d-flex align-items-center">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                    <span>CTF ထုတ်ရန် အထုပ်များကိုထည့်ပါ</span>
                                                  </div>
                                                </div>
                                        @else
                                        <div class="alert alert-warning alert-validation-msg show-msg" role="alert">
                                                  <div class="alert-body d-flex align-items-center">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                                    <span>ပစ္စည်းများအား အထုပ်အရင်ထုပ်ပါ။</span>
                                                  </div>
                                                </div>
                                        @endif
                                        </div>

                                    <div class="show-ctf hide">
                                        <div class="row">
                                            <div class="col-md-3"><img src="https://www.qrcode-monkey.com/img/default-preview-qr.svg" style="height:100px;text-align: center;display: block;"></div>
                                            <div class="col-md-9">
                                                <div class="card shadow-none bg-transparent border-primary mt-6p">
                                                    <div class="px-1 pt-1">
                                                        <span class="text-warning">CTF No:</span>
                                                        <h2 id="ctf_no" class="fs-30 text-primary">000000</h2>
                                                    </div>
                                                </div> 
                                                           
                                            </div>
                                            <div class="col-md-12">
                                                <a href="#" class="btn btn-relief-primary waves-effect print-url" target="_blank">Print QR Code</a> 
                                            </div>
                                        </div>

                                    </div>
                                
                                          <div  id="scanned-lists">
                                            
                                          </div>
                                          <div class="mt-1 export-btn-box hide">
                                            <textarea class="form-control hide" id="multi_scanned_waybills"></textarea>
                                            <button type="button" class="btn btn-relief-primary waves-effect ctf-btn">CTF ထုတ်မည်</button>                    
                                        </div>
                                </div>


                            </div>
                          </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="user_id" value="{{ user()->id }}">
            <input type="hidden" id="username" value="{{ user()->name }}">
            <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
            <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
            <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <!-- END: Page JS-->

    <script>
        

      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      });

      $(document).ready(function(){
        var url     = $("#url").val();
        var packages= [];
        var added   = 0;

        $(".add-btn").on("click",function search(e) {
            $('.export-btn-box').removeClass('hide');
            $('.show-ctf').addClass('hide');

            $('.show-msg').hide();
            id = this.id;
            $('.item-'+id).hide();
            ++added;

            //alert(id);
            packages.push(id);

            data = $('.drag-item-'+id).html();
            $("#scanned-lists").prepend('<div class="transaction-item">'+data+'</div>');
            
            $('#listed').text(added);
        });

      $('#filtered').on("change",function search(e) {
        branch = $("#filtered option:selected").text();
        if(branch == 'ရုံးခွဲအားလုံးကြည့်ရန်'){
            $('.list-item').removeClass('hidden'); 
        }else{
            $('.list-item').addClass('hidden');
            $('.'+branch).removeClass('hidden');
        }
        
        console.log(branch);
        });

      $(".ctf-btn").on("click",function search(e) {
            user_id         = $("#user_id").val();
            username        = $("#username").val();
            user_city_id    = $("#user_city_id").val();
            user_branch_id  = $("#user_branch_id").val();
                
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                       
            $.ajax({
                type: 'post',
                //url: url+'/api/action/ctf-export',
                url: url+'/api/action/inbound',
                dataType:'json',
                data: {
                    'packages'      :packages,
                    'user_id'       :user_id,
                    'username'      :username,
                    'user_city_id'  :user_city_id,
                    'user_branch_id':user_branch_id,
                    'action_id'     :9,
                },
                success: function(data) { 
                    console.log(data);
                    if(data.success == 1){
                        $(".show-ctf").removeClass('hide');
                        $("#ctf_no").text(data.ctf_no);
                        $(".print-url").attr("href", url+"/print/cft?qr="+data.ctf_no)
                    }

                    packages= [];
                    $("#scanned-lists").empty(); 
                    $("#failed-lists").empty();
                    $(".export-btn-box").addClass('hide');
                },
            });
            $(this).val(''); 
                
            added = 0;
            $('#listed').text(added);
        });

  });

    </script>
  </body>
  <!-- END: Body-->
</html>