<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Inbound Actions</title>
    <!-- form 12 -->
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
 <body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Inbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('inbound') }}">Inbound</a>
                                    </li>
                                    <li class="breadcrumb-item active">Scan</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <button type="button" class="btn btn-sm btn-primary dropdown-toggle pull-right" 
                            data-bs-toggle="dropdown" aria-expanded="false" >
                            <span class="limit-lbl">Setting Limit: 20</span>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item btn-limit" href="#" alt="10">10 Waybills</a>
                            <a class="dropdown-item btn-limit" href="#" alt="20">20 Waybills</a>
                            <a class="dropdown-item btn-limit" href="#" alt="30">30 Waybills</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="basic-select2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">စာဝင်လုပ်ဆောင်မှုများ</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    @if(is_sorting(user()->branch_id) == 1)
                                    <div>
                                        <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=1') }}" class="btn btn-wide btn-relief-success waves-effect">1.စာရင်းအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=2') }}" class="btn btn-wide btn-relief-success waves-effect">2.စာအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (စာစီဌာနအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=3&type=branch') }}" class="btn btn-wide btn-relief-success waves-effect disabled">3.ရုံးခွဲ(သို့)နယ်မြို့သို့ <strong class="block">အထုပ်ပို့မည်</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=4') }}" class="btn btn-wide btn-relief-secondary waves-effect">4.ရှိပြီးသား အထုပ်ထဲ<strong class="block">စာထပ်ထည့်မည်</strong></a>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="mb-half">
                                            <a href="{{ url('scan/inbound?action=5') }}" class="btn btn-wide btn-relief-success waves-effect">5.ထုပ်ထားသော အထုပ်များအား <strong class="block">စာရင်းဖြင့်ပို့မည်</strong></a>
                                        </div>
                                    </div>
                                    @else
                                    <div class="denied">
                                        <h5 class="text-primary">Branch In Actions</h5>
                                        <div class="mb-half">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">1.Branch In By <strong class="block">CTF</strong></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">2.Branch In By <strong class="block">Waybill</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-primary">Branch Out Actions</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">3.Create Waybill <strong class="block">Package</strong></span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">4.Edit Waybill <strong class="block">Package</strong></span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">5.Export Package <strong class="block">CTF</strong></span>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <hr>
                                <div class="card-body">
                                    @if(is_sorting(user()->branch_id) == 1)
                                    <div class="denied">
                                        <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">6.စာရင်းအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">7.စာအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-danger waves-effect btn-denied">8.ရုံးခွဲ(သို့)စာစီဌာနသို့ <strong class="block">အထုပ်ပို့မည်</strong></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-danger waves-effect btn-denied">9.ရှိပြီးသား အထုပ်ထဲ<strong class="block">စာထပ်ထည့်မည်</strong></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">10.ထုပ်ထားသော အထုပ်များအား <strong class="block">စာရင်းဖြင့်ပို့မည်</strong></span>
                                        </div>
                                    </div>
                                    @else
                                    <h5 class="text-primary">Branch In Actions</h5>
                                    <div class="mb-half">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=6') }}" class="btn btn-wide btn-relief-success waves-effect">6.Branch In By <strong class="block">CTF</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=7') }}" class="btn btn-wide btn-relief-success waves-effect">7.Branch In By <strong class="block">Waybill</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="text-primary">Branch Out Actions</h5>
                                    <div class="mb-half">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-success waves-effect">8.Create Package <strong class="block">Package</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-secondary waves-effect">9.Edit Waybill <strong class="block">Package</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-half">
                                        <span class="btn btn-wide btn-relief-success waves-effect">10.Export Package <strong class="block">CTF</strong></span>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title text-primary">
                                  မြို့များသို့ပို့မည့်ပစ္စည်းထုပ်ရန်
                                
                              </h4>
                              <a href="{{ url('scan/inbound?action=3&type=branch') }}" class="btn btn-relief-success waves-effect show-ctf btn-sm">ရုံးခွဲများသို့ပို့ရန်</a>
                            </div>
                            <hr>
                            <div class="card-body">
                                <div class="mb-1">
                                    <label class="form-label text-primary w-100" for="select2-icons">အထုပ်ပို့မည့်မြို့ <span class="text-danger">✶</span><span class="pull-right">မြို့(<span id="to-city">0</span>)</span></label>
                                    <select data-placeholder="မြို့ရွေးပါ..." class="select2 form-select" id="to_city">
                                        @foreach($cities as $city)
                                        <option value="{{ $city->id }}" >{{ $city->shortcode.' - '.$city->mm_name.' ('.$city->state.')' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-1">
                                    <label class="form-label text-primary" for="select2-icons">အထုပ်အမျိုးအစား <span class="text-danger">✶</span></label>
                                    <select data-placeholder="Select a state..." class="select2-icons form-select" id="type">
                                        @foreach($types as $type)
                                        <option value="{{ $type->id }}" data-icon="package">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-1">
                                    <label class="form-label text-primary w-100" for="select2-icons">စာအမှတ် <span class="text-danger">✶</span><span class="check-number text-danger pull-right hide">နံပါတ်မှားနေသည်</span></label>
                                    <input type="text" id="waybill" class="form-control" placeholder="Z1234566789" />
                                </div>
                                <div class="mb-1">
                                    <div class="continued-pkg hide">
                                        <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
                                          <div class="alert-body d-flex align-items-center">
                                            <span>Added waybills into current package</span>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="remark">
                                       <label class="form-label text-primary" for="select2-icons">မှတ်ချက်</label>
                                       <textarea class="form-control" id="remark" rows="3" placeholder="Something..."></textarea>
                                   </div>
                                </div>
                                <div class="mb-0">
                                    <textarea class="form-control hide" id="multi_scanned_waybills"></textarea>
                                    <div class="row">
                                        <input type="hidden" id="action" value="save">
                                        <input type="hidden" id="package_id" value="0">
                                        <input type="hidden" id="package_no" value="0">
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-relief-success waves-effect save-btn w-100 disabled" value="save">အထုပ်ပိတ်မည်</button>
                                        </div> 
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-relief-warning waves-effect continue-btn w-100 disabled" value="continue">ပစ္စည်းထပ်ထည့်မည်</button>
                                        </div>              
                                    </div>
                                </div>
                                <div class="mb-0">
                                    <small><span class="text-danger">✶</span> is required fields.</small>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">
                                        <span class="text-primary">Scanned</span> <span id="scanned" class="badge badge-light-primary me-1 count-badge">0</span> 
                                        <span class="text-success">Success</span> <span id="success" class="badge badge-light-success me-1 count-badge">0</span> 
                                        <span class="text-danger">Failed</span> <span id="failed" class="badge badge-light-danger count-badge">0</span>
                                    </h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="alert alert-warning alert-validation-msg get-started" role="alert">
                                        <div class="alert-body d-flex align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                            <span>Scan ဖတ်ထားသောစာများ မရှိသေးပါ။</span>
                                        </div>
                                    </div>
                                    <div class="show-package hide">
                                        <div class="row">
                                            <div class="col-md-3"><img class="mt-6p bg-light-secondary border-rounded" src="{{ asset('_dist/images/label.png') }}" style="height:88px;text-align: center;display: block;padding:0px 4px"></div>
                                            <div class="col-md-9">
                                                <div class="card shadow-none bg-transparent border-success mt-6p success-pkg">
                                                    <div class="px-1 pt-1">
                                                        <span class="text-success">Package No:(အထုပ်တွင်ရေးပါ)</span>
                                                        <h2 id="pkg_no" class="fs-30 text-success">000000</h2>
                                                    </div>
                                                </div>
                                                <div class="card shadow-none bg-transparent border-danger mt-6p error-pkg">
                                                    <div class="px-1 pt-1">
                                                        <span class="text-danger">Package No:(နံပါတ်မရရှိပါ)</span>
                                                        <h2 id="pkg_no" class="fs-30 text-danger">▧▧▧▧▧▧</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <ul class="list-group" id="scanned-lists">
                                    
                                    </ul>
                                    <ul class="list-group" id="failed-lists">
                                                                                                            
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    @include('customizer')
    @include('footer')
    <input type="hidden" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_city_name" value="{{ city(user()->city_id)['shortcode'] }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script> 
    <!-- END: Page JS-->

    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });

        $(document).ready(function(){
            var url     = $("#url").val();
            var scanned = 0;
            var success = 0;
            var failed  = 0;
            var waybills= [];
            var limit   = 20;

            var length = $('#to_city option').length;
            $("#to-city").text(length);

            var start_point   = $("#user_city_name").val();
            var end_point     = $("#to_city option:selected").text().substring(0,3);;
            var label         = start_point+' >> '+end_point;
            $("#remark").val(label);
            
            var voice   = 'alert-1.mp3';
            var city    = $("#city").val();
            var _token  = $("#_token").val();

            $("#form").submit(function(event){
                event.preventDefault();  
            });

            $("#waybill").on("keydown",function search(e){
                if(e.keyCode == 13) {
                    $(".save-btn,.continue-btn").removeClass('disabled');
                    waybill   = $("#waybill").val().toUpperCase();
                    if(waybill.length > 10){
                        $(".get-started").hide();
                        //valid length && continue

                        waybills.push(waybill);
                        console.log(waybills);

                        ++scanned;
                        $("#scanned").text(scanned);
                        $("#success").text(0);
                        $("#failed").text(0);
                                    
                        $("#failed-lists").empty();
                        $("#scanned-lists").show();
           
                        $(".scan-btn").removeAttr('disabled');
                        $("#scanned-lists").prepend('<li class="list-group-item d-flex align-items-center"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text me-1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>'+waybill+'</span>');
                        
                        $(this).val('');
                        $(".check-number").addClass('hide');     
                            
                        //limit scanned count with 25
                        if(scanned == 25){
                            $("#waybill").attr("disabled", true);
                            audioElement.play();
                            $('.bs-example-modal-center').modal('show');
                            setTimeout(function(){
                                $('.bs-example-modal-center').modal('hide');
                            },5000);
                            console.log(voice);
                        }
                    }else{
                        //invalid length && try again
                        $(".check-number").removeClass('hide');
                        $("#waybill").val('');
                    }

                    //removed fixed height for error lists
                    $("#failed-lists").removeClass('scanned-panel');
                    $(".show-package").addClass('hide');
                }
            });

            $(".save-btn,.continue-btn").on("click",function search(e) {
                //call api sent to server function
                $('.bs-example-modal-center-loading').modal('show');
                action = $(this).val();
                $('#action').val(action);
                data_send();
            });

            //scan code for continue action
            $("#continue-action").on("keydown",function search(e) {
                var code = $("#continue-action").val();
                if(e.keyCode == 13) {
                    if(code == 'continue-action'){
                        data_send();
                    }else{
                        $("#continue-action").val('');
                    }
                }
            });

            var data_send = function(){
                user_id         = $("#user_id").val();
                username        = $("#username").val();
                package_id      = $("#package_id").val();
                package_no      = $("#package_no").val();
                user_city_id    = $("#user_city_id").val();
                user_branch_id  = $("#user_branch_id").val();
                to_city_id      = $("#to_city").val();
                to_city_name    = $("#to_city option:selected").text();
                pkg_type        = $("#type").val();
                remark          = $("#remark").val();
                action          = $('#action').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                       
                $.ajax({
                    type: 'post',
                    url: url+'/api/action/outbound',
                    dataType:'json',
                    data: {
                        'waybills'      :waybills,
                        'user_id'       :user_id,
                        'username'      :username,
                        'user_city_id'  :user_city_id,
                        'user_branch_id':user_branch_id,
                        'package_id'    :package_id,
                        'package_no'    :package_no,
                        'to_city_id'    :to_city_id,
                        'to_city_name'  :to_city_name,
                        'pkg_type'      :pkg_type,
                        'remark'        :remark,
                        'action'        :action,
                        'package_id'    :package_id,
                        'action_id'     :5,
                    },
                    success: function(data) { 
                    $(".save-btn,.continue-btn").addClass('disabled');
                        if(data.package_id == 0){
                            $(".continued-pkg").hide;
                            $(".remark").show;
                            $(".success-pkg").addClass('hide');
                            $(".error-pkg").removeClass('hide');
                        }else{
                            Swal.fire({
                                position:"top-end",
                                title:"Good Job",
                                icon:"success",
                                text:"You has been created new package.",
                                showConfirmButton:!1,
                                timer:2000,
                                customClass:{confirmButton:"btn btn-primary"},
                                buttonsStyling:!1
                            });
                            $(".remark").hide;
                            $(".continued-pkg").show;
                            $(".success-pkg").removeClass('hide');
                            $(".error-pkg").addClass('hide');
                        }

                        if(data.option == 'save'){
                            $(".show-package").removeClass('hide');
                            $("#pkg_no").text(data.package_no);
                            $("#package_id").val(0);
                            $("#package_no").val(0);
                        }else{
                            $(".show-package").removeClass('hide');
                            $("#pkg_no").text(data.package_no);

                            $("#package_id").val(data.package_id);
                            $("#package_no").val(data.package_no);
                            //$(".show-package").addClass('hide');
                        }

                    setTimeout(function(){
                        $('.bs-example-modal-center-loading').modal('hide');
                    },1000);
                    $("#scanned-lists").removeClass('scanned-panel');
                    $("#scanned-lists").empty(); 
                    $("#failed-lists").empty();

                    success = $("#scanned").text() - data.failed.length;
                    failed  = data.failed.length;

                    //add scroll max size for item > 10
                    if(failed > 10){
                        $("#failed-lists").addClass('scanned-panel'); 
                    }

                    $("#success").text(success);
                    $("#failed").text(failed);

                    if(data.failed.length > 0 ){
                        for (i = 0; i < data.failed.length; i++) {
                            $("#failed-lists").prepend('<li class="list-group-item sm-item border-warning text-warning"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-frown"><circle cx="12" cy="12" r="10"></circle><path d="M16 16s-1.5-2-4-2-4 2-4 2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg> '+data.failed[i]+'</li>');
                        }
                    }else{
                        $("#failed-lists").prepend('<li class="list-group-item sm-item border-success text-success"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg> စာအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                    }
                },
            });
            $(this).val(''); 

            waybills=[];
            $('#multi_scanned_waybills').empty();
            $('.scan-btn').attr('disabled',true);
            $('.continue-action').hide();
            $("#waybill").attr("disabled", false);
            $('#waybill').trigger('focus');
            scanned = 0;
        }
        
        //limit alert audio background
        var audioElement = document.createElement('audio');
        audioElement.setAttribute('src', url+'/_dist/alerts/'+voice);



        $('#to_city').on("change",function search(e) {
            start_point   = $("#user_city_name").val();
            end_point     = $("#to_city option:selected").text().substring(0,3);
            
            label = start_point+' >> '+end_point;

            $("#remark").val(label);
        }); 

        $('.btn-limit').on("click",function search(e) {
            limit = $(this).attr('alt');
            $(".limit-lbl").text('Setting Limit: '+limit);
        });
    }); 
    </script>
</body>
</html>