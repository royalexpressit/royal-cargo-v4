<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - Inbound Actions</title>
    <!-- form 4 -->
    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">
    
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
    <!-- END: Custom CSS-->

  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">

    <!-- BEGIN: Header-->
    @include('operator.header')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Inbound</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('inbound') }}">Inbound</a>
                                    </li>
                                    <li class="breadcrumb-item active">Scan</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <a href="{{ url('_dist/logs/inbound-handover-failed.txt') }}" class="btn btn-sm btn-warning" download="inbound-handover-failed.txt">
                            <i data-feather='download'></i> Failed Logs
                        </a>&nbsp;
                        @if(user()->branch_id == 11)
                        <a href="{{ url('_dist/logs/scan-handover-ygn.txt') }}" class="btn btn-sm btn-info" download="inbound-handover-ygn.txt">
                            <i data-feather='download'></i> Scanned Logs
                        </a>&nbsp;
                        @endif
                        <button type="button" class="btn btn-sm btn-primary dropdown-toggle pull-right" 
                            data-bs-toggle="dropdown" aria-expanded="false" >
                            <span class="limit-lbl">Setting Limit: 20</span>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item btn-limit" href="#" alt="10">10 Waybills</a>
                            <a class="dropdown-item btn-limit" href="#" alt="20">20 Waybills</a>
                            <a class="dropdown-item btn-limit" href="#" alt="30">30 Waybills</a>
                        </div>
                    </div>
                </div>
            </div>
        <div class="content-body"><!-- Bootstrap Select start -->

<!-- Bootstrap Select end -->

<!-- Select2 Start  -->
<section class="basic-select2">
  <div class="row">
    <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-primary">စာဝင်လုပ်ဆောင်မှုများ</h4>
                                </div>
                                <hr>
                                <div class="card-body">
                                    @if(is_sorting(user()->branch_id) == 1)
                                    <div>
                                        <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ</h5>
                                        <div class="mb-half">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=1') }}" class="btn btn-wide btn-relief-success waves-effect">1.စာရင်းအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=2') }}" class="btn btn-wide btn-relief-success waves-effect">2.စာအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></a>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=3&type=branch') }}" class="btn btn-wide btn-relief-success waves-effect">3.ရုံးခွဲ(သို့)နယ်မြို့သို့ <strong class="block">အထုပ်ပို့မည်</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=4') }}" class="btn btn-wide btn-relief-secondary waves-effect disabled">4.ရှိပြီးသား အထုပ်ထဲ<strong class="block">စာထပ်ထည့်မည်</strong></a>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="mb-half">
                                            <a href="{{ url('scan/inbound?action=5') }}" class="btn btn-wide btn-relief-success waves-effect">5.ထုပ်ထားသော အထုပ်များအား <strong class="block">စာရင်းဖြင့်ပို့မည်</strong></a>
                                        </div>
                                    </div>
                                    @else
                                    <div class="denied">
                                        <h5 class="text-primary">Branch In Actions</h5>
                                        <div class="mb-half">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">1.Branch In By <strong class="block">CTF</strong></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">2.Branch In By <strong class="block">Waybill</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-primary">Branch Out Actions</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">3.Create Waybill <strong class="block">Package</strong></span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">4.Edit Waybill <strong class="block">Package</strong></span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">5.Export Package <strong class="block">CTF</strong></span>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <hr>
                                <div class="card-body">
                                    @if(is_sorting(user()->branch_id) == 1)
                                    <div class="denied">
                                        <h5 class="text-primary">စာလက်ခံလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">6.စာရင်းအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">7.စာအမှတ်ဖြင့် <strong class="block">စာဝင်လုပ်မည်</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="text-primary">စာလွှဲပြောင်းလုပ်ဆောင်မှုများ (ရုံးခွဲအတွက်)</h5>
                                        <div class="mb-half">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-danger waves-effect btn-denied">8.ရုံးခွဲ(သို့)စာစီဌာနသို့ <strong class="block">အထုပ်ပို့မည်</strong></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-danger waves-effect btn-denied">9.ရှိပြီးသား အထုပ်ထဲ<strong class="block">စာထပ်ထည့်မည်</strong></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-half">
                                            <span class="btn btn-wide btn-relief-danger waves-effect btn-denied">10.ထုပ်ထားသော အထုပ်များအား <strong class="block">စာရင်းဖြင့်ပို့မည်</strong></span>
                                        </div>
                                    </div>
                                    @else
                                    <h5 class="text-primary">Branch In Actions</h5>
                                    <div class="mb-half">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=6') }}" class="btn btn-wide btn-relief-success waves-effect">6.Branch In By <strong class="block">CTF</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=7') }}" class="btn btn-wide btn-relief-success waves-effect">7.Branch In By <strong class="block">Waybill</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="text-primary">Branch Out Actions</h5>
                                    <div class="mb-half">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-success waves-effect">8.Create Package <strong class="block">Package</strong></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{ url('scan/inbound?action=8') }}" class="btn btn-wide btn-relief-secondary waves-effect">9.Edit Waybill <strong class="block">Package</strong></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-half">
                                        <span class="btn btn-wide btn-relief-success waves-effect">10.Export Package <strong class="block">CTF</strong></span>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title text-primary">ရှိပြီးသားအထုပ်ထဲ ပစ္စည်းထပ်ထည့်ရန်</h4>
        </div>
        <hr>
        <div class="card-body">
            <!-- Icons -->
            <div class="mb-1">
                <label class="form-label" for="select2-icons">ပစ္စည်းထပ်ထည့်မည့်အထုပ် <span class="text-danger">✶</span></label>
                <select data-placeholder="အထုပ်ရွေးပါ..." class="select2 form-select" id="package">
                    @foreach($packages as $package)
                    <option value="{{ $package->id }}" data-icon="package">{{ $package->package_no }} ({{ $package->to_branch.' , '.date('M-d',strtotime($package->created_at)) }})</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-1">
                          <label class="form-label text-primary w-100" for="select2-icons">စာအမှတ် <span class="text-danger">✶</span><span class="check-number text-danger pull-right hide">နံပါတ်မှားနေသည်</span></label>
                          <input type="text" id="waybill" class="form-control" placeholder="Z1234566789" />
                      </div>
            
            <div class="mb-0">
                <button type="button" class="btn btn-relief-success waves-effect add-btn disabled">ထည့်မည်</button>
            </div>
            <div class="mb-0">
                                        <small><span class="text-danger">✶</span> is required fields.</small>
                                    </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
                    <div class="card-header">
                        <h4 class="card-title text-primary">
                                        <span class="text-primary">Scanned</span> <span id="scanned" class="badge badge-light-primary me-1 count-badge">0</span> 
                                        <span class="text-success">Success</span> <span id="success" class="badge badge-light-success me-1 count-badge">0</span> 
                                        <span class="text-danger">Failed</span> <span id="failed" class="badge badge-light-danger count-badge">0</span>
                                    </h4>
                    </div>
                    <hr>
                    <div class="card-body">
                      <div class="alert alert-warning alert-validation-msg get-started" role="alert">
                                        <div class="alert-body d-flex align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                            <span>Scan ဖတ်ထားသောစာများ မရှိသေးပါ။</span>
                                        </div>
                                    </div>
                        <div class="data-loading text-center hide">
                            <div class="demo-spacing-0">
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <div class="alert-body">
                                        <h5>Loading ...</h5>
                                        <div class="spinner-grow text-warning me-1" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                        <div class="spinner-grow text-danger me-1" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                        <div class="spinner-grow text-success me-1" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            </div>
                        </div>
                        <ul class="list-group" id="scanned-lists">
                      
                        </ul>
                        <ul class="list-group" id="failed-lists">
                                                                                                            
                        </ul>
                        <div class="alert alert-warning hide scroll-msg mt-1" role="alert">
                            <div class="alert-body"><strong>Please scroll to up/down for older items.</strong></div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
            </section>
        </div>
      </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Customizer-->


    @include('customizer')
    @include('footer')
    <input type="hidden" id="selected_package">
    <input type="hidden" id="user_id" value="{{ user()->id }}">
    <input type="hidden" id="username" value="{{ user()->name }}">
    <input type="hidden" id="user_city_id" value="{{ user()->city_id }}">
    <input type="hidden" id="user_branch_id" value="{{ user()->branch_id }}">
    <input type="hidden" id="user_branch_name" value="{{ branch(user()->branch_id)['name'] }}">
    <input type="hidden" id="selected_ctf" value="0">


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/ext-component-sweet-alerts.min.js') }}"></script>

    <script>
      $(window).on('load',  function(){
        if (feather) {
          feather.replace({ width: 14, height: 14 });
        }
      });

      $(document).ready(function(){
            var url     = $("#url").val();
            var scanned = 0;
            var success = 0;
            var failed  = 0;
            var waybills= [];
            var key     = 0;
            var limit   = 20;

            var voice   = 'alert-1.mp3';
            var city    = $("#city").val();
            var _token  = $("#_token").val();

            $("#form").submit(function(event){
                event.preventDefault();  
            });

            $(".add-btn").on("click",function search(e) {
                //call api sent to server function
                //$('.bs-example-modal-center-loading').modal('show');
                action = $(this).val();
                $('#action').val(action);
                $('.data-loading').removeClass('hide');
                data_send();
            });

            $("#waybill").on("keydown",function search(e){
                if(e.keyCode == 13) {
                    waybill   = $("#waybill").val().toUpperCase();
                    $(".add-btn").removeClass('disabled');

                    if(pattern1.test(waybill) == true && pattern2.test(waybill.charAt(0)) == true){
                        input = 1;
                    }else{
                        input = 0;
                    }

                    if(waybill.length > 7 && waybill.length < 20 && input == 1){
                        $(".get-started").hide();
                                               
                        $("#failed-lists").empty();
                        $("#scanned-lists").show();
           
                        $(".scan-btn").removeAttr('disabled');
                        if(waybills.indexOf(waybill) == -1) {
                            ++scanned;
                            $("#scanned-lists").prepend('<li class="list-group-item align-items-center item-'+waybill+'"><svg class="list-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text me-1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg><span class="fs-18">'+waybill+'</span><span class="pull-right"><button type="button" class="btn btn-icon btn-danger waves-effect p2 remove" value="'+waybill+'"><svg class="plus-btn" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></button></span></span>');     
                            $("#scanned").text(scanned);
                            waybills.push(waybill);
                        }
                        $("#success").text(0);
                        $("#failed").text(0);

                        if(scanned > 10){
                          $("#scanned-lists").addClass('content-scroll'); 
                          $(".scroll-msg").removeClass('hide');
                        }

                        $(this).val('');
                        $(".check-number").addClass('hide');     

                        //$('#multi_scanned_waybills').prepend(raw); 
                            
                        //limit scanned count with 25
                        if(scanned == limit){
                            $("#waybill").attr("disabled", true);
                            audioElement.play();
                            Swal.fire({
                                position:"center",
                                title:"အသိပေးခြင်း",
                                icon:"warning",
                                html:"<strong>သတ်မှတ်ထားသော စာအရေအတွက်ပြည့်သွားပါပြီ</strong> <p>စာရင်းသွင်းပြီးမှ စာထပ်ဖတ်ပေးပါ</p>",
                                showConfirmButton:!1,
                                timer:5000,
                                customClass:{confirmButton:"btn btn-primary"},
                                buttonsStyling:!1
                            }); 
                        }

                        ++key;
                    }else{
                        //invalid length && try again
                        $(".check-number").removeClass('hide');
                        $("#waybill").val('');
                    }

                    //removed fixed height for error lists
                    $("#failed-lists").removeClass('scanned-panel');
                }
            });    

            var data_send = function(){
                user_id         = $("#user_id").val();
                username        = $("#username").val();
                //package_id      = $("#package_id").val();
                user_city_id    = $("#user_city_id").val();
                user_branch_id  = $("#user_branch_id").val();
                package_id      = $("#package").val();
                package_no      = $("#package option:selected").text();
                service_point   = $("#service_point").val();
                same_day        = 0;
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                       
                $.ajax({
                    type: 'post',
                    url: url+'/api/action/inbound',
                    dataType:'json',
                    data: {
                        'waybills'      :waybills,
                        'user_id'       :user_id,
                        'username'      :username,
                        'user_city_id'  :user_city_id,
                        'user_branch_id':user_branch_id,
                        'service_point' :service_point,
                        'same_day'      :same_day,
                        'package_id'    :package_id,
                        'package_no'    :package_no,
                        'action_id'     :8,
                    },
                    success: function(data) { 
                        waybills=[];
                        $('.data-loading').addClass('hide');
                        $(".add-btn").addClass('disabled');

                        $("#scanned-lists").removeClass('scanned-panel');
                        $("#scanned-lists").empty(); 
                        $("#failed-lists").empty();

                        success = $("#scanned").text() - data.failed.length;
                        failed  = data.failed.length;

                        //add scroll max size for item > 10
                        if(failed > 10){
                            $("#failed-lists").addClass('scanned-panel'); 
                        }

                        $("#success").text(success);
                        $("#failed").text(failed);

                        if(data.failed.length > 0 ){
                            for (i = 0; i < data.failed.length; i++) {
                                $("#failed-lists").prepend('<li class="list-group-item sm-item text-danger"><span class="fs-20">😭</span><span class="fs-18">'+data.failed[i]+'</span><span class="pull-right">(X)</span></li>');
                            }
                        }else{
                             $("#failed-lists").prepend('<li class="list-group-item sm-item text-success border-success"><span class="fs-20">😎</span> စာအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                        }
                    },
                });
                $(this).val(''); 


                
                $('.scan-btn').attr('disabled',true);
                $("#waybill").attr("disabled", false);
                $('#waybill').trigger('focus');
                scanned = 0;
            }
        
            //limit alert audio background
            var audioElement = document.createElement('audio');
            audioElement.setAttribute('src', url+'/_dist/alerts/'+voice);

            $('body').delegate(".remove","click",function () {
                var waybill_no = $(this).val();
                $(".item-"+waybill_no).addClass('hide');

                result = waybills.filter(function(item) {
                    return item !== waybill_no;
                   
                });
                --scanned;
                if(scanned == 0){
                    $(".add-btn").addClass('disabled');
                }

                waybills = result;
                $("#scanned").text(scanned);
            });

        }); 
    </script>
  </body>
  <!-- END: Body-->
</html>