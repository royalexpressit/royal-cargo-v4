<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title>New Cargo - View City</title>

    <link rel="shortcut icon" href="{{ asset('_dist/images/favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/components.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/semi-dark-layout.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/form-pickadate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/_dist/css/style.css') }}">
</head>
<body class="horizontal-layout horizontal-menu navbar-floating footer-static" data-open="hover" data-menu="horizontal-menu" data-col="">
    @include('operator.header')
    @php $key=0 @endphp
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h3 class="content-header-title float-start mb-0">Cities</h3>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('') }}">Dashboard</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{ url('cities') }}">Cities</a>
                                    </li>
                                    <li class="breadcrumb-item active">View City's Branches</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-end col-md-6 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="profile-info">
                    <div class="row">
                        <div class="col-lg-3 col-12 order-2 order-lg-1">
                            <div class="card">
                                <div class="card-body">
                                    <p class="card-text text-center">
                                        <img src="{{ asset('_dist/images/profile.png') }}">
                                    </p>
                                    <div class="mt-2">
                                        <span class="text-muted">City</span>
                                        <p class="card-text">{{ $city->name }} ({{ $city->mm_name }})</p>
                                    </div>
                                    <div class="mt-2">
                                        <span class="text-muted">Shortcode</span>
                                        <p class="card-text">{{ $city->shortcode }}</p>
                                    </div>
                                    <div class="mt-2">
                                        <span class="text-muted">State</span>
                                        <p class="card-text">{{ $city->state }}</p>
                                    </div>
                                    <div class="mt-2">
                                        <span class="text-muted">Type</span>
                                        <p class="card-text">{{ $city->is_service_point }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-12 order-2 order-lg-1">
                            <div class="card">
                                <div class="card-body">
                                    <span class="text-muted">Branches</span>
                                    <div class="mt-2">
                                        <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th>Branch Name <span class="badge rounded-pill badge-light-primary fs-16" id="total">0</span></th>
                                              <th>Status</th>
                                              <th>Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($branches as $key => $branch)
                                        @php ++$key @endphp
                                        <tr>
                                              <th>{{ $branch->name }} </th>
                                              <th>{{ $branch->is_main_office==1? 'Main Office':'Branch' }}</th>
                                              <th>{!! $branch->active==1? '<span class="badge bg-success">ဖွင့်ထား</span>':'<span class="badge bg-danger">ပိတ်ထား</span>' !!}</th>
                                          </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-12 order-2 order-lg-1">
                            <div class="card">
                                <div class="card-body">
                                    <span class="text-muted">Rejected Team</span>
                                    @if(fetched_reject_branch($city_id))
                                    <div class="mt-1">
                                        <div class="alert alert-warning mt-1 alert-validation-msg" role="alert">
                                            <div class="alert-body d-flex align-items-center">
                                                <span>Rejected ဖြစ်သွားသော waybill များအား <strong id="current_branch">{{ fetched_reject_branch($city_id)->name }}</strong> သို့လွှဲပြောင်းပေးရပါမည်။</span>
                                            </div>
                                        </div>
                                        <div class="mt-1">
                                            <button class="btn btn-primary btn-sm btn-rounded waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#primary">Change</button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @include('customizer')
    @include('footer')
    <input type="hidden" id="city_id" value="{{ $city_id }}">
    <input type="hidden" id="total-branches" value="{{ $key }}">

    <!-- Modal -->
    @if(fetched_reject_branch($city_id))
              <div
                class="modal fade text-start modal-primary"
                id="primary"
                tabindex="-1"
                aria-labelledby="myModalLabel160"
                aria-hidden="true"
              >
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="myModalLabel160">Change Rejected Team</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="demo-inline-spacing">
                            @foreach($cods as $cod)
                                  <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="{{ $cod->id }}" alt="{{ $cod->name }}" {{ $cod->name==fetched_reject_branch($city_id)->name? 'checked':'' }}/>
                          <label class="form-check-label" for="inlineRadio1">{{ $cod->name }}</label>
                        </div>
                        @endforeach
        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary btn-change" data-bs-dismiss="modal">Change</button>
                    </div>
                  </div>
                </div>
              </div>
            @endif

    <script src="{{ asset('/_dist/js/vendors.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.date.js') }}"></script>
    <script src="{{ asset('/_dist/js/picker.time.js') }}"></script>
    <script src="{{ asset('/_dist/js/legacy.js') }}"></script>
    <script src="{{ asset('/_dist/js/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app-menu.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/app.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/customizer.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-select2.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/form-pickers.min.js') }}"></script>
    <script src="{{ asset('/_dist/js/script.js') }}"></script>
    <script src="{{ asset('/_dist/js/config.js') }}"></script>
    <script>
        $(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });

        $("#total").text($("#total-branches").val());

        $('body').delegate(".btn-change","click",function () {
            var id      = $("input[name='inlineRadioOptions']:checked").val();
            var city_id = $("#city_id").val();
            var label   = $("input[name='inlineRadioOptions']:checked").attr('alt');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                           
            $.ajax({
                type: 'post',
                url: url+'/api/change-rejected-team',
                dataType:'json',
                data: {
                    'branch_id' :id,
                    'label'     :label,
                    'city_id'   :city_id
                },
                success: function(data) { 
                    if(data.success == 1){
                        $("#current_branch").text(data.msg);
                    }
                },
            });
        });

        //
    </script>
</body>
</html>