<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCtfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctfs', function (Blueprint $table) {
            $table->id();
            $table->string('ctf_no');
            $table->integer('user_id')->nullable();
            $table->integer('branch_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('action_id')->nullable();
            $table->integer('completed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctfs');
    }
}
