<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('package_no');
            $table->integer('user_id')->nullable();
            $table->integer('from_branch_id')->nullable();
            $table->integer('from_city_id')->nullable();
            $table->integer('to_branch_id')->nullable();
            $table->integer('to_city_id')->nullable();
            $table->integer('completed')->nullable();
            $table->integer('package_type_id')->nullable();
            $table->text('remark')->nullable();
            $table->dateTime('completed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
