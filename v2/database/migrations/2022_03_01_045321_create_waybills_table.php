<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWaybillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //fixed
        Schema::create('waybills', function (Blueprint $table) {
            $table->id();
            $table->string('waybill_no');
            $table->integer('origin')->nullable();
            $table->integer('destination')->nullable();
            $table->integer('batch_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('action_id')->nullable();
            $table->integer('same_day')->default(0);
            $table->integer('transit')->default(0);
            $table->integer('qty')->default(0);
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waybills');
    }
}
