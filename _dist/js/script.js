    
$(document).ready(function(){
    $(".set-date").on("click",function search(e) {
        set_date  = $('#config_date').val();
        _token    = $('#_token').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                          
        $.ajax({
            type: 'post',
            url: url+'/changed-date',
            dataType:'json',
            data: {
                'set_date'  :set_date,
                '_token'  : _token
            },
            success: function(data) { 
               location.reload();
            },
        });
    });

   	$("#comment,#phone").on("keyup",function search(e) {
   		if($("#comment").val() != '' && $("#phone_no").val() != ''){
   			$('.submit-feedback').removeClass('disabled');
   			console.log('worked');
   		}else{
   			$('.submit-feedback').addClass('disabled');
   			console.log('empty');
   		}
   	});

    $(".submit-feedback").on("click",function search(e) {
        var feedback  = $('#comment').val();
        var username  = $('#acc_name').val();
        var phone_no  = $('#phone_no').val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                          
        $.ajax({
            type: 'post',
            url: url+'/api/submit-feedback',
            dataType:'json',
            data: {
                'feedback'  :feedback,
                'username'  :username,
                'phone_no'  :phone_no
            },
            success: function(data) { 
                console.log(data);
                $('.alert-feedback').removeClass('hide');
                loaded_feedbacks();

                $("#phone_no").val('');
                $("#comment").val('');
                $('.submit-feedback').addClass('disabled');
            },
        });
    });

    
    var loaded_feedbacks = function(){
    	
	        $.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });
	                          
	        //loaded feedback
	        $.ajax({
			    type: 'get',
			    url: url+'/api/feedbacks',
			    dataType:'json',
			    data: { },
			    success: function(data) { 
			    	$("#fetched-feedbacks").empty();
			        $.each( data, function( key, value ) {
	                    //++count
	                    $("#fetched-feedbacks").append(
	                        '<div class="card-body">'
		                        +'<blockquote class="blockquote mb-0">'
			                        +'<p class="text-italic">'+value.feedback+'</p>'
			                        +'<img src="'+url+'/_dist/images/'+value.image+'" class="round border-danger me-1" alt="avatar" height="32" width="32"><span class="font-small-4 text-muted">'+value.name+' <span class="text-danger">('+value.phone_no+')</span></span>'
                                    +'<small class="pull-right">'+value.created_at+'</small>'
			                    +'</blockquote>'
		                    +'</div><hr>'
	                    )
	                });
			    },
			});
	    
    }

    $(".loaded-feebacks").on("click",function search(e) {
    	loaded_feedbacks();
    });	
    
});

