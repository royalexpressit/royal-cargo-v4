var url     	= $("#url").val();
var input    	= 0;
var pattern1 	= /^[a-zA-Z0-9-]+$/; 
var pattern2 	= /^[a-zA-Z]+$/; 

function outbound_status(status){
	if(status == 1){
		return '<span class="badge bg-danger"><i data-feather="chevrons-left"></i>ရုံးတွင်ရှိနေဆဲ</span>';
	}else if(status == 2){
		return '<span class="badge bg-warning"><i data-feather="chevrons-left"></i>ရုံး-ရုံး(အထုပ်)</span>';
	}else if(status == 3){
		return '<span class="badge bg-primary"><i data-feather="chevrons-left"></i>ရုံး-ရုံး(လွှဲပြောင်း)</span>';
	}else if(status == 4){
		return '<span class="badge bg-primary"><i data-feather="chevrons-left"></i>ရုံး-ရုံး(လက်ခံပြီး)</span>';
	}else if(status == 5){
		return '<span class="badge bg-primary"><i data-feather="chevrons-left"></i>မြို့-မြို့(အထုပ်)</span>';
	}else if(status == 6){
		return '<span class="badge bg-primary"><i data-feather="chevrons-left"></i>မြို့-မြို့(လွှဲပြောင်း)</span>';
	}else if(status == 7){
		return '<span class="badge bg-success"><i data-feather="chevrons-right"></i>မြို့-မြို့(လက်ခံပြီး)</span>';
	}else if(status == 8){
		return '<span class="badge bg-success"><i data-feather="chevrons-right"></i>ရုံး-ရုံး(အထုပ်)</span>';
	}else if(status == 9){
		return '<span class="badge bg-success"><i data-feather="chevrons-right"></i>ရုံး-ရုံး(လွှဲပြောင်း)</span>';
	}else if(status == 10){
		return '<span class="badge bg-success"><i data-feather="chevrons-right"></i>ရုံး-ရုံး(လက်ခံပြီး)</span>';
	}else{
		return '<span class="badge bg-success">-</span>';
	}
}

function same_day(status){
	if(status == 1){
		return '<svg class="list-icon text-success" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>';
	}else{
		return '<span class="text-muted">-</span>';
	}
}

function city_type(service_point){
	if(service_point == 1){
		return '<span class="badge rounded-pill badge-light-warning">Single City</span>';
	}else{
		return '<span class="badge rounded-pill badge-light-success">Main City</span>';
	}
}

function role(role){
	if(role == 1){
		return '<span class="badge bg-danger">Admin</span>';
	}else if(role == 2){
		return '<span class="badge bg-success">Supervisor</span>';
	}else{
		return '<span class="badge bg-primary">Courier</span>';
	}
}

function route_active(active){
	if(active == 1){
		return '<span class="badge bg-success">ဖွင့်ထား</span>';
	}else{
		return '<span class="badge bg-danger">ပိတ်ထား</span>';
	}
}

function checked_package(status){
	if(status == 1){
		return '<span class="badge rounded-pill bg-light-success">Completed</span>';
	}else{
		return '<span class="badge rounded-pill bg-light-warning">Draft</span>';
	}
}

function checked_ctf(status){
	if(status != null){
		return '<span class="badge rounded-pill bg-light-success">Completed</span>';
	}else{
		return '<span class="badge rounded-pill bg-light-warning">Pending</span>';
	}
}

function active(status){
	if(status == 1){
		return '<span class="badge rounded-pill bg-light-success">Active</span>';
	}else{
		return '<span class="badge rounded-pill bg-light-danger">Deactivate</span>';
	}
}

function replace_icon(icon){
	if(icon == 'map-pin'){
		return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg>';
	}else if(icon == 'eye'){
		return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>';
	}else if(icon == 'trash'){
		return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>';
	}else{
		return '-';
	}
}

function pkg_icons(code){
	if(code == 'package'){
		return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-package"><line x1="16.5" y1="9.4" x2="7.5" y2="4.21"></line><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>';
	}else if(code == 'shield'){
		return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shield"><path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z"></path></svg>';
	}else if(code == 'x-circle'){
		return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>';
	}else if(code == 'shopping-bag'){
		return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>';
	}else if(code == 'alert-circle'){
		return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12.01" y2="16"></line></svg>';
	}else{
		return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-repeat"><polyline points="17 1 21 5 17 9"></polyline><path d="M3 11V9a4 4 0 0 1 4-4h14"></path><polyline points="7 23 3 19 7 15"></polyline><path d="M21 13v2a4 4 0 0 1-4 4H3"></path></svg>';
	}
}