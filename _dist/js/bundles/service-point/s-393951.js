$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var url     = $("#url").val();
    var scanned = 0;
    var success = 0;
    var failed  = 0;
    var waybills= [];
    var key     = 0;
    var limit   = 20;

    var voice   = 'alert-1.mp3';
    var city    = $("#city").val();
    var _token  = $("#_token").val();

    $("#form").submit(function(event){
        event.preventDefault();  
    });

    $(".add-btn").on("click",function search(e) {
        //call api sent to server function
        //$('.bs-example-modal-center-loading').modal('show');
        action = $(this).val();
        $('#action').val(action);
        data_send();
    });

    $("#waybill").on("keydown",function search(e){
        if(e.keyCode == 13) {
            waybill   = $("#waybill").val().toUpperCase();

            if(waybill.length > 7 && waybill.length < 20){
                //added item into array
                $(".add-btn").removeClass('disabled');
                waybills.push(waybill);

                    
                $(".get-started").hide();
                //valid length && continue
                ++scanned;
                $("#scanned").text(scanned);
                $("#success").text(0);
                $("#failed").text(0);
                                    
                $("#failed-lists").empty();
                $("#scanned-lists").show();
           
                $(".scan-btn").removeAttr('disabled');
                $("#scanned-lists").prepend('<li class="list-group-item align-items-center"><svg class="list-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text me-1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg><span class="fs-18">'+waybill+'</span></span>');
                        
                if(scanned > 10){
                    $("#scanned-lists").addClass('content-scroll'); 
                    $(".scroll-msg").removeClass('hide');
                }

                $(this).val('');
                $(".check-number").addClass('hide');     

                //$('#multi_scanned_waybills').prepend(raw); 
                            
                //limit scanned count with 25
                if(scanned == limit){
                    $("#waybill").attr("disabled", true);
                    audioElement.play();
                    Swal.fire({
                        position:"center",
                        title:"အသိပေးခြင်း",
                        icon:"warning",
                        html:"<strong>သတ်မှတ်ထားသော စာအရေအတွက်ပြည့်သွားပါပြီ</strong> <p>စာရင်းသွင်းပြီးမှ စာထပ်ဖတ်ပေးပါ</p>",
                        showConfirmButton:!1,
                        timer:5000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    });
                    console.log(voice);
                }

                ++key;
            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#waybill").val('');
            }

            //removed fixed height for error lists
            $("#failed-lists").removeClass('scanned-panel');
        }
    });

    $(".scan-btn").on("click",function search(e) {
        //call api sent to server function
        //$('.bs-example-modal-center-loading').modal('show');
        data_send();
    });

    var data_send = function(){
        user_id         = $("#user_id").val();
        username        = $("#username").val();
        //package_id      = $("#package_id").val();
        user_city_id    = $("#user_city_id").val();
        user_branch_id  = $("#user_branch_id").val();
        package_id      = $("#package").val();
        package_no      = $("#package option:selected").text();
        service_point   = $("#service_point").val();
        same_day        = 0;
                
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/action/outbound',
            dataType:'json',
            data: {
                'waybills'      :waybills,
                'user_id'       :user_id,
                'username'      :username,
                'user_city_id'  :user_city_id,
                'user_branch_id':user_branch_id,
                'service_point' :service_point,
                'same_day'      :same_day,
                'package_id'    :package_id,
                'package_no'    :package_no,
                'action_id'     :5,
            },
            success: function(data) { 
                console.log(data);
                $("#scanned-lists").removeClass('scanned-panel');
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();

                success = $("#scanned").text() - data.failed.length;
                failed  = data.failed.length;

                //add scroll max size for item > 10
                if(failed > 10){
                    $("#failed-lists").addClass('content-scroll'); 
                    $(".scroll-msg").removeClass('hide');
                }else{
                    $(".scroll-msg").addClass('hide');
                }

                $("#success").text(success);
                $("#failed").text(failed);

                if(data.failed.length > 0 ){
                    for (i = 0; i < data.failed.length; i++) {
                        $("#failed-lists").prepend('<li class="list-group-item sm-item text-danger"><span class="fs-20">😭</span><span class="fs-18">'+data.failed[i]+'</span><span class="pull-right">(X)</span></li>');
                    }
                }else{
                    $("#failed-lists").prepend('<li class="list-group-item sm-item text-success border-success"><span class="fs-20">😎</span> စာအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                }
            },
        });

        $(this).val(''); 
        waybills=[];
        $('.scan-btn').attr('disabled',true);
        $('.continue-action').hide();
        $("#waybill").attr("disabled", false);
        $('#waybill').trigger('focus');
        scanned = 0;
    }
        
    //limit alert audio background
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', url+'/_dist/alerts/'+voice);

    $(".set-voice").on("click",function search(e) {
        $('#voiceModal').modal({show:true});
    });

    $(".fetched-count").on("click",function search(e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/fetched-count',
            dataType:'json',
            data: {
                'status'      :'branch-in',
                'type'        :'inbound'
            },
            success: function(data) { 
                $("#fetched-count").text(data);
            },
        });
    });

    $(".choice-voice").on("click",function search(e) {
        voice = $(this).val();

        $(".choice-voice").removeClass('btn-success btn-primary');
        $(".choice-voice").addClass('btn-primary');
        $(this).addClass('btn-success');

        audioElement.setAttribute('src', url+'/_dist/alerts/'+voice);
        audioElement.load();
        console.log(voice);
    });

    $('body').delegate(".qty-input","keyup",function () {
        var item = parseInt($(this).val());
        var id = $(this).attr('id');
        waybills[id]['qty'] = item;

        //console.log(waybills);
    });

    $('body').delegate(".plus","click",function () {
        var id = $(this).val();

        console.log(id);

        //var val = parseInt($('.input-'+id+'').val());
        var val = $('.input-'+id+'').val();

        ++val;
        $('.minus-'+id+'').removeClass('disabled');

        //updated value
        $('.input-'+id+'').val(val);
        if(val < 1){
            $('.minus-'+id+'').addClass('disabled');
        }


        waybills[id]['qty'] = val;
    });

    $('body').delegate(".minus","click",function () {
        var id = $(this).val();
        var val = parseInt($('.input-'+id+'').val());

        --val;
        if(val < 1){
            $('.minus-'+id+'').addClass('disabled');
        }

        //updated value
        $('.input-'+id+'').val(val);
        console.log(val);

        waybills[id]['qty'] = val;
    });

    $('.btn-limit').on("click",function search(e) {
        limit = $(this).attr('alt');
        $(".limit-lbl").text('Setting Limit: '+limit);
        
    });
}); 