$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var url     = $("#url").val();
    var scanned = 0;
    var success = 0;
    var failed  = 0;
    var waybills= [];
    var limit   = 20;

            
    var voice   = 'alert-1.mp3';
    var city    = $("#city").val();
    var _token  = $("#_token").val();


    $("#form").submit(function(event){
        event.preventDefault();  
    });

    $("#waybill").on("keydown",function search(e){
        if(e.keyCode == 13) {
            waybill   = $("#waybill").val().toUpperCase();
            

            if(waybill.length > 7 && waybill.length < 20){
                $(".get-started").hide();
                //valid length && continue
                $(".save-btn,.continue-btn").removeClass('disabled');

                waybills.push(waybill);
                console.log(waybills);

                ++scanned;
                $("#scanned").text(scanned);
                $("#success").text(0);
                $("#failed").text(0);
                                    
                $("#failed-lists").empty();
                $("#scanned-lists").show();
           
                $(".scan-btn").removeAttr('disabled');
                $("#scanned-lists").prepend('<li class="list-group-item align-items-center"><svg class="list-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text me-1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg><span class="fs-18">'+waybill+'</span></span>');
                        
                $(this).val('');
                $(".check-number").addClass('hide');     

                        
                            
                //default 20 waybills
                if(scanned == limit){
                    $("#waybill").attr("disabled", true);
                    audioElement.play();
                    Swal.fire({
                        position:"center",
                        title:"အသိပေးခြင်း",
                        icon:"warning",
                        html:"<strong>သတ်မှတ်ထားသော စာအရေအတွက်ပြည့်သွားပါပြီ</strong> <p>စာရင်းသွင်းပြီးမှ စာထပ်ဖတ်ပေးပါ</p>",
                        showConfirmButton:!1,
                        timer:5000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    });
                }
            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#waybill").val('');
            }

                    //removed fixed height for error lists
            $("#failed-lists").removeClass('scanned-panel');
            $(".show-package").addClass('hide');
        }
    });

    $(".save-btn,.continue-btn").on("click",function search(e) {
        //call api sent to server function
        action = $(this).val();
        $('#action').val(action);
        data_send();
    });

    //scan code for continue action
    $("#continue-action").on("keydown",function search(e) {
        var code = $("#continue-action").val();
        if(e.keyCode == 13) {
            if(code == 'continue-action'){
                data_send();
            }else{
                $("#continue-action").val('');
            }
        }
    });

    var data_send = function(){
        user_id         = $("#user_id").val();
        username        = $("#username").val();
        package_id      = $("#package_id").val();
        package_no      = $("#package_no").val();
        user_city_id    = $("#user_city_id").val();
        user_branch_id  = $("#user_branch_id").val();
        to_branch_id    = $("#to_branch").val();
        to_branch_name  = $("#to_branch option:selected").text();
        pkg_type        = $("#type").val();
        remark          = $("#remark").val();
        action          = $('#action').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/action/inbound',
            dataType:'json',
            data: {
                'waybills'      :waybills,
                'user_id'       :user_id,
                'username'      :username,
                'user_city_id'  :user_city_id,
                'user_branch_id':user_branch_id,
                'package_id'    :package_id,
                'package_no'    :package_no,
                'to_branch_id'  :to_branch_id,
                'to_branch_name':to_branch_name,
                'pkg_type'      : pkg_type,
                'remark'        : remark,
                'action'        :action,
                'package_id'    :package_id,
                'action_id'     :8,
                'type'          :'outbound'
            },
            success: function(data) { 

            if(data.package_id == 0){
                $(".continued-pkg").hide;
                $(".remark").show;
                $(".success-pkg").addClass('hide');
                $(".error-pkg").removeClass('hide');
            }else{
                Swal.fire({
                    position:"top-end",
                    title:"Good Job",
                    icon:"success",
                    text:"You has been created new package.",
                    showConfirmButton:!1,
                    timer:2000,
                    customClass:{confirmButton:"btn btn-primary"},
                    buttonsStyling:!1
                });
                    $(".remark").hide;
                    $(".continued-pkg").show;
                    $(".success-pkg").removeClass('hide');
                    $(".error-pkg").addClass('hide');
                }

                if(data.option == 'save'){
                    $(".show-package").removeClass('hide');
                    $("#pkg_no").text(data.package_no);
                    $("#package_id").val(0);
                    $("#package_no").val(0);
                }else{
                    $(".show-package").removeClass('hide');
                    $("#pkg_no").text(data.package_no);

                    $("#package_id").val(data.package_id);
                    $("#package_no").val(data.package_no);
                    //$(".show-package").addClass('hide');
                }

                $("#scanned-lists").removeClass('scanned-panel');
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();

                success = $("#scanned").text() - data.failed.length;
                failed  = data.failed.length;

                //add scroll max size for item > 10
                if(failed > 10){
                    $("#failed-lists").addClass('scanned-panel'); 
                }

                $("#success").text(success);
                $("#failed").text(failed);

                if(data.failed.length > 0 ){
                    for (i = 0; i < data.failed.length; i++) {
                        $("#failed-lists").prepend('<li class="list-group-item sm-item text-danger"><span class="fs-20">😭</span><span class="fs-18">'+data.failed[i]+'</span><span class="pull-right">(X)</span></li>');
                    }
                }else{
                    $("#failed-lists").prepend('<li class="list-group-item sm-item text-success border-success fs-18">😎 စာအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                }
            },
        });
        $(this).val(''); 

        waybills=[];
        $('#multi_scanned_waybills').empty();
        $('.scan-btn').attr('disabled',true);
        $('.continue-action').hide();
        $("#waybill").attr("disabled", false);
        $('#waybill').trigger('focus');
        scanned = 0;
    }
        
    //limit alert audio background
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', url+'/_dist/alerts/'+voice);

    
}); 