$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var url     = $("#url").val();
    var packages= [];
    var added   = 0;
    var draft   = $("#draft-total").val();
    var row     = $("#row").val();

    if(row > 6){
        $(".package-list").addClass('mxh-560');
    }

    $("#draft").text(draft);

    $(".add-btn").on("click",function search(e) {
        $('.items').addClass('disabled-item');

        $('.export-btn-box').removeClass('hide');
        $('.show-ctf').addClass('hide');
        group_item = $(this).attr('alt');
        draft--;

        $("#draft").text(draft);
        $('.'+group_item).removeClass('disabled-item');
        $('.show-msg').hide();
        id = this.id;
        $('.item-'+id).hide();
        ++added;

        //alert(id);
        packages.push(id);

        data = $('.drag-item-'+id).html();
        $("#scanned-lists").prepend('<div class="transaction-item selected-item border-warning">'+data+'</div>');
            
        $('#listed').text(added);

        if(draft > 6){
            $("#scanned-lists").addClass('mxh-560');
        }
    });

    $('#filtered').on("change",function search(e) {
        city = $("#filtered option:selected").text();
        if(city == 'မြို့အားလုံးကြည့်ရန်'){
            $('.list-item').removeClass('hidden'); 
        }else{
            $('.list-item').addClass('hidden');
            $('.'+city).removeClass('hidden');
        }
        
    });

    $(".ctf-btn").on("click",function search(e) {
        user_id         = $("#user_id").val();
        username        = $("#username").val();
        user_city_id    = $("#user_city_id").val();
        user_branch_id  = $("#user_branch_id").val();
                
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/action/outbound',
            dataType:'json',
            data: {
                'packages'      :packages,
                'user_id'       :user_id,
                'username'      :username,
                'user_city_id'  :user_city_id,
                'user_branch_id':user_branch_id,
                'action_id'     :6,
            },
            success: function(data) { 
                console.log(data);
                if(data.success == 1){
                    $(".show-ctf").removeClass('hide');
                    $("#ctf_no").text(data.ctf_no);
                    $(".print-url").attr("href", url+"/print/cft?qr="+data.ctf_no)
                }

                packages= [];
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();
                $(".export-btn-box").addClass('hide');
                $('.items').removeClass('disabled-item');
            },
        });
        $(this).val(''); 
                
        added = 0;
        $('#listed').text(added);
    });

    $('#search-package').on("keyup",function search(e) {
        var term = $(this).val();

        console.log(term);
        $('.list-item').hide();
        $(".transaction-item:contains('"+term+"')").show();
    });

    $('body').delegate(".select-pkg","click",function () {
        pkg_id = $(this).val();
        shortcode = $(this).attr('alt');
        $("#selected_pkg").val(pkg_id);
        //$("#to_city option[value="102"]").attr("selected");
        $("#to_city").val(shortcode).trigger('change');
    });

    $('body').delegate(".change-city","click",function () {
        username        = $("#username").val();
        pkg_id          = $("#selected_pkg").val();;
        to_city_id      = $("#to_city").val();
        to_city_name    = $("#to_city option:selected").text();
                
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                          
        $.ajax({
            type: 'post',
            url: url+'/api/change-package-info',
            dataType:'json',
            data: {
                'username':username,
                'pkg_id':pkg_id,
                'to_city_id'  :to_city_id,
                'to_city_name':to_city_name,
                'action_id':5
            },
            success: function(data) { 
                location.reload();
            },
        });
    });
});