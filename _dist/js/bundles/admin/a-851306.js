$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    //$(".data-loading").show();
    var url         = $("#url").val();
    var json        = $("#json").val();

    //declared first loaded json data
    var load_json   = url+'/'+json;
    var waybills    = [];
    var item        = 0;
    var _token      = $("#_token").val();
            
    $.ajax({
        url: load_json,
        type: 'GET',
        data: {},
        success: function(data){
            //console.log(json);
            if(data.total > 0){
                $(".show-alert").hide();
                $.each( data.data, function( key, value ) {
                    ++item;
                    waybills.push(value.waybill_no);
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td class="text-bold-500">'+value.id+'</td>'
                            +'<td class="text-bold-500">'+value.name+'</td>'
                            +'<td class="text-bold-500">'+value.mm_name+'</td>'
                            +'<td class="text-bold-500 h6 text-danger">'+value.shortcode+'</td>'
                            +'<td>'+value.is_service_point+'</td>'
                            +'<td><a href="'+url+'/cities/'+value.id+'" class="btn btn-success btn-sm btn-rounded waves-effect waves-light">View</a> <a href="'+url+'/cities/'+value.id+'/edit" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">Edit</a></td>'
                        +'</tr>'
                    );    
                });
                $(".data-loading").hide();

                $("#to-records").text(data.to);
                $("#total-records").text(data.total);
                                    
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }else{
                $(".show-alert").show();
                $(".pagination").hide();
                $(".data-loading").hide();
            }
        }
    });

    $('.pagination-btn').click(function(){
        //clicked url json data
        $(".data-loading").show();
        $("#fetched-data").empty();
        var clicked_url = $(this).val();
                        
        $(this).siblings().removeClass('active')
        $(this).addClass('active');
        $.ajax({
            url: clicked_url,
            type: 'GET',
            data: {},
            success: function(data){
                $.each( data.data, function( key, value ) {
                    ++item;
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td class="text-bold-500">'+value.id+'</td>'
                            +'<td class="text-bold-500">'+value.name+'</td>'
                            +'<td class="text-bold-500">'+value.mm_name+'</td>'
                            +'<td class="text-bold-500 h6 text-danger">'+value.shortcode+'</td>'
                            +'<td>'+value.is_service_point+'</td>'
                            +'<td><a href="'+url+'/cities/'+value.id+'" class="btn btn-success btn-sm btn-rounded waves-effect waves-light">View</a> <a href="'+url+'/cities/'+value.id+'/edit" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">Edit</a></td>'
                        +'</tr>'
                    );
                });
                $(".data-loading").hide();
                                

                $("#to-records").text(data.to);
                if(data.prev_page_url === null){
                        $("#prev-btn").attr('disabled',true);
                }else{
                        $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                        $("#next-btn").attr('disabled',true);
                }else{
                        $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }
        });
    });
});