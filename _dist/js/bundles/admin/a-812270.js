$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var current_city = $("#city").val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
               
    $.ajax({
        type: 'post',
        url: url+'/api/fetched-branches',
        dataType:'json',
        data: {
            'city_id' :current_city,
        },
        success: function(data) { 
            console.log(data);
            $.each(data, function (i, item) {
                $('#branches').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
            });
            //$('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
        },
    });

   
    $('#city').on("change",function search(e) {
        var id = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                   
        $.ajax({
            type: 'post',
            url: url+'/api/fetched-branches',
            dataType:'json',
            data: {
                'city_id' :id,
            },
            success: function(data) {
                $('#branch').empty(); 
                $.each(data, function (i, item) {
                    $('#branch').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
                });

                //$('#branches option[value="'+current_branch+'"]').attr("selected", "selected");
            },
        });       
    });        


    $(".btn-create").on("click",function search(e) {
        $(".data-loading").show();
        $(".user-form").hide();

        username    = $('#username').val();
        email       = $("#email").val();
        password    = $("#password").val();
        city        = $("#city").val();
        branch      = $("#branch").val();
        role        = $("input[type='radio']:checked").val();

        // if($("#active").prop('checked') == false){
        //     active = 1;
        // }else{ 
        //     active = 0;
        // }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
               
        $.ajax({
            type: 'post',
            url: url+'/api/users/create',
            dataType:'json',
            data: {
                'username'  :username,
                'email'     :email,
                'password'  :password,
                'city_id'   :city,
                'branch_id' :branch,
                'role'      :role,
                //'active'    :active,
            },
            success: function(data) { 
                //finished();
                $(".user-form").hide();
                if(data.success == 1){
                    Swal.fire({
                        position:"top-end",
                        title:"Good Job",
                        icon:"success",
                        text:"You have been created new user account.",
                        showConfirmButton:!1,
                        timer:3000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    }).then(function(){
                        location.href = url+"/users";
                    });
                }else{
                    Swal.fire({
                        position:"top-end",
                        title:"Sorry",
                        icon:"error",
                        text:"Email address is already exists.",
                        showConfirmButton:!1,
                        timer:3000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    });
                }
            },
        });
    });
});