$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){

    $('.btn-create').click(function(){
        var state        = $("#state").val();
        var name         = $("#name").val();
        var mm_name      = $("#mm_name").val();
        var shortcode    = $("#shortcode").val();

        if($("#service_point").prop('checked') == true){
            service_point = 1;
        }else{ 
            service_point = 0;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/cities/create',
            dataType:'json',
            data: {
                'state'         :state,
                'name'          :name,
                'mm_name'       :mm_name,
                'shortcode'     :shortcode,
                'service_point' :service_point
            },
            success: function(data) {
                $('#branch').empty(); 
                $.each(data, function (i, item) {
                    $('#branch').append('<option value="'+item['id']+'" >'+item['name']+'</option>' );
                });

                $('#branch option[value="'+current_branch+'"]').attr("selected", "selected");
            },
        });
    });

});