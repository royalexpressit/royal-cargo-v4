$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var url  = $("#url").val();

    var current_city = $("#user_city_id").val();
    var current_branch = $("#user_branch_id").val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
               
    $.ajax({
        type: 'post',
        url: url+'/api/fetched-branches',
        dataType:'json',
        data: {
            'city_id' :current_city,
        },
        success: function(data) { 
            $.each(data, function (i, item) {
                $('#branch').append('<option value="'+item['id']+'">'+item['name']+'</option>' );
            });
            $('#branch option[value="'+current_branch+'"]').attr("selected", "selected");
        },
    });

    $(".btn-edit").on("click",function search(e) {
        $('.btn-edit').hide();
        $('.form-data').prop("disabled", false);
        $('.btn-save').show();
        $('.btn-cancel').show();
    });

    $(".btn-save,.btn-cancel").on("click",function search(e) {
        $('.btn-edit').show();
        $('.form-data').prop("disabled", true);
        $('.btn-save').hide();
        $('.btn-cancel').hide();
    });

    $('#city').on("change",function search(e) {
        var id = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                   
        $.ajax({
            type: 'post',
            url: url+'/api/fetched-branches',
            dataType:'json',
            data: {
                'city_id' :id,
            },
            success: function(data) {
                $('#branch').empty(); 
                $.each(data, function (i, item) {
                    $('#branch').append('<option value="'+item['id']+'" >'+item['name']+'</option>' );
                });

                $('#branch option[value="'+current_branch+'"]').attr("selected", "selected");
            },
        });
    });

    $(".btn-save").on("click",function search(e) {
        $(".data-loading").show();
        $(".user-form").hide();

        username    = $('#username').val();
        city        = $("#city").val();
        branch      = $("#branch").val();
        role        = $("input[type='radio']:checked").val();
        user_id     = $('#selected_user').val();

        if($("#active").prop('checked') == true){
            active = 1;
        }else{ 
            active = 0;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
               
        $.ajax({
            type: 'post',
            url: url+'/api/users/update',
            dataType:'json',
            data: {
                'user_id'  :user_id,
                'username' :username,
                'city_id'  :city,
                'branch_id':branch,
                'role'     :role,
                'active'   :active,
            },
            success: function(data) { 
                //finished();
                $(".user-form").hide();
                if(data.success == 1){
                    Swal.fire({
                        position:"top-end",
                        title:"Good Job",
                        icon:"success",
                        text:"You have been updated user information.",
                        showConfirmButton:!1,
                        timer:3000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    });
                }
            },
        });
    });

    $("#new_pass,#confirmed_pass").on("keyup",function search(e){
        $new       = $("#new_pass").val().length;
        $confirmed = $("#confirmed_pass").val().length;
        if($new >= 6 && $confirmed >= 6){
            if($("#new_pass").val() == $("#confirmed_pass").val()){
                $(".btn-reset").removeClass('disabled'); 
                $('.confirmed').hide(); 
            }else{
                $(".btn-reset").addClass('disabled');
                $('.confirmed').show();
            }
            $('.character1,.character2').hide();
        }else if($new >= 6){
            $('.character1').hide();
        }else if($confirmed >= 6){
            $('.character2').hide();
        }else{
            $('.character1,.character2').show();
            $(".btn-reset").addClass('disabled');
        }


    });

    $(".btn-reset").on("click",function search(e) {
        $(".data-loading").show();
        $(".user-form").hide();

        new_pass        = $('#new_pass').val();
        confirmed_pass  = $("#confirmed_pass").val();
        user_id         = $('#selected_user').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
               
        $.ajax({
            type: 'post',
            url: url+'/api/users/reset-password',
            dataType:'json',
            data: {
                'new_pass'      :new_pass,
                'confirmed_pass':confirmed_pass,
                'user_id'       :user_id
            },
            success: function(data) { 
                if(data.success == 1){
                    Swal.fire({
                        position:"top-end",
                        title:"Good Job",
                        icon:"success",
                        text:"You have been reset password.",
                        showConfirmButton:!1,
                        timer:3000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    });

                    $('#new_pass').val('');
                    $("#confirmed_pass").val('');
                    $(".btn-reset").addClass('disabled');
                }
            },
        });
    });
});