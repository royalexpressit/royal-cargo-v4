$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var packages= [];
    var added   = 0;
    var count   = 0;
    var row     = $("#row").val();

    if(row > 6){
        $(".package-list").addClass('mxh-560');
    }
    console.log(row);

    //global scope variables
    user_id         = $("#user_id").val();
    username        = $("#username").val();
    user_city_id    = $("#user_city_id").val();
    user_branch_id  = $("#user_branch_id").val();
    current_package = 0;
    pkg_count       = 0;
    ctf_id          = 0;

    $(".show-pkg").on("click",function search(e) {
        id = this.id;
        $("#fetched-packages").empty();
        $(".show-pkg").removeClass('btn-relief-success');
        $(".package-block").removeClass('hide');
        $(".ctf-items").addClass('hide');
        $(".ctf-"+id).addClass('btn-relief-success');

        ctf_id = id;

        //console.log(id);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/fetched-ctf-packages',
            dataType:'json',
            data: {
                'ctf_id'      :id,
            },
            success: function(data) { 
                if(data){
                    pkg_count = data.length;
                        
                    $('#total-pkg').text(pkg_count);

                    $.each( data, function( key, value ) {
                        //++count
                        $("#fetched-packages").append(
                            '<div class="transaction-item list-item item-'+value.package_id+' border-warning">'
                                +'<div class="drag-item-'+value.package_no+' w-100">'
                                    +'<div class="">'
                                        +'<div class="transaction-percentage ">'
                                            +'<h6 class="transaction-title font-medium-5 m-0 text-warning">'+value.package_no
                                                +'<span class="font-small-3 pull-right">'
                                                    +'<span class="text-muted">ရက်စွဲ :</span>'+value.created_at
                                                +'</span>'
                                            +'</h6>'
                                            +'<div class="text-primary">'
                                                +'<span class="text-primary"><span class="text-muted">ရုံးခွဲမှ : </span><span class="badge badge-light-warning">'+value.from_branch_name+'</span></span>'
                                                    +'<div class="pull-right font-small-4 m--t-4">'
                                                        +'<button type="button" class="btn btn-sm btn-success waves-effect waves-float waves-light view-btn" id="'+value.package_id+'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></button>'
                                                    +'</div>'
                                                +'</div>'
                                            +'<div class="bg-light-primary mt-6p border-rounded px-1 h-24">'
                                                +'<small>'+pkg_icons(value.icon)+' '+value.type+'</small>'
                                            +'</div>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        );
                    });
                }else{
                    $('ctf-'+id).hide();
                }
            },
        });
        $(this).val(''); 
                
        added = 0;
    });

    $(".add-btn").on("click",function search(e) {
        $('.ctf-btn').show();
        id = this.id;
        $('.item-'+id).hide();
        ++added;

        //alert(id);
        packages.push(id);

        data = $('.drag-item-'+id).html();
        $("#scanned-lists").prepend('<div class="transaction-item">'+data+'</div>');  
    });

    $('#filtered').on("change",function search(e) {
        branch = $("#filtered option:selected").text();
        if(branch == 'Select Branch'){
            $('.list-item').removeClass('hidden'); 
        }else{
            $('.list-item').addClass('hidden');
            $('.'+branch).removeClass('hidden');
        }
        
    });


    //show-ctf
    $(".show-ctf").on("click",function search(e) {
      	 $(".ctf-items").toggle('show');
    });

    $('body').delegate(".view-btn","click",function () {
        $('.data-loading').removeClass('hide');
      	$(".ctf-items").addClass('hide');
        $(".waybill-form").removeClass('hide');
        $("#fetched-package-waybills").empty();
        var package_id = this.id;

        current_package = package_id;

        $(".get-started").hide();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                               
        $.ajax({
            type: 'post',
            url: url+'/api/fetched-package-waybills',
            dataType:'json',
            data: {
                'package_id'      :package_id,
            },
            success: function(data) { 
                $('.data-loading').addClass('hide');
                count = data.length;
                    if(count == 0){
                        $(".item-"+package_id).hide();
                    }
                    $.each( data, function( key, value ) {
                        $("#count").text(count);

                        $("#fetched-package-waybills").append(
                            '<li class="list-group-item align-items-center text-'+(value.qty == 1? 'warning':'danger')+'" id="waybill-'+value.id+'"><svg class="list-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text me-1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg><span class="fs-18">'+value.waybill_no+'</span> <span class="pull-right">'+replace_icon('map-pin')+': '+value.end_point+', ပူးတွဲ: <span class="badge bg-'+(value.qty == 1? 'light-warning':'danger')+'">'+value.qty+'</span></span></span>'
                        );
                    });
                },
            });
        });

        $(".ctf-btn").on("click",function search(e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                       
            $.ajax({
                type: 'post',
                //url: url+'/api/action/ctf-export',
                url: url+'/api/action/outbound',
                dataType:'json',
                data: {
                    'packages'      :packages,
                    'user_id'       :user_id,
                    'username'      :username,
                    'user_city_id'  :user_city_id,
                    'user_branch_id':user_branch_id,
                    'action_id'     :4,
                },
                success: function(data) { 
                    console.log(data);
                },
            });
            $(this).val(''); 
                
            added = 0;
        });

        $("#waybill").on("keydown",function search(e){
            if(e.keyCode == 13) {
                waybill   = $("#waybill").val().toUpperCase();
                console.log(waybill);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                               
                $.ajax({
                    type: 'post',
                    url: url+'/api/action/inbound',
                    dataType:'json',
                    data: {
                        'waybills'      :waybill,
                        'user_id'       :user_id,
                        'username'      :username,
                        'user_city_id'  :user_city_id,
                        'user_branch_id':user_branch_id,
                        'package_id'    :current_package,
                        'action_id'     :10,
                    },
                    success: function(data) { 
                        if(data.success == 1){
                            --count;
                                $("#count").text(count);
                                //console.log(data.length);
                                $("#waybill-"+data.item).hide();
                        }

                        if(count < 1){
                            //call checked completed waybills package api
                            console.log('call check completed waybills package api');
                            $.ajax({
                                type: 'post',
                                url: url+'/api/check-waybills-package',
                                dataType:'json',
                                data: {
                                    'package_id'    :current_package,
                                },
                                success: function(data) { 
                                    if(data.success == 1){

                                        --pkg_count;
                                        $("#waybill-"+data.item).hide();
                                        $(".item-"+current_package).addClass('hidden');
                                        $('#total-pkg').text(pkg_count);

                                        Swal.fire({
                                            position:"top-end",
                                            title:"Good Job",
                                            icon:"success",
                                            html:"အထုပ်ထဲမှစာအားလုံး လက်ခံပြီးပါပြီ",
                                            showConfirmButton:!1,
                                            timer:3000,
                                            customClass:{confirmButton:"btn btn-primary"},
                                            buttonsStyling:!1
                                        }).then(function (result) {
                                            if(pkg_count == 0){
                                                //call checked completed packages ctf api
                                                console.log('call checked completed packages ctf api');
                                                $.ajax({
                                                    type: 'post',
                                                    url: url+'/api/check-packages-ctf',
                                                    dataType:'json',
                                                    data: {
                                                        'ctf_id'    :ctf_id,
                                                    },
                                                    success: function(data) {
                                                        Swal.fire({
                                                            position:"top-end",
                                                            title:"အသိပေးခြင်း",
                                                            icon:"success",
                                                            text:"စာရင်းအမှတ်ထဲရှိ အထုပ်အားလုံး လက်ခံပြီးပါပြီ",
                                                            showConfirmButton:!1,
                                                            timer:3000,
                                                            customClass:{confirmButton:"btn btn-primary"},
                                                            buttonsStyling:!1
                                                        });
                                                            
                                                        $(".ctf-item-"+ctf_id).addClass('hidden');
                                                    },
                                                });
                                            }
                                        });
                                    }
                                },
                            });
                        }
                    },
                });
                $("#waybill").val('');
            }
        });

        $('#search-ctf').on("keyup",function search(e) {
            var term = $(this).val();

            console.log(term);
            $('.list-item').hide();
            $(".transaction-item:contains('"+term+"')").show();
        });

        //select-ctf
        $('body').delegate(".select-ctf","click",function () {
            ctf_id = $(this).val();
            $("#selected_ctf").val(ctf_id);
            console.log(ctf_id);
        });

        $('body').delegate(".change-branch","click",function () {
            username        = $("#username").val();
            ctf_id          = $("#selected_ctf").val();;
            to_branch_id    = $("#to_branch").val();
            to_branch_name  = $("#to_branch option:selected").text();
                
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                          
            $.ajax({
                type: 'post',
                url: url+'/api/change-ctf-info',
                dataType:'json',
                data: {
                    'username':username,
                    'ctf_id':ctf_id,
                    'to_branch_id'  :to_branch_id,
                    'to_branch_name':to_branch_name,
                    'action_id':8
                },
                success: function(data) { 
                    console.log(data);
                    //location.reload();
                },
            });
        });
    });