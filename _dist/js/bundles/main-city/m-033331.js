$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var scanned = 0;
    var success = 0;
    var failed  = 0;
    var waybills= [];
    var key     = 0;

    var voice   = 'alert-1.mp3';
    var city    = $("#city").val();
    var _token  = $("#_token").val();

    $("#form").submit(function(event){
        event.preventDefault();  
    });

    $("#waybill").on("keydown",function search(e){
        if(e.keyCode == 13) {
            waybill   = $("#waybill").val().toUpperCase();
            qty       = 1;
                    
            $(".scan-btn").removeClass('disabled');

            if(waybill.length > 7 && waybill.length < 20){
                //added item into array
                waybills.push({waybill_no: waybill,qty:qty});

                $(".get-started").hide();
                //valid length && continue
                ++scanned;

                $("#scanned").text(scanned);
                $("#success").text(0);
                $("#failed").text(0);
                                    
                $("#failed-lists").empty();
                $("#scanned-lists").show();
           
                $(".scan-btn").removeAttr('disabled');
                $("#scanned-lists").prepend('<li class="list-group-item align-items-center"><svg class="list-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text me-1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg><span class="fs-18">'+waybill+'</span></span>'
                    +'<span class="pull-right"><button type="button" class="btn btn-icon btn-danger waves-effect p2 disabled minus minus-'+key+'" value="'+key+'"><svg class="plus-btn" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-minus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="8" y1="12" x2="16" y2="12"></line></svg></button>'
                    +'<input type="text" class="form-control inline-control qty-box border-warning qty-input input-'+key+'" placeholder="1" id="'+key+'">'
                    +'<button type="button" class="btn btn-icon btn-success waves-effect p2 plus plus-'+key+'" value="'+key+'"><svg class="minus-btn" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg></button></span>'
                );
                        
                if(scanned > 10){
                    $("#scanned-lists").addClass('content-scroll'); 
                    $(".scroll-msg").removeClass('hide');
                }

                $(this).val('');
                $(".check-number").addClass('hide');
                            
                //limit scanned count with 25
                if(scanned == 25){
                    $("#waybill").attr("disabled", true);
                    audioElement.play();
                    Swal.fire({
                        position:"top-end",
                        title:"Sorry",
                        icon:"warning",
                        text:"We allow 25 waybill to scan one time.",
                        showConfirmButton:!1,
                        timer:5000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    });
                }
                ++key;

            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#waybill").val('');
            }

            //removed fixed height for error lists
            $("#failed-lists").removeClass('scanned-panel');
        }
    });

    $(".scan-btn").on("click",function search(e) {
        //call api sent to server function
        
        data_send();
        $(".scroll-msg").addClass('hide');
    });


    //scan code for continue action
    $("#continue-action").on("keydown",function search(e) {
        var code = $("#continue-action").val();
        if(e.keyCode == 13) {
            if(code == 'continue-action'){
                data_send();
                }else{
                $("#continue-action").val('');
            }
        }
    });

    var data_send = function(){
        user_id         = $("#user_id").val();
        username        = $("#username").val();
        package_id      = $("#package_id").val();
        user_city_id    = $("#user_city_id").val();
        user_branch_id  = $("#user_branch_id").val();
        delivery_id     = $("#delivery").val();
        delivery_name   = $("#delivery option:selected").text();
        service_point   = $("#service_point").val();
        if($("#same_day").prop('checked') == true){
            same_day        = 1;
        }else{
            same_day        = 0; //default
        }
                
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                 

        $.ajax({
            type: 'post',
            url: url+'/api/action/outbound',
            dataType:'json',
            data: {
                'waybills'      :waybills,
                'user_id'       :user_id,
                'username'      :username,
                'user_city_id'  :user_city_id,
                'user_branch_id':user_branch_id,
                'courier_id'   :0,
                'courier_name' :delivery_name,
                'service_point' :service_point,
                'same_day'      :0,
                'action_id'     :0,
            },
            success: function(data) {     
                $("#scanned-lists").removeClass('scanned-panel');
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();

                success = $("#scanned").text() - data.failed.length;
                failed  = data.failed.length;

                //add scroll max size for item > 10
                if(failed > 10){
                    $("#failed-lists").addClass('content-scroll'); 
                }

                $("#success").text(success);
                $("#failed").text(failed);

                if(data.failed.length > 0 ){
                    for (i = 0; i < data.failed.length; i++) {
                        $("#failed-lists").prepend('<li class="list-group-item sm-item text-danger"><svg class="list-icon text-danger" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-frown"><circle cx="12" cy="12" r="10"></circle><path d="M16 16s-1.5-2-4-2-4 2-4 2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg> <span class="fs-18">'+data.failed[i]+'</span><span class="pull-right">(failed)</span></li>');
                    }
                }else{
                    $("#failed-lists").prepend('<li class="list-group-item sm-item text-success"><svg class="list-icon text-success" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-smile"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg> စာအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                }
            },
        });
        $(this).val(''); 


        waybills=[];
        $('.scan-btn').attr('disabled',true);
        $('.continue-action').hide();
        $("#waybill").attr("disabled", false);
        $('#waybill').trigger('focus');
        scanned = 0;
    }
        
    //limit alert audio background
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', url+'/_dist/alerts/'+voice);

    $('body').delegate(".qty-input","keyup",function () {
        var item = parseInt($(this).val());
        var id = $(this).attr('id');
        waybills[id]['qty'] = item;

        //console.log(waybills);
    });

    $('body').delegate(".plus","click",function () {
        var id = $(this).val();

        //var val = parseInt($('.input-'+id+'').val());
        var val = $('.input-'+id+'').val();

        ++val;
        $('.minus-'+id+'').removeClass('disabled');

        //updated value
        $('.input-'+id+'').val(val);
        if(val < 1){
            $('.minus-'+id+'').addClass('disabled');
        }
        console.log(val);
        waybills[id]['qty'] = val;
    });

    $('body').delegate(".minus","click",function () {
        var id = $(this).val();
        var val = parseInt($('.input-'+id+'').val());

                
        --val;
        if(val < 1){
            $('.minus-'+id+'').addClass('disabled');
        }

        //updated value
        $('.input-'+id+'').val(val);
        console.log(val);

        waybills[id]['qty'] = val;
    });
}); 