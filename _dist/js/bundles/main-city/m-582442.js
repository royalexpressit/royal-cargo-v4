$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});
        
$(document).ready(function(){
    var json        = $("#json").val();
    var date        = $("#date").val();
    var from_branch = $("#from_branch").val();
    var to_branch   = $("#to_branch").val();
    var params      = date+'&'+from_branch+'&'+to_branch;

    //declared first loaded json data
    var load_json   = url+'/'+json+'/'+params+'&view';
    var print_json  = url+'/'+json+'/'+params+'&print';
    var waybills 	= [];
    var item     	= 0;
    var _token   	= $("#_token").val();
          
    $.ajax({
        url: load_json,
        type: 'GET',
        data: {},
        success: function(data){
            console.log(data);
            if(data.total > 0){
                $(".show-alert").hide();
                $.each( data.data, function( key, value ) {
                    ++item;
                    waybills.push(value.waybill_no);
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder text-muted">'+value.created_at+'</div>'
                                    +'</div>'
                                +'</div>'
                        	+'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder fs-18 text-primary">'+value.ctf_no+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder"><span class="text-warning">'+value.from_branch+'</span> >> <span class="text-success">'+value.to_branch+'</span></div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td class="text-nowrap">'
                                +'<div class="d-flex flex-column">'
                                    +'<span class="fw-bolder mb-25">'+checked_ctf(value.completed_at)+'</span>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder">'+value.total+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                             +'<td><a href="'+url+'/ctfs/view/'+value.id+'" class="btn btn-relief-primary waves-effect btn-sm">View</a></td>'
                        +'</tr>'
                    ); 
                });
                $(".data-loading").hide();

                $("#to-records").text(data.to);
                $("#total-records").text(data.total);
                                    
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }else{
                $(".show-alert").show();
                $(".pagination").hide();
                $(".data-loading").hide();
            }
        }
    });

    $('.pagination-btn').click(function(){
        //clicked url json data
        $(".data-loading").show();
        $("#fetched-data").empty();
        var clicked_url = $(this).val();
                        
        $(this).siblings().removeClass('active')
        $(this).addClass('active');
        $.ajax({
            url: clicked_url,
            type: 'GET',
            data: {},
            success: function(data){
                $.each( data.data, function( key, value ) {
                    ++item;
                    waybills.push(value.waybill_no);
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder text-muted">'+value.created_at+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder fs-18 text-primary">'+value.ctf_no+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder"><span class="text-warning">'+value.from_branch+'</span> >> <span class="text-success">'+value.to_branch+'</span></div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td class="text-nowrap">'
                                +'<div class="d-flex flex-column">'
                                    +'<span class="fw-bolder mb-25">'+checked_ctf(value.completed_at)+'</span>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder">'+value.total+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                             +'<td><a href="'+url+'/ctfs/view/'+value.id+'" class="btn btn-relief-primary waves-effect btn-sm">View</a></td>'
                        +'</tr>'
                    ); 
                });
                
                $(".data-loading").hide();
                $("#to-records").text(data.to);
               	if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }
        });
    });

    $('.export-csv').click(function(){
        $("#raw").val('test');
        
        var params = $('#params').val();
        $.ajax({
            url: print_json,
            type: 'GET',
            data: {

            },
        	success: function(data){
                console.log(data);
                if(data != ''){
                    json = JSON.stringify(data);
                    $("#raw").val(json);
                    $('.confirmed').show();
                }else{
                    $('.confirmed').hide();
                }
            }
        });
    });
});