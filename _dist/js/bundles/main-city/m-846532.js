$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var packages    = [];
    var added       = 0;    
    var row         = $("#row").val();
    var draft       = $("#draft-total").val();
    $("#draft").text(draft);

    if(row > 6){
        $(".package-list").addClass('mxh-560');
    }

    $(".add-btn").on("click",function search(e) {
        $('.items').addClass('disabled-item');

        $('.export-btn-box').removeClass('hide');
        $('.show-ctf').addClass('hide');
        group_item = $(this).attr('alt');
        ++added;
        draft--;
        $("#draft").text(draft);
                
        $('.'+group_item).removeClass('disabled-item');
        $('.show-msg').hide();
        id = this.id;
        $('.item-'+id).hide();

        //alert(id);
        packages.push(id);

        data = $('.drag-item-'+id).html();
        $("#scanned-lists").prepend('<div class="transaction-item selected-item list-item border-warning">'+data+'</div>');
            
        $('#listed').text(added);

        $('.draft-pkg-lists').each(
            function() {
                if(group_item == 'ရုံးခွဲအားလုံးကြည့်ရန်'){
                    //console.log($('.'+branch, $(this)).length);
                    $(".filtered-pkg-qty").text($('.'+group_item, $(this)).length);
                    $(".filtered-pkg-branch").text('---');
                }else{
                    //console.log($('.'+branch, $(this)).length);
                    $(".filtered-pkg-qty").text($('.'+group_item, $(this)).length);
                    $(".filtered-pkg-branch").text(group_item);
                }
            }
        );
    });

    $('#filtered').on("change",function search(e) {
        branch = $("#filtered option:selected").text();
        if(branch == 'ရုံးခွဲအားလုံးကြည့်ရန်'){
            $('.list-item').removeClass('hidden'); 
        }else{
            $('.list-item').addClass('hidden');
            $('.'+branch).removeClass('hidden');
        }
        
        $('.draft-pkg-lists').each(
            function() {
                if(branch == 'ရုံးခွဲအားလုံးကြည့်ရန်'){
                    //console.log($('.'+branch, $(this)).length);
                    $(".filtered-pkg-qty").text($('.'+branch, $(this)).length);
                    $(".filtered-pkg-branch").text('---');
                }else{
                    //console.log($('.'+branch, $(this)).length);
                    $(".filtered-pkg-qty").text($('.'+branch, $(this)).length);
                    $(".filtered-pkg-branch").text(branch);
                }
            }
        );
    });

    $(".ctf-btn").on("click",function search(e) {
        user_id         = $("#user_id").val();
        username        = $("#username").val();
        user_city_id    = $("#user_city_id").val();
        user_branch_id  = $("#user_branch_id").val();
                
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/action/inbound',
            dataType:'json',
            data: {
                'packages'      :packages,
                'user_id'       :user_id,
                'username'      :username,
                'user_city_id'  :user_city_id,
                'user_branch_id':user_branch_id,
                'action_id'     :9,
            },
            success: function(data) { 
                console.log(data);
                if(data.success == 1){
                    Swal.fire({
                        position:"top-end",
                        title:"အသိပေးခြင်း",
                        icon:"success",
                        html:"<strong>အလုပ်များလွှဲပြောင်းရန် စာရင်းအမှတ်ရရှိပါပြီ</strong>",
                        showConfirmButton:!1,
                        timer:3000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    });

                    $(".show-ctf").removeClass('hide');
                    $("#ctf_no").text(data.ctf_no);
                    $(".print-url").attr("href", url+"/print/cft?qr="+data.ctf_no);

                    $(".filtered-pkg-qty").text(0);
                    $(".filtered-pkg-branch").text('---');
                }

                packages= [];
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();
                $(".export-btn-box").addClass('hide');
                $('.items').removeClass('disabled-item');
            },
        });
        $(this).val(''); 
                
        added = 0;
        $('#listed').text(added);
    });

    $('#search-package').on("keyup",function search(e) {
        var term = $(this).val();

        $('.list-item').hide();
        $(".transaction-item:contains('"+term+"')").show();
    });

    //select-ctf
    $('body').delegate(".select-pkg","click",function () {
        pkg_id = $(this).val();
        branch_id = $(this).attr('alt');
        $("#selected_pkg").val(pkg_id);

        $("#to_branch option").prop('disabled', false);
        $("#to_branch").val(branch_id).trigger('change');
        $("#to_branch option:selected").prop('disabled', true);
    });


    $('body').delegate("#to_branch","change",function () {
        selected_branch_id = $("#to_branch option:selected").val();
        if(selected_branch_id == branch_id){
            $(".change-branch").addClass('disabled');
        }else{
            $(".change-branch").removeClass('disabled');
        }
    });

    $('body').delegate(".change-branch","click",function () {
        username        = $("#username").val();
        pkg_id          = $("#selected_pkg").val();;
        to_branch_id    = $("#to_branch").val();
        to_branch_name  = $("#to_branch option:selected").text();
                
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                          
        $.ajax({
            type: 'post',
            url: url+'/api/change-package-info',
            dataType:'json',
            data: {
                'username':username,
                'pkg_id':pkg_id,
                'to_branch_id'  :to_branch_id,
                'to_branch_name':to_branch_name,
                'action_id':8
            },
            success: function(data) { 
                console.log(data);
                location.reload();
            },
        });
    });
});