$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});
$(document).ready(function(){
    var json        = $("#json").val();

    //declared first loaded json data
    var load_json   = url+'/'+json;
    var waybills    = [];
    var item        = 0;
    var _token      = $("#_token").val();
            
    $.ajax({
        url: load_json,
        type: 'GET',
        data: {},
        success: function(data){
            console.log(json);

            if(data.total > 0){
                $(".show-alert").hide();
                $.each( data.data, function( key, value ) {
                        ++item;
                        waybills.push(value.waybill_no);
                        $("#fetched-data").append(
                            '<tr class="card-blockui"  id="section-block">'
                                +'<td><span class="text-muted">'+value.created_at+'</span></td>'    
                                +'<td><span class="waybill-no text-primary">'+value.waybill_no+'</span></td>'
                                +'<td><span class="btn btn-outline-warning btn-xs round waves-effect">'+value.params+'</span></td>'
                                +'<td><span class="badge rounded-pill badge-light-primary">'+value.status+'</span></td>'
                                +'<td>'+(value.synced==1? '<svg class="text-success" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>':'<svg class="text-warning" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clock"><circle cx="12" cy="12" r="10"></circle><polyline points="12 6 12 12 16 14"></polyline></svg>')+'</td>'
                                +'<td class="text-bold-500 h6 text-danger"><button type="button" class="btn btn-relief-primary btn-sm btn-sync btn-item-'+value.id+'" id='+value.id+'>Sync</button><span class="spinner-border text-secondary custom-wh spin-item-'+value.id+' hide" role="status"><span class="visually-hidden">Loading...</span></span></td>'
                            +'</tr>'
                        );
                });
                $(".data-loading").hide();

                $("#to-records").text(data.to);
                $("#total-records").text(data.total);
                                    
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }else{
                $(".show-alert").show();
                $(".pagination").hide();
                $(".data-loading").hide();
            }
        }
    });

    $('.pagination-btn').click(function(){
        //clicked url json data
        $(".data-loading").show();
        $("#fetched-data").empty();
        var clicked_url = $(this).val();
                        

        $(this).siblings().removeClass('active')
        $(this).addClass('active');
        $.ajax({
            url: clicked_url,
            type: 'GET',
            data: {},
            success: function(data){
                $.each( data.data, function( key, value ) {
                    ++item;
                    $("#fetched-data").append(
                        '<tr class="card-blockui"  id="section-block">'
                            +'<td><span class="text-muted">'+value.created_at+'</span></td>'    
                            +'<td><span class="waybill-no text-primary">'+value.waybill_no+'</span></td>'
                            +'<td><span class="btn btn-outline-warning btn-xs round waves-effect">'+value.params+'</span></td>'
                            +'<td><span class="badge rounded-pill badge-light-primary">'+value.status+'</span></td>'
                            +'<td>'+(value.synced==1? '<svg class="text-success" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>':'<svg class="text-warning" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clock"><circle cx="12" cy="12" r="10"></circle><polyline points="12 6 12 12 16 14"></polyline></svg>')+'</td>'
                            +'<td class="text-bold-500 h6 text-danger"><button type="button" class="btn btn-relief-primary btn-sm btn-sync btn-item-'+value.id+'" id='+value.id+'>Sync</button><span class="spinner-border text-secondary custom-wh spin-item-'+value.id+' hide" role="status"><span class="visually-hidden">Loading...</span></span></td>'
                        +'</tr>'
                    );
                });
                $(".data-loading").hide();
                                

                $("#to-records").text(data.to);
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }
        });
    });

    $('body').delegate(".btn-sync","click",function () {
        var city_id = $('#city_id').val();
        var limit   = $('.limit-lbl').text();

        var params = 'city_id='+city_id+'&limit='+limit;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                 
        $.ajax({
            type: 'get',
            url: url+'/jobs/synced-waybill?'+params,
            dataType:'json',
            data: {},
            success: function(data) {     
                console.log(data);
                //$("#fetched-data").empty();
                //fetched_data();
            },
        });
        //setTimeout(function(){$('#sync-odoo').modal('hide')},1000);
    }); 

    $('.btn-limit').on("click",function search(e) {
        limit = $(this).val();
        $(".limit-lbl").text(limit);
    });
});