$(window).on('load',  function(){
            if (feather) {
                feather.replace({ width: 14, height: 14 });
            }
        });

        $(document).ready(function(){
    var json        = $("#json").val();
    var date        = $("#date").val();
    var from_branch    = $("#from_branch").val();
    var to_branch    = $("#to_branch").val();
    var params      = date+'&'+from_branch+'&'+to_branch;
    var user_branch        = $("#user_branch").val();

    //declared first loaded json data
    var load_json   = url+'/'+json+'/'+params+'&view';
    var print_json  = url+'/'+json+'/'+params+'&print';
    var waybills    = [];
    var item        = 0;
    var _token      = $("#_token").val();
          
    $.ajax({
        url: load_json,
        type: 'GET',
        data: {},
        success: function(data){
            console.log(data);
            if(data.total > 0){
                $(".show-alert").hide();
                $.each( data.data, function( key, value ) {
                    ++item;
                    waybills.push(value.waybill_no);
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td><span class="text-muted">'+value.created_at+'</span></td>'
                            +'<td><span class="waybill-no text-success">'+value.waybill_no+'</span></td>'
                            +'<td class="h6 text-primary">'+user_branch+' >> '+value.to_branch+'</td>'
                            +'<td>'+outbound_status(value.action_id)+'</td>'
                            +'<td class="h6">'+value.qty+'</td>'
                            +'<td>'+value.action_by+'</td>'
                            +'<td><span class="end-point-'+value.id+'">'+(value.end_point ==null? '---':value.end_point)+'</span></td>'
                            +'<td>'
                                +'<a href="'+url+'/waybills/view/'+value.waybill_no+'" class="btn btn-success btn-sm btn-rounded waves-effect waves-light">'+replace_icon('eye')+'</a> '
                            +'</td>'
                        +'</tr>'
                    );
                });
                $(".data-loading").hide();

                $("#to-records").text(data.to);
                $("#total-records").text(data.total);
                                    
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }else{
                $(".show-alert").show();
                $(".pagination").hide();
                $(".data-loading").hide();
            }
        }
    });

    $('.pagination-btn').click(function(){
        //clicked url json data
        $(".data-loading").show();
        $("#fetched-data").empty();
        var clicked_url = $(this).val();
                        
        $(this).siblings().removeClass('active')
        $(this).addClass('active');
        $.ajax({
            url: clicked_url,
            type: 'GET',
            data: {},
            success: function(data){
                $.each( data.data, function( key, value ) {
                    ++item;
                    waybills.push(value.waybill_no);
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td><span class="text-muted">'+value.created_at+'</span></td>'
                            +'<td><span class="waybill-no text-success">'+value.waybill_no+'</span></td>'
                            +'<td class="h6 text-primary">'+user_branch+' >> '+value.to_branch+'</td>'
                            +'<td>'+outbound_status(value.action_id)+'</td>'
                            +'<td class="h6">'+value.qty+'</td>'
                            +'<td>'+value.action_by+'</td>'
                            +'<td><span class="end-point-'+value.id+'">'+(value.end_point ==null? '---':value.end_point)+'</span></td>'
                            +'<td>'
                                +'<a href="'+url+'/waybills/view/'+value.waybill_no+'" class="btn btn-success btn-sm btn-rounded waves-effect waves-light">'+replace_icon('eye')+'</a> '
                            +'</td>'
                        +'</tr>'
                    );
                });
                
                $(".data-loading").hide();
                $("#to-records").text(data.to);
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }
        });
    });

    $('.export-csv').click(function(){
        $("#raw").val('test');
        
        var params = $('#params').val();
        $.ajax({
            url: print_json,
            type: 'GET',
            data: {

            },
            success: function(data){
                console.log(data);
                if(data != ''){
                    json = JSON.stringify(data);
                    $("#raw").val(json);
                    $('.confirmed').show();
                }else{
                    $('.confirmed').hide();
                }
            }
        });
    });
});