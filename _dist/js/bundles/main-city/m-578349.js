$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var json        = $("#json").val();

    //declared first loaded json data
    var load_json   = url+'/'+json;
    var waybills    = [];
    var item        = 0;
    var _token      = $("#_token").val();
    var branches    = [];
    var last = 0;
            
    fetched_couriers();

    function fetched_couriers(){
        //branches.push(1);

        $.ajax({
            url: load_json,
            type: 'GET',
            data: {},
            success: function(data){
                if(data.length > 0){
                    $(".show-alert").hide();

                    last = data.length;
                    $.each( data, function( key, value ) {
                        ++item;
                        branches.push(value.branch);
                            	
	                    $("#fetched-data").append(
	                        '<tr class="branch-item item-'+value.branch+'">'
	                            +'<td>'
		                            +'<div class="d-flex align-items-center">'
		                                +'<div>'
		                                    +'<div class="fw-bolder">'+value.user_id+'</div>'
		                                +'</div>'
		                            +'</div>'
	                            +'</td>'
					            +'<td>'
					                +'<div class="d-flex align-items-center">'
						                +'<div class="avatar rounded bg-white">'
						                    +'<div class="avatar-content">'
						                       +'<img src="'+url+'/_dist/images/logo.png" class="w-100" alt="Toolbar svg" />'
						                    +'</div>'
						                +'</div>'
					                +'<div>'
					                +'<div class="fw-bolder">'+(value.name==null? 'Anonymous':value.name)+'</div>'
						                +'<div class="font-small-2 text-danger">'+(value.branch==null? 'Unknown':value.branch)+'</div>'
						                +'</div>'
						            +'</div>'
					            +'</td>'
					            +'<td class="text-nowrap">'
					                +'<div class="d-flex flex-column">'
					                   +'<span class="fw-bolder mb-25">'+value.count+'</span>'
					                +'</div>'
					            +'</td>'
					            +'<td><a href="'+url+'/outbound/collected-by-courier/'+value.user_id+'" type="button" class="btn btn-relief-primary waves-effect btn-sm">View</a></td>'
					        +'</tr>'
                        );          
                    });

                    $(".data-loading").hide();

                    $("#to-records").text(data.to);
                    $("#total-records").text(data.total);
                                    
	                if(data.prev_page_url === null){
	                    $("#prev-btn").attr('disabled',true);
	                }else{
	                    $("#prev-btn").attr('disabled',false);
	                }
	                if(data.next_page_url === null){
	                    $("#next-btn").attr('disabled',true);
	                }else{
	                    $("#next-btn").attr('disabled',false);
	                }
                    $("#prev-btn").val(data.prev_page_url);
                    $("#next-btn").val(data.next_page_url);

                    let filtered = [...new Set(branches)];

          			console.log(filtered);
					$.each( filtered, function( key, value ) {
						$("#filtered-dropdown").append('<button class="dropdown-item btn-filtered w-100" value="'+value+'">'+value+'</button>');
					});

                }else{
	                $(".show-alert").show();
	                $(".pagination").hide();
	                $(".data-loading").hide();
	            }
            }
        });
    }

    $('body').delegate(".btn-filtered","click",function () {
    	$(".data-loading").show();
    	var filtered = $(this).text();
    	$('.branch-item').addClass('hide');
    	$('.item-'+filtered).removeClass('hide');
    	$(".data-loading").hide();
    });
});