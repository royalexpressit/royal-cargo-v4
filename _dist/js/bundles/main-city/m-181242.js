$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});
$(document).ready(function(){
    var json        	= $("#json").val();
    var status        	= $("#status").val();
    var city_shortcode 	= $("#city").val();

    //declared first loaded json data
    var load_json   = url+'/'+json+'/'+status;
    var waybills    = [];
    var item        = 0;
    var _token      = $("#_token").val();
            
    fetched_package(status);

    $('.btn-filtered').click(function(){
        var filtered = $(this).attr('value');
        $(".data-loading").show();
        $("#fetched-data").empty();
       	fetched_package(filtered);
    });

    function fetched_package(filtered){
        load_json = url+'/'+json+'/'+filtered;

        $("#status-lbl").text(filtered);
        $.ajax({
	        url: load_json,
	        type: 'GET',
	        data: {},
	        success: function(data){
	            console.log(json);
	            if(data.total > 0){
	                $(".show-alert").addClass('hide');
	                $.each( data.data, function( key, value ) {
	                    ++item;
	                    waybills.push(value.waybill_no);
	                    $("#fetched-data").append(
	                        '<tr>'
	                            +'<td>'
		                            +'<div class="d-flex align-items-center">'
		                                +'<div>'
		                                     +'<div class="fw-bolder">'+value.from_branch_id+'</div>'
		                                +'</div>'
		                            +'</div>'
	                            +'</td>'
					            +'<td>'
					                +'<div class="d-flex align-items-center">'
						                +'<div class="avatar rounded bg-white">'
							                +'<div class="avatar-content">'
							                     +'<img src="'+url+'/_dist/images/logo.png" class="w-100" alt="Toolbar svg" />'
							                +'</div>'
						                +'</div>'
						                +'<div>'
						                    +'<div class="fw-bolder">'+value.branch_name+'</div>'
						                    +'<div class="font-small-2 text-danger">'+city_shortcode+'</div>'
						                +'</div>'
					                +'</div>'
					            +'</td>'
					            +'<td class="text-nowrap">'
					                +'<div class="d-flex flex-column">'
					                    +'<span class="fw-bolder mb-25">'+value.count+'</span>'
					                +'</div>'
					            +'</td>'
					            +'<td class="text-nowrap">'
						            +'<div class="d-flex flex-column">'
						                +'<span class="fw-bolder mb-25 text-capitalize"><span class="badge bg-'+filtered+'">'+filtered+'</span></span>'
						            +'</div>'
					            +'</td>'
					            +'<td><a href="'+url+'/outbound/package/view/'+value.from_branch_id+'" class="btn btn-relief-primary waves-effect btn-sm">View</a></td>'
					        +'</tr>'
	                    ); 
	                });
	                $(".data-loading").hide();

	                $("#to-records").text(data.to);
	                $("#total-records").text(data.total);
	                                    
	                if(data.prev_page_url === null){
	                    $("#prev-btn").attr('disabled',true);
	                }else{
	                    $("#prev-btn").attr('disabled',false);
	                }
	                if(data.next_page_url === null){
	                    $("#next-btn").attr('disabled',true);
	                }else{
	                    $("#next-btn").attr('disabled',false);
	                }
	                $("#prev-btn").val(data.prev_page_url);
	                $("#next-btn").val(data.next_page_url);
	            }else{
	                $(".show-alert").removeClass('hide');
	                $(".pagination").hide();
	                $(".data-loading").hide();
	            }
	        }
        });
    }
});