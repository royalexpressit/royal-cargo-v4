$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var json        = $("#json").val();

    //declared first loaded json data
    var load_json   = url+'/'+json;
    var waybills    = [];
    var item        = 0;
    var _token      = $("#_token").val();
    var limit       = 20;
       
    var fetched_data = function(){
        $.ajax({
            url: load_json,
            type: 'GET',
            data: {},
            success: function(data){
                if(data.total > 0){
                    $.each( data.data, function( key, value ) {
                        $("#fetched-data").append(
                            '<tr>'
                                +'<td><span class="text-muted">'+value.created_at+'</span></td>'
                                +'<td><span class="waybill-no text-danger">'+value.waybill_no+'</span></td>'
                                +'<td>'+value.courier+'</td>'
                                +'<td class="h6 text-primary">'+value.branch+'</td>'
                                +'<td>'+outbound_status(value.action_id)+'</td>'
                                +'<td class="h6">'+value.qty+'</td>'
                                +'<td>'+same_day(value.same_day)+'</td>'
                                +'<td><span class="end-point-'+value.id+'">'+(value.end_point ==null? '---':value.end_point)+'</span></td>'
                                +'<td>'
                                    +'<a href="'+url+'/waybills/view/'+value.waybill_no+'" class="btn btn-success btn-sm btn-rounded waves-effect waves-light">'+replace_icon('eye')+'</a> '
                                    +'<button class="btn btn-danger btn-sm btn-rounded waves-effect waves-light sync-odoo" data-bs-toggle="modal" data-bs-target="#sync-odoo" value='+value.waybill_no+' id="'+value.id+'">'+replace_icon('map-pin')+'</button>'
                                +'</td>'
                            +'</tr>'
                        );
                    });
                    $(".data-loading").hide();

                    $("#to-records").text(data.to);
                    $("#total-records").text(data.total);
                                    
                    if(data.prev_page_url === null){
                        $("#prev-btn").attr('disabled',true);
                    }else{
                        $("#prev-btn").attr('disabled',false);
                    }
                    if(data.next_page_url === null){
                        $("#next-btn").attr('disabled',true);
                    }else{
                        $("#next-btn").attr('disabled',false);
                     }
                    $("#prev-btn").val(data.prev_page_url);
                    $("#next-btn").val(data.next_page_url);
                }else{
                    $(".show-alert").show();
                    $(".pagination").hide();
                    $(".data-loading").hide();
                }
            }
        });
    };

    fetched_data();

    $('.pagination-btn').click(function(){
        //clicked url json data
        $(".data-loading").show();
        $("#fetched-data").empty();
        var clicked_url = $(this).val();
                        
        $(this).siblings().removeClass('active')
        $(this).addClass('active');
        $.ajax({
            url    : clicked_url,
            type   : 'GET',
            data   : {},
            success: function(data){
                $.each( data.data, function( key, value ) {
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td><span class="text-muted">'+value.created_at+'</span></td>'
                            +'<td><span class="waybill-no text-danger">'+value.waybill_no+'</span></td>'
                            +'<td>'+value.courier+'</td>'
                            +'<td class="h6 text-primary">'+value.branch+'</td>'
                            +'<td>'+outbound_status(value.action_id)+'</td>'
                            +'<td class="h6">'+value.qty+'</td>'
                            +'<td>'+same_day(value.same_day)+'</td>'
                            +'<td><span class="end-point-'+value.id+'">'+(value.end_point ==null? '---':value.end_point)+'</span></td>'
                            +'<td>'
                                +'<a href="'+url+'/waybills/view/'+value.waybill_no+'" class="btn btn-success btn-sm btn-rounded waves-effect waves-light">'+replace_icon('eye')+'</a> '
                                +'<button class="btn btn-danger btn-sm btn-rounded waves-effect waves-light sync-odoo" data-bs-toggle="modal" data-bs-target="#sync-odoo" value='+value.waybill_no+' id="'+value.id+'">'+replace_icon('map-pin')+'</button>'
                            +'</td>'
                        +'</tr>'
                    );
                });
                $(".data-loading").hide();
                                

                $("#to-records").text(data.to);
                    if(data.prev_page_url === null){
                        $("#prev-btn").attr('disabled',true);
                    }else{
                        $("#prev-btn").attr('disabled',false);
                    }
                    if(data.next_page_url === null){
                        $("#next-btn").attr('disabled',true);
                    }else{
                        $("#next-btn").attr('disabled',false);
                    }
                    $("#prev-btn").val(data.prev_page_url);
                    $("#next-btn").val(data.next_page_url);
                }
            });
    });

    $('body').delegate(".sync-odoo","click",function () {
        var waybill_no = $(this).val();
        var id = $(this).attr('id');
        console.log('sync-odoo - '+waybill_no);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                 
        $.ajax({
            type: 'post',
            url: url+'/api/check-end-point',
            dataType:'json',
            data: {
                'waybill_no' : waybill_no
            },
            success: function(data) {     
                console.log(id);
                $('.end-point-'+id).text(data.end_point);
                setTimeout(function(){$('#sync-odoo').modal('hide')},1000);
            },
        });
    });

    $('body').delegate(".btn-sync","click",function () {
        var city_id = $('#city_id').val();
        var limit   = $('.limit-lbl').text();

        var params = 'city_id='+city_id+'&limit='+limit;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                 
        $.ajax({
            type: 'get',
            url: url+'/jobs/synced-end-point?'+params,
            dataType:'json',
            data: {},
            success: function(data) {     
                console.log(data);
                $("#fetched-data").empty();
                fetched_data();
            },
        });
        setTimeout(function(){$('#sync-odoo').modal('hide')},1000);
    });

    $('.btn-limit').on("click",function search(e) {
        limit = $(this).val();
        $(".limit-lbl").text(limit);
    });
});

