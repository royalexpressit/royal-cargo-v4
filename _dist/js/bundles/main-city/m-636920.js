$(window).on('load',function(){
    if(feather){
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var scanned = 0;
    var success = 0;
    var failed  = 0;
    var waybills= [];
    var limit   = 20;

    var length = $('#from_city option').length;
    $("#from-city").text(length);

    var voice   = 'alert-1.mp3';
    var city    = $("#city").val();
    var _token  = $("#_token").val();

    $("#form").submit(function(event){
        event.preventDefault();  
    });

    $("#waybill").on("keydown",function search(e){
        if(e.keyCode == 13) {
            waybill   = $("#waybill").val().toUpperCase();
            qty       = 1;//$("#qty").val();

            if(pattern1.test(waybill) == true && pattern2.test(waybill.charAt(0)) == true){
                input = 1;
            }else{
                input = 0;
            }

            if(waybill.length > 7 && waybill.length < 20 && input == 1){
                $(".scroll-msg").addClass('hide');

                $(".scan-btn").removeClass('disabled');
                //added item into array
                
                $(".get-started").hide();
                //valid length && continue
                                    
                $("#failed-lists").empty();
                $("#scanned-lists").show();
           
                $(".scan-btn").removeAttr('disabled');
                if(waybills.indexOf(waybill) == -1) {
                    ++scanned;
                    $("#scanned-lists").prepend('<li class="list-group-item align-items-center item-'+waybill+'"><svg class="list-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text me-1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg><span class="fs-18">'+waybill+'</span><span class="pull-right"><button type="button" class="btn btn-icon btn-danger waves-effect p2 remove" value="'+waybill+'"><svg class="plus-btn" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></button></span></span>');
                    $("#scanned").text(scanned);
                    waybills.push(waybill);
                }

                $("#success").text(0);
                $("#failed").text(0);

                if(scanned > 10){
                    $("#scanned-lists").addClass('content-scroll'); 
                    $(".scroll-msg").removeClass('hide');
                }

                $(this).val('');
                $(".check-number").addClass('hide');     

                //$('#multi_scanned_waybills').prepend(raw); 
                            
                //limit scanned count with 25
                if(scanned == limit){
                    $("#waybill").attr("disabled", true);
                    audioElement.play();
                    Swal.fire({
                        position:"center",
                        title:"အသိပေးခြင်း",
                        icon:"warning",
                        html:"<strong>သတ်မှတ်ထားသော စာအရေအတွက်ပြည့်သွားပါပြီ</strong> <p>စာရင်းသွင်းပြီးမှ စာထပ်ဖတ်ပေးပါ</p>",
                        showConfirmButton:!1,
                        timer:5000,
                        customClass:{confirmButton:"btn btn-primary"},
                        buttonsStyling:!1
                    }); 
                    console.log(voice);
                }
            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#waybill").val('');
            }
            //removed fixed height for error lists
            $("#failed-lists").removeClass('scanned-panel');
        }
    });

    $(".scan-btn").on("click",function search(e) {
        $('.data-loading').removeClass('hide');
        data_send();
        $(".scroll-msg").addClass('hide');
    });


    var data_send = function(){
        user_id         = $("#user_id").val();
        username        = $("#username").val();
        package_id      = $("#package_id").val();
        user_city_id    = $("#user_city_id").val();
        user_branch_id  = $("#user_branch_id").val();
        from_city_id    = $("#from_city").val();
        from_city_name  = $("#from_city option:selected").text();
        service_point   = $("#service_point").val();
        same_day        = 0;
                
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/action/inbound',
            dataType:'json',
            data: {
                'waybills'      :waybills,
                'user_id'       :user_id,
                'username'      :username,
                'user_city_id'  :user_city_id,
                'user_branch_id':user_branch_id,
                'from_city_name':from_city_name,
                'from_city_id'  :from_city_id,
                'package'       :0,
                'action_id'     :7,
            },
            success: function(data) { 
                $('.data-loading').addClass('hide');
                //set array to empty
                waybills = [];

                
                $("#scanned-lists").removeClass('scanned-panel');
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();

                success = $("#scanned").text() - data.failed.length;
                failed  = data.failed.length;

                //add scroll max size for item > 10
                if(failed > 10){
                    $("#failed-lists").addClass('content-scroll');
                    $(".scroll-msg").removeClass('hide');
                }

                $("#success").text(success);
                $("#failed").text(failed);

                if(data.failed.length > 0 ){
                    for (i = 0; i < data.failed.length; i++) {
                        $("#failed-lists").prepend('<li class="list-group-item sm-item text-danger"><span class="fs-20">😭</span><span class="fs-18">'+data.failed[i]+'</span><span class="pull-right">(X)</span></li>');
                    }
                }else{
                     $("#failed-lists").prepend('<li class="list-group-item sm-item text-success border-success"><span class="fs-20">😎</span> စာအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                }
            },
        });
        $(this).val(''); 


        waybills=[];
        $('.scan-btn').attr('disabled',true);
        $('.continue-action').hide();
        $("#waybill").attr("disabled", false);
        $('#waybill').trigger('focus');
        scanned = 0;

        unkonwn_count();
    }
        
    //limit alert audio background
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', url+'/_dist/alerts/'+voice);

    var leftPad =  function(value, length) { 
        return ('0'.repeat(length) + value).slice(-length); 
    }

    var unkonwn_count = function(){
        user_city_id    = $("#user_city_id").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/unknown/inbound',
            dataType:'json',
            data: {
                city_id : user_city_id
            },
            success: function(data) { 
                //console.log(data);
                total = leftPad(data.unknown_waybills, 4);
                $('.unknow-count').text(total);
                $('#unknow-total').text(total);
            },
        });
    }

    $('body').delegate(".btn-close","click",function () {
        $(".scan-btn").removeAttr('disabled');
    });

    $('body').delegate(".remove","click",function () {
        var waybill_no = $(this).val();
        $(".item-"+waybill_no).addClass('hide');

        result = waybills.filter(function(item) {
            return item !== waybill_no;
           
        });
        --scanned;
        if(scanned == 0){
            $(".scan-btn").addClass('disabled');
        }
        waybills = result;
        $("#scanned").text(scanned);
    });

    $('body').delegate(".unknow-btn","click",function () {
        unkonwn_count();
    });

    unkonwn_count();
}); 