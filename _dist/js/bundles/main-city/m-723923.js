$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});
        
$(document).ready(function(){
    var json        = $("#json").val();
    var date        = $("#date").val();
    var branch_1    = $("#branch_1").val();
    var branch_2    = $("#branch_2").val();
    var params      = date+'&'+branch_1+'&'+branch_2;

    //declared first loaded json data
    var load_json   = url+'/'+json+'/'+params+'&view';
    var print_json  = url+'/'+json+'/'+params+'&print';
    var waybills    = [];
    var item        = 0;
    var _token      = $("#_token").val();
          
    $.ajax({
        url: load_json,
        type: 'GET',
        data: {},
        success: function(data){
            if(data.total > 0){
                $(".show-alert").hide();
                $.each( data.data, function( key, value ) {
                    ++item;
                    waybills.push(value.waybill_no);
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder">'+value.created_at+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder font-medium-2 text-success">'+value.package_no+'</div>'
                                        +'<div class="text-danger font-small-2">'+value.package_type+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div class="avatar rounded bg-white">'
                                        +'<div class="avatar-content">'
                                            +'<img src="'+url+'/_dist/images/logo.png" class="w-100" alt="Toolbar svg" />'
                                        +'</div>'
                                    +'</div>'
                                    +'<div>'
                                        +'<div class="text-warning">'+value.from_branch+'</div>'
                                        +'<div class="text-success">'+value.to_branch+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="font-small-4 text-muted">'+value.created_by+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div><span class="badge badge-light-warning">'+value.qty+'</span> Qty</div>'
                                +'</div>'
                            +'</td>'
                            +'<td class="text-nowrap">'
                                +'<div class="d-flex flex-column">'
                                    +'<span class="fw-bolder mb-25">'+value.completed+'</span>'
                                +'</div>'
                            +'</td>'
                             +'<td><a href="'+url+'/packages/view/'+value.id+'" class="btn btn-relief-success waves-effect btn-sm">View</a></td>'
                        +'</tr>'
                    ); 
                });
                $(".data-loading").hide();

                $("#to-records").text(data.to);
                $("#total-records").text(data.total);
                                    
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }else{
                $(".show-alert").show();
                $(".pagination").hide();
                $(".data-loading").hide();
            }
        }
    });

    $('.pagination-btn').click(function(){
        //clicked url json data
        $(".data-loading").show();
        $("#fetched-data").empty();
        var clicked_url = $(this).val();
                        
        $(this).siblings().removeClass('active')
        $(this).addClass('active');
        $.ajax({
            url: clicked_url,
            type: 'GET',
            data: {},
            success: function(data){
                $.each( data.data, function( key, value ) {
                    ++item;
                    waybills.push(value.waybill_no);
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder">'+value.created_at+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="fw-bolder font-medium-2 text-success">'+value.package_no+'</div>'
                                        +'<div class="text-danger font-small-2">'+value.package_type+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div class="avatar rounded bg-white">'
                                        +'<div class="avatar-content">'
                                            +'<img src="'+url+'/_dist/images/logo.png" class="w-100" alt="Toolbar svg" />'
                                        +'</div>'
                                    +'</div>'
                                    +'<div>'
                                        +'<div class="text-warning">'+value.from_branch+'</div>'
                                        +'<div class="text-success">'+value.to_branch+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div>'
                                        +'<div class="font-small-4 text-muted">'+value.created_by+'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="d-flex align-items-center">'
                                    +'<div><span class="badge badge-light-warning">'+value.qty+'</span> Qty</div>'
                                +'</div>'
                            +'</td>'
                            +'<td class="text-nowrap">'
                                +'<div class="d-flex flex-column">'
                                    +'<span class="fw-bolder mb-25">'+value.completed+'</span>'
                                +'</div>'
                            +'</td>'
                             +'<td><a href="'+url+'/packages/view/'+value.id+'" class="btn btn-relief-success waves-effect btn-sm">View</a></td>'
                        +'</tr>'
                    ); 
                });
                
                $(".data-loading").hide();
                $("#to-records").text(data.to);
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }
        });
    });

    $('.export-csv').click(function(){
        $("#raw").val('test');
        
        var params = $('#params').val();
        $.ajax({
            url: print_json,
            type: 'GET',
            data: {

            },
            success: function(data){
                console.log(data);
                if(data != ''){
                    json = JSON.stringify(data);
                    $("#raw").val(json);
                    $('.confirmed').show();
                }else{
                    $('.confirmed').hide();
                }
            }
        });
    });
});