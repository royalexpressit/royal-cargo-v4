$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});

$(document).ready(function(){
    var packages= [];
    var added   = 0;

    $(".add-btn").on("click",function search(e) {
        $('.export-btn-box').removeClass('hide');
        $('.show-ctf').addClass('hide');

        $('.show-msg').hide();
        id = this.id;
        $('.item-'+id).hide();
        ++added;

        //alert(id);
        packages.push(id);

        data = $('.drag-item-'+id).html();
        $("#scanned-lists").prepend('<div class="transaction-item">'+data+'</div>');
            
        $('#listed').text(added);
    });

    $('#filtered').on("change",function search(e) {
        branch = $("#filtered option:selected").text();
        if(branch == 'ရုံးခွဲအားလုံးကြည့်ရန်'){
            $('.list-item').removeClass('hidden'); 
        }else{
            $('.list-item').addClass('hidden');
            $('.'+branch).removeClass('hidden');
        }
    });

    $(".ctf-btn").on("click",function search(e) {
        user_id         = $("#user_id").val();
        username        = $("#username").val();
        user_city_id    = $("#user_city_id").val();
        user_branch_id  = $("#user_branch_id").val();
                
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                       
        $.ajax({
            type: 'post',
            url: url+'/api/action/outbound',
            dataType:'json',
            data: {
                'packages'      :packages,
                'user_id'       :user_id,
                'username'      :username,
                'user_city_id'  :user_city_id,
                'user_branch_id':user_branch_id,
                'action_id'     :6,
            },
            success: function(data) { 
                if(data.success == 1){
                    $(".show-ctf").removeClass('hide');
                    $("#ctf_no").text(data.ctf_no);
                    $(".print-url").attr("href", url+"/print/cft?qr="+data.ctf_no)
                }

                packages= [];
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();
                $(".export-btn-box").addClass('hide');
            },
        });
        $(this).val(''); 
                
        added = 0;
        $('#listed').text(added);
    });
});