$(window).on('load',  function(){
    if (feather) {
        feather.replace({ width: 14, height: 14 });
    }
});
$(document).ready(function(){
    var url         = $("#url").val();
    var json        = $("#json").val();

    //declared first loaded json data
    var load_json   = url+'/'+json;
    var waybills    = [];
    var item        = 0;
    var _token      = $("#_token").val();
            
    $.ajax({
        url: load_json,
        type: 'GET',
        data: {},
        success: function(data){
            console.log(json);
            if(data.total > 0){
                $(".show-alert").hide();
                $.each( data.data, function( key, value ) {
                    ++item;
                    waybills.push(value.waybill_no);
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td class="text-bold-500">'+value.id+'</td>'
                            +'<td class="text-bold-500"><img src="'+url+'/_dist/images/'+value.image+'" class="round" alt="avatar" height="36" width="36"> '+value.name+'</td>'
                            +'<td class="text-bold-500">'+value.branch+'</td>'
                            +'<td class="text-bold-500">'+value.city+'</td>'
                            +'<td>'+role(value.role)+'</td>'
                            +'<td>'+active(value.active)+'</td>'
                            +'<td><a href="'+url+'/users/'+value.id+'" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">View Profile</a></td>'
                        +'</tr>'
                    ); 
                });
                $(".data-loading").hide();

                $("#to-records").text(data.to);
                $("#total-records").text(data.total);
                                    
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }else{
                $(".show-alert").show();
                $(".pagination").hide();
                $(".data-loading").hide();
            }
        }
    });

    $('.pagination-btn').click(function(){
        //clicked url json data
        $(".data-loading").show();
        $("#fetched-data").empty();
        var clicked_url = $(this).val();
                        
        $(this).siblings().removeClass('active')
        $(this).addClass('active');
        $.ajax({
            url: clicked_url,
            type: 'GET',
            data: {},
            success: function(data){
                $.each( data.data, function( key, value ) {
                    ++item;
                    $("#fetched-data").append(
                        '<tr>'
                            +'<td class="text-bold-500">'+value.id+'</td>'
                            +'<td class="text-bold-500"><img src="'+url+'/_dist/images/'+value.image+'" class="round" alt="avatar" height="36" width="36"> '+value.name+'</td>'
                            +'<td class="text-bold-500">'+value.branch+'</td>'
                            +'<td class="text-bold-500">'+value.city+'</td>'
                            +'<td>'+role(value.role)+'</td>'
                            +'<td>'+active(value.active)+'</td>'
                            +'<td><a href="'+url+'/users/'+value.id+'" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">View Profile</a></td>'
                        +'</tr>'
                    );
                });
                $(".data-loading").hide();
                                

                $("#to-records").text(data.to);
                if(data.prev_page_url === null){
                    $("#prev-btn").attr('disabled',true);
                }else{
                    $("#prev-btn").attr('disabled',false);
                }
                if(data.next_page_url === null){
                    $("#next-btn").attr('disabled',true);
                }else{
                    $("#next-btn").attr('disabled',false);
                }
                $("#prev-btn").val(data.prev_page_url);
                $("#next-btn").val(data.next_page_url);
            }
        });
    });

    $('body').delegate(".load-modal","click",function () {
        var id = $(this).attr('id');
                        
        $.ajax({
            url: url+'/inbound/'+id+'/view',
            type: 'POST',
            data: {
                'id':id,
                '_token': _token
            },
            success: function(data){
                $("#waybill_label").text(data.waybill_no);
                $("#action_date").text(data.action_date);
                $("#from_city").text('_ '+data.origin);
                $("#transit_city").text('_ '+data.transit);
                $("#to_city").text('_ '+data.destination);
                $("#branch_in_by").text('_ '+data.branch_in_by);
                $("#current_status").html(data.current_status);
                $(".view-logs").attr("href",url+'/inbound/view/'+data.id+'/logs')   
            }
        });     
    });

    $('.sort').click(function(){
        $("#fetched-data").empty();
        var sort = $(this).attr('id');
        if(sort == 'sort-by-name'){
            $.ajax({
                url: url+'/json/users/name',
                type: 'GET',
                data: {},
                success: function(data){
                    console.log(json);
                    if(data.total > 0){
                        $(".show-alert").hide();
                        $.each( data.data, function( key, value ) {
                            ++item;
                            waybills.push(value.waybill_no);
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td class="text-bold-500">'+value.id+'</td>'
                                    +'<td class="text-bold-500"><img src="'+url+'/_dist/images/'+value.image+'" class="round" alt="avatar" height="36" width="36"> '+value.name+'</td>'
                                    +'<td class="text-bold-500">'+value.branch+'</td>'
                                    +'<td class="text-bold-500">'+value.city+'</td>'
                                    +'<td>'+role(value.role)+'</td>'
                                    +'<td>'+active(value.active)+'</td>'
                                    +'<td><a href="'+url+'/users/'+value.id+'" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">View Profile</a></td>'
                                +'</tr>'
                            ); 
                        });

                        $(".data-loading").hide();

                        $("#to-records").text(data.to);
                        $("#total-records").text(data.total);
                                            
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }else{
                        $(".show-alert").show();
                        $(".pagination").hide();
                        $(".data-loading").hide();
                    }
                }
            });
        }else{
            $.ajax({
                url: url+'/json/users/id',
                type: 'GET',
                data: {},
                success: function(data){
                    console.log(json);
                    if(data.total > 0){
                        $(".show-alert").hide();
                        $.each( data.data, function( key, value ) {
                            ++item;
                            waybills.push(value.waybill_no);
                            $("#fetched-data").append(
                                '<tr>'
                                    +'<td class="text-bold-500">'+value.id+'</td>'
                                    +'<td class="text-bold-500"><img src="'+url+'/_dist/images/'+value.image+'" class="round" alt="avatar" height="36" width="36"> '+value.name+'</td>'
                                    +'<td class="text-bold-500">'+value.branch+'</td>'
                                    +'<td class="text-bold-500">'+value.city+'</td>'
                                    +'<td>'+role(value.role)+'</td>'
                                    +'<td>'+active(value.active)+'</td>'
                                    +'<td><a href="'+url+'/users/'+value.id+'" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light">View Profile</a></td>'
                                +'</tr>'
                            );          
                        });
                        $(".data-loading").hide();
                        $("#to-records").text(data.to);
                        $("#total-records").text(data.total);
                                                
                        if(data.prev_page_url === null){
                            $("#prev-btn").attr('disabled',true);
                        }else{
                            $("#prev-btn").attr('disabled',false);
                        }
                        if(data.next_page_url === null){
                            $("#next-btn").attr('disabled',true);
                        }else{
                            $("#next-btn").attr('disabled',false);
                        }
                        $("#prev-btn").val(data.prev_page_url);
                        $("#next-btn").val(data.next_page_url);
                    }else{
                        $(".show-alert").show();
                        $(".pagination").hide();
                        $(".data-loading").hide();
                    }
                }
            });
        }
    });
});